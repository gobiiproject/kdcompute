/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UsernameOnlyAuthenticationManagerBuilderProvider extends AuthenticationManagerBuilderProvider {

	public static final String KD_COMPUTE_USER = "KDCompute_User";

	public List<String> login(String username, String password)
			throws IncorrectUsernameOrPasswordException, UserNotFoundException {
		return new ArrayList<String>(Arrays.asList("ROLE_ADMIN"));
	}

	@Override
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(new AbstractUserDetailsAuthenticationProvider() {

			@Override
			protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
					throws AuthenticationException {
				List<GrantedAuthority> authorities;
				try {
					authorities = login(username, null).stream().map(e -> new SimpleGrantedAuthority(e))
							.collect(Collectors.toList());
					User user = new User(username, "", authorities);
					return user;
				} catch (IncorrectUsernameOrPasswordException e) {
				} catch (UserNotFoundException e) {
				}
				return null;
			}

			@Override
			protected void additionalAuthenticationChecks(UserDetails userDetails,
					UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
			}
		});
	}

	@Override
	public String getEmail(String username) {
		return null;
	}

	@Override
	public LoginMode getLoginMode() {
		return LoginMode.USERNAME_ONLY;
	}

	@Override
	public String loginViaDialgue() throws IncorrectUsernameOrPasswordException, UserNotFoundException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
