package com.diversityarrays.kdcompute.gobiitestapi;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.diversityarrays.kdcompute.QCPurge;
import com.diversityarrays.kdcompute.QCStart;
import com.diversityarrays.kdcompute.QCStatus;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.gobiiqc.StartResponse;
import com.diversityarrays.kdcompute.gobiiqc.containers.StatusResponse;

public class ApiTest {

	private static final int STATUS_OK = 200;
	private static final String REPORT_XLSX = "Report.xlsx";

	public static void main(String[] args) throws Exception {

		if (args.length < 2) {
			System.err.println("Expected 2 arguments: [KDCompute base URL] [Datasets directory paths...]");
			System.exit(1);
		}

		int timeoutMilli = 10000;
		String qcTimeoutValue = System.getProperties().getProperty("QC_TIMEOUT");
		if(qcTimeoutValue!=null) {
			timeoutMilli = Integer.parseInt(qcTimeoutValue);
		}
		
		String baseUrl = args[0];
		HtmlCaller htmlCaller = new HtmlCaller(baseUrl);

		for (int i = 1; i < args.length; i++) {
			String directory = args[i];
			File directoryF = new File(directory);
			String datasetName = directoryF.getName();

			/*
			 * QC Start
			 */
			info(datasetName, QCStart.QC_START, "Starting job");
			StartResponse startResponse = htmlCaller
					.get(QCStart.SLASH_QC_START + "?datasetId=" + i + "&directory=" + directory, StartResponse.class);
			Long jobId = startResponse.getJobId();
			if (jobId == null) {
				fail(datasetName, QCStart.QC_START, "Bad id returned");
			} else {
				info(datasetName, QCStart.QC_START, "Successful submission as job " + jobId);
			}

			long startTime = System.currentTimeMillis();

			String queryStatus = QCStatus.SLASH_QC_STATUS + "?jobid=" + jobId;
			StatusResponse statusResponse = htmlCaller.get(queryStatus, StatusResponse.class);
			while ((System.currentTimeMillis() - startTime) < timeoutMilli) {
				Thread.sleep(2000);
				if (!statusResponse.getStatus().isActive()) {
					break;
				}
				statusResponse = htmlCaller.get(queryStatus, StatusResponse.class);
			}
			String purgeQuery = QCPurge.SLASH_QC_PURGE + "?jobid=" + jobId;
			if ((System.currentTimeMillis() - startTime) > timeoutMilli) {
				fail(datasetName, QCStatus.QC_STATUS,
						"Waited too long (" + timeoutMilli + " milliseconds) for job " + jobId + " to complete");
				htmlCaller.delete(purgeQuery);
			}

			if (!statusResponse.getStatus().equals(RunState.COMPLETED)) {
				fail(datasetName, QCStatus.QC_STATUS, "Run State did not finish with " + RunState.COMPLETED);
			} else {
				info(datasetName, QCStatus.QC_STATUS, "Job " + jobId + " completed, checking output...");
				statusResponse.getResultsFilepaths().stream().anyMatch(e -> e.contains(REPORT_XLSX));
				info(datasetName, QCStatus.QC_STATUS, REPORT_XLSX + " is listed, job " + jobId + " is successful");

				info(datasetName, QCPurge.SLASH_QC_PURGE, "Purging job " + jobId + "...");
				int deleteCode = htmlCaller.delete(purgeQuery);
				if (deleteCode != STATUS_OK) {
					fail(datasetName, QCPurge.SLASH_QC_PURGE,
							"Attempt to purge job " + jobId + " returned non OK code: " + deleteCode);
				}
				try {
					statusResponse = htmlCaller.get(queryStatus, StatusResponse.class);
					fail(datasetName, QCStatus.QC_STATUS, "Failed to purge job " + jobId);
				} catch (Exception e) {
					if (!e.getMessage().contains("Server returned HTTP response code: 500")) {
						fail(datasetName, QCStatus.QC_STATUS,
								"Expected internal server error for qcStatus after job was deleted, but got: "
										+ e.getMessage());
					}
				}
			}
		}

	}

	private static void fail(String datasetName, String apiMethod, String message) {
		Logger.getLogger(ApiTest.class.getName()).log(Level.SEVERE,
				"Dataset=" + datasetName + ", method=" + apiMethod + ". Severe: " + message);
		System.exit(1);
	}

	private static void info(String datasetName, String apiMethod, String message) {
		Logger.getLogger(ApiTest.class.getName()).log(Level.INFO,
				"Dataset=" + datasetName + ", method=" + apiMethod + ". Info: " + message);
	}

}
