package com.diversityarrays.kdcompute.gobiitestapi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

class HtmlCaller {

	/**
	 * Taken from https://stackoverflow.com/a/41958414 to deal with incompatible
	 * date stamp
	 * 
	 * @author andrew
	 *
	 */
	public class DateDeserializer implements JsonDeserializer<Date> {

		@Override
		public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2)
				throws JsonParseException {
			String date = element.getAsString();
			try {
				long parseLong = Long.parseLong(date);
				return new Date(parseLong);
			} catch (NumberFormatException e) {
				try {
					return new SimpleDateFormat().parse(date);
				} catch (ParseException e1) {
					Logger.getLogger(HtmlCaller.class.getName()).log(Level.SEVERE, "Failed to parse date: " + date);
					System.exit(0);
					return null;
				}
			}

		}
	}

	private final Gson gson = new GsonBuilder() //
			.registerTypeAdapter(Date.class, new DateDeserializer()).create();

	// new GsonBuilder() //
	// .registerTypeAdapter(Date.class, new DateTypeAdapter()) //
	// .registerTypeAdapter(BigInteger.class, new BigIntegerAdapter()) //
	// .serializeNulls().create(); //

	private final String baseUrl;

	public HtmlCaller(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	<T> T get(String query, Class<T> t) throws Exception {
		String resultString = htmlGet(baseUrl + "/" + query);
		Logger.getLogger(HtmlCaller.class.getName()).log(Level.FINE, resultString);
		T responseT = gson.fromJson(resultString, t);
		return responseT;
	}

	<T> int delete(String query) throws Exception {
		return htmlDelete(baseUrl + "/" + query);
	}

	private static String htmlGet(String urlToRead) throws Exception {
		StringBuilder result = new StringBuilder();
		URL url = new URL(urlToRead);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		rd.close();
		return result.toString();
	}

	private static int htmlDelete(String urlToRead) throws Exception {
		URL url = new URL(urlToRead);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("DELETE");
		return connection.getResponseCode();
	}

}