/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.script;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AdditionalVariables  extends AdditionalEnvironmentVariables, AdditionalKDComputeSystemVariables{


	public static AdditionalVariables emptyAdditionalVariables(){
		return new AdditionalVariables() {

			@Override
			public Map<String, String> instantiateAdditionalKDComputeSystemVariables(String username) {
				return new HashMap<>();
			}

			@Override
			public List<SystemVariable> getAdditionalKDComputeSystemVariablesScope() {
				return new ArrayList<>();
			}

			@Override
			public Map<String, String> instantiateEnvironmentVariables(String username) {
				return new HashMap<>();
			}
		};
	}

}
