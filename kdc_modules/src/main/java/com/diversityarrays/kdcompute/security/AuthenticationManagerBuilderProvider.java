/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class AuthenticationManagerBuilderProvider {

	/**
	 * Configures server Authentication protocol
	 * 
	 * @param auth
	 */
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(new AbstractUserDetailsAuthenticationProvider() {

			@Override
			protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
					throws AuthenticationException {
				Object password = authentication.getCredentials();
				List<String> roles;
				Logger.getLogger(AuthenticationManagerBuilderProvider.class.getName()).log(Level.INFO,
						"User " + username + " attempting to login");
				try {
					roles = login(username, (String) password);
					Logger.getLogger(AuthenticationManagerBuilderProvider.class.getName()).log(Level.INFO,
							"User " + username + " logged in");
				} catch (IncorrectUsernameOrPasswordException e) {
					String msg = "Bad password for user: " + username;
					Logger.getLogger(AuthenticationManagerBuilderProvider.class.getName()).log(Level.WARNING, msg, e);
					throw new AuthenticationCredentialsNotFoundException(msg);
				} catch (UserNotFoundException e) {
					String msg = "User " + username + " was not found";
					Logger.getLogger(AuthenticationManagerBuilderProvider.class.getName()).log(Level.WARNING, msg, e);
					throw new AuthenticationCredentialsNotFoundException(msg);
				} catch (Exception e) {
					String msg = "Login for user=" + username + " threw exception";
					Logger.getLogger(AuthenticationManagerBuilderProvider.class.getName()).log(Level.WARNING, msg, e);
					throw new AuthenticationServiceException(e.getMessage(), e.getCause());
				}
				List<GrantedAuthority> authorities = roles.stream().map(role -> new SimpleGrantedAuthority(role))
						.collect(Collectors.toList());
				User user = new User(username, (String) password, authorities);
				return user;
			}

			@Override
			protected void additionalAuthenticationChecks(UserDetails userDetails,
					UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
				Map<String, String> details = new HashMap<>();
				String email = getEmail(userDetails.getUsername());
				if (email != null)
					details.put("email", email);
				authentication.setDetails(details);
			}
		});
	}

	/**
	 * Called by studio and by default AuthenticationProvider.configureGlobal
	 * 
	 * @param username
	 * @param password
	 * @return Either.left(Exception msg), Either.Right(List of authorities this
	 *         user is in, e.g. ROLE_USER,ROLE_ADMIN)
	 * @throws IncorrectUsernameOrPasswordException
	 * @throws UserNotFoundException
	 */
	public abstract List<String> login(String username, String password)
			throws IncorrectUsernameOrPasswordException, UserNotFoundException, Exception;

	@Bean
	public abstract LoginMode getLoginMode();

	public abstract String getEmail(String username);

	/**
	 * Obtain a username via some sort of GUI or w/e.
	 * 
	 * @return Username of logged in user
	 * @throws IncorrectUsernameOrPasswordException
	 * @throws UserNotFoundException
	 * @throws Exception
	 */
	public abstract String loginViaDialgue()
			throws IncorrectUsernameOrPasswordException, UserNotFoundException, Exception;
}
