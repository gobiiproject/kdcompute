/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

@SuppressWarnings("nls")
public class Pair<F, S> {

	public final F first;
	public final S second;

	public Pair(F f, S s) {
		this.first = f;
		this.second = s;
	}

	public F getFirst() {
		return first;
	}

	public S getSecond() {
		return second;
	}

	@Override
	public int hashCode() {
		return (first == null ? 0 : first.hashCode()) * 17 + (second == null ? 0 : second.hashCode());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Pair))
			return false;

		@SuppressWarnings("rawtypes")
		Pair other = (Pair) o;
		if (first == null) {
			return (other.first == null);
		}
		if (other.first == null) {
			return false;
		}
		if (!first.equals(other.first)) {
			return false;
		}

		if (second == null) {
			return (other.second == null);
		}
		return second.equals(other.second);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Pair(");
		if (first == null) {
			sb.append("<null>");
		} else {
			sb.append(first);
		}
		sb.append(" , ");
		if (second == null) {
			sb.append("<null>");
		} else {
			sb.append(second);
		}
		sb.append(')');
		return sb.toString();
	}

	public static <F, S> Pair<F, S> of(F f, S s) {
		return new Pair<F, S>(f, s);
	}
}
