/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.diversityarrays.kdcompute.db.User;
import com.diversityarrays.kdcompute.register.Register;
import com.diversityarrays.kdcompute.runtime.UserRoleSpecification;
import com.diversityarrays.kdcompute.userstore.UserDoesNotExistException;
import com.diversityarrays.kdcompute.userstore.UserExistsException;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
@Configuration
public class RegisterController {
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String userManagement(Model model, Principal principal) {
		UtilServer.setBannerModel(model, principal);

		List<String> unavailableUsers = KdcServerApplication.getInstance().getEntityStore().getUsers().stream()
				.map(User::getUserName).collect(Collectors.toList());

		model.addAttribute("unavailableUsers", unavailableUsers);
		model.addAttribute("register", new Register());

		return "register";
	}

	@Value("${kdcompute.registration.isEnabled}")
	Boolean registrationEnabled;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@ModelAttribute Register register, final RedirectAttributes redirectAttributes, Model model)
			throws UserExistsException {
		KdcServerApplication.getInstance().getUsersStore().addUser(register.getUserName(), register.getPass(),
				UserRoleSpecification.ROLE_USER);

		model.addAttribute("username", register.getUserName());
		model.addAttribute("password", register.getPass());

		model.addAttribute("registrationEnabled",
				registrationEnabled && KdcServerApplication.getInstance().getUsersStore().registrationEnabled());

		redirectAttributes.addAttribute("msg", "Successfully created user " + register.getUserName());
		return "redirect:/login";
	}

	@RequestMapping(value = "/admin/deleteuser/{username}", method = RequestMethod.GET)
	public String deleteUser(@PathVariable("username") String username) throws UserDoesNotExistException {

		KdcServerApplication.getInstance().getUsersStore().deleteUser(username);

		return "redirect:/admin";
	}

}
