/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.jobsummary;

import java.util.List;

public class JobSummaryPlugin {

	public final String name;
	public final String link_winzip;
	public final String link_gzip;
	public final List<String> script; // Array of lines
	public final List<String> stdout;
	public final List<String> stderr;
	public final List<FileContent> other;
	public JobSummaryPlugin(String name, String link_winzip, String link_gzip, List<String> script,
			List<String> stdout, List<String> stderr, List<FileContent> other) {
		super();
		this.name = name;
		this.link_winzip = link_winzip;
		this.link_gzip = link_gzip;
		this.script = script;
		this.stdout = stdout;
		this.stderr = stderr;
		this.other = other;
	}
	
	
}
