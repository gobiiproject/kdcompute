/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginGroup;
import com.diversityarrays.kdcompute.db.PluginVersion;
import com.diversityarrays.kdcompute.pluginview.PluginDisplay;
import com.diversityarrays.kdcompute.pluginview.PluginGroupDisplay;
import com.diversityarrays.kdcompute.pluginview.VersionDisplay;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
public class PluginsController {

	public static final String PLUGIN_HELP_PREFIX_URL = "/plugin/help/";
	private static final Comparator<? super PluginGroup> PLUGIN_GROUP_BY_NAME = new Comparator<PluginGroup>() {
		@Override
		public int compare(PluginGroup o1, PluginGroup o2) {
			return o1.getGroupName().compareToIgnoreCase(o2.getGroupName());
		}
	};

	@RequestMapping("/plugins")
	public String plugins(Model model, Principal principal) {
		UtilServer.setBannerModel(model, principal);

		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		List<PluginGroup> pluginsGroups = entityStore.getPluginGroups();
		pluginsGroups.sort(PLUGIN_GROUP_BY_NAME);
		model.addAttribute("plugins_tab_active", "active");

		List<PluginGroupDisplay> asList = new ArrayList<>();

		Comparator<Integer> integerComparator = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.intValue() - o2.intValue();
			}
		};

		Comparator<VersionDisplay> versionSorter = new Comparator<VersionDisplay>() {

			@Override
			public int compare(VersionDisplay o1, VersionDisplay o2) {
				return o2.getVersion().compareTo(o1.getVersion());
			}
		};

		// Construct menu objects. This contains the plugins, along with their
		// versions.
		for (PluginGroup pluginGroup : pluginsGroups) {
			if (pluginGroup.getPlugins().isEmpty())
				continue;
			List<PluginDisplay> pluginDisplays = new ArrayList<>();
			for (Plugin plugin : pluginGroup.getPlugins()) {
				if (pluginDisplays.stream().filter(e -> e.getAlgorithmName().equals(plugin.getAlgorithmName()))
						.count() == 0) {
					// Algorithm not added. Add, with it's versions
					Plugin[] array = pluginGroup.getPlugins().stream()
							.filter(e -> e.getAlgorithmName().equals(plugin.getAlgorithmName()))
							.toArray(e -> new Plugin[e]);
					Supplier<Stream<Plugin>> streamSupplier = () -> Stream.of(array);
					List<VersionDisplay> versions = Arrays
							.asList(streamSupplier.get().map(e -> new VersionDisplay(e.getVersion(), e.getId()))
									.sorted(versionSorter).toArray(s -> new VersionDisplay[s]));

					List<PluginVersion> collect = streamSupplier.get().map(e -> e.getVersion()).sorted()
							.collect(Collectors.toList());
					PluginVersion latestVersion = collect.get(collect.size() - 1);

					Long idOfLatest = streamSupplier.get().filter(e -> e.getVersion().equals(latestVersion)).findFirst()
							.get().getId();

					boolean hasMultipleVersions = versions.size() > 1;
					pluginDisplays.add(
							new PluginDisplay(idOfLatest, plugin.getAlgorithmName(), versions, hasMultipleVersions));

				}
			}
			asList.add(new PluginGroupDisplay(pluginDisplays, pluginGroup.getGroupName()));
		}

		model.addAttribute("pluginGroups", asList);

		return "plugins";
	}

	@RequestMapping(PLUGIN_HELP_PREFIX_URL + "{pluginId}")
	@ResponseBody
	public String pluginHelp(@PathVariable("pluginId") Long pluginId, HttpServletResponse response) throws IOException {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		Plugin plugin = null;
		for (Plugin p : entityStore.getPlugins()) {
			if (p.getId().equals(pluginId)) {
				plugin = p;
				break;
			}
		}
		if (plugin == null || plugin.getHtmlHelp() == null) {
			response.sendRedirect("plugin/nohelp");
			return "";
		}

		try {
			File pluginFolder = KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin);
			String urlPath = pluginFolder.getPath() + File.separator + plugin.getHtmlHelp();
			String readFileToString = FileUtils.readFileToString(new File(urlPath), "UTF-8");
			return readFileToString;
		} catch (IOException e) {
			response.sendRedirect("plugin/nohelp");
			return "";
		}
	}

	@RequestMapping("/plugin/help/{pluginId}/plugin/nohelp")
	public String noPluginHelp(@PathVariable("pluginId") Long pluginId) {
		return "nopluginhelp";
	}

	@RequestMapping(value = "/plugin/help/{pluginId}/plugin/static/img/{rem}.{ext}", produces = MediaType.IMAGE_PNG_VALUE)
	public String pluginHelpRestImg(@PathVariable("pluginId") Long pluginId, @PathVariable("rem") String rem,
			@PathVariable("ext") String ext) {
		return "redirect:/static/img/" + rem + "." + ext;
	}

	@RequestMapping(value = "/plugin/help/{pluginId}/**/*.png", produces = MediaType.IMAGE_PNG_VALUE)
	@ResponseBody
	public byte[] pluginHelpRestPng(@PathVariable("pluginId") Long pluginId, HttpServletRequest request) {
		return getFile(pluginId, request);
	}

	@RequestMapping(value = "/plugin/help/{pluginId}/**/*.gif", produces = MediaType.IMAGE_GIF_VALUE)
	@ResponseBody
	public byte[] pluginHelpRestGif(@PathVariable("pluginId") Long pluginId, HttpServletRequest request) {
		return getFile(pluginId, request);
	}

	@RequestMapping(value = "/plugin/help/{pluginId}/**/*.jpeg", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] pluginHelpRestJpeg(@PathVariable("pluginId") Long pluginId, HttpServletRequest request) {
		return getFile(pluginId, request);
	}

	@RequestMapping(value = "/plugin/help/{pluginId}/**/*.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	@ResponseBody
	public byte[] pluginHelpRestPdf(@PathVariable("pluginId") Long pluginId, HttpServletRequest request) {
		return getFile(pluginId, request);
	}

	@RequestMapping(value = "/plugin/help/{pluginId}/**/*.*")
	public void pluginHelpRestOther(@PathVariable("pluginId") Long pluginId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		Plugin plugin = null;
		for (Plugin p : entityStore.getPlugins()) {
			if (p.getId().equals(pluginId)) {
				plugin = p;
				break;
			}
		}

		File pluginFolder = KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin);
		File helpFilePath = new File(pluginFolder.getPath() + File.separator + plugin.getHtmlHelp());

		// String filePathFromHelpFolder = new AntPathMatcher()
		// .extractPathWithinPattern( "/plugin/help/"
		// +Long.toString(pluginId)+"/**", request.getRequestURI() );

		String requestURI = request.getRequestURI();
		String pattern = PLUGIN_HELP_PREFIX_URL + Long.toString(pluginId) + "/";
		String filePathFromHelpFolder = requestURI.replaceAll(".*" + pattern, "");
		File urlPath = new File(helpFilePath.getParentFile(), filePathFromHelpFolder);
		InputStream in = new FileInputStream(urlPath);

		// get your file as InputStream
		// copy it to response's OutputStream
		org.apache.commons.io.IOUtils.copy(in, response.getOutputStream());
		response.flushBuffer();

	}

	public byte[] getFile(Long pluginId, HttpServletRequest request) {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		Plugin plugin = null;
		for (Plugin p : entityStore.getPlugins()) {
			if (p.getId().equals(pluginId)) {
				plugin = p;
				break;
			}
		}

		try {
			File pluginFolder = KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin);
			File helpFilePath = new File(pluginFolder.getPath() + File.separator + plugin.getHtmlHelp());

			// String filePathFromHelpFolder = new AntPathMatcher()
			// .extractPathWithinPattern( "/plugin/help/"
			// +Long.toString(pluginId)+"/**", request.getRequestURI() );

			String requestURI = request.getRequestURI();
			String pattern = PLUGIN_HELP_PREFIX_URL + Long.toString(pluginId) + "/";
			String filePathFromHelpFolder = requestURI.replaceAll(".*" + pattern, "");
			File urlPath = new File(helpFilePath.getParentFile(), filePathFromHelpFolder);
			InputStream in = new FileInputStream(urlPath);
			return IOUtils.toByteArray(in);
		} catch (IOException e) {
			return null;
		}
	}

}
