/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.diversityarrays.kdcompute.customhtml.Fragments;
import com.diversityarrays.kdcompute.customhtml.HtmlForm;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.plugin.files.single.ExamplesListAndKnobBindings;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
public class PluginFormController {

	@RequestMapping(value = { "/showalgo/{id}" }, method = RequestMethod.GET)
	public String getAlgorithmForm(@PathVariable("id") String id, Model model, Principal principal)
			throws NullPointerException, IOException {
		UtilServer.setBannerModel(model, principal);

		Long algoId = Long.parseLong(id);
		Plugin plugin = KdcServerApplication.getInstance().getPluginFromId(algoId);
		model.addAttribute("algorithm", plugin);

		List<Knob> allKnobs = new ArrayList<>();
		List<Knob> allAlgoKnobs = plugin.getKnobs();
		allKnobs.addAll(allAlgoKnobs);

		String htmlFormTemplateFilename = plugin.getHtmlFormTemplateFilename();
		if (htmlFormTemplateFilename != null && !htmlFormTemplateFilename.isEmpty()) {
			return "redirect:/showalgo/" + id + "/custom";
		}

		/*
		 * Add examples
		 */
		PluginFiles pluginFiles = null;
		try {
			String customHtmlKnobsBody = Fragments.createHtmlPage(new HtmlForm(plugin).getKnobs(), plugin);
			String customHtmlKnobsBodyWithAlgorithmId = "<input id=\"" + id + "\" name=\"algorithmId\" value=\"" + id
					+ "\" type=\"hidden\">" + customHtmlKnobsBody;
			model.addAttribute("knobs_body", customHtmlKnobsBodyWithAlgorithmId);
			File pluginFolder = KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin);
			pluginFiles = PluginFiles.readPluginFiles(pluginFolder);
			ExamplesListAndKnobBindings examplesListAndKnobBindings = pluginFiles.getExamplesListAndKnobBindings();
			model.addAttribute("examples", examplesListAndKnobBindings.getExamples());
			model.addAttribute("examplesKnobbindings", examplesListAndKnobBindings.getBindings());
		} catch (Exception e) {
			e.printStackTrace();
		}

		File userDir = KdcServerApplication.getInstance().getFileStorage()
				.getUserDir(KdcServerApplication.getInstance().getUserId(principal));
		String path = userDir.getPath();
		model.addAttribute("rootDir", path);

		return "algorithmformCustom";
	}

	@RequestMapping(value = { "/showalgo/{id}/custom" }, method = RequestMethod.GET)
	public String getCustomAlgorithmForm(@PathVariable("id") String id, Model model, Principal principal) {
		UtilServer.setBannerModel(model, principal);

		Long algoId = Long.parseLong(id);
		Plugin plugin = KdcServerApplication.getInstance().getPluginFromId(algoId);
		model.addAttribute("algorithm", plugin);

		List<Knob> allKnobs = new ArrayList<>();
		List<Knob> allAlgoKnobs = plugin.getKnobs();
		allKnobs.addAll(allAlgoKnobs);

		model.addAttribute("knobsValidationString", Fragments.buildMap(plugin));

		String htmlFormTemplateFilename = plugin.getHtmlFormTemplateFilename();
		PluginFiles pluginFiles = null;
		try {
			pluginFiles = PluginFiles
					.readPluginFiles(KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin));
			if (htmlFormTemplateFilename != null && !htmlFormTemplateFilename.isEmpty()) {

				String customHtmlKnobsBody = FileUtils.readFileToString(pluginFiles.getCustomHtmlForm(),
						Charset.defaultCharset());

				String customHtmlKnobsBodyWithAlgorithmId = "<input id=\"" + id + "\" name=\"algorithmId\" value=\""
						+ id + "\" type=\"hidden\">" + customHtmlKnobsBody;

				model.addAttribute("knobs_body", customHtmlKnobsBodyWithAlgorithmId);

				// Add examples
				ExamplesListAndKnobBindings examplesListAndKnobBindings = pluginFiles.getExamplesListAndKnobBindings();
				model.addAttribute("examples", examplesListAndKnobBindings.getExamples());
				model.addAttribute("examplesKnobbindings", examplesListAndKnobBindings.getBindings());

				File userDir = KdcServerApplication.getInstance().getFileStorage()
						.getUserDir(KdcServerApplication.getInstance().getUserId(principal));
				String path = userDir.getPath();
				model.addAttribute("rootDir", path);

				return "algorithmformCustom";
			}
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return "Failed to retrieve Custom HTML form for " + plugin.getAlgorithmName();
	}

}
