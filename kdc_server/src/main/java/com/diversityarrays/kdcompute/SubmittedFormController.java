/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PostCompletionCall;
import com.diversityarrays.kdcompute.db.RequiredKnobNotProvidedException;
import com.diversityarrays.kdcompute.email.EmailSender;
import com.diversityarrays.kdcompute.email.EmailStatus;
import com.diversityarrays.kdcompute.jobsummary.JobSummaryUtil;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.JobQueueException;
import com.diversityarrays.kdcompute.runtime.ParameterException;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;
import com.diversityarrays.kdcompute.submitjob.PluginJobParams;
import com.diversityarrays.kdcompute.submitjob.SubmitJobUtils;
import com.diversityarrays.kdcompute.util.Check;
import com.diversityarrays.kdcompute.util.Either;
import com.diversityarrays.kdcompute.util.ServerUtils;
import com.diversityarrays.kdcompute.util.UtilServer;
import com.google.common.collect.ImmutableMap;

@Controller
@Configuration
public class SubmittedFormController {

	static private final String inputsDir = "plugin_inputs";

	@RequestMapping(method = RequestMethod.POST, value = "/submitform")
	public String getSubmittedForm(HttpServletRequest request, HttpServletResponse respose,
			@RequestParam(value = "file_kdcompute_knob", required = false) MultipartFile[] file,
			RedirectAttributes model, Principal principal, final RedirectAttributes redirectAttributes)
			throws RequiredKnobNotProvidedException, NullPointerException, IOException {
		String result = null;

		KdcServerApplication application = KdcServerApplication.getInstance();

		if (respose.getStatus() == 200) {
			// WUHU it went through;
			long algorithmId = Long.parseLong(request.getParameter("algorithmId"));
			Plugin plugin = application.getPluginFromId(algorithmId);
			if (plugin != null) {
				File userTempDir;
				Pair<List<File>, List<KnobBinding>> r = null;
				try {
					userTempDir = KdcServerApplication.getUserTmpDir(principal);
					PluginFiles pluginFiles = PluginFiles.readPluginFiles(
							KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin));
					r = SubmitJobUtils.getKnobsAndValues(request, file, pluginFiles, userTempDir);
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}

				final List<File> inputFilesToCopy = r.first;
				final List<KnobBinding> knobsAndValues = r.second;

				knobsAndValues.forEach(knobBinding -> redirectAttributes
						.addAttribute(knobBinding.getKnob().getKnobName(), knobBinding.getKnobValue()));

				final String serverBase = JobSummaryController.getURLBase(request);

				result = submitKnobsAndValues(principal, plugin, knobsAndValues,
						(job) -> onFinish(serverBase, principal, job, plugin, inputFilesToCopy));
			}
		}
		return result;
	}

	private void onFinish(String serverBase, Principal principal, AnalysisJob analysisJob, Plugin plugin,
			List<File> inputFilesToCopy) {
		if (!Check.isEmpty(inputFilesToCopy)) {
			for (File f : inputFilesToCopy) {
				try {
					String pluginFolderSafe = FileStorage.makeFileSafe(plugin.getAlgorithmName());
					File pluginJobDir = new File(analysisJob.getResultsFolder(), pluginFolderSafe);
					File pluginInputDir = new File(pluginJobDir, inputsDir);
					pluginJobDir.mkdirs();
					FileUtils.copyFileToDirectory(f, pluginInputDir);
				} catch (IOException e) {
					// FIXME [BSP] really should be surfacing this to the
					// response
					e.printStackTrace();
				}
			}
		}

		if (principal != null) {
			String email = null;
			if (principal instanceof OAuth2Authentication) {
				OAuth2Authentication oauthentication = (OAuth2Authentication) principal;
				@SuppressWarnings("unchecked")
				Map<String, String> details = (Map<String, String>) oauthentication.getUserAuthentication()
						.getDetails();
				if (details.containsKey("email"))
					email = details.get("email");
			} else if (principal instanceof UsernamePasswordAuthenticationToken) {
				try {
					UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) principal;
					Object details2 = auth.getDetails();
					if (details2 instanceof Map<?, ?>) {
						Map<String, String> details = (Map<String, String>) details2;
						if (details.containsKey("email")) {
							email = details.get("email");
						}
					}
				} catch (Exception e) {
					// move on
				}
			}
			if (email != null) {
				try {
					sendEmail(serverBase, email, analysisJob.getId());
				} catch (Exception e) {
					Logger.getLogger(SubmittedFormController.class).log(Level.ERROR,
							"Exception occured when sending email to " + email + ": " + e);
				}
			}
		}
	}

	private String submitKnobsAndValues(Principal principal, Plugin plugin, List<KnobBinding> knobsAndValues,
			PostCompletionCall postCompletionCall) throws NullPointerException, IOException {
		String userId = KdcServerApplication.getInstance().getUserId(principal);
		PluginJobParams params = new PluginJobParams(plugin, knobsAndValues, userId, postCompletionCall, null);
		Either<Exception, AnalysisJob> either = submitJob(params);
		if (either.isLeft()) {
			return either.left().getMessage();
		}

		String buildPrefixedCommand = JobSummaryController.buildPrefixedCommand("redirect:", userId, either.right());
		return buildPrefixedCommand;
	}

	static private Either<Exception, AnalysisJob> createJobOrError(AnalysisRequest req,
			KdcServerApplication application) {
		try {
			AnalysisJob job = application.getAnalysisJobFactory().createAnalysisJob(req, req.getForUser(),
					(f) -> Logger.getLogger(SubmittedFormController.class).log(Level.INFO,
							"Created job: " + f.getPath()),
					application.getAdditionalVariables()
							.instantiateAdditionalKDComputeSystemVariables(req.getForUser()));
			return Either.right(job);
		} catch (UserIsNotLoggedInException | IOException | ParameterException e) {
			return Either.left(e);
		}
	}

	public static Either<Exception, AnalysisJob> submitJob(PluginJobParams params) {
		KdcServerApplication application = KdcServerApplication.getInstance();
		EntityStore store = application.getEntityStore();

		Either<Exception, AnalysisJob> either = SubmitJobUtils.submitJob(params, (obj) -> store.persist(obj),
				(ar) -> createJobOrError(ar, application), (job) -> doJobQueueSubmit(application, params, job));

		return either;
	}

	private static Optional<JobQueueException> doJobQueueSubmit(KdcServerApplication app, PluginJobParams params,
			AnalysisJob job) {
		JobQueue jobQueue = app.getJobQueue();
		try {
			jobQueue.submitJob(job);
			params.onSubmission(job);
			return Optional.empty();
		} catch (JobQueueException e) {
			return Optional.of(e);
		}
	}

	@Autowired
	private EmailSender emailSender;

	@Autowired
	private TemplateEngine templateEngine;

	@RequestMapping("/emailJobSummaryTest")
	@ResponseBody
	public EmailStatus sendEmailTest() throws AddressException, IOException, URISyntaxException {
		return sendEmail("http://localhost:8080/", "kow.andrew@gmail.com", (long) 356);
	}

	@Autowired
	@Value("${mail.maxAttachmentSizeKB}")
	private Long maxAttachmentSizeKB;

	private static final String ANDYEMAIL = "andrew@diversityarrays.com";

	@RequestMapping("/emailJobSummary")
	@ResponseBody
	public EmailStatus sendEmail(@RequestParam String serverBase, @RequestParam String to, @RequestParam Long jobId)
			throws AddressException, IOException, URISyntaxException {

		String jobSummaryUrl = ServerUtils.buildParamUrl(JobSummaryController.JOBSUMMARY,
				ImmutableMap.of(JobSummaryController.JOBID, Long.toString(jobId)));
		KdcServerApplication instance = KdcServerApplication.getInstance();
		EntityStore entityStore = instance.getEntityStore();
		AnalysisJob analysisJob = entityStore.getById(AnalysisJob.class, jobId);

		Context context = new Context();

		context.setVariable("kdcVersion", KdcServerApplication.getInstance().getVersion());

		context.setVariable("kdcUrl", serverBase);
		context.setVariable("title", "Algorithm: " + analysisJob.getDisplayName());
		context.setVariable("jobid", analysisJob.getId());
		context.setVariable("jobStateMsg", JobSummaryController.jobstatemsg(jobId));
		context.setVariable("jobSummaryUrl", serverBase + jobSummaryUrl);
		context.setVariable("jobcreated", analysisJob.getCreationDateTime());
		context.setVariable("jobstart", analysisJob.getStartDateTime());
		context.setVariable("jobend", analysisJob.getEndDateTime());

		String body = templateEngine.process("email/jobSummary", context);

		List<Pair<String, Resource>> inlineResources = new ArrayList<Pair<String, Resource>>();
		inlineResources
				.add(new Pair<String, Resource>("gotoKDCImg", new ClassPathResource("gotoKDC.png", EmailSender.class)));
		inlineResources.add(new Pair<String, Resource>("jobStateImage",
				new ClassPathResource(JobSummaryController.jobstateimg(jobId), EmailSender.class)));
		inlineResources.add(new Pair<String, Resource>("bannerImage",
				new ClassPathResource("KDCompute_with_text.png", EmailSender.class)));

		List<Pair<String, File>> files = new ArrayList<Pair<String, File>>();
		List<File> filesRaw = JobSummaryUtil.getFilesOfJob(entityStore, analysisJob);
		Long totalKb = filesRaw.stream().mapToLong(f -> f.length()).sum() / 1000;
		if (totalKb < maxAttachmentSizeKB) {
			filesRaw.forEach(e -> {
				files.add(new Pair<String, File>(e.getName(), e));
			});

		} else {
			HashMap<String, String> filename2LinksMap = JobSummaryController.filename2link(analysisJob.getId(),
					serverBase);
			context.setVariable("filename2LinksMap", filename2LinksMap);
		}

		EmailStatus emailStatus = emailSender.send(to, Arrays.asList(),
				"KDC Job " + Long.toString(analysisJob.getId()) + " Report: " + analysisJob.getDisplayName(), body,
				inlineResources, files);

		return emailStatus;
	}

	@RequestMapping("/emailTest")
	public String testEmail(HttpServletRequest request, Model model) throws MalformedURLException {

		UtilServer.setBannerModel(model, null);

		final String serverBase = JobSummaryController.getURLBase(request);
		Long jobId = 15L;

		String jobSummaryUrl = ServerUtils.buildParamUrl("/" + JobSummaryController.JOBSUMMARY,
				ImmutableMap.of(JobSummaryController.JOBID, Long.toString(jobId)));
		KdcServerApplication instance = KdcServerApplication.getInstance();
		EntityStore entityStore = instance.getEntityStore();
		AnalysisJob analysisJob = entityStore.getById(AnalysisJob.class, jobId);

		model.addAttribute("kdcUrl", serverBase);
		model.addAttribute("title", "Algorithm: " + analysisJob.getDisplayName());
		model.addAttribute("jobid", analysisJob.getId());
		model.addAttribute("jobStateMsg", JobSummaryController.jobstatemsg(jobId));
		model.addAttribute("jobSummaryUrl", serverBase + "/" + jobSummaryUrl);
		model.addAttribute("jobcreated", analysisJob.getCreationDateTime());
		model.addAttribute("jobstart", analysisJob.getStartDateTime());
		model.addAttribute("jobend", analysisJob.getEndDateTime());

		return "email/jobSummary";
	}
}
