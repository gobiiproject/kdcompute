/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.Server;
import org.apache.catalina.Service;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.ProtocolHandler;
import org.apache.coyote.http11.Http11AprProtocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.coyote.http11.Http11Protocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.ui.Model;

import com.diversityarrays.kdcompute.KdcServerApplication;
import com.diversityarrays.kdcompute.headerfooter.KDCHeaderButton;
import com.diversityarrays.kdcompute.runtime.EntityStore;

@Configuration
public class UtilServer {

	static Function<String, List<KDCHeaderButton>> additionalHeadersSupplier;

	@Autowired(required = false)
	public void setAdditionalHeaderButtons(Function<String, List<KDCHeaderButton>> additionalHeadersSupplier) {
		UtilServer.additionalHeadersSupplier = additionalHeadersSupplier;
	}

	public static int getPortServerIsListenerTo() {
		MBeanServer mBeanServer = MBeanServerFactory.findMBeanServer(null).get(0);

		Server server = null;
		try {
			ObjectName name = new ObjectName("Tomcat", "type", "Server");
			server = (Server) mBeanServer.getAttribute(name, "managedResource");
		} catch (AttributeNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MBeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ReflectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedObjectNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int serverPort = 0;

		Service[] services = server.findServices();
		for (Service service : services) {
			for (Connector connector : service.findConnectors()) {
				ProtocolHandler protocolHandler = connector.getProtocolHandler();
				if (protocolHandler instanceof Http11Protocol || protocolHandler instanceof Http11AprProtocol
						|| protocolHandler instanceof Http11NioProtocol) {
					serverPort = connector.getLocalPort();
					System.out.println("HTTP Port: " + serverPort);
				}
			}
		}
		return serverPort;
	}

	/**
	 * Set User Name in the banner
	 * 
	 * @param model
	 *            of the controller to set this parameter to.
	 */
	public static String setBannerModel(Model model, Principal principal) {

		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		String loggedInUser = null;
		String userAsIs = null;
		if (authentication != null) {
			loggedInUser = authentication.getName();
			userAsIs = loggedInUser;
			StringBuilder builder = new StringBuilder("Logged in as ");
			builder.append(loggedInUser);
			model.addAttribute("userName", builder.toString());

			if (principal != null) {

				if (principal instanceof OAuth2Authentication) {
					OAuth2Authentication oauthentication = (OAuth2Authentication) principal;
					@SuppressWarnings("unchecked")
					Map<String, String> details = (Map<String, String>) oauthentication.getUserAuthentication()
							.getDetails();
					// if (details.containsKey("username")) {
					// model.addAttribute("username", details.get("username"));
					// } else if (details.containsKey("name")) {
					// model.addAttribute("username", details.get("name"));
					// }
					if (details.containsKey("email"))
						model.addAttribute("email", details.get("email"));
					if (details.containsKey("picture")) {
						model.addAttribute("picture", details.get("picture"));
					} else if (details.containsKey("avatar_url")) {
						model.addAttribute("picture", details.get("avatar_url"));
					}
				} else if (principal instanceof UsernamePasswordAuthenticationToken) {
					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) principal;
					Collection<GrantedAuthority> authorities = usernamePasswordAuthenticationToken.getAuthorities();
					if (authorities.stream().anyMatch(e -> e.getAuthority().equals("ROLE_ADMIN"))) {
						model.addAttribute("user_is_admin", true);
					}
				}
				loggedInUser = principal.getName();
				if (!model.containsAttribute("username") && loggedInUser != null && !loggedInUser.isEmpty()) {
					model.addAttribute("username", loggedInUser);
				}
				loggedInUser = "server." + loggedInUser;
			}

			model.addAttribute("kdcVersion", KdcServerApplication.getInstance().getVersion());
			EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
			if (entityStore.isUserAnAdmin(loggedInUser)) {
				model.addAttribute("user_is_admin", true);
			}
		} else {
			model.addAttribute("userName", "");
			model.addAttribute("kdcVersion", KdcServerApplication.getInstance().getVersion());
		}

		if (additionalHeadersSupplier != null)
			model.addAttribute("additionalHeaderButtons", additionalHeadersSupplier.apply(userAsIs));

		return loggedInUser;
	}

	public static boolean isUserAdmin(Principal principal) {
		if (principal != null) {
			if (principal instanceof UsernamePasswordAuthenticationToken) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) principal;
				Collection<GrantedAuthority> authorities = usernamePasswordAuthenticationToken.getAuthorities();
				if (authorities.stream().anyMatch(e -> e.getAuthority().equals("ROLE_ADMIN"))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets a email from the principle component, or null if non can be
	 * found/obtained
	 * 
	 * @param principal
	 * @return
	 */
	public static String getEmail(Principal principal) {
		String email = null;
		if (principal != null) {
			if (principal instanceof OAuth2Authentication) {
				OAuth2Authentication oauthentication = (OAuth2Authentication) principal;
				@SuppressWarnings("unchecked")
				Map<String, String> details = (Map<String, String>) oauthentication.getUserAuthentication()
						.getDetails();
				if (details.containsKey("email"))
					email = details.get("email");
			} else if (principal instanceof UsernamePasswordAuthenticationToken) {
				try {
					UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) principal;
					Object details2 = auth.getDetails();
					if (details2 instanceof Map<?, ?>) {
						Map<String, String> details = (Map<String, String>) details2;
						if (details.containsKey("email")) {
							email = details.get("email");
						}
					}
				} catch (Exception e) {
					// move on
				}
			}
		}
		return email;
	}

	public static String curlPage(String webpageUrl) throws UnsupportedEncodingException, IOException {
		URL obj = new URL(webpageUrl);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		conn.setReadTimeout(5000);
		conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");

		boolean redirect = false;

		// normally, 3xx is redirect
		int status = conn.getResponseCode();
		if (status != HttpURLConnection.HTTP_OK) {
			if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM
					|| status == HttpURLConnection.HTTP_SEE_OTHER) {
				redirect = true;
			} else {
				throw new IOException("Failed to retreive page " + webpageUrl + ", responseCode=" + status);
			}
		}

		if (redirect) {

			// get redirect url from "location" header field
			String newUrl = conn.getHeaderField("Location");

			// get the cookie if need, for login
			String cookies = conn.getHeaderField("Set-Cookie");

			// open the new connnection again
			conn = (HttpURLConnection) new URL(newUrl).openConnection();
			conn.setRequestProperty("Cookie", cookies);
			conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			conn.addRequestProperty("User-Agent", "Mozilla");
			conn.addRequestProperty("Referer", "google.com");

		}

		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		StringBuffer html = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			html.append(inputLine);
		}
		in.close();

		String string = html.toString();
		return string;
	}

	public static String getUrlRedirect(String webpageUrl) throws IOException {
		URL obj = new URL(webpageUrl);
		HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
		conn.setReadTimeout(5000);
		conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");

		boolean redirect = false;

		// normally, 3xx is redirect
		int status = conn.getResponseCode();
		if (status != HttpURLConnection.HTTP_OK) {
			if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM
					|| status == HttpURLConnection.HTTP_SEE_OTHER) {
				redirect = true;
			} else {
				throw new IOException("Failed to retreive page " + webpageUrl + ", responseCode=" + status);
			}
		}

		if (redirect) {

			// get redirect url from "location" header field
			String newUrl = conn.getHeaderField("Location");

			// get the cookie if need, for login
			String cookies = conn.getHeaderField("Set-Cookie");

			// open the new connnection again
			conn = (HttpURLConnection) new URL(newUrl).openConnection();
			conn.setRequestProperty("Cookie", cookies);
			conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			conn.addRequestProperty("User-Agent", "Mozilla");
			conn.addRequestProperty("Referer", "google.com");

			return newUrl;
		}
		return webpageUrl;
	}

	public static String getBaseUrl(HttpServletRequest request) {
		String scheme = request.getScheme() + "://";
		String serverName = request.getServerName();
		String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		String contextPath = request.getContextPath();
		return scheme + serverName + serverPort + contextPath;
	}

	public static String getOsDistro() {
		String[] cmd = { "/bin/sh", "-c", "cat /etc/*-release" };

		try {
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));

			return org.apache.commons.io.IOUtils.toString(bri);
		} catch (IOException e) {

			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Export a resource embedded into a Jar file to the local file path.
	 *
	 * @param resourceName
	 *            ie.: "/SmartLibrary.dll"
	 * @return The path to the exported resource
	 * @throws Exception
	 */
	static public File ExportResource(Class<?> resourceClass, String resourceName, File destDir) throws Exception {
		InputStream stream = null;
		OutputStream resStreamOut = null;
		File dest = null;

		if (!destDir.isDirectory()) {
			throw new IOException(destDir + " does not exist!");
		}

		try {

			/*
			 * note that each / is a directory down in the "jar tree" been the
			 * jar the root of the tree
			 */
			stream = resourceClass.getResourceAsStream(resourceName);

			if (stream == null) {
				throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");
			}

			int readBytes;
			byte[] buffer = new byte[4096];
			dest = new File(destDir, resourceName);
			resStreamOut = new FileOutputStream(dest);
			while ((readBytes = stream.read(buffer)) > 0) {
				resStreamOut.write(buffer, 0, readBytes);
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			stream.close();
			resStreamOut.close();
		}

		return dest;
	}

}
