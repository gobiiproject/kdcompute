/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.commons.io.FilenameUtils;
import org.springframework.transaction.annotation.Transactional;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.JobStillActiveException;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginGroup;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.db.User;
import com.diversityarrays.kdcompute.db.UserRole;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.KDCDbUtil;
import com.diversityarrays.kdcompute.runtime.ScriptNotSupportedException;
import com.diversityarrays.kdcompute.runtime.UserRoleSpecification;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobNotFoundException;

public class KdcomputeEntityManagerEntityStore implements EntityStore {


	public static final Comparator<Plugin> REVERSE_VERSION_NUMBER = new Comparator<Plugin>() {
		@Override
		public int compare(Plugin o1, Plugin o2) {
			return o1.getVersion().compareTo(o2.getVersion());
		}
	};

	@PersistenceContext
	private EntityManager manager;

	private FileStorage fileStorage;
	private EntityManagerFactory factory;

	private List<Consumer<Plugin>> deleteJobsOfPluginResponseList = new ArrayList<>();

	private Map<String, CallScript> callScripts = new HashMap<>();

	@Override
	@Transactional
	public <T> T persist(T record) {
		manager.persist(record);
		return record;
	}

	@Override
	@Transactional
	public <T> T merge(T record) {
		manager.merge(record);

		return null;
	}

	@Override
	@Transactional
	public void refresh(Object record) {

		manager.refresh(record);

	}

	@Override
	@Transactional
	public void remove(Object record) {
		manager.remove(manager.contains(record) ? record : manager.merge(record));

	}

	@Override
	public <T> T getById(Class<T> rClass, Long id) {
		T entity = KDCDbUtil.searchEntityById(manager, id, rClass);
		return entity;
	}

	@Override
	public List<PluginGroup> getPluginGroups() {
		List<PluginGroup> pluginGroups = KDCDbUtil.getAllEntities(this, PluginGroup.class);
		return pluginGroups;
	}

	@Override
	public List<Plugin> getPlugins() {
		List<Plugin> plugins = KDCDbUtil.getAllEntities(this, Plugin.class);
		return plugins;
	}

	@Override
	public Throwable doTransaction(Consumer<EntityTransaction> worker) {
		// TODO Auto-generated method stub
		return null;
	}

	public void createTables() {

	}

	@Override
	public boolean isInMemory() {
		return false;
	}

	@Override
	public PluginGroup getPluginGroupBy(String groupName) {

		List<PluginGroup> groups = KDCDbUtil.getEntityByColumnNameMatch(manager, PluginGroup.PluginGroupName,
				PluginGroup.class, groupName);
		// There can onlu be one, unique field.
		if (groups != null && groups.size() > 0) {
			return groups.get(0);
		}

		return null;
	}

	@Override
	public boolean isPluginUsedInAnyRunBindings(Plugin plugin) {
		TypedQuery<RunBinding> qry = manager.createQuery("select r from RunBinding r where algorithm=:a",
				RunBinding.class);
		qry.setParameter(":a", plugin);

		try {
			return null != qry.getSingleResult();
		} catch (NoResultException e) {
			return false;
		} catch (NonUniqueResultException e) {
			return true;
		}
	}

	@Override
	public List<String> getListOfPluginNames() {
		List<Plugin> plugins = KDCDbUtil.getAllEntities(this, Plugin.class);
		List<String> collect = plugins.stream().map(e -> e.getPluginName()).distinct().collect(Collectors.toList());
		return collect;
	}

	@Override
	public void setEntityManager(EntityManager manager) {
		this.manager = manager;
	}

	@Override
	public void setFileStorage(FileStorage fileStorage) {
		this.fileStorage = fileStorage;

	}

	@Override
	public FileStorage getFileStorage() {
		return this.fileStorage;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return this.factory;
	}

	@Override
	public EntityManager getEntityManager() {
		return this.manager;
	}

	@Override
    public List<AnalysisJob> getAnalysisJobs(RunStateChoice runStateChoice) {
	    if (ALL_JOBS == runStateChoice || runStateChoice.isEmpty()) {
	        return getAllAnalysisJobs();
	    }

	    List<AnalysisJob> result = KDCDbUtil.getEntityByColumnNameMatch(
	            manager,
	            AnalysisJob.ANALYSIS_JOB_RUNSTATE,
	            AnalysisJob.class,
	            runStateChoice.getRunStateNames());
	    return result;
	}

	@Override
	public List<AnalysisJob> getAllAnalysisJobs() {
		List<AnalysisJob> analysisJobs = KDCDbUtil.getAllEntities(this, AnalysisJob.class);
		return analysisJobs;
	}

	@Override
	public String getDatabaseVersion() {
		String versionNumber = KDCDbUtil.checkDatabaseVersionIfExist(this);
		return versionNumber;
	}

	@Override
	public void setJobQueue(JobQueue jobQue) {

	}

	@Override
	public List<User> getUsers() {
		List<User> users = KDCDbUtil.getAllEntities(this, User.class);
		return Collections.unmodifiableList(users);
	}

	@Override
	@Transactional
	public void addUser(User user, UserRole userRole) {
		persist(userRole);
		persist(user);
	}

	@Transactional
	@Override
	public void removeJobsOfPlugin(Plugin plugin) {
		if (plugin == null) {
			return;
		}
		deleteJobsOfPluginResponseList.forEach(e -> e.accept(plugin)); // Any
		// 3rd
		// party
		// db
		// entries

		List<AnalysisJob> analysisJobs = KDCDbUtil.getAllEntities(this, AnalysisJob.class);
		for (AnalysisJob analysisJob : analysisJobs) {
			if (analysisJob.getAnalysisRequest().getAlgorithms().contains(plugin)) {
				remove(analysisJob);
			}
		}

		List<RunBinding> runBindings = KDCDbUtil.getAllEntities(this, RunBinding.class);
		for (RunBinding runBinding : runBindings) {
			if (runBinding.getPlugin().equals(plugin)) {
				remove(runBinding);
			}
		}

		List<AnalysisRequest> analysisRequests = KDCDbUtil.getAllEntities(this, AnalysisRequest.class);
		for (AnalysisRequest analysisRequest : analysisRequests) {
			if (analysisRequest.getAlgorithms().contains(plugin)) {
				remove(analysisRequest);
			}
		}
	}

	// private Set<String> adminRoleNames = new HashSet<>(
	// Arrays.asList("ADMIN", UserRoleSpecification.ROLE_ADMIN.name())
	// );

	@Override
	public boolean isUserAnAdmin(String loggedInUser) {
		List<User> users = getUsers();
		Optional<User> filter = users.stream().filter(u -> u.getUserName().equals(loggedInUser)).findFirst();
		if (filter.isPresent()) {
			User user = filter.get();
			String userRoleSpecString = user.getUserRole().getUserRoleSpecString();
			return UserRoleSpecification.ROLE_ADMIN.name().equals(userRoleSpecString);
		}
		return false;
	}

	@Override
	public void addDeleteJobsOfPluginResponseList(Consumer<Plugin> c) {
		deleteJobsOfPluginResponseList.add(c);
	}

	@Override
	public boolean supportsVisitListOfEntities() {
		return false;
	}

	@Override
	public <T> Optional<T> visitListOfEntities(Class<? extends T> tclass, Predicate<T> predicate) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Plugin findHighestVersionPlugin(String pluginName) {
		List<Plugin> plugins = KDCDbUtil.getEntityByColumnNameMatch(manager, Plugin.PLUGIN_NAME, Plugin.class,
				pluginName);

		plugins.sort(REVERSE_VERSION_NUMBER);

		// There can onlu be one, unique field plugin Name.
		if (plugins != null && plugins.size() > 0) {
			return plugins.get(0);
		}

		return null;
	}

	@Override
	public Plugin findPluginFromPluginNameVersion(PluginNameVersion pluginNameVersion) {
		List<Plugin> plugins = KDCDbUtil.getEntityByColumnNameMatch(manager, Plugin.PLUGIN_NAME, Plugin.class,
				pluginNameVersion.getPluginName());

		if (plugins != null && plugins.size() > 0) {
			if (pluginNameVersion.getPluginVersion() == null) {
				Plugin plugin = plugins.get(plugins.size() - 1);
				return plugin;
			} else {
				return plugins.stream().filter(e -> e.getVersion().equals(pluginNameVersion.getPluginVersion()))
						.findAny().get();
			}
		}

		return null;
	}

	//@Override
	public void visitFoldersOfAnalysisJobs(Predicate<AnalysisJob> predicate,
			Consumer<Either<Exception, File>> visitor) {
		getAllAnalysisJobs().stream().filter(predicate).forEach(a -> {
			File f;
			try {
				f = a.getResultsFolder();
				visitor.accept(Either.right(f));
			} catch (IOException e) {
				visitor.accept(Either.left(e));
			}
		});

	}

	@Override
	@Transactional
	public void removeAnalysisJob(Long jobid) throws JobNotFoundException, IOException, JobStillActiveException {
		AnalysisJob byId = getById(AnalysisJob.class, jobid);
		if (byId == null) {
			throw new JobNotFoundException();
		} else {
			byId.eraseResults();
		}
		remove(byId);
	}

	@Override
	@Transactional
	public <T> List<T> getEntityByColumnNameMatch(String columnNameSearch, Class<T> clazz, Object value) {
		return KDCDbUtil.getEntityByColumnNameMatch(manager, columnNameSearch, clazz, value);
	}

	@Override
	public void addCallScript(CallScript callScript) {
		for (String ext : callScript.getSupportedExtensions()) {
			this.callScripts.put(ext, callScript);
		}
	}

	@Override
	public CallScript getCallScriptForPlugin(Plugin plugin) throws ScriptNotSupportedException {
		String scriptTemplateFilename = plugin.getScriptTemplateFilename();
		String tmplExt = FilenameUtils.getExtension(scriptTemplateFilename);
		if (!tmplExt.equals(CallScript.TMPL)) {
			throw new ScriptNotSupportedException(
					"Illformed Script template file. Expected '.tmpl' on the end of " + scriptTemplateFilename);
		}
		String extension = FilenameUtils.getExtension(scriptTemplateFilename.replace("." + CallScript.TMPL, ""));
		CallScript callScript = callScripts.get(extension);
		if (callScript == null) {
			throw new ScriptNotSupportedException("Plugin script extension " + extension + " not supported");
		}
		return callScript;
	}

	@Override
	public Map<String, CallScript> getCallScriptMap() {
		return callScripts;
	}

}
