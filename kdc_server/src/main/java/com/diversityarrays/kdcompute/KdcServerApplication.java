/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.DispatcherServlet;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.cleaners.PeriodicRemover;
import com.diversityarrays.kdcompute.cleaners.PeriodicZipper;
import com.diversityarrays.kdcompute.configuration.Configuration;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginGroup;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.db.User;
import com.diversityarrays.kdcompute.db.UserRole;
import com.diversityarrays.kdcompute.elfinder.ElfinderGetMappedDirectories;
import com.diversityarrays.kdcompute.runtime.AnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.BasicJobQueue;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.EntityStore.RunStateChoice;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.SimpleAnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.SimpleFileStorage;
import com.diversityarrays.kdcompute.runtime.UserRoleSpecification;
import com.diversityarrays.kdcompute.runtime.UsersStore;
import com.diversityarrays.kdcompute.runtime.jobscheduler.BashCallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobScheduler;
import com.diversityarrays.kdcompute.runtime.jobscheduler.SimpleJobScheduler;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.SystemVariable;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;
import com.diversityarrays.kdcompute.util.UtilServer;
import com.diversityarrays.kdcompute.util.VersionUtils;
import com.diversityarrays.kdcompute.util.ZipUtils;

import cn.bluejoe.elfinder.servlet.ConnectorServlet;

@SpringBootApplication
@Component
@Service
@Configurable
public class KdcServerApplication extends SpringBootServletInitializer implements EntitiesService {

	private static final String KDCOMPUTE_N_CONCURRENT_JOBS = "kdcompute.nConcurrentJobs";

	private static final String KDCOMPUTE_WORKING_DIRECTORY_AUTOCREATE = "kdcompute.working-directory.autocreate";

	static final String KDCOMPUTE_WORKING_DIRECTORY = "kdcompute.working-directory";

	private static final String KD_COMPUTE_PLUGINS = "KDCompute_Plugins";

	private static final String TEST_OUTPUT_USER_DIRS = "TestOutput_UserDirs";

	private static final ConnectorServlet servlet_connector = new ConnectorServlet();

	static private final String CLASS_NAME = KdcServerApplication.class.getName();

	public static final String DEVELOPMENT_STAGE = "beta";

	@Override
	public final String getVersion() {
		return Long.toString(VersionUtils.MAJOR_VERSION) + "." + Long.toString(VersionUtils.MINOR_VERSION) + "."
				+ Long.toString(VersionUtils.REVISION) + "." + DEVELOPMENT_STAGE;
	}

	public static ConnectorServlet getServletConnector() {
		return servlet_connector;
	}

	public static boolean using_database = true;

	public static boolean USE_GROOVY_PLUGIN_SCRIPTS = true;

	private EntityStore entityStore;
	private UsersStore usersStore;
	private FileStorage fileStorage;
	private JobQueue jobQueue;
	private AnalysisJobFactory analysisJobFactory;
	// private JobProcessor jobProcessor;
	private JobScheduler jobScheduler;

	@Override
	public JobScheduler getJobScheduler() {
		return jobScheduler;
	}

	@Autowired
	private void datasourceUrl(@Value("${spring.datasource.url}") String url) {
		Logger.getLogger(KdcServerApplication.class.getName()).log(Level.INFO, "Database url configured as: " + url);
	}

	private AdditionalVariables additionalVariables = new AdditionalVariables() {

		@Override
		public Map<String, String> instantiateAdditionalKDComputeSystemVariables(String username)
				throws UserIsNotLoggedInException {
			return new HashMap<>();
		}

		@Override
		public List<SystemVariable> getAdditionalKDComputeSystemVariablesScope() {
			return new ArrayList<>();
		}

		@Override
		public Map<String, String> instantiateEnvironmentVariables(String username) throws UserIsNotLoggedInException {
			return new HashMap<>();
		}
	};

	@Autowired(required = false)
	public void setAdditionalVariables(AdditionalVariables additionalVariables) {
		this.additionalVariables = additionalVariables;
	}

	@Autowired
	@Value("${kdcompute.results.zipAfterSecondsSinceAccess}")
	private Long zipResultsAfterSecondsSinceAccess;

	private Thread zippingThread;

	@Autowired
	@Value("${kdcompute.results.deleteAfterSecondsSinceAccess}")
	private Long deleteResultsAfterSecondsSinceAccess;

	private Thread deleteThread;

	private String workingDirectory;

	private Integer nConcurrentJobs;

	@Autowired
	public KdcServerApplication(@Value("${" + KDCOMPUTE_WORKING_DIRECTORY + "}") String workingDirectory,
			@Value("${" + KDCOMPUTE_WORKING_DIRECTORY_AUTOCREATE + "}") Boolean autocreate,
			@Value("${" + KDCOMPUTE_N_CONCURRENT_JOBS + "}") Integer nConcurrentJobs) throws Exception {
		if (workingDirectory != null && !workingDirectory.isEmpty()) {
			this.workingDirectory = workingDirectory;
		} else {
			throw new RuntimeException("Working directory not set, please configure '" + KDCOMPUTE_WORKING_DIRECTORY
					+ "' in applicaiton.properties");
		}

		this.nConcurrentJobs = nConcurrentJobs;

		File workingDirectoryFile = new File(workingDirectory);

		if (autocreate && !workingDirectoryFile.isDirectory()) {
			if (!workingDirectoryFile.mkdirs()) {
				throw new RuntimeException("Failed to create directory " + workingDirectoryFile);
			}
		}

		if (!workingDirectoryFile.isDirectory()) {
			throw new RuntimeException("Working directory '" + workingDirectory + "' does not exist!");
		}

		// Setup groovy
		File groovyFolder = new File(workingDirectory, "groovy-2.4.7");
		if (!groovyFolder.exists()) {

			groovyFolder.mkdirs();
			try {
				File targetFile = UtilServer.ExportResource(KdcServerApplication.class, "groovy-2.4.7.zip",
						workingDirectoryFile);
				ZipUtils.unzip(targetFile, groovyFolder);
				targetFile.delete();
			} catch (Exception e) {
				throw new RuntimeException("Failed to export resource groovy-2.4.7.zip to " + workingDirectoryFile, e);
			}
		}
		Arrays.asList(new File(groovyFolder, "bin").listFiles()).stream().forEach(e -> e.setExecutable(true));

		singleton = this;
	}

	public KdcServerApplication() {
		super();
		singleton = this;
	}

	public static KdcServerStartupFrame kdcServerStartupFrame;

	public static void main(String[] args) {

		boolean isHeadless = GraphicsEnvironment.isHeadless();

		// if (!isHeadless) {
		// JFrame frame = new JFrame("");
		// JFolderChooser folderChooser = new JFolderChooser("Select folder");
		// frame.addWindowListener(new WindowAdapter() {
		// @Override
		// public void windowClosing(WindowEvent e) {
		// System.exit(0);
		// }
		// });
		// frame.getContentPane().add(folderChooser, "Center");
		// frame.setSize(folderChooser.getPreferredSize());
		// frame.setVisible(true);
		//
		// File folder = folderChooser.getPath();
		// System.err.println(folder);
		// }

		final SpringApplication application = createSpringApplication();

		if (!isHeadless) {
			IntFunction<Exception> startServer = new IntFunction<Exception>() {
				@Override
				public Exception apply(int portNumber) {
					Exception result = null;
					System.setProperty("server.port", Integer.toString(portNumber));
					try {
						application.run(args);
					} catch (Exception error) {
						result = error;
					}
					return result;
				}
			};
			KdcServerApplication.kdcServerStartupFrame = new KdcServerStartupFrame(startServer);
			KdcServerApplication.kdcServerStartupFrame.setLocationRelativeTo(null);
			KdcServerApplication.kdcServerStartupFrame.setVisible(true);
		} else {
			// System is headless
			Logger.getLogger(CLASS_NAME).info("Using headless");

			switch (args.length) {
			case 0:
				// System.getProperties().put( "server.port", 0);
				break;
			case 1:
				System.getProperties().put("server.port", args[0]);
				break;
			default:
				System.out.println("Usage: java -jar <Program>");
				System.out.println("       java -jar <Program> [Port]");
				break;
			}

			Thread startSpring = new Thread() {

				@Override
				public void run() {
					application.setHeadless(isHeadless);
					application.run(args);
				}

			};
			startSpring.start(); // Thread to allow autowired to initialize and
		}
	}

	private static SpringApplication createSpringApplication() {
		return new SpringApplication(new Object[] { KdcServerApplication.class, Configuration.class });
	}

	private static KdcServerApplication singleton;

	public static KdcServerApplication getInstance() {
		return singleton;
	}

	@Override
	public UsersStore getUsersStore() {
		return usersStore;
	}

	@Autowired
	public void setEntityStore(EntityStore store) {
		this.entityStore = store;
		init();
	}

	@Autowired
	public void setUsersStore(UsersStore usersStore) {
		this.usersStore = usersStore;
	}

	@Override
	public EntityStore getEntityStore() {
		return entityStore;
	}

	@Override
	public FileStorage getFileStorage() {
		return fileStorage;
	}

	@Override
	public JobQueue getJobQueue() {
		return jobQueue;
	}

	@Override
	public AnalysisJobFactory getAnalysisJobFactory() {
		return analysisJobFactory;
	}

	private void init() {

		if (workingDirectory == null || workingDirectory.isEmpty()) {
			throw new RuntimeException(
					"kdcompute.working-directory not provided. Please set in application.properties");
		}

		File workingDirectoryFile = new File(workingDirectory);
		boolean fileStorageDirExists = workingDirectoryFile.isDirectory();
		if (!fileStorageDirExists) {
			workingDirectoryFile.mkdirs();
		}
		if (!fileStorageDirExists) {
			Logger.getLogger(CLASS_NAME).info("Created new working directory:" + workingDirectoryFile.getPath());
		}
		Logger.getLogger(CLASS_NAME).info("Base directory configured to:" + workingDirectoryFile.getPath());

		List<String> dirs = new ArrayList<>();
		dirs.addAll(Arrays.asList(TEST_OUTPUT_USER_DIRS, KD_COMPUTE_PLUGINS));
		dirs.addAll(Arrays.asList(SimpleFileStorage.DROPIN_DIRNAME, SimpleFileStorage.PLUGINS_DIRNAME).stream()
				.map(e -> KD_COMPUTE_PLUGINS + File.separator + e).collect(Collectors.toList()));
		dirs.forEach(p -> {
			File dirFile = new File(workingDirectoryFile, p);
			boolean dirFileExists = dirFile.isDirectory();
			if (!dirFileExists) {
				dirFile.mkdirs();
			}
			if (!dirFileExists) {
				Logger.getLogger(CLASS_NAME).info("Created new directory:" + dirFile.getPath());
			}
			throwErrorIfDirectoryNotAvailable(dirFile);
		});

		File resultsDir = new File(workingDirectoryFile, TEST_OUTPUT_USER_DIRS);
		File kdcomputePlugins = new File(workingDirectoryFile, KD_COMPUTE_PLUGINS);

		fileStorage = new SimpleFileStorage();
		fileStorage.setResultsRootDir(resultsDir);
		fileStorage.setKdcomputePluginsDir(kdcomputePlugins);
		Logger.getLogger(CLASS_NAME).info("Initialized FileStorage");

		entityStore.setFileStorage(fileStorage);
		Logger.getLogger(CLASS_NAME).info("Initialized EntityStore");

		BasicJobQueue djobQueue = new BasicJobQueue();
		djobQueue.setEntityStore(entityStore);

		// FIXME Bidirectional inclusion, fix this up after release 1
		entityStore.setJobQueue(djobQueue);
		jobQueue = djobQueue;

		Logger.getLogger(CLASS_NAME).info("Initialized JobQueue");

		entityStore.addCallScript(new BashCallScript());

		SimpleJobScheduler simpleJobScheduler = nConcurrentJobs != null && nConcurrentJobs >= 1
				? new SimpleJobScheduler(djobQueue, additionalVariables, nConcurrentJobs)
				: new SimpleJobScheduler(djobQueue, additionalVariables);
		simpleJobScheduler.setCallScriptMap(entityStore.getCallScriptMap());
		jobScheduler = simpleJobScheduler;

		// jobScheduler = new SlurmJobScheduler(djobQueue);
		jobScheduler.start();
		Logger.getLogger(CLASS_NAME).info("Initialized and started JobScheduler");

		SimpleAnalysisJobFactory jobFactory = new SimpleAnalysisJobFactory();
		jobFactory.setFileStorage(fileStorage);

		analysisJobFactory = jobFactory;
		Logger.getLogger(CLASS_NAME).info("Initialized JobFactory");

		Logger.getLogger(CLASS_NAME).info("Loaded Plugins");

		/*
		 * Setup elfinder
		 */
		Map<String, ElfinderGetMappedDirectories> page2IDFileMap = new HashMap<>();
		page2IDFileMap.put("/jobsummary", new ElfinderGetMappedDirectories() {

			@Override
			public Map<String, File> getDirMapping(String urlSuffix, Principal principal,
					Map<String, String[]> paramMap) throws NumberFormatException, IOException {
				Map<String, File> map = new HashMap<>();
				String s = urlSuffix.split("/")[3]; // 8 in /jobsummary/dev/8
				File file = KdcServerApplication.getInstance().getEntityStore()
						.getById(AnalysisJob.class, Long.parseLong(s)).getResultsFolder();
				map.put(s, file);
				return map;
			}
		});
		page2IDFileMap.put("/connector", new ElfinderGetMappedDirectories() {

			@Override
			public Map<String, File> getDirMapping(String urlSuffix, Principal principal,
					Map<String, String[]> paramMap) {
				return getUserDirMapping(principal);
			}

			private Map<String, File> getUserDirMapping(Principal principal) {
				try {
					Map<String, File> map = new HashMap<>();
					map.put("My_Files", KdcServerApplication.getUserDir(principal));
					File shared = KdcServerApplication.getInstance().getFileStorage()
							.getUserDir(FilemanagerController.SHARED_FOLDER);
					shared.mkdirs();
					map.put("Shared", shared);

					if (UtilServer.isUserAdmin(principal)) {
						List<String> users = getUsersStore().getUsers().stream()
								.filter(e -> !e.equals(principal.getName())).collect(Collectors.toList());
						List<File> allUserFolders = Arrays.asList(
								KdcServerApplication.getInstance().getFileStorage().getUserStorage().listFiles())
								.stream().collect(Collectors.toList());
						for (String user : users) {

							String userPrefixed = "user_" + user;
							if (!map.containsKey(userPrefixed)) {
								Optional<File> findAny = allUserFolders.stream().filter(e -> e.getName().contains(user))
										.findAny();
								if (findAny.isPresent()) {
									map.put(userPrefixed, findAny.get());
								}
							}
						}

					}

					return map;
				} catch (IOException e) {
					e.printStackTrace();
				}
				return new HashMap<>();
			}
		});
		page2IDFileMap.put("/admin/plugintest", new ElfinderGetMappedDirectories() {

			@Override
			public Map<String, File> getDirMapping(String urlSuffix, Principal principal,
					Map<String, String[]> paramMap) {

				Map<String, File> map = new HashMap<>();

				File shared = new File(paramMap.get("dirPath")[0]);
				if (shared.getPath().contains(KDCFileUtils.getExamplesDir().getParent())) {
					map.put("Test_Output", shared);
				}

				// map.put(FilemanagerController.SHARED_FOLDER,shared);

				return map;
			}
		});

		ElfinderConnector.initialize(page2IDFileMap);

		/*
		 * Ensure admin user is available
		 */
		final String adminUser = "admin";
		if (!entityStore.getUsers().stream().anyMatch(e -> {
			return e.getUserName().equals(adminUser);
		})) {

			UserRole userRole = new UserRole();
			userRole.setUserRole(UserRoleSpecification.ROLE_ADMIN);
			userRole.setUserName(adminUser);
			userRole.setUserRoleSpecString(userRole.getUserRole().name());

			User user = new User();
			user.setUserName(userRole.getUserName());
			user.setPassword(WebSecurityConfig.passwordEncoder().encode("password"));
			user.setUserRole(userRole);
			user.setEnabled(true);

			entityStore.addUser(user, userRole);
		}

		/*
		 * Creating a zipping daemon to save space
		 */
		Function<RunStateChoice, Consumer<Predicate<File>>> visitResultsFolderWithPredicate = new Function<RunStateChoice, Consumer<Predicate<File>>>() {

			@Override
			public Consumer<Predicate<File>> apply(RunStateChoice rsc) {
				return new Consumer<Predicate<File>>() {
					@Override
					public void accept(Predicate<File> t) {
						KdcServerApplication.getInstance().getEntityStore().getAnalysisJobs(rsc).stream()
								.forEach(job -> t.test(new File(job.getResultsFolderPath())));
					}

				};
			}
		};
		if (zipResultsAfterSecondsSinceAccess > 0) {
			zippingThread = new Thread(new PeriodicZipper(visitResultsFolderWithPredicate,
					Duration.ofSeconds(zipResultsAfterSecondsSinceAccess)));
			zippingThread.start();
		}

		/*
		 * Creating a removal daemon to save space
		 */
		Function<RunStateChoice, Consumer<Predicate<File>>> visitRemovalResultsFolderWithPredicate = new Function<RunStateChoice, Consumer<Predicate<File>>>() {
			@Override
			public Consumer<Predicate<File>> apply(RunStateChoice rsc) {
				return new Consumer<Predicate<File>>() {
					@Override
					public void accept(Predicate<File> remover) {
						KdcServerApplication.getInstance().getEntityStore().getAnalysisJobs(rsc).stream()
								.forEach(job -> {
									boolean erased = false;

									File resultsFolder = new File(job.getResultsFolderPath());
									if (remover.test(AnalysisJob.toPlainFile(resultsFolder))) {
										erased = true;
									} else if (remover.test(AnalysisJob.toZipFile(resultsFolder))) {
										erased = true;
									}

									if (erased) {
										job.setRunState(RunState.ERASED);
										KdcServerApplication.getInstance().getEntityStore().merge(job);
									}
								});
					}

				};
			}

		};
		if (deleteResultsAfterSecondsSinceAccess > 0) {
			PeriodicRemover target = new PeriodicRemover(visitRemovalResultsFolderWithPredicate,
					Duration.ofSeconds(deleteResultsAfterSecondsSinceAccess));
			deleteThread = new Thread(target);
			deleteThread.start();
		}
	}

	public void throwErrorIfDirectoryNotAvailable(File dir) {
		if (!dir.isDirectory()) {
			// TODO should probably be mkdirs()
			if (!dir.mkdir()) {
				throw new RuntimeException("Unable to create " + dir.getPath());
			}
		}
	}

	public static void removeNormalPlugin(EntityStore entityStore, Plugin plugin) {

		for (PluginGroup pluginGroup : entityStore.getPluginGroups()) {
			List<Plugin> plugins2 = pluginGroup.getPlugins();
			if (plugins2.contains(plugin)) {
				pluginGroup.removePlugin(plugin);
				entityStore.merge(pluginGroup);
			}
		}
		entityStore.remove(plugin);
	}

	public Plugin getPluginFromId(Long id) {

		Plugin alg = null;

		if (id == null || id < 0) {
			return alg;
		}

		return entityStore.getById(Plugin.class, id);
	}

	@Override
	protected void finalize() throws Throwable {
		// EventBus.getDefault().unregister(this);
		super.finalize();
	}

	@Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration3() {
		return new ServletRegistrationBean(servlet_connector, "/elfinder-servlet/*");
	}

	public AdditionalVariables getAdditionalVariables() {
		if (additionalVariables == null) {
			return AdditionalVariables.emptyAdditionalVariables();
		} else {
			return additionalVariables;
		}

	}

	public static File getUserDir(Principal principal) throws NullPointerException, IOException {
		return KdcServerApplication.getInstance().getFileStorage().getUserDir(getInstance().getUserId(principal));
	}

	public static File getUserTmpDir(Principal principal) throws NullPointerException, IOException {
		return KdcServerApplication.getInstance().getFileStorage().getUserTempDir(getInstance().getUserId(principal));
	}

	public String getUserId(Principal principal) throws NullPointerException {
		if (principal == null) {
			throw new NullPointerException("Principle is null");
		}
		if (principal instanceof OAuth2Authentication) {
			OAuth2Authentication oauthentication = (OAuth2Authentication) principal;
			return String.join(".", Arrays.asList(oauthentication.getOAuth2Request().getClientId(),
					FileStorage.makeFileSafe(principal.getName())));
		} else if (principal instanceof UsernamePasswordAuthenticationToken) {
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) principal;
			if (usernamePasswordAuthenticationToken.getPrincipal() instanceof LdapUserDetailsImpl) {
				return String.join(".", Arrays.asList("ldap", FileStorage.makeFileSafe(principal.getName())));
			} else {
				return String.join(".", Arrays.asList("server", FileStorage.makeFileSafe(principal.getName())));
			}
		} else {
			return String.join(".", Arrays.asList("unknown", FileStorage.makeFileSafe(principal.getName())));
		}
	}

}
