/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.diversityarrays.kdcompute.util.Either;
import com.diversityarrays.kdcompute.util.MsgBox;
import com.diversityarrays.kdcompute.util.UtilServer;
import com.diversityarrays.panels.Errors;
import com.diversityarrays.swingutils.SwingUtils;
import com.diversityarrays.ui.Toast;

import net.pearcan.ui.GuiUtil;
import net.pearcan.ui.desktop.MacApplication;
import net.pearcan.ui.desktop.MacApplicationException;
import net.pearcan.util.StringUtil;
import net.pearcan.util.Util;

public class KdcServerStartupFrame extends JFrame {

	private final Action browseWorkspaceAction = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Desktop desktop = Desktop.getDesktop();
				desktop.open(KdcServerApplication.getInstance().getFileStorage().getDropinPluginsDir());
			} catch (IOException e1) {
				Errors.exceptionWrapper(e1);
			}
		}
	};

	private final String KDCOMPUTE_PNG = "KDCompute_with_text.png";
	private final String KDTUB_PNG = "kdtub.png";
	private static final int NUMBER_OF_PORTS_TO_OFFER = 4;
	private static final String[] INTRO_LINES = new String[] {};
	private final JButton openDropinsDir = new JButton("Open dropins directory");
	private final IntFunction<Exception> startServer;

	private JComboBox<Integer> portNumberCombo = new JComboBox<>();

	private MacApplication macapp = null;

	private Image getImage(String resourceName) {
		Image result = null;
		InputStream is = getClass().getResourceAsStream(resourceName);
		if (is != null) {
			try {
				result = ImageIO.read(is);
			} catch (IOException e) {
				Logger.getLogger(getClass()).warn("Missing resource " + resourceName);
			}
		}
		return result;
	}

	public KdcServerStartupFrame(IntFunction<Exception> startServer) {

		openDropinsDir.setAction(browseWorkspaceAction);
		openDropinsDir.setEnabled(true);

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.startServer = startServer;

		Image img = getImage(KDTUB_PNG);
		if (img != null) {
			setIconImage(img);
			if (Util.isMacOS()) {
				try {
					System.setProperty("apple.laf.useScreenMenuBar", "true"); //$NON-NLS-1$ //$NON-NLS-2$
					macapp = new MacApplication(null);
					// macapp.setAboutHandler(aboutAction);
					macapp.setQuitHandler(exitAction);
					if (img != null) {
						macapp.setDockIconImage(img);
					}
					// macapp.setPreferencesHandler(settingsAction);
					// macapp.setAboutHandler(aboutAction);
					macapp.setQuitHandler(exitAction);
				} catch (MacApplicationException e) {
					macapp = null;
				}
			}
		}
		showPortSelectionPanel();
	}

	private final Action exitAction = new AbstractAction("Exit") {
		@Override
		public void actionPerformed(ActionEvent e) {
			boolean quit = true;
			if (quit) {
				if (macapp != null) {
					System.out.println(e.getClass());
				}
				System.exit(0);
			}
		}
	};

	private final Action startServerAction = new AbstractAction("Start Server on port ") {
		@Override
		public void actionPerformed(ActionEvent e) {

			String portString = String.valueOf(portNumberCombo.getSelectedItem());
			portString = portString.replaceAll(",", "");

			int portNumber = Integer.parseInt(portString);

			showStartingPanel();

			new Thread(() -> {
				Exception error = startServer.apply(portNumber);
				if (error == null) {
					portNumberUsed = UtilServer.getPortServerIsListenerTo();
					showStopPanel();
				} else {
					error.printStackTrace();
					showErrorDetails(error);
				}
			}).start();
		}
	};

	private int portNumberUsed;
	private final Action openLinkAction = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Desktop.isDesktopSupported()) {
				String linkUrl = "http://localhost:" + portNumberUsed;
				try {
					Desktop.getDesktop().browse(new URL(linkUrl).toURI());
				} catch (Exception e1) {
					MsgBox.error(KdcServerStartupFrame.this, e1, "Error Browsing " + linkUrl);
				}
			}
		}
	};

	private final Action stopServerAction = new AbstractAction("Stop Server") {
		@Override
		public void actionPerformed(ActionEvent e) {
			shutdown();
		}
	};

	private void showStopPanel() {

		setTitle("Stop Server");

		JButton linkButton = new JButton(openLinkAction);

		StringBuilder builder = new StringBuilder("<HTML>Click to open web browser<BR>");
		builder.append("<FONT color=\"#000099\"><U>").append("http://localhost:").append(portNumberUsed)
				.append("</FONT>");
		linkButton.setText(builder.toString());

		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalGlue());
		box.add(linkButton, BorderLayout.CENTER);
		box.add(Box.createVerticalGlue());

		StringBuilder builderOpen = new StringBuilder("<HTML>Open Dropins folder<BR>");
		openDropinsDir.setText(builderOpen.toString());

		box.add(openDropinsDir, BorderLayout.CENTER);
		box.add(Box.createVerticalGlue());

		replaceContentPane(box, new JButton(stopServerAction));

		File dropinPluginsDir = KdcServerApplication.getInstance().getFileStorage().getDropinPluginsDir();
		if (KdcServerApplication.getInstance().getFileStorage().getPluginsDir().listFiles().length == 0
				&& dropinPluginsDir.listFiles().length == 0) {
			SwingUtils.notify(this, "Add new plugins",
					"New plugins can be added into the dropins folder, found on the server console, "
							+ "and installed via the admin panel");
		}
	}

	private void shutdown() {
		dispose(); // should stop the server
		System.exit(0);
	}

	private void showErrorDetails(Exception error) {

		setTitle("Error Starting Server");

		JTextPane textPane = new JTextPane();
		textPane.setContentType("text/html");
		textPane.setText(createErrorDetails(error));

		JButton closer = new JButton("Close");
		closer.addActionListener((e) -> shutdown());

		Box btns = Box.createHorizontalBox();
		btns.add(Box.createHorizontalStrut(10));
		btns.add(closer);
		btns.add(Box.createHorizontalGlue());

		replaceContentPane(new JScrollPane(textPane), btns);
	}

	private String createErrorDetails(Exception error) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		error.printStackTrace(pw);
		pw.close();
		StringBuilder sb = new StringBuilder("<HTML>");
		sb.append("<b>Server Start Error Details</b>");
		sb.append("<hr><pre>");
		sb.append(StringUtil.htmlEscape(sw.toString()));
		sb.append("</pre>");
		String errorDetails = sb.toString();
		return errorDetails;
	}

	private void replaceContentPane(JComponent centre, JComponent south) {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(getKDcomputeImageLabel(), BorderLayout.NORTH);
		if (centre != null)
			panel.add(centre, BorderLayout.CENTER);
		if (south != null)
			panel.add(south, BorderLayout.SOUTH);
		setContentPane(panel);
		packAndSize();
	}

	private void packAndSize() {
		pack();
		setSize(640, 480);
		// setSize(800, 600);
		revalidate();

	}

	private Either<IOException, Optional<String>> getApplicationPropertiesContent() {
		String result = null;
		InputStream is = getClass().getResourceAsStream("/application.properties");
		if (is != null) {
			try {
				result = IOUtils.readLines(is, "UTF-8").stream().collect(Collectors.joining("\n"));
			} catch (IOException e) {
				return Either.left(e);
			}
		}
		return Either.right(Optional.ofNullable(result));
	}

	private void showPortSelectionPanel() {

		portNumberCombo.setModel(getAvailablePorts());
		Box btns = Box.createHorizontalBox();
		btns.add(Box.createHorizontalGlue());
		btns.add(new JButton(startServerAction));
		btns.add(portNumberCombo);
		btns.add(Box.createHorizontalGlue());

		replaceContentPane(createIntroPanel(), btns);
	}

	private JComponent createIntroPanel() {

		JTextArea ta = new JTextArea();
		ta.setEditable(false);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);

		JButton copyButton = null;
		String heading;
		if (INTRO_LINES.length > 0) {
			heading = "Startup Instructions";

			ta.setText(Arrays.asList(INTRO_LINES).stream().collect(Collectors.joining("\n")));
		} else {
			heading = "<HTML>Default <I>application.properties</I>";

			Either<IOException, Optional<String>> either = getApplicationPropertiesContent();
			if (either.isRight()) {
				Optional<String> content = either.right();
				if (content.isPresent()) {
					ta.setText(content.get());
					copyButton = new JButton("Copy to Clipboard");
					copyButton.setToolTipText("copy to clipboard");
					final JButton cb = copyButton;
					copyButton.addActionListener((e) -> {
						Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(ta.getText()),
								null);
						new Toast(cb, "Content copied to clipboard", Toast.SHORT).show();
					});
				} else {
					ta.setText("Missing default application.properties");
				}
			} else {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				either.left().printStackTrace(pw);
				pw.close();
				ta.setText(sw.toString());
			}
		}

		JPanel main = new JPanel(new BorderLayout());
		main.setBorder(new EmptyBorder(2, 2, 2, 2));
		main.add(GuiUtil.createLabelSeparator(heading, copyButton), BorderLayout.NORTH);
		main.add(new JScrollPane(ta), BorderLayout.CENTER);
		return main;
	}

	private ComboBoxModel<Integer> getAvailablePorts() {

		List<Integer> options = new ArrayList<>();
		ServerSocket tempServerSocket = null;

		List<ServerSocket> socketsToClose = new ArrayList<>();
		try {
			for (;;) {
				try {
					tempServerSocket = new ServerSocket(0);
					options.add(tempServerSocket.getLocalPort());
					socketsToClose.add(tempServerSocket);
					if (options.size() >= NUMBER_OF_PORTS_TO_OFFER) {
						break;
					}
				} catch (IOException ignore) {
					// Don't use it then!
				}
			}
		} finally {
			// Close them all now
			for (ServerSocket socket : socketsToClose) {
				try {
					socket.close();
				} catch (IOException ignore) {
				}
			}
		}

		return new DefaultComboBoxModel<>(options.toArray(new Integer[options.size()]));
	}

	private JLabel imageLabel;

	private JLabel getKDcomputeImageLabel() {
		if (imageLabel == null) {
			Image img = getImage(KDCOMPUTE_PNG);
			if (img == null) {
				imageLabel = new JLabel("<HTML><B>KDCompute</B><BR><i>Data Analysis</i>", JLabel.CENTER);
			} else {
				imageLabel = new JLabel(new ImageIcon(img));
			}
		}
		return imageLabel;
	}

	private void showStartingPanel() {

		setTitle("Starting Server");

		JLabel label = new JLabel("Starting Server", JLabel.CENTER);
		JProgressBar startServerProgressBar = new JProgressBar();
		startServerProgressBar.setIndeterminate(true);

		replaceContentPane(label, startServerProgressBar);
	}
}
