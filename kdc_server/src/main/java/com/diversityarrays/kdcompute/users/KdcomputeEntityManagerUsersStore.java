/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.users;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;

import com.diversityarrays.kdcompute.WebSecurityConfig;
import com.diversityarrays.kdcompute.db.User;
import com.diversityarrays.kdcompute.db.UserRole;
import com.diversityarrays.kdcompute.runtime.UserRoleSpecification;
import com.diversityarrays.kdcompute.runtime.UsersStore;
import com.diversityarrays.kdcompute.userstore.UserDoesNotExistException;
import com.diversityarrays.kdcompute.userstore.UserExistsException;
import com.diversityarrays.kdcompute.util.KdcomputeEntityManagerEntityStore;

public class KdcomputeEntityManagerUsersStore implements UsersStore {

	KdcomputeEntityManagerEntityStore entityManagerEntityStore;

	public KdcomputeEntityManagerUsersStore(KdcomputeEntityManagerEntityStore entityManagerEntityStore) {
		super();
		this.entityManagerEntityStore = entityManagerEntityStore;
	}

	@Transactional
	public void addUser(User user) throws UserExistsException {
		String lookingFor = user.getUserName();
		if (entityManagerEntityStore.getUsers().stream().anyMatch(u -> lookingFor.equals(u.getUserName()))) {
			throw new UserExistsException();
		} else {
			entityManagerEntityStore.persist(user.getUserRole());
			entityManagerEntityStore.persist(user);
		}
	}

	@Override
	@Transactional
	public void deleteUser(String username) throws UserDoesNotExistException {
		// Users[] users = entityManagerEntityStore.getUsers().stream()
		// .filter(u -> u.getUserName().equals(username))
		// .toArray(size -> new Users[size]);

		List<User> users = entityManagerEntityStore.getUsers().stream().filter(u -> u.getUserName().equals(username))
				.collect(Collectors.toList());

		if (!users.isEmpty()) {
			// TODO [BSP] check about the UserRole removal
			users.stream().forEach(u -> {
				entityManagerEntityStore.remove(u.getUserRole());
				entityManagerEntityStore.remove(u);
			});
		} else {
			throw new UserDoesNotExistException();
		}
	}

	@Override
	public boolean registrationEnabled() {
		return true;
	}

	@Override
	@Transactional
	public void addUser(String username, String password, UserRoleSpecification userRoleSpecification)
			throws UserExistsException {
		List<User> users = entityManagerEntityStore.getUsers();
		if (users.stream().filter(u -> u.getUserName().equals(username)).findAny().isPresent()) {
			throw new UserExistsException();
		}

		User user = new User();
		user.setEnabled(true);
		user.setUserName(username);
		user.setPassword(WebSecurityConfig.passwordEncoder().encode(password));

		UserRole userRole = new UserRole();
		userRole.setUserRole(UserRoleSpecification.ROLE_USER);
		userRole.setUserRoleSpecString(UserRoleSpecification.ROLE_USER.name());
		userRole.setUserName(user.getUserName());

		user.setUserRole(userRole);

		entityManagerEntityStore.persist(userRole);
		entityManagerEntityStore.persist(user);
	}

	@Override
	@Transactional
	public List<String> getUsers() {
		return entityManagerEntityStore.getUsers().stream().map(e -> e.getUserName()).collect(Collectors.toList());
	}
}
