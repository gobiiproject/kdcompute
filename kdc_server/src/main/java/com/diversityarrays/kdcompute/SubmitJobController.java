/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.submitjob.SubmitJobConstants;
import com.diversityarrays.kdcompute.submitjob.SubmitJobForPlugin;
import com.diversityarrays.kdcompute.submitjob.SubmitJobResponse;
import com.diversityarrays.kdcompute.util.UtilServer;
import com.google.gson.Gson;

@RestController
public class SubmitJobController {

	@RequestMapping("/submitjob")
	public static String submitJob(Principal principal, @RequestParam Map<String, String> allRequestParams,
			ModelMap modelMap, Model model) throws Exception {
		String loggedInUser = UtilServer.setBannerModel(model, principal);

		String pluginName = allRequestParams.get(SubmitJobConstants.PLUGIN_NAME);
		if (pluginName != null)
			pluginName = pluginName.trim();

		String base_usage = String.format(
				"[SERVERURL]/submitjob?%s=[plugin name,required]&%s=[post completion url,optional]",
				SubmitJobConstants.PLUGIN_NAME, SubmitJobConstants.POSTCOMPLETIONURL);

		SubmitJobResponse result = SubmitJobForPlugin.submitJobForPlugin(allRequestParams, loggedInUser, pluginName,
				base_usage, (job) -> job, // Note: ResponseJob implements
											// SubmitJobResponse
				(analysisJob) -> onFinish(analysisJob), null);
		return new Gson().toJson(result);
	}

	static private void onFinish(AnalysisJob analysisJob) {
		try {
			List<RunBinding> runBindings = analysisJob.getAnalysisRequest().getRunBindings();
			if (runBindings.isEmpty())
				return;
			String postCompletionUrl = runBindings.get(0).getPostCompletionUrl();
			if (postCompletionUrl == null || postCompletionUrl.isEmpty())
				return;

			String postUrl = postCompletionUrl;
			Gson gson = new Gson();
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(postUrl);
			StringEntity postingString = new StringEntity(gson.toJson(analysisJob));
			post.setEntity(postingString);
			post.setHeader("Content-type", "application/json");
			@SuppressWarnings("unused")
			HttpResponse response = httpClient.execute(post);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
