/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedClientException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Configuration
public class Controllers {

	@Value("${ldap.source.name:null}")
	private String ldapSourceName;

	@Value("${ldap.source.image:null}")
	private String ldapSourceImage;

	@Autowired
	ApplicationContext applicationContext;

	private boolean isOauth2Defined(ApplicationContext applicationContext, String name) {
		return applicationContext.containsBean(name);
	}

	@RequestMapping({ "/login" })
	@ResponseBody
	public void query(Principal principal, Model model) {
		throw new UnauthorizedClientException("Authentication required");
	}

	@RequestMapping(path = "/logout", method = RequestMethod.POST)
	public String logout() {
		return "redirect:/login";
	}

	@RequestMapping("/home")
	public String home(Principal principal, Model model) {
		if (principal != null) {
			if (principal instanceof OAuth2Authentication) {
				OAuth2Authentication authentication = (OAuth2Authentication) principal;
				@SuppressWarnings("unchecked")
				Map<String, String> details = (Map<String, String>) authentication.getUserAuthentication().getDetails();
				if (details.containsKey("username")) {
					model.addAttribute("username", details.get("username"));
				} else if (details.containsKey("name")) {
					model.addAttribute("username", details.get("name"));
				}
				if (details.containsKey("email"))
					model.addAttribute("email", details.get("email"));
				if (details.containsKey("picture"))
					model.addAttribute("picture", details.get("picture"));
			} else if (principal instanceof UsernamePasswordAuthenticationToken) {
				// No email available for this one
			}
			String name = principal.getName();
			if (!model.containsAttribute("username") && name != null && !name.isEmpty()) {
				model.addAttribute("username", name);
			}
		}
		return "home";
	}

}
