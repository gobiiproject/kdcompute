/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.plugindeployment.DeployPlugin;
import com.diversityarrays.kdcompute.plugindeployment.DeployPluginNoscript;
import com.diversityarrays.kdcompute.plugindeployment.DeployPluginScript;
import com.diversityarrays.kdcompute.plugindeployment.FailedPluginTestsException;
import com.diversityarrays.kdcompute.plugindeployment.PluginAlreadyExistsInDatabaseException;
import com.diversityarrays.kdcompute.plugindeployment.PluginDeploymentInspector;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.ScriptNotSupportedException;
import com.diversityarrays.kdcompute.runtime.jobscheduler.GroovyCallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.GroovyNotInstalledException;

import thatkow.ansible.AnsiblePlaybookNotFoundException;
import thatkow.ansible.AnsibleScriptException;

@Component
@Configurable
public class PluginsAutowired {

	@Order(1)
	@Autowired
	public void setGroovyCommand(@Value("${groovyCommand}") String groovyCommand,
			@Value("${groovyCommand.linux:}") String groovyCommandLinux,
			@Value("${groovyCommand.windows:}") String groovyCommandWindows,
			@Value("${kdcompute.autodeployplugins}") String autodeployplugins) throws GroovyNotInstalledException,
			InterruptedException, IOException, PluginAlreadyExistsInDatabaseException, FailedPluginTestsException,
			ScriptNotSupportedException, AnsiblePlaybookNotFoundException, AnsibleScriptException {
		final String OS = System.getProperty("os.name").toLowerCase();
		GroovyCallScript groovyCallScript = null;

		if (OS.contains("nix") || OS.contains("nux") || OS.contains("aix") || OS.contains("mac")) {

			if (groovyCommandLinux != null && !groovyCommandLinux.isEmpty()) {
				groovyCallScript = new GroovyCallScript(groovyCommandLinux);
			} else if (groovyCommand != null && !groovyCommand.isEmpty()) {
				groovyCallScript = new GroovyCallScript(groovyCommand);
			} else {
				Logger.getGlobal().log(Level.WARNING,
						"Groovy binary path not provided, plugins using groovy will not be supported. Please set either 'groovyCommand' or 'groovyCommandLinux' in application.properties:");
			}
		} else if (OS.contains("win")) {
			if (groovyCommandWindows != null && !groovyCommandWindows.isEmpty()) {
				groovyCallScript = new GroovyCallScript(groovyCommandWindows);
			} else if (groovyCommand != null && !groovyCommand.isEmpty()) {
				groovyCallScript = new GroovyCallScript(groovyCommand);
			} else {
				Logger.getGlobal().log(Level.WARNING,
						"Groovy binary path not provided, plugins using groovy will not be supported. Please set either 'groovyCommand' or 'groovyCommandWindows' in application.properties:");
			}
		} else {
			Logger.getGlobal().log(Level.SEVERE, "Unsupported OS '" + OS + "', groovy scripts not supported");
		}

		if (groovyCallScript != null) {
			KdcServerApplication.getInstance().getEntityStore().addCallScript(groovyCallScript);
			Logger.getGlobal().log(Level.INFO,
					"Groovy binary added to available call scripts: " + groovyCallScript.getCommand());
		} else {
			Logger.getGlobal().log(Level.SEVERE, "Groovy binary was not found");
		}

		FileStorage fileStorage = KdcServerApplication.getInstance().getFileStorage();
		autodeployplugins = autodeployplugins.trim();

		File dropinPluginsDir = fileStorage.getDropinPluginsDir();
		File pluginsDir = fileStorage.getPluginsDir();

		if (autodeployplugins.equals("*")) {
			List<File> allDirsPlain = new ArrayList<>();
			List<File> allDirs = new ArrayList<>();

			allDirsPlain.addAll(Arrays.asList(dropinPluginsDir.listFiles()));
			allDirs.addAll(Arrays.asList(pluginsDir.listFiles()));

			for (File f : allDirsPlain) {
				allDirs.add(PluginFiles.getUnzippedForm(f, true));
			}

			List<String> collect = allDirs.stream().map(e -> e.getName()).distinct().collect(Collectors.toList());

			autodeployplugins = String.join(",", collect);

			Logger.getLogger(PluginsAutowired.class.getName()).log(Level.INFO,
					"Auto-configured property kdcompute.autodeployplugins: " + autodeployplugins);
		}

		if (!autodeployplugins.isEmpty()) {
			List<String> asList = Arrays.asList(autodeployplugins.split(","));
			for (String pluginDepScript : asList) {

				String[] pluginDepScriptSplit = pluginDepScript.split(":");
				String plugin = pluginDepScriptSplit[0];

				PluginNameVersion pluginNameVersion = null;
				try {
					pluginNameVersion = PluginNameVersion.from(plugin);
				} catch (ParseException e) {
					pluginNameVersion = new PluginNameVersion(plugin);
				}

				EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
				Plugin findPluginFromPluginNameVersion = entityStore.findPluginFromPluginNameVersion(pluginNameVersion);
				if (findPluginFromPluginNameVersion == null) {

					File pluginFolder = fileStorage.getPluginFolderAsStaged(pluginNameVersion);

					if (pluginFolder == null) {
						throw new RuntimeException(
								"Couldn't find plugin " + pluginNameVersion + " in either expected folders:"
										+ pluginsDir.getPath() + ", " + dropinPluginsDir.getPath());
					}

					PluginFiles pluginFiles = PluginFiles.readPluginFiles(pluginFolder);
					PluginDependencyScript pluginDependencyScript = null;

					if (pluginDepScriptSplit.length == 2 && !pluginDepScriptSplit[1].trim().isEmpty()) {
						List<PluginDependencyScript> availableDependencyScripts = pluginFiles
								.getAvailableDependencyScripts();
						Optional<PluginDependencyScript> findAny = availableDependencyScripts.stream()
								.filter(e -> e.getDisplayName().contains(pluginDepScriptSplit[1])).findAny();
						if (!findAny.isPresent()) {
							throw new RuntimeException(
									"Unknown dependency '" + pluginDepScriptSplit[1] + "' script for plugin '" + plugin
											+ "'. Available scripts: " + availableDependencyScripts);
						} else {
							pluginDependencyScript = findAny.get();
						}
					}

					PluginDeploymentInspector deployPlugin = new PluginDeploymentInspector(pluginFiles,
							pluginDependencyScript);
					DeployPlugin create = deployPlugin.create(System.out, System.err, KDCFileUtils.getExampleTempDir(),
							KdcServerApplication.getInstance().getAdditionalVariables());
					if (create instanceof DeployPluginNoscript) {
						((DeployPluginNoscript) create).deploy(entityStore);
					} else if (create instanceof DeployPluginScript) {
						((DeployPluginScript) create).deploy(entityStore);
					} else {
						throw new RuntimeException("Unsupported deployment script: " + create);
					}

				}
			}
		}
	}

}
