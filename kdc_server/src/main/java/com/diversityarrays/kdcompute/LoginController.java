/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.diversityarrays.kdcompute.security.LoginMode;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
@Configuration
public class LoginController {

	@Value("${ldap.source.name:null}")
	private String ldapSourceName;

	@Value("${ldap.source.image:null}")
	private String ldapSourceImage;

	@Autowired
	ApplicationContext applicationContext;

	private boolean isOauth2Defined(ApplicationContext applicationContext, String name) {
		return applicationContext.containsBean(name);
	}

	@Autowired(required = false)
	LoginMode loginMode = LoginMode.USERNAME_AND_PASSWORD;

	@Value("${kdcompute.registration.isEnabled}")
	Boolean registrationEnabled;

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "msg", required = false) String msg, Model model, Principal principal) {
		UtilServer.setBannerModel(model, principal);

		model.addAttribute("ldapSourceName", ldapSourceName);
		model.addAttribute("ldapSourceImage", ldapSourceImage);

		model.addAttribute("googleBeanDefined", isOauth2Defined(applicationContext, "google"));
		model.addAttribute("githubBeanDefined", isOauth2Defined(applicationContext, "github"));
		model.addAttribute("facebookBeanDefined", isOauth2Defined(applicationContext, "facebook"));
		model.addAttribute("ldapBeanDefined", isOauth2Defined(applicationContext, "ldap"));

		model.addAttribute("usingOauth2", isOauth2Defined(applicationContext, "google")
				|| isOauth2Defined(applicationContext, "github") || isOauth2Defined(applicationContext, "facebook"));

		model.addAttribute("msg", msg);

		switch (loginMode) {
		case BLANK:
			return "loginnone";
		case USERNAME_AND_PASSWORD:
			model.addAttribute("registrationEnabled",
					registrationEnabled && KdcServerApplication.getInstance().getUsersStore().registrationEnabled());
			return "login";
		case USERNAME_ONLY:
			model.addAttribute("registrationEnabled", false);
			return "loginusername";
		default:
			throw new RuntimeException("Unhandled login mode: " + loginMode);
		}
	}

}
