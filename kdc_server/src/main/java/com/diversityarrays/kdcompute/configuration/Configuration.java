/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.configuration;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.EntityStoreProvider;
import com.diversityarrays.kdcompute.runtime.UsersStore;
import com.diversityarrays.kdcompute.users.KdcomputeEntityManagerUsersStore;
import com.diversityarrays.kdcompute.userstore.UserStoreProvider;
import com.diversityarrays.kdcompute.util.KdcomputeEntityManagerEntityStore;

@Service
public class Configuration {

	@Autowired(required = false)
	EntityStoreProvider storeProvider;

	@Autowired(required = false)
	UserStoreProvider userStoreProvider;

	private static KdcomputeEntityManagerEntityStore entityStore = null;

	@Bean(name = "entityStoreBean")
	@Autowired
	public EntityStore getEntityStore(EntityManagerFactory factory) {
		if (storeProvider != null) {
			EntityStore entityStore2 = storeProvider.getEntityStore(factory);
			return entityStore2;
		} else {
			JpaTransactionManager transactionManager = new JpaTransactionManager();
			entityStore = new KdcomputeEntityManagerEntityStore();
			entityStore.setEntityManager(factory.createEntityManager());
			transactionManager.setEntityManagerFactory(factory);
			Logger.getGlobal().log(Level.INFO, "Using KdcomputeEntityManagerEntityStore");
			return entityStore;
		}
	}

	@Bean(name = "usersStoreBean")
	@Autowired
	public UsersStore getUsersStore() {
		if (userStoreProvider != null) {
			return userStoreProvider.getUserStore();
		} else {
			return new KdcomputeEntityManagerUsersStore(entityStore);
		}
	}

}
