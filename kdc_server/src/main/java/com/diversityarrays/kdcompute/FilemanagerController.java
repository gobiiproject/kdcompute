/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.util.UtilServer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/*
 * Source: http://websystique.com/springmvc/spring-mvc-4-file-download-example/
 */

@Controller
public class FilemanagerController {

	protected static final String SHARED_FOLDER = "Shared_files";

	@RequestMapping(method = RequestMethod.GET, value = "/filemanager")
	public String provideUploadInfo(Model model, HttpServletRequest request, Principal principal) {

		UtilServer.setBannerModel(model, principal);
		String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
		StringBuilder nameBuilder = new StringBuilder("Logged in as ");
		nameBuilder.append(loggedInUser);
		model.addAttribute("userName", nameBuilder.toString());

		FileStorage fileStorage = KdcServerApplication.getInstance().getFileStorage();
		StringBuilder builder = new StringBuilder();
		try {
			File folderOfUser = fileStorage.getUserDir(loggedInUser);

			if (folderOfUser.isDirectory()) {

				// "Touch" the folder
				fileStorage.getSubmittedJobsDir(loggedInUser);

				File[] files = folderOfUser.listFiles();
				if (files.length <= 0) {
					builder.append("<p>You do not have any files");
				} else {
					builder.append("<ul id =\"file-managerlist\">");
					for (File folderOrFile : files) {
						if (folderOrFile.isDirectory()) {
							builder.append("<li id=\"" + folderOrFile.getAbsolutePath() + "\">");
							builder.append(
									"<input type=\"checkbox\" onclick=\"folderSelected(this);\" class=\"foldercheck\">")
									.append(folderOrFile.getName()).append("</input>");
							builder.append(createFileHtmlString(folderOrFile));
						}
						builder.append("</li>");
					}
					builder.append("</ul>");
				}
			} else {
				builder.append("Your Home Folder does not exist");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("userFolder", builder.toString());
		model.addAttribute("filemanager_tab_active", "active");
		return "filemanager";
	}

	private String createFileHtmlString(File folder) {
		StringBuilder builder = new StringBuilder();
		builder.append("<ul>");

		for (File f : folder.listFiles()) {
			if (f.isDirectory()) {
				builder.append("<li id=\"" + f.getAbsolutePath() + "\">");
				builder.append("<input type=\"checkbox\" onclick=\"folderSelected(this);\" class=\"foldercheck\">")
						.append(f.getName()).append("</input>");
				builder.append(createFileHtmlString(f));
			}

		}
		builder.append("</li>");
		builder.append("</ul>");
		return builder.toString();
	}

	@ResponseBody
	@RequestMapping(value = "/listfiles")
	public String getFiles(String path, HttpServletRequest request, HttpServletResponse response) {

		if (response.getStatus() == 200) {

			if (!path.isEmpty()) {

				File folder = new File(path);
				Map<String, String> fileMap = new HashMap<>();

				File[] files = folder.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return pathname.isFile();
					}
				});

				if (files != null && files.length > 0) {

					for (File f : files) {
						fileMap.put(f.getName(), f.getAbsolutePath());

					}
					GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();
					Gson gson = gsonBuilder.create();

					String mapstr = gson.toJson(fileMap);
					return mapstr;
				}
			}

		}
		return "";
	}

	@RequestMapping(value = "/uploadFile")
	public String uploadFile(@RequestParam("file") MultipartFile file, HttpServletResponse response,
			HttpServletRequest request) {

		if (response.getStatus() == 200) {
			// Making sure if the filename is not empty.
			if (file != null && !file.getOriginalFilename().isEmpty()) {
				String nameNeeded = request.getParameter("name");
				String uploadDestination = request.getParameter("uploaddestination");

				if (nameNeeded == null || nameNeeded.isEmpty()) {
					nameNeeded = file.getOriginalFilename();
				}

				String fileAbsolutePath = uploadDestination + "/" + nameNeeded;
				File newFile = new File(fileAbsolutePath);
				FileOutputStream stream;
				try {
					stream = new FileOutputStream(newFile);
					IOUtils.copy(file.getInputStream(), stream);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		return "redirect:/filemanager";
	}

	@ResponseBody
	@RequestMapping(value = "/deletefile")
	public String deleteFile(String path, HttpServletRequest request, HttpServletResponse response) {
		String errorMessage = "";
		if (response.getStatus() == 200) {

			if (path != null && !path.isEmpty()) {
				File file = new File(path);
				if (file.exists() && file.isFile()) {
					file.delete();
				} else {
					errorMessage = "Path:" + "couldnt delete file";
				}

			} else {
				errorMessage = "Path:" + path + " doesnt exist: Cant delete file";
			}

		} else {
			errorMessage = "Server Error: Cant delete File";
		}
		if (errorMessage.isEmpty()) {
			errorMessage = "File Deleted";
		}
		return errorMessage;
	}

	@RequestMapping("/elfinder")
	public String elfinder() {
		// KdcServerApplication.getServletConnector().
		return "elfinder";
	}

}
