/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.home;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Jobdbquery {
	String jobTag;
	Long jobIdLower;
	Long jobIdUpper;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date jobDateLower;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date jobDateUpper;
	Integer jobCount;
	public Jobdbquery() {
		super();
	}
	public Jobdbquery(String jobTag, Long jobIdLower, Long jobIdUpper, Date jobDateLower, Date jobDateUpper,
			Integer jobCount) {
		super();
		this.jobTag = jobTag;
		this.jobIdLower = jobIdLower;
		this.jobIdUpper = jobIdUpper;
		this.jobDateLower = jobDateLower;
		this.jobDateUpper = jobDateUpper;
		this.jobCount = jobCount;
	}
	public String getJobTag() {
		return jobTag;
	}
	public void setJobTag(String jobTag) {
		this.jobTag = jobTag;
	}
	public Long getJobIdLower() {
		return jobIdLower;
	}
	public void setJobIdLower(Long jobIdLower) {
		this.jobIdLower = jobIdLower;
	}
	public Long getJobIdUpper() {
		return jobIdUpper;
	}
	public void setJobIdUpper(Long jobIdUpper) {
		this.jobIdUpper = jobIdUpper;
	}
	public Date getJobDateLower() {
		return jobDateLower;
	}
	public void setJobDateLower(Date jobDateLower) {
		this.jobDateLower = jobDateLower;
	}
	public Date getJobDateUpper() {
		return jobDateUpper;
	}
	public void setJobDateUpper(Date jobDateUpper) {
		this.jobDateUpper = jobDateUpper;
	}
	public Integer getJobCount() {
		return jobCount;
	}
	public void setJobCount(Integer jobCount) {
		this.jobCount = jobCount;
	}
	
	


}
