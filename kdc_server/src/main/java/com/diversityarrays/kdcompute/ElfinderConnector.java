/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.diversityarrays.kdcompute.elfinder.ElfinderGetMappedDirectories;

import cn.bluejoe.elfinder.controller.ConnectorController;
import cn.bluejoe.elfinder.controller.FsVolumeFilter;

public class ElfinderConnector {

	public static void initialize(Map<String, ElfinderGetMappedDirectories> page2IDFileMap) {

		// ConnectorController get_connectorController =
		// servlet_connector.get_connectorController();

		final Map<String, ElfinderGetMappedDirectories> page2IDFileMapF = page2IDFileMap;
		ConnectorController.setVolumeFilterer(new FsVolumeFilter() {
			@Override
			public Map<String, File> getAccessableVolumes(Principal principal, HttpServletRequest request) {

				Map<String, File> map = new HashMap<>();

				/*
				 * TODO: Fix this ugly hack
				 */
				String url = request.getRequestURL().toString();

				Optional<String> findFirst = page2IDFileMapF.keySet().stream().filter(e -> url.contains(e)).findFirst();
				if (!findFirst.isPresent()) {
					Logger.getGlobal().log(Level.WARNING, "URL is not mapped to any registered ELFinder key: " + url);
					return new HashMap<>();
				} else {
					Optional<String> findFirst2 = page2IDFileMapF.keySet().stream().filter(e -> url.contains(e))
							.findFirst();
					if (findFirst2.isPresent()) {
						int indexOf = url.indexOf(findFirst2.get());
						String urlSuffix = url.substring(indexOf);
						Map<String, File> map2;
						try {
							map2 = page2IDFileMapF.get(findFirst.get()).getDirMapping(urlSuffix, principal,
									request.getParameterMap());
							map2.entrySet().forEach(e -> {
								map.put(e.getKey(), e.getValue());
							});
						} catch (NumberFormatException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
					return map;
				}
			}
		});
	}
}
