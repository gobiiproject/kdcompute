/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.home.Jobdbquery;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
public class HomeController {

	private static final String JOB_ID_LOWER = "jobIdLower";

	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String home(Model model, Principal principal) throws IOException {
		return homeWithQuery(model, principal, null, null, null, null, null, 100);
	}

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String homeGet(Model model, Principal principal) throws IOException {
		return "redirect:/home";
	}

	private DateFormat dateInstance = DateFormat.getDateInstance();

	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String homePost(@ModelAttribute @Valid Jobdbquery jobdbquery, BindingResult bindingResultJobdbquery,
			Model model, RedirectAttributes redirectAttrs) {
		redirectAttrs.addAttribute("jobTag", jobdbquery.getJobTag());
		redirectAttrs.addAttribute(JOB_ID_LOWER,
				jobdbquery.getJobIdLower() != null ? Long.toString(jobdbquery.getJobIdLower()) : "");
		redirectAttrs.addAttribute("jobIdUpper",
				jobdbquery.getJobIdUpper() != null ? Long.toString(jobdbquery.getJobIdUpper()) : "");
		redirectAttrs.addAttribute("jobDateLower",
				jobdbquery.getJobDateLower() != null ? dateInstance.format(jobdbquery.getJobDateLower()) : "");
		redirectAttrs.addAttribute("jobDateUpper",
				jobdbquery.getJobDateUpper() != null ? dateInstance.format(jobdbquery.getJobDateUpper()) : "");
		redirectAttrs.addAttribute("jobCount", jobdbquery.getJobCount() != null ? jobdbquery.getJobCount() : 100);
		return "redirect:/home/query";
	}

	@RequestMapping(value = "/home/runstate", method = RequestMethod.GET)
	@ResponseBody
	public String queryJobState(Model model, Principal principal,
			@RequestParam(JobSummaryController.JOBID) Long jobId) {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		AnalysisJob job = entityStore.getById(AnalysisJob.class, jobId);
		RunState runState = job != null ? job.getRunState() : RunState.UNKNOWN;
		return runState.name();
	}

	@RequestMapping(value = "/home/query", method = RequestMethod.GET)
	public String homeWithQuery(Model model, Principal principal, @RequestParam("jobTag") String jobTag,
			@RequestParam(JOB_ID_LOWER) Long jobIdLower, @RequestParam("jobIdUpper") Long jobIdUpper,
			@RequestParam("jobDateLower") String jobDateLowerStr, @RequestParam("jobDateUpper") String jobDateUpperStr,
			@RequestParam("jobCount") Integer jobCount) {
		Date jobDateLower = null;
		Date jobDateUpper = null;
		if (jobDateLowerStr != null && !jobDateLowerStr.isEmpty()) {
			try {
				jobDateLower = dateInstance.parse(jobDateLowerStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if (jobDateUpperStr != null && !jobDateUpperStr.isEmpty()) {
			try {
				jobDateUpper = dateInstance.parse(jobDateUpperStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Jobdbquery attributeValue = new Jobdbquery(jobTag, jobIdLower, jobIdUpper, jobDateLower, jobDateUpper,
				jobCount);
		model.addAttribute("jobdbquery", attributeValue);

		UtilServer.setBannerModel(model, principal);
		String userId = KdcServerApplication.getInstance().getUserId(principal);

		List<JobRow> allJobs = new ArrayList<>();
		KdcServerApplication instance = KdcServerApplication.getInstance();

		EntityManager manager = instance.getEntityStore().getEntityManager();

		int startPosition = 0;

		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<AnalysisJob> criteriaQuery = criteriaBuilder.createQuery(AnalysisJob.class);
		Root<AnalysisJob> root = criteriaQuery.from(AnalysisJob.class);
		Predicate alwaysTruePredicate = criteriaBuilder.isTrue(criteriaBuilder.literal(true));

		Predicate displayNamePreidcate = alwaysTruePredicate;
		if (jobTag != null && !jobTag.isEmpty()) {
			displayNamePreidcate = criteriaBuilder.like(root.<String>get("displayName"), "%" + jobTag + "%");
		}

		Predicate jobIdLowerPredicate = alwaysTruePredicate;
		if (jobIdLower != null) {
			jobIdLowerPredicate = criteriaBuilder.greaterThanOrEqualTo(root.<String>get("id"),
					Long.toString(jobIdLower));
		}
		Predicate jobIdUpperPredicate = alwaysTruePredicate;
		if (jobIdUpper != null) {
			jobIdUpperPredicate = criteriaBuilder.lessThanOrEqualTo(root.<String>get("id"), Long.toString(jobIdUpper));
		}

		Predicate jobDateLowerPredicate = alwaysTruePredicate;
		if (jobDateLower != null) {
			jobDateLowerPredicate = criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("creationDateTime"),
					jobDateLower);
		}
		Predicate jobDateUpperPredicate = alwaysTruePredicate;
		if (jobDateUpper != null) {
			jobDateUpperPredicate = criteriaBuilder.lessThanOrEqualTo(root.<Date>get("creationDateTime"), jobDateUpper);
		}

		Predicate requestingUserPredicate = criteriaBuilder.equal(root.get("requestingUser"), userId);
		Order p3 = criteriaBuilder.desc(root.get("creationDateTime"));

		TypedQuery<AnalysisJob> tq = manager.createQuery(criteriaQuery);
		criteriaQuery.where(displayNamePreidcate, jobIdLowerPredicate, jobIdUpperPredicate, jobDateLowerPredicate,
				jobDateUpperPredicate, requestingUserPredicate).orderBy(p3);
		tq = manager.createQuery(criteriaQuery);

		/*
		 * Get total
		 */
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class).where(displayNamePreidcate,
				jobIdLowerPredicate, jobIdUpperPredicate, jobDateLowerPredicate, jobDateUpperPredicate,
				requestingUserPredicate);
		cq.select(cb.count(cq.from(AnalysisJob.class)));
		Long total_count = manager.createQuery(cq).getSingleResult();
		model.addAttribute("nJobsTotal", total_count);

		tq.setMaxResults(jobCount).setFirstResult(startPosition);

		List<AnalysisJob> retrievedJobs = tq.getResultList();
		for (AnalysisJob job : retrievedJobs) {

			String cmd = JobSummaryController.buildCommand(userId, job);

			String displayName = job.getDisplayName();
			RunState runState = job.getRunState();
			String startDateTime = job.getCreationDateTime() == null ? "" : job.getCreationDateTime().toString();

			allJobs.add(new JobRow(job.getId(), displayName, runState, startDateTime, cmd));
		}

		model.addAttribute("jobs", allJobs);
		model.addAttribute("user", userId);
		model.addAttribute("home_tab_active", "active");
		model.addAttribute("now", new Date().getTime());

		model.addAttribute("nJobsRetrieved", retrievedJobs.size());
		return "home";
	}

	@RequestMapping(value = "/home/getnewjobs", method = RequestMethod.GET)
	@ResponseBody
	public Pair<Long, List<JobRow>> getJobsAfterTime(Model model, Principal principal,
			@RequestParam("jobDateLower") Long jobDateLowerL) throws ParseException {

		String userId = KdcServerApplication.getInstance().getUserId(principal);

		List<JobRow> allJobs = new ArrayList<>();
		KdcServerApplication instance = KdcServerApplication.getInstance();

		EntityManager manager = instance.getEntityStore().getEntityManager();

		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<AnalysisJob> criteriaQuery = criteriaBuilder.createQuery(AnalysisJob.class);
		Root<AnalysisJob> root = criteriaQuery.from(AnalysisJob.class);
		Predicate alwaysTruePredicate = criteriaBuilder.isTrue(criteriaBuilder.literal(true));

		Predicate jobDateLowerPredicate = alwaysTruePredicate;

		Date jobDateLower = new Date(jobDateLowerL);
		if (jobDateLower != null) {
			jobDateLowerPredicate = criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("creationDateTime"),
					jobDateLower);
		}

		Predicate requestingUserPredicate = criteriaBuilder.equal(root.get("requestingUser"), userId);
		Order p3 = criteriaBuilder.desc(root.get("creationDateTime"));

		TypedQuery<AnalysisJob> tq = manager.createQuery(criteriaQuery);
		criteriaQuery.where(jobDateLowerPredicate, requestingUserPredicate).orderBy(p3);
		tq = manager.createQuery(criteriaQuery);

		/*
		 * Get total
		 */
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class).where(jobDateLowerPredicate,
				requestingUserPredicate);
		cq.select(cb.count(cq.from(AnalysisJob.class)));

		List<AnalysisJob> retrievedJobs = tq.getResultList();
		for (AnalysisJob job : retrievedJobs) {

			String cmd = JobSummaryController.buildCommand(userId, job);

			String displayName = job.getDisplayName();
			RunState runState = job.getRunState();
			String startDateTime = job.getCreationDateTime() == null ? "" : job.getCreationDateTime().toString();

			allJobs.add(new JobRow(job.getId(), displayName, runState, startDateTime, cmd));
		}
		return Pair.of(new Date().getTime(), allJobs);
	}

	public static <T> Long getRowCount(EntityManager entityManager, CriteriaQuery<T> criteriaQuery,
			CriteriaBuilder criteriaBuilder, Root<T> root) {
		CriteriaQuery<Long> countCriteria = criteriaBuilder.createQuery(Long.class);
		Root<?> entityRoot = countCriteria.from(root.getJavaType());
		entityRoot.alias(root.getAlias());
		doJoins(root.getJoins(), entityRoot);
		countCriteria.select(criteriaBuilder.count(entityRoot));
		countCriteria.where(criteriaQuery.getRestriction());
		return entityManager.createQuery(countCriteria).getSingleResult();
	}

	private static void doJoins(Set<? extends Join<?, ?>> joins, Root<?> root_) {
		for (Join<?, ?> join : joins) {
			Join<?, ?> joined = root_.join(join.getAttribute().getName(), join.getJoinType());
			doJoins(join.getJoins(), joined);
		}
	}

	private static void doJoins(Set<? extends Join<?, ?>> joins, Join<?, ?> root_) {
		for (Join<?, ?> join : joins) {
			Join<?, ?> joined = root_.join(join.getAttribute().getName(), join.getJoinType());
			doJoins(join.getJoins(), joined);
		}
	}
}
