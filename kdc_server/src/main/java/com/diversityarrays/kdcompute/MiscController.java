/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.Principal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.misc.PluginMisc;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
public class MiscController {

	private static final String MISC_DOCUMENTALL = "/misc/documentall/";

	@GetMapping("/misc")
	public String misc(Model model, Principal principal) {
		UtilServer.setBannerModel(model, principal);
		return "misc/misc";
	}

	@Autowired
	private TemplateEngine templateEngine;

	@GetMapping("/misc/documentall/body")
	@ResponseBody
	public String buildDocumentAllString() {

		List<Plugin> highestVersionPlugins = getHighestVersionPlugins();

		Context context = new Context();
		context.setVariable("plugins", highestVersionPlugins);
		context.setVariable("kdcVersion", KdcServerApplication.getInstance().getVersion());

		String body = templateEngine.process("misc/doco/summary", context);
		return body;
	}

	private List<Plugin> getHighestVersionPlugins() {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		final List<Plugin> plugins = entityStore.getPlugins();

		List<String> pluginsByName = plugins.stream().map(e -> e.getPluginName()).distinct()
				.collect(Collectors.toList());

		List<Plugin> highestVersionPlugins = pluginsByName.stream().map(e -> {
			Optional<Plugin> findFirst = plugins.stream().filter(ee -> ee.getPluginName().equals(e))
					.sorted(new Comparator<Plugin>() {

						@Override
						public int compare(Plugin o1, Plugin o2) {
							return o2.getVersion().compareTo(o1.getVersion());
						}
					}).findFirst();
			return findFirst.get();
		}).collect(Collectors.toList());
		return highestVersionPlugins;
	}

	@GetMapping(MISC_DOCUMENTALL + "html")
	public String buildDocumentAll(HttpServletRequest request, Model model, Principal principal) {
		UtilServer.setBannerModel(model, principal);

		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		final List<Plugin> plugins = entityStore.getPlugins();

		List<String> pluginsByName = plugins.stream().map(e -> e.getPluginName()).distinct()
				.collect(Collectors.toList());

		List<PluginMisc> highestVersionPlugins = pluginsByName.stream().map(e -> {
			Optional<Plugin> findFirst = plugins.stream().filter(ee -> ee.getPluginName().equals(e))
					.sorted(new Comparator<Plugin>() {

						@Override
						public int compare(Plugin o1, Plugin o2) {
							return o2.getVersion().compareTo(o1.getVersion());
						}
					}).findFirst();
			Plugin plugin = findFirst.get();
			return new PluginMisc(plugin, plugin.getAlgorithmName().replaceAll("[^\\p{L}\\p{Nd}]+", "") + "_jump");
		}).collect(Collectors.toList());

		model.addAttribute("plugins", highestVersionPlugins);

		List<String> pluginHelpUrls = highestVersionPlugins.stream().map(plugin -> {
			return String.join("\n", Arrays.asList("<div id=\"" + plugin.getRef() + "\"></div>",
					getPluginHelpAsString(request, plugin.getPlugin())));
		}).collect(Collectors.toList());
		model.addAttribute("pluginHelpHtmlList", pluginHelpUrls);

		return "misc/doco/summary";
	}

	private String getPluginHelpAsString(HttpServletRequest request, Plugin plugin) {
		try {
			String webpageUrl = UtilServer.getBaseUrl(request) + PluginsController.PLUGIN_HELP_PREFIX_URL
					+ Long.toString(plugin.getId());
			String curlPage = UtilServer.curlPage(webpageUrl);
			String replaceAll = curlPage.replaceAll("src=\"", "src=\"" + getBaseUrl(request)
					+ PluginsController.PLUGIN_HELP_PREFIX_URL + Long.toString(plugin.getId()) + "/");
			// replaceAll = replaceAll.replaceAll("src=\"logo",
			// "src=\"" + PluginsController.PLUGIN_HELP_PREFIX_URL +
			// Long.toString(plugin.getId()) + "/logo");
			return replaceAll;
		} catch (IOException e1) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e1.printStackTrace(pw);
			// stack trace as a string
			return "Failed to export documentation for plugin " + plugin.getAlgorithmName() + "Exception:\n"
					+ sw.toString();
		}
	}

	@RequestMapping("/getBaseUrl")
	@ResponseBody
	String getBaseUrl(HttpServletRequest request) {
		return UtilServer.getBaseUrl(request);
	}

}
