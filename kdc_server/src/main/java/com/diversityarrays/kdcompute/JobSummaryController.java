/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.diversityarrays.kdcompute.cleaners.PeriodicZipper;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.jobsummary.JobSummaryUtil;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobNotFoundException;
import com.diversityarrays.kdcompute.util.UtilServer;

@Controller
public class JobSummaryController {

	public static final String JOBID = "jobid";
	private static final String USER = "user";
	public static final String USER_ID = "userid";
	private static final String STATIC_IMG_JOBSTATE = "/static/img/jobstate/";
	public static final String JOBSUMMARYWITHUSERID = "jobsummaryuser";
	private static final String DOWNLOAD_ZIP_URL = "/" + JOBSUMMARYWITHUSERID + "/" + "downloadzip";
	public static final String JOBSUMMARY = "jobsummary";

	public static String buildCommand(String userId, AnalysisJob job) {
		return buildPrefixedCommand("", userId, job);
	}

	public static String buildPrefixedCommand(String prefix, String userId, AnalysisJob job) {

		String params = String.join("&",
				Arrays.asList(USER_ID + "=" + userId, JOBID + "=" + Long.toString(job.getId())));

		return String.format("%s/%s?%s", prefix, JOBSUMMARYWITHUSERID, params);
	}

	@RequestMapping(value = "/" + JOBSUMMARY, method = RequestMethod.GET)
	public String displayJobSummaryNoUserId(HttpServletResponse response, HttpServletRequest request, Model model,
			RedirectAttributes redirectAttributes, @RequestParam(JOBID) long jobid, Principal principal)
			throws Exception {
		return displayJobSummary(response, request, model, redirectAttributes, jobid, principal);
	}

	@RequestMapping(value = "/" + JOBSUMMARYWITHUSERID, method = RequestMethod.GET)
	public String displayJobSummary(HttpServletResponse response, HttpServletRequest request, Model model,
			RedirectAttributes redirectAttributes, @RequestParam(JOBID) long jobid, Principal principal)
			throws Exception {
		UtilServer.setBannerModel(model, principal);
		if (response.getStatus() == 200) {

			AnalysisJob job = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class,
					Long.valueOf(jobid));
			if (job == null) {
				throw new IOException("Could not find job " + String.valueOf(jobid));
			}

			String requestingUser = KdcServerApplication.getInstance().getUserId(principal);

			String jobUser = job.getRequestingUser();

			if (!(jobUser.equalsIgnoreCase(requestingUser) || UtilServer.isUserAdmin(principal))) {
				throw new IOException("You do not have permission to access userid='" + jobUser + "' workspace");
			}

			RunState runState = job.getRunState();
			if (runState == null) {
				runState = RunState.UNKNOWN;
			}
			model.addAttribute("jobStateMsg", runState.description);
			String imageResource = runState.imageResource;
			model.addAttribute("jobstateimg", STATIC_IMG_JOBSTATE + imageResource);
			model.addAttribute(JOBID, jobid);
			model.addAttribute("baseurl", getURLBase(request));
			String displayName = job.getDisplayName().replace(" ", "_");
			model.addAttribute("jobName", displayName);
			model.addAttribute("jobZip", displayName + ".zip");

			File resultsFolder = job.getResultsFolder();
			if (resultsFolder == null || !resultsFolder.exists()) {
				return "jobsummaryNoExist";
			} else {
				model.addAttribute("results_folder_path", resultsFolder.getPath());
				return "jobsummary";
			}

		}
		return null;
	}

	@RequestMapping(value = DOWNLOAD_ZIP_URL, method = RequestMethod.GET)
	public void zipAndDownloadJob(HttpServletResponse response, HttpServletRequest request, Model model,
			RedirectAttributes redirectAttributes, @RequestParam(JOBID) long jobid, Principal principal)
			throws Exception {

		File resultFolderPath = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobid)
				.getResultsFolder();

		File zipFolder = PeriodicZipper.zipFolder(resultFolderPath);
		JobSummaryUtil.downloadFile(response, zipFolder);
	}

	@RequestMapping(value = "/download/jobsummary/{user}/{jobid}/{alg}/{path:.+}", method = RequestMethod.GET)
	public void downloadFile(HttpServletResponse response, @PathVariable(USER) String user,
			@PathVariable(JOBID) Long jobId, @PathVariable("alg") String alg, @PathVariable("path") String path)
			throws IOException, URISyntaxException {
		alg = alg.replace(" ", "_");
		File resultFolderPath = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId)
				.getResultsFolder();
		File file = new File(resultFolderPath, alg + File.separator + path);
		JobSummaryUtil.downloadFile(response, file);
	}

	/**
	 * @param jobId
	 * @param request
	 * @return relative links to files of job, of full links if request != null
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@RequestMapping(value = "/list/jobsummary/filename2link")
	@ResponseBody
	public static HashMap<String, String> filename2link(@RequestParam(JOBID) Long jobId, String prefix)
			throws IOException, URISyntaxException {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();

		AnalysisJob job = entityStore.getById(AnalysisJob.class, jobId);

		if (job != null) {
			final String URL_SEP = "/";
			String jobIdString = Long.toString(jobId);

			Function<String, String> linkFactory = new Function<String, String>() {
				@Override
				public String apply(String part) {
					return String.join(URL_SEP, Arrays.asList(prefix, "download", "jobsummary", job.getRequestingUser(),
							jobIdString, part));
				}
			};
			return JobSummaryUtil.makeName2DownloadLinks(entityStore, job, linkFactory);
		} else {
			return new HashMap<>();
		}
	}

	/**
	 * @param response
	 * @param user
	 * @param jobId
	 * @return A list of all files in this job directory as download links
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@RequestMapping(value = "/list/jobsummary/{user}/{jobid}", method = RequestMethod.GET)
	@ResponseBody
	public static List<String> listFiles(HttpServletResponse response, @PathVariable(USER) String user,
			@PathVariable(JOBID) Long jobId) throws IOException, URISyntaxException {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();

		AnalysisJob job = entityStore.getById(AnalysisJob.class, jobId);

		List<String> result;
		if (job == null) {
			result = Collections.emptyList();
		} else {
			final String URL_SEP = "/";
			String jobIdString = Long.toString(jobId);

			Function<String, String> linkFactory = new Function<String, String>() {
				@Override
				public String apply(String part) {
					return String.join(URL_SEP, Arrays.asList("", "download", "jobsummary", user, jobIdString, part));
				}
			};
			result = JobSummaryUtil.makeDownloadLinks(entityStore, job, linkFactory);
		}
		return result;
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/jobstateimg", method = RequestMethod.GET)
	@ResponseBody
	public static String jobstateimgUser(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		try {
			return getURLBase(request) + jobstateimg(jobId);
		} catch (MalformedURLException e) {
			return "";
		}
	}

	@RequestMapping(value = JOBSUMMARY + "/jobstateimg", method = RequestMethod.GET)
	@ResponseBody
	public static String jobstateimg(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		return jobstateimgUser(request, jobId);
	}

	public static String jobstateimg(Long jobId) {
		return getJobRunStateInfo(jobId, (rs) -> STATIC_IMG_JOBSTATE + rs.imageResource);
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/jobstatemsg", method = RequestMethod.GET)
	@ResponseBody
	public static String jobstatemsgUser(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		return jobstatemsg(jobId);
	}

	@RequestMapping(value = JOBSUMMARY + "/jobstatemsg", method = RequestMethod.GET)
	@ResponseBody
	public static String jobstatemsg(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		return jobstatemsgUser(request, jobId);
	}

	public static String jobstatemsg(Long jobId) {
		return getJobRunStateInfo(jobId, (rs) -> rs.description);
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/jobstart", method = RequestMethod.GET)
	@ResponseBody
	public static Date jobstartWithUser(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		AnalysisJob byId = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId);
		if (byId != null) {
			return byId.getStartDateTime();
		} else {
			return null;
		}
	}

	@RequestMapping(value = JOBSUMMARY + "/jobstart", method = RequestMethod.GET)
	@ResponseBody
	public static Date jobstart(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		return KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId).getStartDateTime();
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/jobend", method = RequestMethod.GET)
	@ResponseBody
	public static Date jobendWithUser(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		AnalysisJob byId = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId);
		if (byId != null) {
			return byId.getEndDateTime();
		} else {
			return null;
		}
	}

	@RequestMapping(value = JOBSUMMARY + "/jobend", method = RequestMethod.GET)
	@ResponseBody
	public static Date jobend(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		return KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId).getEndDateTime();
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/currDate", method = RequestMethod.GET)
	@ResponseBody
	public static Date currDateWithUser(HttpServletRequest request) {
		return new Date();
	}

	@RequestMapping(value = JOBSUMMARY + "/currDate", method = RequestMethod.GET)
	@ResponseBody
	public static Date currDate(HttpServletRequest request) {
		return currDateWithUser(request);
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/jobrunstate", method = RequestMethod.GET)
	@ResponseBody
	public static String jobRunStateWithUser(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		AnalysisJob byId = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId);
		if (byId != null) {
			return byId.getRunState().toString();
		} else {
			return RunState.UNKNOWN.name();
		}
	}

	@RequestMapping(value = JOBSUMMARY + "/jobrunstate", method = RequestMethod.GET)
	@ResponseBody
	public static String jobRunState(HttpServletRequest request, @RequestParam(JOBID) Long jobId) {
		return jobRunStateWithUser(request, jobId);
	}

	private static String getJobRunStateInfo(Long jobId, Function<RunState, String> getFeature) {
		EntityStore entityStore = KdcServerApplication.getInstance().getEntityStore();
		AnalysisJob job = entityStore.getById(AnalysisJob.class, jobId);
		RunState runState = job != null ? job.getRunState() : RunState.UNKNOWN;
		return getFeature.apply(runState);
	}

	public static String getURLBase(HttpServletRequest req) throws MalformedURLException {
		StringBuffer url = req.getRequestURL();
		String uri = req.getRequestURI();
		String ctx = req.getContextPath();
		String base = url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
		return base;
	}

	@RequestMapping(value = JOBSUMMARYWITHUSERID + "/jobcancel", method = RequestMethod.GET)
	@ResponseBody
	public static String cancelWithUser(HttpServletRequest request, @RequestParam(JOBID) Long jobId,
			Principal principal) {
		AnalysisJob byId = KdcServerApplication.getInstance().getEntityStore().getById(AnalysisJob.class, jobId);
		String userId = KdcServerApplication.getInstance().getUserId(principal);
		String jobUser = byId.getRequestingUser();
		if (userId.equals(jobUser) || UtilServer.isUserAdmin(principal)) {
			try {
				KdcServerApplication.getInstance().getJobScheduler().cancelJob(byId);
			} catch (JobNotFoundException e) {
				return "ERROR: Job was not running";
			}
			return "Job cancelled";
		} else {
			return "ERROR: You do not have permission to cancel that job";
		}
	}

	@RequestMapping(value = JOBSUMMARY + "/cancel", method = RequestMethod.GET)
	@ResponseBody
	public static String cancel(HttpServletRequest request, @RequestParam(JOBID) Long jobId, Principal principal) {
		return cancelWithUser(request, jobId, principal);
	}

}
