/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.submitjob;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.diversityarrays.api.ResponseJob;
import com.diversityarrays.kdcompute.JobSummaryController;
import com.diversityarrays.kdcompute.KdcServerApplication;
import com.diversityarrays.kdcompute.SubmittedFormController;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.db.PostCompletionCall;
import com.diversityarrays.kdcompute.db.PostSubmissionCall;
import com.diversityarrays.kdcompute.util.Either;

@Service
@Configurable
public class SubmitJobForPlugin implements SubmitJobForPluginService {

	public static SubmitJobResponse submitJobForPlugin(Map<String, String> allRequestParams, String loggedInUser,
			String pluginName, String base_usage, Function<ResponseJob, SubmitJobResponse> responseFactory,
			PostCompletionCall postCompletionCall, PostSubmissionCall postSubmissionCall) throws Exception {
		KdcServerApplication application = KdcServerApplication.getInstance();

		SubmitPluginJob spj = new SubmitPluginJob(
				(pName, versionCode) -> application.getEntityStore()
						.findPluginFromPluginNameVersion(new PluginNameVersion(pluginName, versionCode)),
				() -> application.getEntityStore().getListOfPluginNames(), (userName) -> {
					Either<IOException, File> result;
					try {
						result = Either.right(application.getFileStorage().getUserTempDir(userName));
					} catch (IOException e) {
						result = Either.left(e);
					}
					return result;
				}, () -> application.getVersion(),
				(job) -> JobSummaryController.buildCommand(job.getRequestingUser(), job));

		return spj.submitJobForPlugin(allRequestParams, loggedInUser, pluginName, base_usage, responseFactory,
				postCompletionCall, postSubmissionCall, (params) -> SubmittedFormController.submitJob(params));
	}

	@Override
	public SubmitJobResponse submitPluginJob(String requestingUser, Map<String, String> allRequestParams,
			String pluginName, String base_usage, Function<ResponseJob, SubmitJobResponse> responseFactory,
			PostCompletionCall postCompletionCall, PostSubmissionCall postSubmissionCall)
			throws FileNotFoundException, UnsupportedEncodingException, MalformedURLException, IOException, Exception {
		// String loggedInUser =
		// SecurityContextHolder.getContext().getAuthentication().getName();

		return submitJobForPlugin(allRequestParams, requestingUser, pluginName, base_usage, responseFactory,
				postCompletionCall, postSubmissionCall);
	}
}
