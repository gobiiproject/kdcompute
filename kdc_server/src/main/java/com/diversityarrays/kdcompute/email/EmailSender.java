/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.email;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.diversityarrays.kdcompute.Pair;

@Component
public class EmailSender {

	@Autowired(required = false)
	private void setMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
		mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
		mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
		mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
		mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
		mc.addMailcap("message/rfc822;; x-java-content- handler=com.sun.mail.handlers.message_rfc822");
	}

	private JavaMailSender javaMailSender;

	public EmailStatus send(String to, List<String> cc, String subject, String text,
			List<Pair<String, Resource>> inlineResources, List<Pair<String, File>> files) {
		try {

			if (!enabled()) {
				return null;
			}
			MimeMessage mail = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text, true);
			if (cc != null && !cc.isEmpty())
				helper.setCc(cc.toArray(new String[cc.size()]));

			for (Pair<String, Resource> e : inlineResources) {
				helper.addInline(e.getFirst(), e.getSecond());
			}

			for (Pair<String, File> f : files) {
				helper.addAttachment(f.getFirst(), f.getSecond());
			}

			Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
			javaMailSender.send(mail);
			return new EmailStatus(to, cc, subject, text).success();
		} catch (Exception e) {
			Logger.getGlobal().log(Level.WARNING,
					"Problem with sending email to: " + to + ", error message:" + e.getMessage());
			e.printStackTrace();
			return new EmailStatus(to, cc, subject, text).error(e.getMessage());
		}
	}

	public boolean enabled() {
		return javaMailSender != null;
	}
}
