/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.email;

import java.util.List;

public class EmailStatus {

	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR = "ERROR";

	private final String to;
	private final String subject;
	private final String body;
	private List<String> cc;

	private String status;
	private String errorMessage;

	public EmailStatus(String to, List<String> cc, String subject, String body) {
		this.to = to;
		this.subject = subject;
		this.body = body;
		this.cc = cc;
	}

	public EmailStatus success() {
		this.status = SUCCESS;
		return this;
	}

	public EmailStatus error(String errorMessage) {
		this.status = ERROR;
		this.errorMessage = errorMessage;
		return this;
	}

	public boolean isSuccess() {
		return SUCCESS.equals(this.status);
	}

	public boolean isError() {
		return ERROR.equals(this.status);
	}

	public String getTo() {
		return to;
	}

	public String getSubject() {
		return subject;
	}

	public String getBody() {
		return body;
	}

	public String getStatus() {
		return status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public List<String> getCc() {
		return cc;
	}
}
