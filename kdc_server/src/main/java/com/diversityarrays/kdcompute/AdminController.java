/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.admin.ActivePlugin;
import com.diversityarrays.kdcompute.admin.DiscoveredPlugin;
import com.diversityarrays.kdcompute.admin.ModelOutputCapture;
import com.diversityarrays.kdcompute.admin.PluginGit;
import com.diversityarrays.kdcompute.admin.PluginReportContainer;
import com.diversityarrays.kdcompute.admin.SelectedDep;
import com.diversityarrays.kdcompute.admin.StagedPlugin;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.plugin.Contact;
import com.diversityarrays.kdcompute.plugin.GitPlugin;
import com.diversityarrays.kdcompute.plugin.LocalGitPlugin;
import com.diversityarrays.kdcompute.plugin.PluginUtil;
import com.diversityarrays.kdcompute.plugin.RemoteGitPlugin;
import com.diversityarrays.kdcompute.plugin.files.collection.PluginCollection;
import com.diversityarrays.kdcompute.plugin.files.collection.PluginCollectionFiles;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.plugindeployment.DeployPlugin;
import com.diversityarrays.kdcompute.plugindeployment.DeployPluginNoscript;
import com.diversityarrays.kdcompute.plugindeployment.DeployPluginNoscriptNoTest;
import com.diversityarrays.kdcompute.plugindeployment.DeployPluginScript;
import com.diversityarrays.kdcompute.plugindeployment.DeployPluginSudoScript;
import com.diversityarrays.kdcompute.plugindeployment.FailedPluginTestsException;
import com.diversityarrays.kdcompute.plugindeployment.PluginAlreadyExistsInDatabaseException;
import com.diversityarrays.kdcompute.plugindeployment.PluginDeploymentInspector;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.ScriptNotSupportedException;
import com.diversityarrays.kdcompute.util.ServerUtils;
import com.diversityarrays.kdcompute.util.UtilServer;
import com.google.common.io.Files;
import com.google.gson.Gson;

import thatkow.ansible.AnsibleCreds;
import thatkow.ansible.AnsiblePlaybookNotFoundException;
import thatkow.ansible.AnsibleScriptBase;
import thatkow.ansible.AnsibleScriptCredentialsException;
import thatkow.ansible.AnsibleScriptException;
import thatkow.ansible.AnsibleSudoScript;
import thatkow.ansible.AnsibleVaultDoesNotExistException;

@Controller
@Configuration
public class AdminController {

	private static final String LOCAL_URL = "localUrl";
	private static final String SELECT_DEP = "selectedDependency";
	@Value("${ansibleCommand:#{null}}")
	String ansibleCommand;

	@Value("${groovyCommand:#{null}}")
	String groovyCommand;

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(Model model, Principal principal) throws IOException {
		UtilServer.setBannerModel(model, principal);

		List<ActivePlugin> activePlugins = new ArrayList<>();

		List<StagedPlugin> stagedPlugins = new ArrayList<>();
		List<String> unknownStagedFolders = new ArrayList<>();

		List<DiscoveredPlugin> discoveredPlugins = new ArrayList<>();
		List<String> unknownDiscoveredPlugins = new ArrayList<>();

		List<Plugin> dbplugins = KdcServerApplication.getInstance().getEntityStore().getPlugins();
		for (Plugin p : dbplugins) {
			activePlugins.add(new ActivePlugin(p, getUpdateUrl(p), ServerUtils.buildParamUrl("/admin/plugin/undeploy",
					Pair.of(Plugin.PLUGIN_NAME_WITH_VERSION, p.getPluginNameAndVersion()))));
		}

		File kdcomputePluginsDir = KdcServerApplication.getInstance().getFileStorage().getPluginsDir();
		File[] pluginsInDir = kdcomputePluginsDir.listFiles();

		for (File pluginFile : pluginsInDir) {
			File spec = new File(pluginFile, PluginUtil.PLUGIN_JSON);
			File specCollection = new File(pluginFile, PluginCollectionFiles.PLUGINCOLLECTION_JSON);
			if (pluginFile.isDirectory()) {

				try {
					if (spec.exists()) {
						Plugin p = Plugin.createPluginFromFile(spec);
						if (!dbplugins.contains(p)) {
							StagedPlugin sP = new StagedPlugin(p.getPluginName(), getPluginDeployPath(p),
									p.getVersion().toString(), getUpdateUrl(p), getDeleteUrl(p));
							stagedPlugins.add(sP);
						}
					} else if (specCollection.exists()) {
						PluginCollectionFiles pluginCollectionsDir = PluginCollectionFiles.createFromDir(pluginFile);
						PluginCollection pluginCollection = pluginCollectionsDir.getPluginCollection();
						String name = pluginCollection.getPluginGroup();
						List<Plugin> plugins = pluginCollectionsDir.getPlugins();
						if (plugins.stream().anyMatch(p -> !dbplugins.contains(p))) {
							StagedPlugin sP = new StagedPlugin(name,
									getPluginCollectionDeployPath(pluginCollectionsDir),
									pluginCollection.getVersion().toString(), "", "");
							stagedPlugins.add(sP);
						}
					}
				} catch (IOException e) {
					unknownStagedFolders.add(pluginFile.getName());
				}
			}
		}

		File dropinPluginsDir = KdcServerApplication.getInstance().getFileStorage().getDropinPluginsDir();
		if (!dropinPluginsDir.exists()) {
			dropinPluginsDir.mkdirs();
		}

		List<File> dropinPluginsPlain = Arrays.asList(dropinPluginsDir.listFiles());

		List<File> pluginsDropinPluginsDir = new ArrayList<>();

		for (File f : dropinPluginsPlain) {
			pluginsDropinPluginsDir.add(PluginFiles.getUnzippedForm(f, true));
		}

		for (File pluginFile : pluginsDropinPluginsDir) {
			File spec = new File(pluginFile, PluginUtil.PLUGIN_JSON);
			File specCollection = new File(pluginFile, PluginCollectionFiles.PLUGINCOLLECTION_JSON);
			if (pluginFile.isDirectory()) {
				if (spec.exists()) {
					Plugin p = null;
					String name = pluginFile.getName();
					try {
						p = Plugin.createPluginFromFile(spec);
						if (!dbplugins.contains(p)) {
							discoveredPlugins.add(new DiscoveredPlugin(p.getPluginName(), getMoveToStagedPath(name),
									p.getVersion().toString()));
						}
					} catch (IOException e) {
						unknownDiscoveredPlugins.add(name);
					}
				} else if (specCollection.exists()) {
					PluginCollectionFiles createFromDir = PluginCollectionFiles.createFromDir(pluginFile);
					String name = createFromDir.getPluginCollection().getPluginGroup();
					List<Plugin> plugins = createFromDir.getPlugins();
					if (plugins.stream().anyMatch(p -> !dbplugins.contains(p))) {
						discoveredPlugins.add(new DiscoveredPlugin(name, getMoveToStagedPath(name),
								createFromDir.getPluginCollection().getVersion().toString()));
					}
				}
			}
		}

		model.addAttribute("pluginGit", new PluginGit());
		model.addAttribute("activePlugins", activePlugins);

		model.addAttribute("stagedPlugins", stagedPlugins);
		model.addAttribute("unknownStagedFolders", unknownStagedFolders);

		model.addAttribute("discoveredPlugins", discoveredPlugins);
		model.addAttribute("unknownDiscoveredPlugins", unknownDiscoveredPlugins);

		model.addAttribute("unknownPluginsRoot", kdcomputePluginsDir.getAbsolutePath());
		model.addAttribute("admin_tab_active", "active");
		return "admin";
	}

	public String getPluginDeployPath(Plugin plugin) {
		String buildParamUrl = ServerUtils.buildParamUrl("/admin/plugin/selectDependency",
				Arrays.asList(Pair.of(Plugin.PLUGIN_NAME_WITH_VERSION, plugin.getPluginNameAndVersion())));
		return buildParamUrl;
	}

	public String getPluginCollectionDeployPath(PluginCollectionFiles pluginCollectionFiles) {
		String buildParamUrl = ServerUtils.buildParamUrl("/admin/plugin/installplugincollection",
				Arrays.asList(Pair.of(LOCAL_URL, pluginCollectionFiles.getRootDir().getPath())));
		return buildParamUrl;
	}

	public String getMoveToStagedPath(String dropinFolderName) {
		return ServerUtils.buildParamUrl("/admin/plugin/moveToStaged",
				Pair.of(Plugin.PLUGIN_NAME_WITH_VERSION, dropinFolderName));
	}

	@RequestMapping(value = "/admin/plugin/moveToStaged", method = RequestMethod.GET)
	public String moveToStaged(@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginName,
			final RedirectAttributes redirectAttributes, Model model) throws Exception {
		FileStorage fileStorage = KdcServerApplication.getInstance().getFileStorage();
		PluginUtil.moveOrCopyFromDropin(PluginUtil.MOVE, pluginName, fileStorage);
		return "redirect:/admin";
	}

	private String getUpdateUrl(Plugin p) {
		return ServerUtils.buildParamUrl("/admin/plugin/update",
				Pair.of(Plugin.PLUGIN_NAME_WITH_VERSION, p.getPluginNameAndVersion()));
	}

	private String getDeleteUrl(Plugin p) {
		return ServerUtils.buildParamUrl("/admin/plugin/delete",
				Pair.of(Plugin.PLUGIN_NAME_WITH_VERSION, p.getPluginNameAndVersion()));
	}

	@RequestMapping(value = "/admin/plugin/delete", method = RequestMethod.GET)
	public String delete(@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion, Model model,
			Principal principal) throws IOException, ParseException {
		UtilServer.setBannerModel(model, principal);
		File pluginFolder = KdcServerApplication.getInstance().getFileStorage()
				.getPluginFolder(PluginNameVersion.from(pluginNameVersion));
		try {
			FileUtils.deleteDirectory(pluginFolder);
		} catch (IOException e) {
			Files.move(pluginFolder, new File(new File(System.getProperty("java.io.tmpdir")),
					"kdcompute_dump" + Integer.toString(new Random().nextInt())));
		}
		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/plugin/install")
	public static String identifySchemeAndOverlay(@ModelAttribute PluginGit pluginGit,
			final RedirectAttributes redirectAttributes, Model model) throws Exception {
		// Trim whitespace
		pluginGit.setGitUrl(pluginGit.getGitUrl().trim());
		pluginGit.setCommitId(pluginGit.getCommitId().trim());
		pluginGit.setGitUsername(pluginGit.getGitUsername().trim());
		pluginGit.setGitPassword(pluginGit.getGitPassword().trim());

		String gitUrl = pluginGit.getGitUrl().trim();
		URI uri = new URI(gitUrl.replace(" ", "%20").replace("\\", "/"));

		String scheme = uri.getScheme();
		model.addAttribute("pluginGit", pluginGit);
		if (scheme == null) {
			throw new Exception("Could not identify scheme in provided uri " + gitUrl);
		}
		if (scheme.equals("http") || scheme.equals("https")) {
			model.addAttribute("schemeImg", "/static/img/world2kdc.gif");
			model.addAttribute("schemeMsg", "Cloning Plugin from " + gitUrl);
			return "schemeOverlay";
		} else if (scheme.compareTo("file") == 0) {
			model.addAttribute("schemeImg", "/static/img/folder2kdc.gif");
			model.addAttribute("schemeMsg", "Copying Plugin from " + gitUrl);
			return "schemeOverlay";
		} else {
			throw new Exception("Unsupported scheme (" + scheme + ") in provided uri " + gitUrl);
		}
	}

	@RequestMapping(value = "/admin/plugin/installplugin", method = RequestMethod.POST)
	public String greetingSubmit(@ModelAttribute PluginGit pluginGit, final RedirectAttributes redirectAttributes,
			Model model, Principal principal) throws IOException {
		UtilServer.setBannerModel(model, principal);
		ModelOutputCapture outputRedirect = new ModelOutputCapture();

		File pluginsDir = null;
		File destFolder = null;

		try {
			// Check
			URI uri = new URL(pluginGit.getGitUrl().replaceAll(" ", "%20")).toURI();
			String scheme = uri.getScheme();
			GitPlugin clonedPlugin = null;
			if (scheme == null) {
				throw new Exception("Could not identify scheme in provided uri " + pluginGit.getGitUrl());
			}

			File localUrl = new File(uri.getPath());
			if (scheme.equals("http") || scheme.equals("https")) {
				RemoteGitPlugin remoteGitPlugin = null;
				if (pluginGit.getGitUsingCreds()) {
					UsernamePasswordCredentialsProvider creds = new UsernamePasswordCredentialsProvider(
							pluginGit.getGitUsername(), pluginGit.getGitPassword());
					remoteGitPlugin = new RemoteGitPlugin(uri.toURL(), outputRedirect, creds);
				} else {
					remoteGitPlugin = new RemoteGitPlugin(uri.toURL(), outputRedirect);
				}
				localUrl = remoteGitPlugin.gitClone(pluginGit.getCommitId());
				clonedPlugin = remoteGitPlugin;
			} else if (scheme.compareTo("file") != 0) {
				throw new Exception("Unsupported scheme (" + scheme + ") in provided uri " + pluginGit.getGitUrl());
			}

			if (!localUrl.exists()) {
				throw new Exception("URI file( " + localUrl.getPath() + " ) does not exist");
			}

			/*
			 * Check if Plugin Collection
			 */
			if (PluginCollectionFiles.isPluginCollection(localUrl)) {
				return installCollection(model, principal, localUrl.getPath());
			} else {
				PluginFiles localPluginFiles = PluginFiles.readPluginFiles(localUrl);
				LocalGitPlugin localGitPlugin = new LocalGitPlugin(outputRedirect, localPluginFiles);
				clonedPlugin = localGitPlugin;
				pluginsDir = getPluginsDir();

				PluginFiles pluginFiles = PluginFiles.readPluginFiles(localUrl);

				destFolder = new File(pluginsDir, pluginFiles.getPluginNameWithVersion());

				if (!destFolder.exists()) {
					clonedPlugin.migrate(pluginsDir);
				} else {
					throw new IOException("A plugin with this name already exists @ " + destFolder.getAbsolutePath()
							+ "\n Delete it from Staged Plugins first then clone it again");
				}
				redirectAttributes.addAttribute("pluginFolder", pluginFiles.getPluginNameWithVersion());

				return ServerUtils.buildParamUrl("redirect:/admin/plugin/selectDependency", Arrays
						.asList(Pair.of(Plugin.PLUGIN_NAME_WITH_VERSION, pluginFiles.getPluginNameWithVersion())));
			}
		} catch (Exception e) {
			outputRedirect.appendStderr(e.getMessage(), AdminController.class);
		}

		String memail = null;
		String destFolderString = "";
		try {
			if (destFolder != null) {
				memail = getMaintainersEmail(destFolder);
				destFolderString = destFolder.getName();
			}
		} catch (IOException e) {
			outputRedirect.appendStderr("Failed to add contact:\n" + e.getMessage(), AdminController.class);
			model.addAttribute("memail", "Not Provided");
		}

		outputRedirect.addToModelFailPlugin(model, destFolderString, "", memail, "");
		return "adminplugin";
	}

	@RequestMapping(value = "/admin/plugin/installplugincollection", method = RequestMethod.GET)
	public String installCollection(Model model, Principal principle, @RequestParam(LOCAL_URL) String localUrl)
			throws IOException {
		UtilServer.setBannerModel(model, principle);
		ModelOutputCapture outputRedirect = new ModelOutputCapture();

		PluginCollectionFiles pluginCollectionFiles = PluginCollectionFiles.createFromDir(new File(localUrl));
		throw new RuntimeException("NYI: Collections not supported");
		// try {
		// String loggedInUser =
		// SecurityContextHolder.getContext().getAuthentication().getName();
		// Map<String, CallScript> callScripts;
		// DeployPluginCollection deployPluginCollection =
		// DeployPluginCollection.create(callScripts, outputRedirect,
		// pluginCollectionFiles,
		// KdcServerApplication.getInstance().getAdditionalVariables(),
		// loggedInUser);
		// List<File> collectionDependenyScripts =
		// deployPluginCollection.getCollectionDependenyScripts();
		// if (!collectionDependenyScripts.isEmpty()) {
		// if(collectionDependenyScripts.size()>1){
		// throw new Exception("NYI: Cannot handle more then one )
		// }
		//
		//
		// AnsibleScript ansibleScript;
		// if (ansiblePlaybookPath != null && !ansiblePlaybookPath.isEmpty()) {
		// ansibleScript = AnsibleScript.create(outputRedirect,
		// collectionDependenyScripts.get(0), ansiblePlaybookPath);
		// } else {
		// ansibleScript = AnsibleScript.create(outputRedirect,
		// collectionDependenyScripts.get(0));
		// }
		// if (ansibleScript.isSudo()) {
		//
		// collectionDir =
		// KdcServerApplication.getInstance().getFileStorage().getKdcomputePluginsDir();
		// collectionDir = new File(collectionDir, "collectionFolder");
		//
		// FileUtils.copyDirectory(pluginCollectionFiles.getRootDir(),
		// collectionDir);
		//
		// redirectAttributes.addAttribute("collectionFolder",
		// pluginCollectionFiles.getPluginCollection().getPluginGroup());
		// redirectAttributes.addAttribute("mainDependencyScript",
		// collectionDependenyScripts);
		//
		// return "redirect:/admin/collection/selectCollectionDependency/"
		// + pluginCollectionFiles.getPluginCollection().getPluginGroup();
		//
		// }
		// if (!deployPluginCollection.getPluginsDependencyScripts().isEmpty())
		// {
		// outputRedirect.appendStderr("Not handling sub plugin dependencies");
		// outputRedirect.addToModelFailPlugin(model, localUrl.getPath(),
		// pluginCollectionFiles.getPluginCollection().getPluginGroup(), "",
		// "");
		// return "adminplugin";
		// }
		// ansibleScript.install();
		// }
		// Map<String, List<String>> failedTests =
		// DeployPluginCollection.dummyDeploy(deployPluginCollection,
		// outputRedirect, ansiblePlaybookPath);
		// if (!failedTests.isEmpty()) {
		// outputRedirect.appendStderr(failedTests.toString());
		// outputRedirect.addToModelFailPlugin(model, localUrl.getPath(),
		// pluginCollectionFiles.getPluginCollection().getPluginGroup(), "",
		// "");
		// return "adminplugin";
		// } else {
		// for (String plugin :
		// pluginCollectionFiles.getPluginCollection().getPlugins()) {
		// PluginFiles localPluginFiles = PluginFiles.readPluginFiles(new
		// File(localUrl, plugin));
		// LocalGitPlugin localGitPlugin = new LocalGitPlugin(outputRedirect,
		// localPluginFiles);
		// destFolder = new File(getPluginsDir(),
		// localPluginFiles.getPluginNameWithVersion());
		// if (!destFolder.exists()) {
		// File migrate = localGitPlugin.migrate(getPluginsDir());
		// PluginFiles pluginFiles = PluginFiles.readPluginFiles(migrate);
		// buildAndMergeIntoDb(pluginFiles);
		// } else {
		// outputRedirect.appendStderr("Plugin " + destFolder.getName() + "
		// already exists");
		// }
		// }
		//
		// model = outputRedirect.addToModel(model);
		//
		// // TODO
		// model.addAttribute("title", "Success, Plugin Collection "
		// + pluginCollectionFiles.getPluginCollection().getPluginGroup() + "
		// deployed");
		// model.addAttribute("titleColor", "green");
		// model.addAttribute("success", true);
		// model.addAttribute("pluginname",
		// pluginCollectionFiles.getPluginCollection().getPlugins());
		//
		// List<String> pluginDepScripts = new ArrayList<>();
		// for (Map.Entry<String, List<File>> entry :
		// deployPluginCollection.getPluginsDependencyScripts()
		// .entrySet()) {
		// if (!entry.getValue().isEmpty())
		// pluginDepScripts.add(entry.getValue().get(0).getName());
		// }
		// model.addAttribute("script", collectionDependenyScripts.get(0)
		// + String.join(", ", pluginDepScripts));
		// model.addAttribute("additionalMsg", "This plugin is now ready for
		// use. Although tests have passed"
		// + ", it's good practice to check that it works manually");
		// return "adminplugin";
		// }
		// } catch (Exception e) {
		// outputRedirect.appendStderr(e.getMessage());
		// outputRedirect.addToModelFailPlugin(model, localUrl,
		// pluginCollectionFiles.getPluginCollection().getPluginGroup(), "",
		// "");
		// return "adminplugin";
		// }
	}

	@RequestMapping(value = "/admin/plugin/selectDependency", method = RequestMethod.GET)
	public String selectDependency(@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion,
			final RedirectAttributes redirectAttributes, Model model, Principal principal)
			throws ParseException, IOException {
		UtilServer.setBannerModel(model, principal);
		ModelOutputCapture outputCapture = new ModelOutputCapture();

		PluginNameVersion pluginNameVersion2 = PluginNameVersion.from(pluginNameVersion);
		File pluginDir = KdcServerApplication.getInstance().getFileStorage().getPluginFolder(pluginNameVersion2);

		if (pluginDir == null || !pluginDir.isDirectory()) {
			throw new IOException("Directory " + pluginDir + " does not exist");
		}

		model.addAttribute(Plugin.PLUGIN_NAME_WITH_VERSION, pluginNameVersion2);
		model.addAttribute("pluginNameVersion", pluginNameVersion);

		String memail = null;
		try {
			memail = getMaintainersEmail(pluginDir);
		} catch (IOException e) {
			outputCapture.appendStderr("Failed to add contact:\n" + e.getMessage(), AdminController.class);
			model.addAttribute("memail", "Not Provided");
		}

		try {
			PluginFiles pluginFiles = PluginFiles.readPluginFiles(pluginDir);
			SelectedDep attributeValue = new SelectedDep();
			List<File> listOfDepedencies = pluginFiles.getListOfDepedencies();

			if (listOfDepedencies.isEmpty()) {
				return sudoCheck(new SelectedDep(), pluginNameVersion, redirectAttributes, model);
			}
			// else if (listOfDepedencies.size() == 1) {
			// return sudoCheck(new
			// SelectedDep(listOfDepedencies.get(0).getName(),
			// pluginDir.getName()), pluginFolder,
			// redirectAttributes, model);
			// }
			else {
				model.addAttribute("selectedDependency", attributeValue);
				model.addAttribute("dependencies", listOfDepedencies);
				model.addAttribute("stdout", outputCapture.getStdout());
				model.addAttribute("stderr", outputCapture.getStderr());
				return "selectDep";
			}
		} catch (Exception e) {
			outputCapture.appendStderr(e.getMessage(), AdminController.class);
		}

		outputCapture.addToModelFailPlugin(model, pluginNameVersion, "", memail, "");
		return "adminplugin";
	}

	@RequestMapping(value = "/admin/plugin/selectDependency", method = RequestMethod.POST)
	public String sudoCheck(@ModelAttribute SelectedDep selectedDep,
			@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion,
			final RedirectAttributes redirectAttributes, Model model)
			throws InterruptedException, IOException, ParseException {
		UtilServer.setBannerModel(model, null);
		ModelOutputCapture outputCapture = new ModelOutputCapture();

		File pluginDir = KdcServerApplication.getInstance().getFileStorage()
				.getPluginFolder(PluginNameVersion.from(pluginNameVersion));
		File depDir = PluginFiles.makeDependenciesDir(pluginDir);

		String memail = null;
		try {
			memail = getMaintainersEmail(pluginDir);
		} catch (IOException e) {
			outputCapture.appendStderr("Failed to add contact:\n" + e.getMessage(), AdminController.class);
			model.addAttribute("memail", "Not Provided");
		}
		File script = null;
		if (selectedDep.getScript() != null && !selectedDep.getScript().isEmpty()) {

			// Check is ansible is installed
			AnsibleScriptBase ansibleScript;
			script = new File(depDir, selectedDep.getScript());
			try {
				if (ansibleCommand != null && !ansibleCommand.isEmpty()) {
					ansibleScript = AnsibleScriptBase.create(script, ansibleCommand);
				} else {
					ansibleScript = AnsibleScriptBase.create(script);
				}
			} catch (AnsiblePlaybookNotFoundException e) {
				model.addAttribute("exception", e);
				return "noansibleplaybook";
			}

			try {

				AnsibleCreds attributeValue = new AnsibleCreds();
				model.addAttribute("needsSudo", ansibleScript instanceof AnsibleSudoScript);
				model.addAttribute("ansibleCreds", attributeValue);
				model.addAttribute("pluginNameVersion", pluginNameVersion);
				model.addAttribute("selectedScript", selectedDep.getScript());
				String[] split = FileUtils.readFileToString(script, "UTF-8").replaceAll(" ", "  ").split("\n");
				model.addAttribute("script_rows", Arrays.asList(split));
				outputCapture.addToModel(model);
				return "ansiblecreds";

			} catch (IOException e) {
				outputCapture.appendStderr(e.getMessage(), AdminController.class);
				outputCapture.addToModelFailPlugin(model, pluginNameVersion, script.getName(), memail, "");
				return "adminplugin";
			}
		} else {
			// try {
			String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
			return ServerUtils.buildParamUrl("redirect:/admin/plugin/runAnsible/nodep",
					Pair.of("pluginNameVersion", pluginNameVersion));
			// used",
			// model,
			// outputCapture,
			// pluginDir,loggedInUser);
			// } catch (IOException e) {
			// outputCapture.appendStderr(e.getMessage(),AdminController.class);
			// outputCapture.addToModelFailPlugin(model, pluginFolder, "No
			// Script", memail, "");
			// return "adminplugin";
			// }
		}
	}

	public File getPluginsDir() {
		return PluginFiles.makePluginDir(KdcServerApplication.getInstance().getFileStorage().getKdcomputePluginsDir());
	}

	public File getCollectionDir() {
		return KdcServerApplication.getInstance().getFileStorage().getKdcomputePluginsDir();
	}

	@RequestMapping(value = "/admin/plugin/runAnsible/nodep")
	public String splashDeployNoDep(@ModelAttribute AnsibleCreds ansibleCreds,
			@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion, Model model) {
		return splashDeploy(ansibleCreds, pluginNameVersion, "", model);
	}

	@RequestMapping(value = "/admin/plugin/runAnsible")
	public String splashDeploy(@ModelAttribute AnsibleCreds ansibleCreds,
			@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion,
			@RequestParam(SELECT_DEP) String selectedDep, Model model) {

		model.addAttribute(ansibleCreds);
		model.addAttribute("pluginNameVersion", pluginNameVersion);
		model.addAttribute("selectedDependency", selectedDep);

		return "deployPlugin";

	}

	@RequestMapping(value = "/admin/plugin/deploy/nodep", method = { RequestMethod.GET, RequestMethod.POST })
	public String installWithoutDep(@ModelAttribute AnsibleCreds ansibleCreds,
			@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion, Model model, Principal principal,
			RedirectAttributes redirectAttributes) throws IOException, InterruptedException,
			ScriptNotSupportedException, ParseException, PluginAlreadyExistsInDatabaseException {
		return installDep(ansibleCreds, pluginNameVersion, "", model, principal, redirectAttributes);
	}

	@RequestMapping(value = "/admin/plugin/deploy", method = RequestMethod.POST)
	public String installDep(@ModelAttribute AnsibleCreds ansibleCreds,
			@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersionStr,
			@RequestParam(AdminController.SELECT_DEP) String selectedDep, Model model, Principal principal,
			RedirectAttributes redirectAttributes) throws IOException, InterruptedException,
			ScriptNotSupportedException, ParseException, PluginAlreadyExistsInDatabaseException {
		UtilServer.setBannerModel(model, principal);

		ModelOutputCapture outputCapture = new ModelOutputCapture();

		KdcServerApplication instance = KdcServerApplication.getInstance();
		EntityStore entityStore = instance.getEntityStore();
		PluginNameVersion pluginNameVersion = PluginNameVersion.from(pluginNameVersionStr);
		File pluginDir = instance.getFileStorage().getPluginFolder(pluginNameVersion);

		/*
		 * Elfinder access to tests
		 */
		File unitTestRoot = KDCFileUtils.getExampleTempDir();
		model.addAttribute("dirpath", KDCFileUtils.fileSafeUrl(unitTestRoot));
		model.addAttribute("pluginName", pluginNameVersion);
		model.addAttribute(Plugin.PLUGIN_NAME_WITH_VERSION, pluginNameVersion);

		String memail = "";
		try {
			memail = getMaintainersEmail(pluginDir);
		} catch (IOException e) {
			outputCapture.appendStderr("Failed to add contact:\n" + e.getMessage(), AdminController.class);
			model.addAttribute("memail", "Not Provided");
		}

		PluginFiles pluginFiles = PluginFiles
				.readPluginFiles(instance.getFileStorage().getPluginFolderAsStaged(pluginNameVersion));

		PluginDependencyScript pluginDependencyScript = null;

		if (selectedDep != null && !selectedDep.isEmpty()) {
			List<PluginDependencyScript> availableDependencyScripts = pluginFiles.getAvailableDependencyScripts();
			Optional<PluginDependencyScript> selectedDepFindAny = availableDependencyScripts.stream()
					.filter(e -> e.getDisplayName().contains(selectedDep)).findAny();
			pluginDependencyScript = selectedDepFindAny.get();
		}

		PluginDeploymentInspector deployPlugin = new PluginDeploymentInspector(pluginFiles, pluginDependencyScript);

		DeployPlugin create = deployPlugin.create(outputCapture.getPsOut(), outputCapture.getPsErr(), unitTestRoot,
				KdcServerApplication.getInstance().getAdditionalVariables());
		try {
			if (create instanceof DeployPluginNoscript) {
				((DeployPluginNoscript) create).deploy(entityStore);
			} else if (create instanceof DeployPluginScript) {
				try {
					((DeployPluginScript) create).deploy(entityStore);
				} catch (AnsiblePlaybookNotFoundException e) {
					model.addAttribute("exception", e);
					return "noansibleplaybook";
				} catch (AnsibleScriptException e) {
					addReportButton(selectedDep, model, pluginDir, unitTestRoot, Arrays.asList(), memail,
							Arrays.asList(outputCapture.getStdout().split("\\n")),
							Arrays.asList(outputCapture.getStderr().split("\\n")), true);
					return failedAdminpluginedBuilder(selectedDep, model, outputCapture, pluginDir, "",
							"Ansible script returned non zero status");
				}
			} else if (create instanceof DeployPluginSudoScript) {
				try {
					((DeployPluginSudoScript) create).deploy(entityStore, ansibleCreds);
				} catch (AnsibleScriptCredentialsException e) {
					model.addAttribute("err", e);
					return "badansiblecreds";
				} catch (AnsiblePlaybookNotFoundException e) {
					model.addAttribute("exception", e);
					return "noansibleplaybook";
				} catch (AnsibleScriptException e) {
					addReportButton(selectedDep, model, pluginDir, unitTestRoot, Arrays.asList(), memail,
							Arrays.asList(outputCapture.getStdout().split("\\n")),
							Arrays.asList(outputCapture.getStderr().split("\\n")), true);
					return failedAdminpluginedBuilder(selectedDep, model, outputCapture, pluginDir, "",
							"Ansible script returned non zero status");
				} catch (AnsibleVaultDoesNotExistException e) {
					model.addAttribute("err", e);
					return "badansiblecreds";
				}
			} else {
				throw new RuntimeException("Unsupported deployment script: " + create);
			}
		} catch (FailedPluginTestsException e) {
			addReportButton(selectedDep, model, pluginDir, unitTestRoot, e.getFailedTests(), memail,
					Arrays.asList(outputCapture.getStderr().split("\\n")),
					Arrays.asList(outputCapture.getStdout().split("\\n")), true);
			return failedAdminpluginedBuilder(selectedDep, model, outputCapture, pluginDir, memail,
					"Some deployment tests failed:\n" + "\t" + String.join("\t", e.getFailedTests()));
		}

		model = outputCapture.addToModel(model);
		model.addAttribute("title", "Success. " + pluginDir.getName() + " deployed!");
		model.addAttribute("titleColor", "green");
		model.addAttribute("success", true);
		model.addAttribute("pluginname", pluginDir.getName());
		model.addAttribute("script", selectedDep);
		model.addAttribute("additionalMsg", "This plugin is now ready for use. Although tests have passed"
				+ ", it's good practice to check that it works manually");
		model.addAttribute("jobCompletedImgUrl", "/static/img/jobstate/green_tick.png");

		addReportButton(selectedDep, model, pluginDir, unitTestRoot, Arrays.asList(), memail,
				Arrays.asList(outputCapture.getStderr().split("\\n")),
				Arrays.asList(outputCapture.getStdout().split("\\n")), false);

		return "adminplugin";

	}

	private void addReportButton(String selectedDep, Model model, File pluginDir, File unitTestRoot,
			List<String> failedTests, String email, List<String> stderr, List<String> stdout, boolean hasFailed) {
		PluginReportContainer pluginReportContainer = new PluginReportContainer(pluginDir, unitTestRoot, failedTests,
				selectedDep, email, stderr, stdout);
		model.addAttribute("pluginReportContainer", pluginReportContainer);
		model.addAttribute("hasFailed", hasFailed);
	}

	private String failedAdminpluginedBuilder(String selectedDep, Model model, ModelOutputCapture outputCapture,
			File pluginDir, String memail, String message) {
		model.addAttribute("title", "Deployment Failed. " + pluginDir.getName() + " encountered errors");
		model.addAttribute("titleColor", "red");
		model.addAttribute("success", false);
		model.addAttribute("pluginname", pluginDir.getName());
		model.addAttribute("script", selectedDep);
		model.addAttribute("jobCompletedImgUrl", "/static/img/jobstate/cross.png");

		outputCapture.appendStderr(message, AdminController.class);
		outputCapture.addToModelFailPlugin(model, pluginDir.getName(), selectedDep, memail, "");
		return "adminplugin";
	}

	public String getMaintainersEmail(File pluginDir) throws IOException {
		String contactJsonAsString = FileUtils.readFileToString(new File(pluginDir, Contact.FILENAME), "UTF-8");
		Contact contact = new Gson().fromJson(contactJsonAsString, Contact.class);
		return contact.getEmail();
	}

	@RequestMapping(value = "/admin/plugin/undeploy", method = RequestMethod.GET)
	public String deleteDep(@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion, Model model)
			throws ParseException {

		/*
		 * Remove any jobs dependent on this
		 */
		Plugin plugin = KdcServerApplication.getInstance().getEntityStore()
				.findPluginFromPluginNameVersion(PluginNameVersion.from(pluginNameVersion));

		KdcServerApplication.getInstance().getEntityStore().removeJobsOfPlugin(plugin);
		KdcServerApplication.removeNormalPlugin(KdcServerApplication.getInstance().getEntityStore(), plugin);
		return "redirect:/admin";
	}

	@RequestMapping(value = "/admin/plugin/forcedeploy", method = RequestMethod.GET)
	public String forceDeploy(@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion, Model model)
			throws IOException, PluginAlreadyExistsInDatabaseException, FailedPluginTestsException,
			ScriptNotSupportedException {
		File pluginDir = new File(getPluginsDir(), pluginNameVersion);

		DeployPluginNoscriptNoTest deployPluginNoscript = new DeployPluginNoscriptNoTest(
				PluginFiles.readPluginFiles(pluginDir), System.out, System.err, Files.createTempDir(),
				KdcServerApplication.getInstance().getAdditionalVariables());
		deployPluginNoscript.deploy(KdcServerApplication.getInstance().getEntityStore());

		model.addAttribute("title", pluginDir.getName() + " deployment forced");
		model.addAttribute("titleColor", "orange");
		model.addAttribute("success", true);
		model.addAttribute("pluginname", pluginDir.getName());
		model.addAttribute("script", "");
		model.addAttribute("additionalMsg",
				"This plugin is now ready for use. A forced installation gives no guarantee of correct operation and so the plugin"
						+ " should be tested manually");
		model.addAttribute("jobCompletedImgUrl", "/static/img/jobstate/green_tick.png");

		return "adminplugin";
	}

	@RequestMapping(value = "/admin/plugin/testOnly", method = RequestMethod.GET)
	public String deployWithTestsOnly(@RequestParam(Plugin.PLUGIN_NAME) String pluginName) throws IOException {
		return "redirect:/admin/plugin/deploy/" + pluginName;
	}

	@RequestMapping(value = "/admin/plugin/update", method = RequestMethod.GET)
	public String pullRepo(@RequestParam(Plugin.PLUGIN_NAME_WITH_VERSION) String pluginNameVersion, Model model,
			Principal principal) throws ParseException {
		UtilServer.setBannerModel(model, principal);
		ModelOutputCapture outputCapture = new ModelOutputCapture();
		PluginGit git = new PluginGit();

		Git open;
		try {
			File pluginFolder = KdcServerApplication.getInstance().getFileStorage()
					.getPluginFolder(PluginNameVersion.from(pluginNameVersion));
			open = Git.open(pluginFolder);
			String f = open.getRepository().getConfig().getString("remote", "origin", "url");
			git.setGitUrl(f);
			model.addAttribute("pluginGit", git);
			return "pullrepo";
		} catch (IOException e) {
			outputCapture.appendStderr(e.getMessage(), AdminController.class);
		}

		outputCapture.addToModelFailPlugin(model, "", "", "", "");
		return "adminplugin";
	}

}
