/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.diversityarrays.fileutils.InvalidAccessException;

@Controller
public class FiletreeController {

	@RequestMapping("/filetree/java")
	@ResponseBody
	public String fileTreeJava(HttpServletRequest request, Principal principal)
			throws IOException, InvalidAccessException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(baos);

		String dir = request.getParameter("dir");

		String userId = KdcServerApplication.getInstance().getUserId(principal);
		File userDir = KdcServerApplication.getInstance().getFileStorage().getUserDir(userId);

		if (dir == null) {
			throw new NullPointerException("Parameter dir not provided");
		}

		if (userDir == null) {
			throw new NullPointerException("User directory is null");
		}

		if (!dir.startsWith(userDir.getPath())) {
			throw new InvalidAccessException("User " + userId + " does not have permission to access " + dir);
		}

		if (dir.charAt(dir.length() - 1) == '\\') {
			dir = dir.substring(0, dir.length() - 1) + "/";
		} else if (dir.charAt(dir.length() - 1) != '/') {
			dir += "/";
		}

		dir = java.net.URLDecoder.decode(dir, "UTF-8");

		if (new File(dir).exists()) {
			String[] files = new File(dir).list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.charAt(0) != '.';
				}
			});
			Arrays.sort(files, String.CASE_INSENSITIVE_ORDER);

			out.print("<ul class=\"jqueryFileTree\" style=\"display: none;\">");
			// All dirs
			for (String file : files) {
				if (new File(dir, file).isDirectory()) {
					out.print("<li class=\"directory collapsed\"><a href=\"#\" rel=\"" + dir + file + "/\">" + file
							+ "</a></li>");
				}
			}
			// All files
			for (String file : files) {
				if (!new File(dir, file).isDirectory()) {
					int dotIndex = file.lastIndexOf('.');
					String ext = dotIndex > 0 ? file.substring(dotIndex + 1) : "";
					out.print("<li class=\"file ext_" + ext + "\"><a href=\"#\" rel=\"" + dir + file + "\">" + file
							+ "</a></li>");
				}
			}
			out.print("</ul>");
		}
		String content = new String(baos.toByteArray(), StandardCharsets.UTF_8);
		return content;
	}

}
