/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.zeroturnaround.zip.ZipUtil;

import com.diversityarrays.kdcompute.admin.PluginReportContainer;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.email.BuggedPluginReport;
import com.diversityarrays.kdcompute.email.EmailSender;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.util.UtilServer;

/**
 * Hosts controllers for reporting plugins that failed to deploy, or jobs which
 * admins wish to email user on
 * 
 * @author andrewkowalczyk
 *
 */
@Controller
public class BuggedPluginReporterController {

	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private EmailSender emailSender;

	@Autowired
	@Value("${mail.maxAttachmentSizeKB}")
	private Long maxAttachmentSizeKB;

	@RequestMapping(value = "/emailplugin", method = RequestMethod.POST)
	public String DeploymentSuccess(@ModelAttribute PluginReportContainer pluginReportContainer,
			BindingResult bindingResult, Model model, Principal principal, RedirectAttributes redirectAttributes)
			throws IOException {
		String templateName = "email/contactTemplate";

		return fillEmailAndSend(pluginReportContainer, model, principal, templateName, "KDC Plugin Deployment");
	}

	@RequestMapping(value = "/buggedplugin", method = RequestMethod.POST)
	public String DeploymentFailed(@ModelAttribute PluginReportContainer pluginReportContainer,
			BindingResult bindingResult, Model model, Principal principal, RedirectAttributes redirectAttributes)
			throws IOException {
		String templateName = "email/failedDeploymentTemplate";

		return fillEmailAndSend(pluginReportContainer, model, principal, templateName, "KDC Failed Deployment Report");
	}

	private String fillEmailAndSend(PluginReportContainer pluginReportContainer, Model model, Principal principal,
			String templateName, String titlePrefix) throws IOException {
		UtilServer.setBannerModel(model, principal);

		/*
		 * Construct a submission form for admin to fill out. This page should
		 * include: send to whom: (plugin developer, but with optional override)
		 * text area to fill out additional message Listing of failed tests, and
		 * passed tests Elfinder connection to review files cc field, obtained
		 * from the principle. Use UtilServer.getEmail(principal)
		 */
		BuggedPluginReport buggedPluginReport = new BuggedPluginReport();
		model.addAttribute(buggedPluginReport);

		PluginFiles readPluginFiles = PluginFiles.readPluginFiles(pluginReportContainer.getPluginFiles());

		Plugin plugin = readPluginFiles.getPlugin();

		File testBundleFile = File.createTempFile("plugin_tests_", ".zip");
		ZipUtil.pack(pluginReportContainer.getUnitTestRoot(), testBundleFile);

		Context context = new Context();

		context.setVariable("title", readPluginFiles.getPlugin().getAlgorithmName());
		context.setVariable("pluginName", readPluginFiles.getPlugin().getPluginName());
		context.setVariable("pluginVersion", readPluginFiles.getPlugin().getVersion().toString());
		context.setVariable("osSystem", System.getProperty("os.name").toLowerCase());
		context.setVariable("osDistro", UtilServer.getOsDistro());
		context.setVariable("kdcVersion", KdcServerApplication.getInstance().getVersion());
		context.setVariable("senderDescription", pluginReportContainer.getDescription());
		context.setVariable("dependencyScript", pluginReportContainer.getSelectedDep());
		context.setVariable("stderr", pluginReportContainer.getStderr());
		context.setVariable("stdout", pluginReportContainer.getStdout());
		context.setVariable("failedtests", pluginReportContainer.getFailedTests());

		String body = templateEngine.process(templateName, context);

		List<Pair<String, Resource>> inlineResources = new ArrayList<Pair<String, Resource>>();
		inlineResources.add(new Pair<String, Resource>("bannerImage",
				new ClassPathResource("KDCompute_with_text.png", EmailSender.class)));

		List<Pair<String, File>> files = new ArrayList<Pair<String, File>>();
		List<File> failTestFiles = Arrays.asList(testBundleFile);
		Long totalKb = failTestFiles.stream().mapToLong(f -> f.length()).sum() / 1000;

		if (totalKb < maxAttachmentSizeKB) {
			failTestFiles.forEach(e -> {
				files.add(new Pair<String, File>(e.getName(), e));
			});
		}

		String emailcc = pluginReportContainer.getEmailcc();
		emailSender.send(pluginReportContainer.getEmail(),
				emailcc != null && !emailcc.isEmpty() ? Arrays.asList(emailcc.split(",")) : null,
				titlePrefix + ": " + plugin.getPluginNameAndVersion(), body, inlineResources, files);

		model.addAttribute("address", pluginReportContainer.getEmail());
		return "email/sendemail";
	}

	@RequestMapping(value = "/buggedpluginfilled", method = RequestMethod.POST)
	public String reportFailedDeploymentPost(@ModelAttribute BuggedPluginReport buggedPluginReport,
			@ModelAttribute File unitTestDir) {

		if (new EmailSender().enabled()) {
			// Use new EmailSender().send
		} else {
			// Provide details for manual emailing via copy and paste, with
			// links to zipped attachments
		}

		return null;
	}

	@RequestMapping(value = "/dev/buggedplugintemplate", method = RequestMethod.GET)
	public String reportFailedDeploymentPostA(Model model, BindingResult bindingResult, Principal principal,
			RedirectAttributes redirectAttributes) throws IOException {

		Plugin plugin = KdcServerApplication.getInstance().getEntityStore().getPlugins().get(0);

		String selectedDep = "dep script.yml";
		String email = "andrew@diversityarrays.com";
		java.lang.String emailcc = "";
		List<String> description = Arrays.asList("a description goes here");
		List<java.lang.String> failedTests = Arrays.asList("failed test");
		File pluginFiles = KdcServerApplication.getInstance().getFileStorage().getPluginFolder(plugin);
		File unitTestRoot = new File(
				"/var/folders/sz/r6j9xyk92c1f4kp64x3pxq7m0000h2/T/kdcompute_plugin_test8624436313267545205447384449245786");
		PluginReportContainer pluginReportContainer = new PluginReportContainer(pluginFiles, unitTestRoot, failedTests,
				selectedDep, email, emailcc, description, Arrays.asList("stderr", "newline"),
				Arrays.asList("stdout", "newline"));
		return DeploymentFailed(pluginReportContainer, bindingResult, model, principal, redirectAttributes);
	}

}
