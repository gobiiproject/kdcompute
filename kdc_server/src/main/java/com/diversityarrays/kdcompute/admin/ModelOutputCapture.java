/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.admin;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.ui.Model;

import com.diversityarrays.kdcompute.plugin.OutputCapture;

public class ModelOutputCapture extends OutputCapture{

	public Model addToModel(Model model) {
		List<String> stdoutSplit = Arrays.asList(super.getStdout().split("\n"));
		model.addAttribute("stdout_rows", stdoutSplit);
		List<String> split = Arrays.asList(super.getStderr().split("\n"));
		model.addAttribute("stderr_rows", split);
		return model;
	}
	
	public void addToModelFailPlugin(Model model,String pluginName, String script,String memail,String additionalMsg){
		addToModel(model);
		model.addAttribute("pluginName", pluginName);
		model.addAttribute("script", script);
		model.addAttribute("memail", memail);
		model.addAttribute("additionalMsg", additionalMsg);
	}
	
	public void appendStdout(String msg,Class<?> clazz ) {
		super.appendStdout(msg);
		Logger.getLogger(clazz.getName()).log(Level.INFO, msg);
	}

	public void appendStderr(String msg, Class<?> clazz   ) {
		super.appendStderr(msg);
		Logger.getLogger(clazz.getName()).log(Level.WARNING, msg);
	}



}
