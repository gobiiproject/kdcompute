/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.admin;

public class PluginGit {

	String gitUrl;
	String commitId;
	Boolean gitUsingCreds;
	String gitUsername;
	String gitPassword;
	
	
	
	public PluginGit() {
		super();
		gitUrl = "";
		commitId = "";
		gitUsingCreds = false;
		gitUsername = "";
		gitPassword = "";
	}



	public PluginGit(String gitUrl, String commitId, Boolean gitUsingCreds, String gitUsername, String gitPassword) {
		super();
		this.gitUrl = gitUrl;
		this.commitId = commitId;
		this.gitUsingCreds = gitUsingCreds;
		this.gitUsername = gitUsername;
		this.gitPassword = gitPassword;
	}



	public String getGitUrl() {
		return gitUrl;
	}



	public String getCommitId() {
		return commitId;
	}



	public Boolean getGitUsingCreds() {
		return gitUsingCreds;
	}



	public String getGitUsername() {
		return gitUsername;
	}



	public void setGitUrl(String gitUrl) {
		this.gitUrl = gitUrl;
	}



	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}



	public void setGitUsingCreds(Boolean gitUsingCreds) {
		this.gitUsingCreds = gitUsingCreds;
	}



	public void setGitUsername(String gitUsername) {
		this.gitUsername = gitUsername;
	}



	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}



	public String getGitPassword() {
		return gitPassword;
	}
	
	
}
