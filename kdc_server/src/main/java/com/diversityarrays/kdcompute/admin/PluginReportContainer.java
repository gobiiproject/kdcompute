/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.admin;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class PluginReportContainer {
	private File pluginFiles;
	private File unitTestRoot;
	private List<String> failedTests;
	private String selectedDep;

	private String email;
	private String emailcc;
	private List<String> description;

	private List<String> stdout;
	private List<String> stderr;

	public File getPluginFiles() {
		return pluginFiles;
	}

	public File getUnitTestRoot() {
		return unitTestRoot;
	}

	public PluginReportContainer() {
		super();
	}

	public List<String> getFailedTests() {
		return failedTests;
	}

	public String getSelectedDep() {
		return selectedDep;
	}

	public PluginReportContainer(File pluginFiles, File unitTestRoot, List<String> failedTests, String selectedDep,
			String email, List<String> stderr, List<String> stdout) {
		super();
		this.pluginFiles = pluginFiles;
		this.unitTestRoot = unitTestRoot;
		this.failedTests = failedTests;
		this.selectedDep = selectedDep;
		this.email = email;
		this.stdout = stdout;
		this.stderr = stderr;
	}

	public PluginReportContainer(File pluginFiles, File unitTestRoot, List<String> failedTests, String selectedDep,
			String email, String emailcc, List<String> description, List<String> stdout, List<String> stderr) {
		super();
		this.pluginFiles = pluginFiles;
		this.unitTestRoot = unitTestRoot;
		this.failedTests = failedTests;
		this.selectedDep = selectedDep;
		this.email = email;
		this.emailcc = emailcc;
		this.description = description;
		this.stdout = stdout;
		this.stderr = stderr;
	}

	public String getEmail() {
		return email;
	}

	public String getEmailcc() {
		return emailcc;
	}

	public List<String> getDescription() {
		return description;
	}

	public List<String> getStdout() {
		return stdout;
	}

	public List<String> getStderr() {
		return stderr;
	}

	public void setPluginFiles(File pluginFiles) {
		this.pluginFiles = pluginFiles;
	}

	public void setUnitTestRoot(File unitTestRoot) {
		this.unitTestRoot = unitTestRoot;
	}

	public void setFailedTests(List<String> failedTests) {
		this.failedTests = failedTests;
	}

	public void setSelectedDep(String selectedDep) {
		this.selectedDep = selectedDep;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmailcc(String emailcc) {
		this.emailcc = emailcc;
	}

	public void setDescription(List<String> description) {
		this.description = description;
	}

	public void setStdout(List<String> stdout) {
		this.stdout = stdout;
	}

	public void setStderr(List<String> stderr) {
		this.stderr = stderr;
	}

	public PluginReportContainer(File pluginFiles, File unitTestRoot, List<String> failedTests, String selectedDep,
			String email) {
		super();
		this.pluginFiles = pluginFiles;
		this.unitTestRoot = unitTestRoot;
		this.failedTests = failedTests;
		this.selectedDep = selectedDep;
		this.email = email;
		this.emailcc = "";
		this.description = Arrays.asList();
		this.stdout = Arrays.asList();
		this.stderr = Arrays.asList();
	}

	@Override
	public String toString() {
		return "pluginFiles=" + pluginFiles + "\nunitTestRoot=" + unitTestRoot + "\nfailedTests=" + failedTests
				+ "\nselectedDep=" + selectedDep + "\nemail=" + email + "\ndescription=" + description;
	}

}
