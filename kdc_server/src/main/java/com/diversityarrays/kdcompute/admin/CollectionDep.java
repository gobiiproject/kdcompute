/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.admin;

import java.util.List;
import java.util.Map;

public class CollectionDep {

	private Map<String, List<String>> pluginNames;

	private String ansibleVaultPath;
	private String ansibleVaultSecret;
	private List<String> collectionPlugins;

	public List<String> getCollectionPlugins() {
		return collectionPlugins;
	}

	public void setCollectionPlugins(List<String> collectionPlugins) {
		this.collectionPlugins = collectionPlugins;
	}

	public Map<String, List<String>> getPluginNames() {
		return pluginNames;
	}

	public void setPluginNames(Map<String, List<String>> pluginNames) {
		this.pluginNames = pluginNames;
	}

	public String getAnsibleVaultPath() {
		return ansibleVaultPath;
	}

	public void setAnsibleVaultPath(String ansibleVaultPath) {
		this.ansibleVaultPath = ansibleVaultPath;
	}

	public String getAnsibleVaultSecret() {
		return ansibleVaultSecret;
	}

	public void setAnsibleVaultSecret(String ansibleVaultSecret) {
		this.ansibleVaultSecret = ansibleVaultSecret;
	}

	public CollectionDep() {
		super();
	}

}
