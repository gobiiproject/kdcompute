//TODO write a jquery function to on every input element inside table and use the id to assign it a step value.
//TODO may be even change evalValRule to use the same approach

/*<![CDATA[*/

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].onclick = function() {
		this.classList.toggle("active");
		this.nextElementSibling.classList.toggle("show");
	}
}

var acc = document.getElementsByClassName("accordionVersion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].onclick = function() {
		this.classList.toggle("active");
		this.nextElementSibling.classList.toggle("show");
	}
}


var div = document.getElementById("plugins_listview");

function getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}

div.onclick = function(event) {
	var target = getEventTarget(event);
	var tagName = $(event.target).get(0).tagName;

	if (tagName == "A") {
		var context = getBaseUrl();//@{\/};
		var algoId = $(event.target).get(0).id;
		var url = context + 'showalgo/' + algoId;
		$('#algorithm_content').load(url);

		var helpUrl = 'plugin/help/' + algoId +'/';

		$('#includedContent').empty();
                //$('#includedContent').load(helpUrl);
                
		$('<iframe>', {
			src: helpUrl,
			frameborder: 0,
			scrolling: 'yes',
			style: 'border-style: solid',
		}).appendTo('#includedContent'); 

	}
};


/*]]>*/
