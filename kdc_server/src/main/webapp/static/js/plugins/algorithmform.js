/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
	document.getElementById("myDropdown").classList.toggle("show");
}

function replaceTargetWith( targetID, html ){
	/// find our target
	var i, div, elm, last, target = document.getElementById(targetID);
	/// create a temporary div
	div = document.createElement('td');
	/// fill that div with our html, this generates our children
	div.innerHTML = html;
	/// step through the temporary div's children and insertBefore our target
	i = div.childNodes.length;
	/// the insertBefore method was more complicated than I first thought so I 
	/// have improved it. Have to be careful when dealing with child lists as they 
	/// are counted as live lists and so will update as and when you make changes.
	/// This is why it is best to work backwards when moving children around, 
	/// and why I'm assigning the elements I'm working with to `elm` and `last`
	last = target;
	while(i--){
		target.parentNode.insertBefore((elm = div.childNodes[i]), last);
		last = elm;
	}
	/// remove the target.
	target.parentNode.removeChild(target);
}


var obj;
function parseJsonValidationRule(knobValidationRules) {
	allKnobs = knobValidationRules;
	obj = JSON.parse(allKnobs);
	findChoiceKnobs(obj);
}
function findValidationRuleByKnobName(knobName) {

	if (obj!=null && knobName in obj) {
		var result = obj[knobName];
		return result;
	}
	else  {
		return null;
	}


}


var shouldBeSubmitable = true;

function assignChangeListener(knobName) {
	// Only called by File Input Boxes.
	var inputBox = document.getElementById(knobName);
	var errorMessageId = "message" + knobName;
	var errorInputBox = document.getElementById(errorMessageId);

	var text = inputBox.value;

	if (text) {
		errorInputBox.innerText = "";
	}
	validateForSubmitButton();
}

function hideAllKDCDuelFileModals(){
	for (e of document.getElementsByClassName("modal")) {
		e.style.display = "none";
	}
}



function assignKDCDuelFileListerner(filetreeurl,knobName) {
	var modal = document.getElementById(knobName+'_fileUploadMyModal');
	var btn = document.getElementById(knobName+"_fileUploadMyBtn");
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal 
	btn.onclick = function() {
		fileTree(
				'#'+knobName+'_fileTree',
				rootDir,
				filetreeurl,
				function(file) {
					var fileshort = file.replace(rootDir+"/","").replace(rootDir+"\\","");
					setFileTreeModalServerFile(
							knobName,
							knobName+'_fileUploadMyModal',
							knobName+'_fileUploadMyBtnText',
							fileshort,
							file);
				});

		modal.style.display = "block";
	}

	$(window).click(function(event) {
		if (event.target == modal) {
			hideAllKDCDuelFileModals();
		}
	});
}

function assignKDCChangeListenerModal(knobName) {

	var modalId = knobName+'_fileUploadMyModal';
	var modalText = knobName+'_fileUploadMyBtnText';

	// Only called by File Input Boxes.
	var inputBox = document.getElementById(knobName);
	var errorMessageId = "message" + knobName;
	var errorInputBox = document.getElementById(errorMessageId);

	var text = inputBox.value;

	if (text) {
		errorInputBox.innerText = "";
	}

	document.getElementById(modalId).style.display = "none";

	document.getElementById(modalText).innerHTML = text;

	validateForSubmitButton();
}

function setFileTreeModalServerFile(knobName,modalId,modalText,fileshort,file){
	var inputBox = document.getElementById(knobName);
//	inputBox.setAttribute('type', 'button');
//	inputBox.value = file;

	var parent = document.getElementById(knobName).parentNode;
	parent.id = "kdcompute_text_datatype";
	replaceTargetWith(knobName, "<input id='" + knobName + "' "
			+ " type=\"text\" value=\"" + file + "\" "
			+ "oninput=\"assignChangeListener('" + knobName + "');\""
			+ "name=\"" + knobName + "\""
			+ " readonly=\"readonly\" class=\"knobE\" />");
	fireEventChangeKnob(knobName);


	var errorMessageId = "message" + knobName;
	var errorInputBox = document.getElementById(errorMessageId);
	errorInputBox.innerText = "";

	document.getElementById(modalId).style.display = "none";

	document.getElementById(modalText).innerHTML = fileshort;

	validateForSubmitButton();
}

function validateForSubmitButton() {
	// Initial check for submit button based on Validation Rule and on every input on input boxes.
	var submitButtonId = "submit_button";
	var submitButton = document.getElementById(submitButtonId);
	shouldBeSubmitable = true;

	$("input")
	.each(
			function() {

				var knobName = this.id;
				var isKnob = findValidationRuleByKnobName(knobName);

				if (isKnob != null) {
					var isKnobInputBox = document
					.getElementById(knobName);
					var errorMessageInputId = "message"
						+ knobName;

					var errorMessageInput = document
					.getElementById(errorMessageInputId);

					var errorValue = errorMessageInput.innerText;
					var knobInputBoxValue = isKnobInputBox.value;

					var isValueNeeded = isKnob.isKnobValueNeeded;

					if (isValueNeeded === true) {
						if (!knobInputBoxValue) {
							errorMessageInput.innerText = "Value Needed";
							shouldBeSubmitable = false;
						}
					}

					if (typeof variable !== 'undefined'
						|| errorValue !== 'undefined' && errorValue && 0 !== errorValue.length) {
						shouldBeSubmitable = false;
					}



				}
			});
	if (shouldBeSubmitable === false) {
		submitButton.disabled = true;
	} else {
		submitButton.disabled = false;
	}

}

//window.onload = function(){
//$('#algorithm_content').bind("DOMSubtreeModified",function(){
//bindKnobE();    
//});
//}

MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

var observer = new MutationObserver(function(mutations, observer) {
	bindKnobE();   
});

//define what element should be observed by the observer
//and what types of mutations trigger the callback
observer.observe(document, {
	subtree: true,
	attributes: true
	//...
});

function updateKnobE(){
	var submitButtonId = "submit_button";
	var submitButton = document
	.getElementById(submitButtonId);
	var knobName = this.id;
	var ifKnob = findValidationRuleByKnobName(knobName);

	if (ifKnob != null) {
		var input = document.getElementById(knobName);

		var errorMessageInputId = "message" + knobName;
		var errorMessageAsterick = "warning" + knobName;
		var inputBox = document
		.getElementById(knobName);

		var shouldBeSubmitable = true;

		var knobDataType = ifKnob.knobDataType;
		var enteredValue = input.value;

		if (knobDataType === "INTEGER") {
			var result = integerEval(ifKnob,
					enteredValue);

			if (result) {
				document
				.getElementById(errorMessageAsterick).innerHTML = "*";
				inputBox.style.color = "red";
			} else {
				document
				.getElementById(errorMessageAsterick).innerHTML = "";
				inputBox.style.color = "black";
			}

			document
			.getElementById(errorMessageInputId).innerHTML = result;

		} else if (knobDataType === "DECIMAL") {
			var result = decimalEval(ifKnob,
					enteredValue);

			if (result) {
				document
				.getElementById(errorMessageAsterick).innerHTML = "*";
				inputBox.style.color = "red";
			} else {
				document
				.getElementById(errorMessageAsterick).innerHTML = "";
				inputBox.style.color = "black";
			}

			document
			.getElementById(errorMessageInputId).innerHTML = result;

		}
	}

	// We check every input box on any input box entry.
	validateForSubmitButton();
}

function bindKnobE(){
	var elements = document.getElementsByClassName("knobE");
	for (var i = 0 ; i < elements.length; i++) {
		elements[i].addEventListener('change',updateKnobE);
	}
}

function findChoiceKnobs(jsonKnobMap) {

	for ( var key in jsonKnobMap) {
		if (jsonKnobMap.hasOwnProperty(key)) {
			valRule = jsonKnobMap[key];

			if (valRule.knobDataType == "CHOICE") {

				var inputBox = document.getElementById(key);
				var divBox = document.getElementById(key).parentElement;
				inputBox.className += " knobE";

				choices = valRule.choices;
				if (choices.length == 1) {
					var label = document.createElement('label');
					inputBox.type = "checkbox";
					var value = choices[0];
					inputBox.value = value;
					inputBox.name = key;

					// No need of a label if only one choice.
					/* 							label.htmlFor = key;
							 label.appendChild(document.createTextNode(value));
							 divBox.appendChild(label); */

				} else if (choices.length < 4) {
					$(inputBox).remove();
					for (i = 0; i < choices.length; i++) {
						var label = document.createElement('label');
						var newInputBox = document
						.createElement("INPUT");
						newInputBox.id = (key + i);
						newInputBox.type = "radio";
						newInputBox.value = choices[i];
						newInputBox.name = key; //knobName
						newInputBox.className += " knobE";

						label.htmlFor = (key + i);
						label.appendChild(document
								.createTextNode(choices[i]));
						divBox.appendChild(newInputBox);
						divBox.appendChild(label);
					}
					var defaultVal = valRule.defaultVal;
					var allRadios = document.getElementsByName(key);

					if (defaultVal) {
						for (i = 0; i < allRadios.length; i++) {
							if (allRadios[i].value.toLowerCase() == defaultVal
									.toLowerCase()) {
								var radio = allRadios[i];
								radio.checked = true;
							}
						}
					} else {
						var firstRadio = allRadios[0];
						firstRadio.checked = true;
					}

				} else if (choices.length > 4) {
					$(inputBox).remove();
					var selectList = document.createElement("select");
					selectList.id = key;
					selectList.name = key;
					selectList.className += " knobE";

					for (var i = 0; i < choices.length; i++) {
						var option = document.createElement("option");
						option.value = choices[i];
						option.text = choices[i];
						selectList.appendChild(option);
					}
					divBox.appendChild(selectList);
				}
			}
		}
	}
}

function getEventTarget(e) {
	e = e || window.event;
	return e.target || e.srcElement;
}


$("#algorithm_content_body").on("focus", "input", function() {

	var knobName = this.id;
	var ifKnob = findValidationRuleByKnobName(knobName);

	if (ifKnob != null) {

		var knobDataType = ifKnob.knobDataType;
		var incrementVal = ifKnob.incrementVal;

		if (knobDataType === "DECIMAL") {
			//this is definately an knob input box
			this.step = incrementVal;
		} else if (knobDataType === "INTEGER") {
			this.step = parseInt(incrementVal, 10);
		}
	}
});



function decimalEval(thisKnobValRule, enteredValue) {
	var result = false;

	var includeMin = thisKnobValRule.isMinIncluded;
	var includeMax = thisKnobValRule.isMaxIncluded;
	var min = thisKnobValRule.min;
	var max = thisKnobValRule.max;

	var shouldCompareMin = thisKnobValRule.shouldCompareMin;
	var shouldCompareMax = thisKnobValRule.shouldCompareMax;

	if (shouldCompareMin === true && shouldCompareMax === true) {
		return compareBoth(min, max, includeMin, includeMax,
				enteredValue);
	} else if (shouldCompareMin === false && shouldCompareMax === true) {
		return compareMax(max, includeMax, enteredValue);
	} else if (shouldCompareMin === true && shouldCompareMax === false) {
		return compareMin(min, includeMin, enteredValue);
	}
}

function integerEval(thisKnobValRule, enteredValue) {
	var result = false;

	var includeMin = thisKnobValRule.isMinIncluded;
	var includeMax = thisKnobValRule.isMaxIncluded;
	var min = thisKnobValRule.min;
	var max = thisKnobValRule.max;

	var intMin = parseInt(min, 10);
	var intMax = parseInt(max, 10);
	var shouldCompareMin = thisKnobValRule.shouldCompareMin;
	var shouldCompareMax = thisKnobValRule.shouldCompareMax;

	if (shouldCompareMin === true && shouldCompareMax === true) {
		return compareBoth(intMin, intMax, includeMin, includeMax,
				enteredValue);
	} else if (shouldCompareMin === false && shouldCompareMax === true) {
		return compareMax(intMax, includeMax, enteredValue);
	} else if (shouldCompareMin === true && shouldCompareMax === false) {
		return compareMin(intMin, includeMin, enteredValue);
	}

}

function compareBoth(min, max, includeMin, includeMax, enteredValue) {

	switch (includeMin) {
	case true:
		switch (includeMax) {
		case true:
			if (min <= enteredValue && enteredValue <= max) {
				result = true;
			} else {
				result = false;
			}
			break;
		case false:
			if (min <= enteredValue && enteredValue < max) {
				result = true;
			} else {
				result = false;
			}
			break;
		}
		break;
	case false:
		switch (includeMax) {
		case true:
			if (min < enteredValue && enteredValue <= max) {
				result = true;
			} else {
				result = false;
			}
			break;
		case false:
			if (min < enteredValue && enteredValue < max) {
				result = true;
			} else {
				result = false;
			}
			break;
		}
		break;

	}

	var errorMessage = "";
	switch (result) {
	case false:
		errorMessage = "Invalid value " + enteredValue;
		break;
	case true:
		errorMessage = "";
		break;
	}
	return errorMessage;
}

function compareMin(min, includeMin, enteredValue) {

	switch (includeMin) {
	case true:
		if (min <= enteredValue) {
			result = true;
		} else {
			result = false;
		}
		break;
	case false:
		if (min < enteredValue) {
			result = true;
		} else {
			result = false;
		}
		break;
	}

	var errorMessage = "";

	switch (result) {

	case false:
		errorMessage = "Invalid value " + enteredValue;
		break;
	case true:
		errorMessage = "";
		break;
	}

	return errorMessage;
}

function compareMax(max, includeMax, enteredValue) {

	switch (includeMax) {
	case true:
		if (enteredValue <= max) {
			result = true;
		} else {
			result = false;
		}
		break;
	case false:
		if (enteredValue < max) {
			result = true;
		} else {
			result = false;
		}
		break;
	}

	var errorMessage = "";

	switch (result) {

	case false:
		errorMessage = "Invalid value " + enteredValue;
		break;
	case true:
		errorMessage = "";
		break;
	}

	return errorMessage;
}
