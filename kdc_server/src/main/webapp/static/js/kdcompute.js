/**
  * 
  * KDCompute core Javascript file
  * Requires jQuery and jQuery UI
  * 
  * Written by Stanley Wijoyo 
  * Copyright (c) 2013, Diversity Arrays Pty Ltd
  *
*/

// Global Variables
var json_args_arr = {};
var json_algo_data = {};
var dal_base_url = "";
var kdc_base_uri = "";
var algo_selection_object = {};
var algo_group_idx = {};
var algo_selection_html_content = '<ul><li class="ui-widget-header">List of algorithms by groups</li>';
var algo_list_content = '';
var group_list = '';
var url_regex = /<%|%>/;

$.ajaxSetup({
  
    timeout: 60000 // 60 seconds timeout for any ajax call
});

function KDC_isEnter(e, func) {

      var keynum;

      if (window.event) // IE
      {
	keynum = e.keyCode;
      }
      else if(e.which) // Netscape/Firefox/Opera
      {
	keynum = e.which;
      }

      if (keynum == 13)
      {
	func();
	return false;
      }
      else
      {
	return true;
      }
}

function KDC_sortObject(o) {
  
    var sorted = {},
    key, a = [];

    for (key in o)
    	if (o.hasOwnProperty(key)) a.push(key);

    a.sort();

    for (key = 0; key < a.length; key++)
    	sorted[a[key]] = o[a[key]];
    
    return sorted;
}

function KDC_submitLogin() {

      var logon_name = document.login_form_local.username_local.value;
      var plain_pass = document.login_form_local.password_local.value;  
      var r = document.login_form.rand.value;

      if (logon_name.length == 0) {

	  alert("Username is missing.");
      }
      else {

	  logon_name = logon_name.toLowerCase();  
	  document.login_form.username.value = logon_name;

	  if (plain_pass.length > 0) {

	      var pass_hash = hex_hmac_sha1(plain_pass, logon_name);
	      var final_pass = hex_hmac_sha1(pass_hash, r);

	      document.login_form.password.value = final_pass;
	      document.login_form.submit();
	  }
	  else {

	      alert("Password is missing");
	  }
      }
}

function KDC_submitLoginSimple() {

      var logon_name = document.login_form_local.username_local.value;
      var plain_pass = document.login_form_local.password_local.value;  
      var r = document.login_form.rand.value;

      if (logon_name.length == 0) {

	  alert("Username is missing.");
      }
      else {

	  // logon_name = logon_name.toLowerCase(); # CASE SENSITIVE! NOT ALWAYS LOWER CASE 
	  document.login_form.username.value = logon_name;

	  if (plain_pass.length > 0) {

	      // We do not need hmac sha1 algorithm for DArTdb Rest login
	      // var pass_hash = hex_hmac_sha1(plain_pass, logon_name);
	      // var final_pass = hex_hmac_sha1(pass_hash, r);

	      document.login_form.password.value = plain_pass;
	      document.login_form.submit();
	  }
	  else {

	      alert("Password is missing");
	  }
      }
}

function KDC_switchGroup(id, dal_url, base_uri) {
  
      kdc_base_uri = base_uri;
      
      var html_content = "<form name='switch_group_form' method='post' action='" + base_uri + "/switchgroup'>";
      html_content += "<div class='form-container'><strong>Select a group</strong>: &nbsp;";
      html_content += "<select id='groupid' name='groupid' class='option-list'>";
      
      $.getJSON(dal_url, function(json_data) {

	  var group_list = json_data.SystemGroup;
	  for (var i=0; i < group_list.length; i++) {
	    
	      html_content += '<option id='+ group_list[i].SystemGroupId +' value="'+ group_list[i].SystemGroupId +'">'+ group_list[i].SystemGroupName +'</option>';      
	  }
	  html_content += '</select>';
	  html_content += '<p><input type="submit" class="btn" value="Switch"></p></div></form>'
	  
	  $("#" + id).html(html_content);
	  $("#" + id).hide();

	  var current_url = document.URL;
	  var regex_link = /\?/;
	  
	  if (regex_link.test(current_url)) {
	    
	      var arr = current_url.split("?");
	      var switch_group_msg = arr.pop();
	      var regex_switch_grp = /true$/;
	      
	      if (regex_switch_grp.test(switch_group_msg)) {
		
		  KDC_dispSwitchGroupUI();
	      }
	  }
	  
      });
}

function KDC_constructCurrentBaseUrl() {
  
    var base_url = window.location.protocol + "//" + window.location.host + "/";
    
    if (window.location.pathname != "") {
      
	var path_arr = window.location.pathname.split('/');
	
	if (path_arr.length > 2) { // only add second level location if there are multiple level of locations
	  
	    var second_level_location = path_arr[1];
	    
	    base_url += second_level_location + "/";
	}
    }
    
    return base_url;
}

function KDC_parseJSON(json_data) {
  
    var return_val = "";
    
    if (json_data == "") {
     
	return_val = '<span style="color:red;">Please enter a JSON data!</span>'
    }
    else {
      
	try {
	  
	    var parsed_data = $.parseJSON(json_data);
	    
	    if (parsed_data === null) 
	      
		return_val = '<span style="color:red;">There is a syntax error in your JSON data.</span>';
	    
	    else {
	      
		var json_args_check_msg = "";
		
		if (typeof parsed_data.AlgorithmName === 'undefined') json_args_check_msg += "AlgorithmName is missing<br />";
		if (typeof parsed_data.AlgorithmDescription === 'undefined') json_args_check_msg += "AlgorithmDescription is missing<br />";
		if (typeof parsed_data.AlgorithmGroup === 'undefined') json_args_check_msg += "AlgorithmGroup is missing<br />";
		if (typeof parsed_data.AlgorithmHelp === 'undefined') json_args_check_msg += "AlgorithmHelp is missing<br />";
		if (typeof parsed_data.AlgorithmVersion === 'undefined') json_args_check_msg += "AlgorithmVersion is missing<br />";
		if (typeof parsed_data.Author === 'undefined') json_args_check_msg += "Author is missing<br />";
		if (typeof parsed_data.LastUpdate === 'undefined') json_args_check_msg += "LastUpdate is missing<br />";
		if (typeof parsed_data.BashTmplPath === 'undefined') json_args_check_msg += "BashTmplPath is missing<br />";
		if (typeof parsed_data.FormType === 'undefined') json_args_check_msg += "FormType is missing<br />";
		if (typeof parsed_data.FormTmplPath === 'undefined') json_args_check_msg += "FormTmplPath is missing. If you select form type as \"dynamic\", please put \"none\" as the value here.<br />";
		
		if (typeof parsed_data.Args === 'undefined') {
		  
		    json_args_check_msg += "Args is missing. Each JSON specification file must have at least one argument<br />";
		}
		else {
		  
		    var form_type = "";
		    if (typeof parsed_data.FormType !== 'undefined') form_type = parsed_data.FormType;
		   
		    if (form_type == "dynamic") {
		      
			for (var i = 0; i < parsed_data.Args.length; ++i) {
			  
			    if (typeof parsed_data.Args[i].ArgName === 'undefined') json_args_check_msg += "ArgName is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].ArgLabel === 'undefined') json_args_check_msg += "ArgLabel is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].DataType === 'undefined') json_args_check_msg += "DataType is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].HTMLType === 'undefined') json_args_check_msg += "HTMLType is missing in Args number: " + i + "<br />";
			    
			    if (typeof parsed_data.Args[i].SrcType === 'undefined') 
				json_args_check_msg += "SrcType is missing in Args number: " + i + "<br />";
			    
			    else {
			      
				if (parsed_data.Args[i].SrcType == "local" || parsed_data.Args[i].SrcType == "external")		    
				    
				    continue;
				else
				  
				    json_args_check_msg += "SrcType: \"" + parsed_data.Args[i].SrcType + "\" is not recognised by KDCompute. It has to be either \"local\" or \"external\" in Args number: " + i + "<br />";
			    }
			    if (typeof parsed_data.Args[i].HTMLSrcId === 'undefined') json_args_check_msg += "HTMLSrcId is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].DefaultValue === 'undefined') json_args_check_msg += "DefaultValue is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].TmplVarName === 'undefined') json_args_check_msg += "TmplVarName is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].Validation === 'undefined') json_args_check_msg += "Validation is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].Required === 'undefined') json_args_check_msg += "Required is missing in Args number: " + i + "<br />";
			    
			    json_args_check_msg += "<br />";
			}
		    }
		    else if (form_type == "template") {
		      
			for (var i = 0; i < parsed_data.Args.length; ++i) {
			  
			    if (typeof parsed_data.Args[i].ArgName === 'undefined') json_args_check_msg += "ArgName is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].ArgLabel === 'undefined') json_args_check_msg += "ArgLabel is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].DataType === 'undefined') json_args_check_msg += "DataType is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].TmplVarName === 'undefined') json_args_check_msg += "TmplVarName is missing in Args number: " + i + "<br />";
			    if (typeof parsed_data.Args[i].Required === 'undefined') json_args_check_msg += "Required is missing in Args number: " + i + "<br />";
			    
			    json_args_check_msg += "<br />";
			}
		    }
		    else {
		       
			if (form_type != "")
			    json_args_check_msg += "Unrecognize form type: \"" + form_type + "\". Form type has to be either \"dynamic\" or \"template\"<br />";
		    }
		}
		
		(json_args_check_msg != "") ? return_val = "<span style=\"color:red;\">There are errors in your JSON specification file: <br /><br />" + json_args_check_msg + "</span>" : return_val = '<span style="color:green;">JSON Syntax OK.</span>';
	    }
	}
	catch(e) {
	  
	    return_val = '<span style="color:red;">There is a syntax error in your JSON data.</span>';
	}
    }
    return return_val;
}


function KDC_buildExtSelectList(json_form_data, id) {
  
      $('#select_list_'+id).removeClass("wait");
      $('#select_list_'+id).html(json_form_data);
}

function KDC_buildTmplForm(tmpl_data) {
  
      $('#ext_tmpl').removeClass("wait");
      $('#ext_tmpl').html(tmpl_data);
}

function KDC_getHTMLDispFormat(arr) {
  
      var output = "";
      
      for (var i=0; i < arr.length; i++) {
	
	  output += arr[i].FieldName + " ";
	  
	  if ( typeof arr[i].Separator !== 'undefined' ) {
	    
	      output += arr[i].Separator + " ";
	  }
      }
      
      return output;
}

function KDC_getHTMLText(disp_tags_arr, disp, json) {
  
      var HTMLoutput = disp;
        
      for (var i=0; i < disp_tags_arr.length; i++) {
	
	  var field_name = disp_tags_arr[i].FieldName;
	  HTMLoutput = HTMLoutput.replace(field_name, json[field_name]);
      }
      
      return HTMLoutput;
}

function KDC_constructUrl(raw_url, arg_name, tag_id) {
  
      var pattern = "<%" + arg_name + "%>";
      var new_url = raw_url.replace(pattern, tag_id);
      
      return new_url;
}

function KDC_dispUserFiles(file_data, id) {
  
      $('#file_list_' + id).removeClass("wait");
      $('#file_list_' + id).html(file_data);
}

function KDC_buildChildForm(tag_id, child_arr, opt_id, parent_arg_name) {
 
      for (var i=0; i < child_arr.length; i++) {
	
	  // Work with the local `i` rather than the shared `i` from the loop
	  // getJSON Async call will finish later than the loop, thus doing this is needed
	  
	  (function(i) {
	    
	      var child_arg_name = child_arr[i].ArgName;
	      var arr_index = 0;
	      
	      // Find the index of the child element
	      for (var j=0; j < json_args_arr[opt_id].length; j++) {
		
		  if (json_args_arr[opt_id][j].ArgName == child_arg_name) {
		    
		      arr_index = j;
		  }
	      }

	      var raw_list_url = json_args_arr[opt_id][arr_index].SrcURL;
	      var list_url = KDC_constructUrl(raw_list_url, parent_arg_name, tag_id);
	      var json_dal_url = dal_base_url + "/" + list_url;
	      var html_type = json_args_arr[opt_id][arr_index].HTMLType;
	      var src_id = json_args_arr[opt_id][arr_index].HTMLSrcId;
	      var src_disp = json_args_arr[opt_id][arr_index].HTMLSrcDisplay;
	      var default_value = json_args_arr[opt_id][arr_index].DefaultValue;
	      var display_format = KDC_getHTMLDispFormat(src_disp);

	      $.getJSON(json_dal_url, function(json_data) {
		
		  var tag_name = json_data.RecordMeta[0].TagName;
		  var list_content = '<span class="form-field">';	      
		    
		  if (html_type == "dropdownlist") {
		  
		      list_content += '<select id="' + child_arg_name + '" name="' + child_arg_name + '" class="option-list">';
		  }
		  else if (html_type == "multilist") {
		   
		      list_content += '<select id="' + child_arg_name + '" name="' + child_arg_name + '" class="option-list" multiple>';
		  }
		  
		  if (default_value == "") {
		      
		      list_content += '<option id="none" value="" selected="selected"></option>';
		  }
		  for (var j=0; j < json_data[tag_name].length; j++) {
	
		      list_content += '<option id='+ j; 
		      list_content += ' value="'+ json_data[tag_name][j][src_id] +'">'; 
		      
		      var htmlText = KDC_getHTMLText(src_disp, display_format, json_data[tag_name][j]);
		      list_content += htmlText +'</option>';				  
		  }
		  list_content += '</select>';
		  list_content += '<span class="label-error" id="' + json_args_arr[opt_id][i].ArgName + '_errmsg"></span>';
		  list_content +='</span></div>';
		  KDC_buildExtSelectList(list_content, child_arg_name);
	      });
	  })(i);
      }
}

function KDC_updateAlgoSelection(grp_name) {
      
      $('#algorithm_form_content').html(" ");
      $('#algo_selection').empty().append('<option id="none" value="" selected="selected">Select an algorithm... </option>');
      
      for (var i=0; i < json_algo_data.length; i++) {
	
	  if (grp_name == "All") {
	    
	      var opt = '<option id='+ i +' value="'+ json_algo_data[i].AlgorithmName +'">'+ json_algo_data[i].AlgorithmName +'</option>';
	      $('#algo_selection').append(opt); 
	  }
	  else { 
	    
	      if (json_algo_data[i].AlgorithmGroup == grp_name) {
	    
		  var opt = '<option id='+ i +' value="'+ json_algo_data[i].AlgorithmName +'">'+ json_algo_data[i].AlgorithmName +'</option>';
		  $('#algo_selection').append(opt);
	      }
	  }
      }
}

function KDC_buildAlgoGroupSelection() {
  
      var get_url = window.location.protocol + "//" + window.location.host + "/getalgogroup";
      group_list += '<select name="algo_group" class="option-list" onChange="KDC_updateAlgoSelection(options[selectedIndex].value);">';
      group_list += '<option id="none" value="All" selected="selected">All</option>';
      
      $.getJSON(get_url, function(json_data) {
	
	  for (var i=0; i < json_data.length; i++) {
	    
	      group_list += '<option id='+ i +' value="'+ json_data[i].GroupName +'">'+ json_data[i].GroupName +'</option>';	      
	  }
	  group_list += '</select>';
	  $('#algorithm_group').html(group_list);
      });
}

function KDC_showHideAlgoContent(grp_id) {
  
    $('#grp_ul' + grp_id).toggle('fast');
}

function KDC_buildAlgoPanel(obj, val) {
  
    if ($.isEmptyObject(obj)) {
      
	(val == "") ? $('#algo_list').html(algo_list_content) : $('#algo_list').html("<p align='justify' style='padding-left:3px;'>No algorithm found for <strong>" + val + "</strong></p>");
    }
    else {
      
      	if (val == "") {
      
	    $('#algo_list').html(algo_list_content);
	}
	else {
	  
	    var search_result_html = "";
	    
	    for (var algo_grp_name in obj) {
		  
		  search_result_html += '<li id="grp' + algo_group_idx[algo_grp_name] + '" class="algo-list-block">';
		  search_result_html += '<a class="widget-link" onclick="KDC_showHideAlgoContent(' + algo_group_idx[algo_grp_name] + '); return false;" href="#">' + algo_grp_name + '</a>';
		  search_result_html += '<ul id="grp_ul' + algo_group_idx[algo_grp_name] + '" class="grp_list">';
		  
		  for (var i = 0; i < obj[algo_grp_name].length; ++i) {
		    
		      // We do not want the id to have <span> tag, so remove them
		      var id = obj[algo_grp_name][i].replace('<span style="background-color:yellow">', '');
		      id = id.replace('</span>', '');
		      
		      search_result_html += '<li><a href="#" id="' + id + '">' + obj[algo_grp_name][i] + '</a></li>';
		  }
		  
		  search_result_html += '</ul></li>'; 
	    }
	    
	    // JS script to add an icon to clear text
	    search_result_html += '<script>';
	    search_result_html += 'jQuery(function($) {';
	    search_result_html += 'function tog(v){return v?"addClass":"removeClass";}';
	    search_result_html += '$(document).on("input", ".algo-search-box", function(){';
	    search_result_html += '$(this)[tog(this.value)]("x");';
	    search_result_html += '}).on("mousemove", ".x", function( e ){';
	    search_result_html += '$(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]("onX");';   
	    search_result_html += '}).on("click", ".onX", function(){';
	    search_result_html += '$(this).removeClass("x onX").val("");';
	    search_result_html += 'KDC_searchAlgoName($(".algo-search-box").val())';
	    search_result_html += '});';
	    search_result_html += '});';
	    
	    // JS script to change the background when user selects an element
	    search_result_html += '$(".grp_list li a").on("click", function(e) {';
	    search_result_html += '$(".grp_list li").removeClass("algo-selected");';
	    search_result_html += '$(this).parent("li").addClass("algo-selected");';
	    search_result_html += 'KDC_buildAlgoForm(e.target.id);';
	    search_result_html += 'e.preventDefault();';
	    search_result_html += '});';
	    
	    // JS script to change search-box look on focus
	    search_result_html += '$(function() {';
	    search_result_html += '$(".algo-search-box").focus(function() {';
	    search_result_html += '$(this).addClass("algo-search-box-focused")';
	    search_result_html += '}).blur(function(){';
	    search_result_html += '$(this).removeClass("algo-search-box-focused")';
	    search_result_html += '});';
	    search_result_html += '});</script>';
	    
	    $('#algo_list').html(search_result_html);
	}
    }
}

function KDC_searchAlgoName(val) {

    var return_obj = {};
    var regex_val = new RegExp(val, 'i');
    
    for (var algo_grp_name in algo_selection_object) {
        
	for (var i = 0; i < algo_selection_object[algo_grp_name].length; ++i) {
	 
	    if ( regex_val.test(algo_selection_object[algo_grp_name][i]) ) {
	      
		var algo_name = algo_selection_object[algo_grp_name][i];
		var match_arr = algo_name.match(regex_val);
		
		for (var j = 0; j < match_arr.length; ++j) {
		  
		    var replacement_text = '<span style="background-color:yellow">' + match_arr[j] + '</span>';
		    algo_name = algo_name.replace(match_arr[j], replacement_text);
		}
		
		if ( !return_obj.hasOwnProperty(algo_grp_name) ) {
		  
		    return_obj[algo_grp_name]= [];
		    return_obj[algo_grp_name].push(algo_name);
		}
		else
		    return_obj[algo_grp_name].push(algo_name);
	    }
	}
    }
    
    KDC_buildAlgoPanel(return_obj, val);
}

function KDC_buildAlgoSelection(json_url, dal_url) {
  
      dal_base_url = dal_url;
      
      $.getJSON(json_url, function(json_data) {
	
	  algo_selection_html_content += '<li id="search_box" class="first-item"><input type="text" class="algo-search-box" placeholder="Search for algorithm name here" maxlength="30" onkeyup="KDC_searchAlgoName(this.value);"></li>';
	  algo_selection_html_content += '<span id="algo_list">';
	  
	  for (var i=0; i < json_data.length; i++) {
	    
	      if ( !algo_selection_object.hasOwnProperty(json_data[i].AlgorithmGroup) ) {
		
		  algo_selection_object[json_data[i].AlgorithmGroup]= [];
		  algo_selection_object[json_data[i].AlgorithmGroup].push(json_data[i].AlgorithmName);
	      }
	      else	
		  algo_selection_object[json_data[i].AlgorithmGroup].push(json_data[i].AlgorithmName);

	      json_args_arr[json_data[i].AlgorithmName] = json_data[i].Args;
	      json_algo_data[json_data[i].AlgorithmName] = json_data[i];	      
	  }
	  
	  algo_selection_object = KDC_sortObject(algo_selection_object);
	  
	  var grp_id = 0;
	  
	  for (var algo_grp_name in algo_selection_object) {
	    
	      algo_group_idx[algo_grp_name] = grp_id; // set group id for each algorithm group for searching
	      
	      algo_selection_html_content += '<li id="grp' + grp_id + '" class="algo-list">';
	      algo_selection_html_content += '<a class="widget-link" onclick="KDC_showHideAlgoContent(' + grp_id + '); return false" href="#">' + algo_grp_name + '</a>';
	      algo_selection_html_content += '<ul id="grp_ul' + grp_id + '" class="grp_list">';
	      
	      algo_list_content += '<li id="grp' + grp_id + '" class="algo-list">';
	      algo_list_content += '<a class="widget-link" onclick="KDC_showHideAlgoContent(' + grp_id + '); return false" href="#">' + algo_grp_name + '</a>';
	      algo_list_content += '<ul id="grp_ul' + grp_id + '" class="grp_list">';
	      
	      for (var i = 0; i < algo_selection_object[algo_grp_name].length; ++i) {
		
		  algo_selection_html_content += '<li><a href="#" id="' + algo_selection_object[algo_grp_name][i] + '">' + algo_selection_object[algo_grp_name][i] + '</a></li>';
		  
		  algo_list_content += '<li><a href="#" id="' + algo_selection_object[algo_grp_name][i] + '">' + algo_selection_object[algo_grp_name][i] + '</a></li>';
	      }
	      
	      algo_selection_html_content += '</ul></li>'; 
	      algo_list_content += '</ul></li>';
	      
	      ++grp_id;
	  }
	  algo_selection_html_content += '</span></ul>';
	  
	  // JS script to add an icon to clear text
	  algo_selection_html_content += '<script>';
	  algo_selection_html_content += 'jQuery(function($) {';
	  algo_selection_html_content += 'function tog(v){return v?"addClass":"removeClass";}';
	  algo_selection_html_content += '$(document).on("input", ".algo-search-box", function(){';
	  algo_selection_html_content += '$(this)[tog(this.value)]("x");';
	  algo_selection_html_content += '}).on("mousemove", ".x", function( e ){';
	  algo_selection_html_content += '$(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]("onX");';   
	  algo_selection_html_content += '}).on("click", ".onX", function(){';
	  algo_selection_html_content += '$(this).removeClass("x onX").val("");';
	  algo_selection_html_content += 'KDC_searchAlgoName($(".algo-search-box").val())';
	  algo_selection_html_content += '});';
	  algo_selection_html_content += '});';
	  
	  // JS script to change the background when user selects an element
	  algo_selection_html_content += '$(".grp_list li a").on("click", function(e) {';
	  algo_selection_html_content += '$(".grp_list li").removeClass("algo-selected");';
	  algo_selection_html_content += '$(this).parent("li").addClass("algo-selected");';
	  algo_selection_html_content += 'KDC_buildAlgoForm(e.target.id);';
	  algo_selection_html_content += 'e.preventDefault();';
	  algo_selection_html_content += '});';
	  
	  // JS script to change search-box look on focus
	  algo_selection_html_content += '$(function() {';
	  algo_selection_html_content += '$(".algo-search-box").focus(function() {';
	  algo_selection_html_content += '$(this).addClass("algo-search-box-focused")';
	  algo_selection_html_content += '}).blur(function(){';
	  algo_selection_html_content += '$(this).removeClass("algo-search-box-focused")';
	  algo_selection_html_content += '});';
	  algo_selection_html_content += '});</script>';
	  
	  algo_list_content += '<script>';
	  algo_list_content += 'jQuery(function($) {';
	  algo_list_content += 'function tog(v){return v?"addClass":"removeClass";}';
	  algo_list_content += '$(document).on("input", ".algo-search-box", function(){';
	  algo_list_content += '$(this)[tog(this.value)]("x");';
	  algo_list_content += '}).on("mousemove", ".x", function( e ){';
	  algo_list_content += '$(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]("onX");';   
	  algo_list_content += '}).on("click", ".onX", function(){';
	  algo_list_content += '$(this).removeClass("x onX").val("");';
	  algo_list_content += 'KDC_searchAlgoName($(".algo-search-box").val())';
	  algo_list_content += '});';
	  algo_list_content += '});';
	  
	  algo_list_content += '$(".grp_list li a").on("click", function(e) {';
	  algo_list_content += '$(".grp_list li").removeClass("algo-selected");';
	  algo_list_content += '$(this).parent("li").addClass("algo-selected");';
	  algo_list_content += 'KDC_buildAlgoForm(e.target.id);';
	  algo_list_content += 'e.preventDefault();';
	  algo_list_content += '});';
	  
	  algo_list_content += '$(function() {';
	  algo_list_content += '$(".algo-search-box").focus(function() {';
	  algo_list_content += '$(this).addClass("algo-search-box-focused")';
	  algo_list_content += '}).blur(function(){';
	  algo_list_content += '$(this).removeClass("algo-search-box-focused")';
	  algo_list_content += '});';
	  algo_list_content += '});</script>';
	  
	  $('#algorithm_selection').removeClass("wait");
	  $('#algorithm_selection').html(algo_selection_html_content);
      });
}

function KDC_buildAlgoForm(option_id) {
      
      $('#error_msg').html(" "); // reset any error messages
      $('#algo_title').html("<strong>" + json_algo_data[option_id].AlgorithmName + "</strong>");
      
      var form_content = '<p name="algorithm" style="font-size:16px; text-align:center; margin: 0; padding: 0;"><strong>' + json_algo_data[option_id].AlgorithmName + '</strong></p>';
      form_content += '<input type="hidden" name="algorithm" value="' + json_algo_data[option_id].AlgorithmName + '"/>';

      if (option_id != "none") {
	
	  var algo_description = "Not provided"; 
	  var algo_help = "Not provided";
	  var algo_version = "Not provided";
	  var author = "Not provided";
	  var last_update = "Not provided";

	  if (typeof json_algo_data[option_id].AlgorithmDescription !== 'undefined') algo_description = json_algo_data[option_id].AlgorithmDescription;
	  if (typeof json_algo_data[option_id].AlgorithmHelp !== 'undefined') algo_help = json_algo_data[option_id].AlgorithmHelp;
	  if (typeof json_algo_data[option_id].AlgorithmVersion !== 'undefined') algo_version = json_algo_data[option_id].AlgorithmVersion;
	  if (typeof json_algo_data[option_id].Author !== 'undefined') author = json_algo_data[option_id].Author;
	  if (typeof json_algo_data[option_id].LastUpdate !== 'undefined') last_update = json_algo_data[option_id].LastUpdate;
	  
	  form_content += '<div class="algo-form"><strong>Algorithm Description:</strong> ' + algo_description + '</div>';
	  form_content += '<div class="algo-form"><strong>Algorithm Help:</strong> ' + algo_help + '</div>';
	  form_content += '<div class="algo-form"><strong>Algorithm Version:</strong> ' + algo_version + '</div>';
	  form_content += '<div class="algo-form"><strong>Author:</strong> ' + author + '</div>';
	  form_content += '<div class="algo-form"><strong>Date of Last Update:</strong> ' + last_update + '</div>';
      
	  if (json_algo_data[option_id].FormType == "dynamic") {
	    
	      for (var i=0; i < json_args_arr[option_id].length; i++) {
		
		  (function(i) {
		  
		      if (json_args_arr[option_id][i].SrcType == "external") {

			  var json_dal_url = dal_base_url + "/" + json_args_arr[option_id][i].SrcURL;
			  var arg_name = json_args_arr[option_id][i].ArgName;
			  var label = json_args_arr[option_id][i].ArgLabel;
			  
			  form_content += '<div class="algo-form"><strong>' + label + ':</strong> ';
			  
			  // Check if URL contains <% and %> so they will not be executed
			  if (url_regex.test(json_dal_url)) {
			      
			      form_content += '<span id="select_list_' + arg_name + '" class="wait">Waiting for data...</span></div>';
			  }
			  else {
			    
			      var arg = arg_name;
			      var src_id = json_args_arr[option_id][i].HTMLSrcId;
			      var src_disp = json_args_arr[option_id][i].HTMLSrcDisplay;
			      var default_value = json_args_arr[option_id][i].DefaultValue;
			      var display_format = KDC_getHTMLDispFormat(src_disp);
			      var child_args = json_args_arr[option_id][i].ChildArgs;
			      var html_type = json_args_arr[option_id][i].HTMLType;
			      
			      form_content += '<span id="select_list_' + arg + '" class="wait">Loading data...</span>';
					  
			      $.getJSON(json_dal_url, function(json_data) {

				  var tag_name = json_data.RecordMeta[0].TagName;
				  var list_content = "";
				  
				  if (html_type == "dropdownlist") {
				    
				      list_content += '<span class="form-field"><select id="' + arg + '" name="' + arg + '" class="option-list">';
				      
				      (default_value == "") ? list_content += '<option id="none" value="" selected="selected"></option>' : list_content += '<option id="none" value="' + default_value + '" selected="selected"></option>';

				      for (var j=0; j < json_data[tag_name].length; j++) {
			    
					  list_content += '<option id='+ j; 
					  list_content += ' value="'+ json_data[tag_name][j][src_id] +'">'; 
					  
					  var htmlText = KDC_getHTMLText(src_disp, display_format, json_data[tag_name][j]);
					  list_content += htmlText +'</option>';				  
				      }
				      list_content += '</select>';
				      list_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span>';
				      list_content +='</span></div>';
				      KDC_buildExtSelectList(list_content, arg);
				  }
				  else if (html_type == "multilist") {
				    
				      list_content += '<span class="form-field"><select id="' + arg + '" name="' + arg + '" class="option-list" multiple>';
				      
				      (default_value == "") ? list_content += '<option id="none" value="" selected="selected"></option>' : list_content += '<option id="none" value="' + default_value + '" selected="selected"></option>';
				      
				      for (var j=0; j < json_data[tag_name].length; j++) {
			    
					  list_content += '<option id='+ j; 
					  list_content += ' value="'+ json_data[tag_name][j][src_id] +'">'; 
					  
					  var htmlText = KDC_getHTMLText(src_disp, display_format, json_data[tag_name][j]);
					  list_content += htmlText +'</option>';				  
				      }
				      list_content += '</select>';
				      list_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span>';
				      list_content +='</span></div>';
				      KDC_buildExtSelectList(list_content, arg);
				  }
				  
				  if ( typeof child_args !== 'undefined' && child_args.length > 0 && html_type == "dropdownlist") {
				    
				      $('#'+ arg).change(function(){
					
					    KDC_buildChildForm($(this).val(), child_args, option_id, arg);
				      });
				  }
			      })
			      .fail(function() {
				
				  var error_msg = "<span style='color:red;'>Error: Unable to retrieve data for " + json_args_arr[option_id][i].ArgLabel + ". Possible DAL server error. Please try again later.</span>";
				  $('#select_list_'+arg).removeClass("wait");
				  $('#select_list_'+arg).html(error_msg);
			      });
			  }
		      }
		      else {

			  if (json_args_arr[option_id][i].HTMLType == "inputbox") {
						
			      form_content += '<div class="algo-form"><strong>';
			      form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> ';
			      form_content += '<span class="form-field"><input type="text" name="' + json_args_arr[option_id][i].ArgName + '"';
		    
			      if (json_args_arr[option_id][i].DefaultValue != "") form_content += ' value="' + json_args_arr[option_id][i].DefaultValue + '"';
			      if (typeof json_args_arr[option_id][i].Tooltip !== 'undefined' && json_args_arr[option_id][i].Tooltip != "") form_content += ' title="' + json_args_arr[option_id][i].Tooltip + '"';
		   
			      form_content += '>';
			      form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			  }
			  else if (json_args_arr[option_id][i].HTMLType == "date") {
			      
			      form_content += '<div class="algo-form"><strong>';
			      form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> ';
			      form_content += '<span class="form-field"><input type="text" name="' + json_args_arr[option_id][i].ArgName + '" class="date" on>';
			      form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			      form_content += '<script>$(".date").datepicker({ dateFormat:"dd/mm/yy" });</script>';
			  }
			  else if (json_args_arr[option_id][i].HTMLType == "file") {
			    
			      var arg_name = json_args_arr[option_id][i].ArgName;
			      var base_url = KDC_constructCurrentBaseUrl(); // window.location.protocol + "//" + window.location.host;
			      
			      form_content += '<link rel="stylesheet" type="text/css" href="' + base_url +'/css/jqueryFileTree.css" />';
			      form_content += '<script src="' + base_url + '/javascripts/jqueryFileTree.js"></script>';
			      form_content += '<div id="dialog_file_selection_' + arg_name + '" title="Select a file">';
			      form_content += '<strong>Select a file from the list below:</strong><br />';
			      form_content += '<span id="file_list_' + arg_name + '" class="wait">Loading data...</span>';
			      form_content += "<script>";
			      form_content += "$(document).ready( function() {";
			      form_content += "$('#file_list_" + arg_name + "').fileTree({ }, function(file_path) {";
			      form_content += "var final_file_path = file_path.substring(1);";
			      form_content += "final_file_path = final_file_path.slice(0, -1);";
			      form_content += "$('#file_list_" + arg_name + "').html(final_file_path);";
			      form_content += "$('#input_" + arg_name + "').val(final_file_path);";
			      form_content += "});";
			      form_content += "});</script>";
			      form_content += '<br /><button onclick="KDC_closeDialog' + arg_name + '(); return false;">Select this file</button>';
			      form_content += '</div>';
			      form_content += '<div class="algo-form"><strong>';
			      form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> ';
			      form_content += '<span class="form-field"><input type="hidden" name="' + arg_name + '" id="input_' + arg_name + '" value="">'; 
			      form_content += '<span id="file_list_' + arg_name + '"></span>&nbsp;&nbsp;<button onclick="KDC_dispFileSelection(\'' + arg_name + '\'); return false;">Select a file</button>';
			      form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			      form_content += "<script>";
			      form_content += "function KDC_closeDialog"+ arg_name +"() {";
			      form_content += 'var user_selection = $("#input_' + arg_name + '").val();';
			      form_content += 'if (user_selection != "") {';
			      form_content += '$("#dialog_file_selection_' + arg_name + '").dialog("close");';
			      form_content += '} else {';
			      form_content += 'alert("Please select a file!");';
			      form_content += '}';
			      form_content += '}';
			      form_content += '$("#dialog_file_selection_' + arg_name + '").hide();';
			      form_content += '</script>';
			      
			  }
			  else if (json_args_arr[option_id][i].HTMLType == "dropdownlist") {
			    
			      var json_local_src = json_args_arr[option_id][i].SrcLocal;
			      
			      if ( typeof json_local_src !== 'undefined' && json_local_src.length > 0) {
				
				  form_content += '<div class="algo-form"><strong>';
				  form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> '; 
				  form_content += '<span class="form-field"><select name="' + json_args_arr[option_id][i].ArgName + '" class="option-list">';
				  
				  for (j=0; j < json_local_src.length; j++) {
			
				      form_content += '<option id='+ j; 
				      form_content += ' value="'+ json_local_src[j].OptionValue +'">';
				      form_content += json_local_src[j].OptionValue +'</option>';				  
				  }
				  form_content += '</select>';
				  form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			      }
			  }
			  else if (json_args_arr[option_id][i].HTMLType == "multilist") {
			    
			      var json_local_src = json_args_arr[option_id][i].SrcLocal;
			      
			      if ( typeof json_local_src !== 'undefined' && json_local_src.length > 0) {
				
				  form_content += '<div class="algo-form"><strong>';
				  form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> '; 
				  form_content += '<span class="form-field"><select name="' + json_args_arr[option_id][i].ArgName + '" class="option-list" multiple>';
				  
				  for (j=0; j < json_local_src.length; j++) {
			
				      form_content += '<option id='+ j; 
				      form_content += ' value="'+ json_local_src[j].OptionValue +'">';
				      form_content += json_local_src[j].OptionValue +'</option>';				  
				  }
				  form_content += '</select>';
				  form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			      }
			  }
			  else if (json_args_arr[option_id][i].HTMLType == "checkbox") {
			    
			      var json_local_src = json_args_arr[option_id][i].SrcLocal;
			      
			      if ( typeof json_local_src !== 'undefined' && json_local_src.length > 0) {
				
				  form_content += '<div class="algo-form"><strong>';
				  form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> <br /><br /><span class="form-field">'; 
				  
				  for (j=0; j < json_local_src.length; j++) {
			
				      form_content += '<input type="checkbox" name="' + json_args_arr[option_id][i].ArgName + '" value="' + json_local_src[j].OptionValue + '">';
				      form_content += json_local_src[j].OptionValue + '<br />';				  
				  }
				  form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			      }
			  }
			  else if (json_args_arr[option_id][i].HTMLType == "radiobutton") {
			    
			      var json_local_src = json_args_arr[option_id][i].SrcLocal;
			      
			      if ( typeof json_local_src !== 'undefined' && json_local_src.length > 0) {
				
				  form_content += '<div class="algo-form"><strong>';
				  form_content += json_args_arr[option_id][i].ArgLabel + ':</strong> <br /><br /><span class="form-field">'; 
				  
				  for (j=0; j < json_local_src.length; j++) {
			
				      form_content += '<input type="radio" name="' + json_args_arr[option_id][i].ArgName + '" value="' + json_local_src[j].OptionValue + '">';
				      form_content += json_local_src[j].OptionValue + '<br />';				  
				  }
				  form_content += '<span class="label-error" id="' + json_args_arr[option_id][i].ArgName + '_errmsg"></span></span></div>';
			      }
			  }
		      }
		  })(i);
	      }
	
	      form_content += '<script>$(function() { $( "[title]" ).tooltip(); });</script>';
	      form_content += '<div class="algo-form">';
	      form_content += '<input type="submit" value="Submit">';
	      form_content += '</div>';
	  }
	  else {

	      var html_tmpl_path = json_algo_data[option_id].FormTmplPath ;
	      
	      form_content += '<br/><span id="ext_tmpl" class="wait">Loading template...</span>';
	      var get_url = KDC_constructCurrentBaseUrl() + 'rendertmpl';
	      
	      $.get( get_url , { tmpl_path: html_tmpl_path }, function(tmpl_data) {
		
		    KDC_buildTmplForm(tmpl_data);
	      });
	  }
	  $('#algorithm_form_content').html(form_content);  
    }
    else {
	// alert("Please select an algorithm");
	$('#algorithm_form_content').html(" ");
    }
}

function KDC_dispFileSelection(arg) {
  
    $(function() {
	    $("#dialog_file_selection_" + arg).dialog({
		    height: 700,
		    width : 750,
		    modal : true,
	    });
    });
}

function KDC_dispSwitchGroupUI() {
    
    var current_full_url = document.URL;
    var redirect_url = "";
    
    (current_full_url.indexOf('?') === -1) ? redirect_url = current_full_url : redirect_url = current_full_url.substr(0, current_full_url.indexOf('?'));
    
    $(function() {
	    $("#switch_grp_view").dialog({
		    height: 300,
		    width : 350,
		    modal : true,
		    closeOnEscape: false,
		    open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
	    });
    });
}

