function toggleCreds(checkbox) {
	document.getElementById('gitusername').disabled = !checkbox.checked;
	document.getElementById('gitpassword').disabled = !checkbox.checked;
};