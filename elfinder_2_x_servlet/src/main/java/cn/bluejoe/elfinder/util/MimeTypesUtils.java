/*
    Copyright (c) 2015, shen zhihong
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    * Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package cn.bluejoe.elfinder.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;

public abstract class MimeTypesUtils
{
	private static Map<String, String> _map;

	public static final String UNKNOWN_MIME_TYPE = "application/oct-stream";

	static
	{
		_map = new HashMap<String, String>();
		try
		{
			load();
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	public static String getMimeType(String ext)
	{
		return _map.get(ext.toLowerCase());
	}

	public static boolean isUnknownType(String mime)
	{
		return mime == null || UNKNOWN_MIME_TYPE.equals(mime);
	}

	private static void load() throws IOException
	{
		InputStream is = new ClassPathResource("/mime.types").getInputStream();
		BufferedReader fr = new BufferedReader(new InputStreamReader(is));
		String line;
		while ((line = fr.readLine()) != null)
		{
			line = line.trim();
			if (line.startsWith("#") || line.isEmpty())
			{
				continue;
			}

			String[] tokens = line.split("\\s+");
			if (tokens.length < 2)
				continue;

			for (int i = 1; i < tokens.length; i++)
			{
				putMimeType(tokens[i], tokens[0]);
			}
		}

		is.close();
	}

	public static void putMimeType(String ext, String type)
	{
		if (ext == null || type == null)
			return;

		_map.put(ext.toLowerCase(), type);
	}
}
