/*
    Copyright (c) 2015, shen zhihong
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    * Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package cn.bluejoe.elfinder.controller.executors;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemStream;
import org.json.JSONObject;

import cn.bluejoe.elfinder.controller.executor.AbstractJsonCommandExecutor;
import cn.bluejoe.elfinder.controller.executor.CommandExecutor;
import cn.bluejoe.elfinder.controller.executor.FsItemEx;
import cn.bluejoe.elfinder.service.FsItemFilter;
import cn.bluejoe.elfinder.service.FsService;

public class UploadCommandExecutor extends AbstractJsonCommandExecutor implements CommandExecutor {
	@Override
	public void execute(FsService fsService, HttpServletRequest request, ServletContext servletContext, JSONObject json)
			throws Exception {
		Object attribute = request.getAttribute(FileItemStream.class.getName());
		if (attribute instanceof List<?>) {
			@SuppressWarnings("unchecked")
			List<FileItemStream> listFiles = (List<FileItemStream>) attribute;
			List<FsItemEx> added = new ArrayList<FsItemEx>();

			String target = request.getParameter("target");
			FsItemEx dir = super.findItem(fsService, target);

			FsItemFilter filter = getRequestedFilter(request);
			for (FileItemStream fis : listFiles) {
				// fis.getName() returns full path such as 'C:\temp\abc.txt' in
				// IE10
				// while returns 'abc.txt' in Chrome
				// see
				// https://github.com/bluejoe2008/elfinder-2.x-servlet/issues/22
				java.nio.file.Path p = java.nio.file.Paths.get(fis.getName());
				FsItemEx newFile = null;

				String partName = request.getParameter("chunk");
				if (partName != null) {
					String[] range = request.getParameter("range").split(",");

					int start = Integer.parseInt(range[0]);
					int length = Integer.parseInt(range[1]);

					String fileName = partName.replaceFirst("\\.\\d*_\\d*\\.part$", "");

					newFile = new FsItemEx(dir, fileName);

					File dest = new File(dir.getURL(), fileName);
					if (!dest.exists()) {
						newFile.createFile();
					}

					InputStream is = fis.openStream();
					newFile.writeStream(is, start, length);
				} else {

					newFile = new FsItemEx(dir, p.getFileName().toString());
					/*
					 * String fileName = fis.getName(); FsItemEx newFile = new
					 * FsItemEx(dir, fileName);
					 */
					newFile.createFile();
					InputStream is = fis.openStream();
					newFile.writeStream(is);

				}
				if (filter.accepts(newFile))
					added.add(newFile);
			}

			json.put("added", files2JsonArray(request, added));
		} else {
			throw new RuntimeException("Cannot cast attribute to List");
		}
	}
}
