/*
    Copyright (c) 2015, shen zhihong
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    * Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package cn.bluejoe.elfinder.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface FsVolume {
	void createFile(FsItem fsi) throws IOException;

	void createFolder(FsItem fsi) throws IOException;

	void deleteFile(FsItem fsi) throws IOException;

	void deleteFolder(FsItem fsi) throws IOException;

	boolean exists(FsItem newFile);

	FsItem fromPath(String relativePath);

	String getDimensions(FsItem fsi);

	long getLastModified(FsItem fsi);

	String getMimeType(FsItem fsi);

	String getName();

	String getName(FsItem fsi);

	FsItem getParent(FsItem fsi);

	String getPath(FsItem fsi) throws IOException;

	FsItem getRoot();

	long getSize(FsItem fsi) throws IOException;

	String getThumbnailFileName(FsItem fsi);

	boolean hasChildFolder(FsItem fsi);

	boolean isFolder(FsItem fsi);

	boolean isRoot(FsItem fsi);

	FsItem[] listChildren(FsItem fsi);

	InputStream openInputStream(FsItem fsi) throws IOException;

	void writeStream(FsItem f, InputStream is) throws IOException;

	void rename(FsItem src, FsItem dst) throws IOException;

	/**
	 * Gets the URL for the supplied item.
	 * 
	 * @param f
	 *            The item to get the URL for.
	 * @return An absolute URL or <code>null</code> if we should not send back a
	 *         URL.
	 */
	String getURL(FsItem f);

	/**
	 * This allows volumes to change the options returned to the client for a
	 * particular item. Implementations should update the map they are passed.
	 * 
	 * @param f
	 *            The item to filter the options for.
	 * @param map
	 *            The options that are going to be returned.
	 */
	void filterOptions(FsItem f, Map<String, Object> map);

	void writeStream(FsItem _f, InputStream is, int start, int length) throws IOException;
}
