/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisJobNotFoundException;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.db.other.GobiiDataset;
import com.diversityarrays.kdcompute.gobiiqc.GobiiDatasetIdNotFoundException;
import com.diversityarrays.kdcompute.gobiiqc.containers.StatusResponse;
import com.diversityarrays.kdcompute.jobsummary.JobSummaryUtil;
import com.diversityarrays.kdcompute.runtime.EntityStore;

@RestController
@SuppressWarnings("nls")
public class QCStatus {

	public static final String JOBID = "jobid";

	public static final String QC_STATUS = "qcStatus";

	public static final String SLASH_QC_STATUS = "/" + QC_STATUS;

	@Autowired
	EntitiesService entitiesService;

	@RequestMapping(value = QCStart.QCJOBS, method = RequestMethod.GET)
	@ResponseBody
	public StatusResponse qcStatusWSAPIBP(@RequestParam(JOBID) Long jobid, HttpServletRequest request)
			throws GobiiDatasetIdNotFoundException, AnalysisJobNotFoundException, IOException, AuthenticationException {
		return qcStatus(jobid, request);
	}

	@RequestMapping(value = SLASH_QC_STATUS, method = RequestMethod.GET)
	@ResponseBody
	public StatusResponse qcStatus(@RequestParam(JOBID) Long jobid, HttpServletRequest request)
			throws GobiiDatasetIdNotFoundException, AnalysisJobNotFoundException, IOException, AuthenticationException {
		IPAddressAuthenticationProvider.checkIfAddressIsValid(request.getRequestURI(), request.getRemoteAddr(),
				GobiiConfiguration.getIpAddressRegex());

		EntityStore entityStore = entitiesService.getEntityStore();

		AnalysisJob analysisJob = null;
		if (jobid != null) {
			analysisJob = entityStore.getById(AnalysisJob.class, jobid);
		}
		if (analysisJob == null) {
			throw new AnalysisJobNotFoundException(jobid);
		}

		List<GobiiDataset> list = getGobiiDataset(entityStore, analysisJob);

		if (list.isEmpty()) {
			throw new GobiiDatasetIdNotFoundException("Could not find GobiiDatasetId for jobid=" + jobid);
		}

		GobiiDataset gobiiDataset = list.get(0);
		Long gobiiDatasetId = gobiiDataset.getGobiiDatasetId();
		RunState runState = analysisJob.getRunState();
		Integer exitCode = analysisJob.getExitCode();

		List<String> files = QCDownload.getDownloadLinksForQcJob(entityStore, analysisJob);

		String viewJobFolderUrl = String.join("/",
				Arrays.asList("", "jobsummary", analysisJob.getRequestingUser(), Long.toString(analysisJob.getId())));

		Function<String, String> toFileBase = new Function<String, String>() {

			@Override
			public String apply(String t) {
				return new File(t).getName();
			}
		};

		Map<String, String> resultsUrls = files.stream().collect(Collectors.toMap(toFileBase, Function.identity()));
		// TODO [BSP] how about just returning the "result" files and possibly
		// the stdout/stderr
		StatusResponse statusResponse = new StatusResponse(gobiiDatasetId, runState,
				(exitCode == null ? "" : Integer.toString(exitCode)), analysisJob.getStartDateTime(),
				analysisJob.getEndDateTime(), viewJobFolderUrl, resultsUrls,
				JobSummaryUtil.getFilesOfJob(entitiesService.getEntityStore(), analysisJob).stream()
						.map(e -> e.getAbsolutePath()).collect(Collectors.toList()));
		return statusResponse;
	}

	public static List<GobiiDataset> getGobiiDataset(EntityStore entityStore, AnalysisJob analysisJob) {
		List<GobiiDataset> result;
		if (entityStore.supportsVisitListOfEntities()) {
			Long lookingFor = analysisJob.getId();
			if (lookingFor == null) {
				result = Collections.emptyList();
			} else {
				// basically here for unit tests
				result = new ArrayList<>();
				entityStore.visitListOfEntities(GobiiDataset.class, (t) -> {
					if (lookingFor.equals(t.getAnalysisJob().getId())) {
						result.add(t);
					}
					return false; // false to get them all
				});
			}
		} else {
			EntityManager manager = entityStore.getEntityManager();
			CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
			CriteriaQuery<GobiiDataset> criteria = criteriaBuilder.createQuery(GobiiDataset.class);
			Root<GobiiDataset> root = criteria.from(GobiiDataset.class);
			criteria = criteria.select(root).where(criteriaBuilder.equal(root.get("analysisJob"), analysisJob.getId()));
			TypedQuery<GobiiDataset> query = manager.createQuery(criteria);
			result = query.getResultList();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static final List<String> getFilesOfJob(String user, Long id, HttpServletRequest request) {
		/*
		 * As in JobSummaryController
		 */

		RestTemplate restTemplate = new RestTemplate();
		String requestURL = request.getRequestURL().toString().replace(request.getRequestURI(),
				request.getContextPath());
		String listPath = String.join("/",
				Arrays.asList(requestURL.toString(), "list", "jobsummary", user, Long.toString(id)));
		return restTemplate.getForObject(listPath, List.class);
	}
}
