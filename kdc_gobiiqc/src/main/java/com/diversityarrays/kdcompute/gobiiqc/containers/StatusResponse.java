/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.gobiiqc.containers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.diversityarrays.kdcompute.db.RunState;

public class StatusResponse {
	final Long gobiiDatasetId;
	final RunState status;
	final String completionCode; // Is string as may not yet be complete and
									// response cannot have null
	final Date start;
	final Date end;
	final String viewJobFolderUrl;
	final Map<String, String> resultsUrls;
	final List<String> resultsFilepaths;

	public StatusResponse(Long gobiiDatasetId, RunState status, String completionCode, Date start, Date end,
			String viewJobFolderUrl, Map<String, String> resultsUrls, List<String> resultsFilepaths) {
		super();
		this.gobiiDatasetId = gobiiDatasetId;
		this.status = status;
		this.completionCode = completionCode;
		this.start = start;
		this.end = end;
		this.viewJobFolderUrl = viewJobFolderUrl;
		this.resultsUrls = resultsUrls;
		this.resultsFilepaths = resultsFilepaths;
	}

	public Long getGobiiDatasetId() {
		return gobiiDatasetId;
	}

	public RunState getStatus() {
		return status;
	}

	public String getCompletionCode() {
		return completionCode;
	}

	public Date getStart() {
		return start;
	}

	public Date getEnd() {
		return end;
	}

	public String getViewJobFolderUrl() {
		return viewJobFolderUrl;
	}

	public Map<String, String> getResultsUrls() {
		return resultsUrls;
	}

	public List<String> getResultsFilepaths() {
		return resultsFilepaths;
	}

}
