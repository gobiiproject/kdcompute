/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.gobiiqc.containers;

import java.util.Arrays;

public class Data {
	final Long dataSetId;
	final String dataFile;
	final String directory;
	final String qualityFile;
	// BSP: note - the only caller actually provides the database id of the AnalysisJob
	final Long contactId;
	public Data(Long dataSetId, String dataFile, String directory, String qualityFile, Long contactId) {
		super();
		this.dataSetId = dataSetId;
		this.dataFile = dataFile;
		this.directory = directory;
		this.qualityFile = qualityFile;
		this.contactId = contactId;
	}
	public Long getDataSetId() {
		return dataSetId;
	}
	public String getDataFile() {
		return dataFile;
	}
	public String getDirectory() {
		return directory;
	}
	public String getQualityFile() {
		return qualityFile;
	}
	public Long getContactId() {
		return contactId;
	}

	@Override
	public String toString() {
		return String.join(",",Arrays.asList(
				"dataSetId="+dataSetId,
				"dataFile="+dataFile,
				"directory="+directory,
				"qualityFile="+qualityFile,
				"contactId="+contactId
				));
	}


}
