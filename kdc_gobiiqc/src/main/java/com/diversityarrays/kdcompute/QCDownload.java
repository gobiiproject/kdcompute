/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisJobNotFoundException;
import com.diversityarrays.kdcompute.jobsummary.JobSummaryUtil;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.google.gson.GsonBuilder;

@RestController
@SuppressWarnings("nls")
public class QCDownload {

	public static final String QCJOBS_FILES = QCStart.QCJOBS + "/" + "files";
	public static final String QC_DOWNLOAD = "qcDownload";
	public static final String SLASH_QC_DOWNLOAD = "/" + QC_DOWNLOAD;
	public static final String QC_FILES = "qcFiles";
	public static final String SLASH_QC_FILES = "/" + QC_FILES;

	static class LinksList {
		List<String> links;

		public LinksList(List<String> links) {
			super();
			this.links = links;
		}

	}

	@Autowired
	EntitiesService entitiesService;

	@RequestMapping(value = QCJOBS_FILES, method = RequestMethod.GET)
	@ResponseBody
	public String qcFilesWSAPIBP(@RequestParam(QCStatus.JOBID) Long jobId, HttpServletRequest request)
			throws AuthenticationException, IOException, AnalysisJobNotFoundException {
		return qcFiles(jobId, request);
	}

	@RequestMapping(value = "/" + QC_FILES, method = RequestMethod.GET)
	@ResponseBody
	public String qcFiles(@RequestParam(QCStart.QCJOBS) Long jobId, HttpServletRequest request)
			throws AuthenticationException, IOException, AnalysisJobNotFoundException {
		IPAddressAuthenticationProvider.checkIfAddressIsValid(request.getRequestURI(), request.getRemoteAddr(),
				GobiiConfiguration.getIpAddressRegex());

		EntityStore entityStore = entitiesService.getEntityStore();
		AnalysisJob job = entityStore.getById(AnalysisJob.class, jobId);
		List<String> returnObj = null;
		if (job == null) {
			throw new AnalysisJobNotFoundException(jobId);
		} else {
			returnObj = getDownloadLinksForQcJob(entityStore, job);
		}

		GsonBuilder builder = new GsonBuilder();
		if (QCStart.PRETTY_PRINT_JSON) {
			builder.setPrettyPrinting();
		}
		return builder.create().toJson(new LinksList(returnObj));
	}

	public static List<String> getDownloadLinksForQcJob(EntityStore entityStore, AnalysisJob job) throws IOException {
		String URL_SEP = "/";
		String jobIdString = Long.toString(job.getId());

		Function<String, String> linkFactory = new Function<String, String>() {
			@Override
			public String apply(String part) {
				return String.join(URL_SEP, Arrays.asList("", QC_DOWNLOAD, jobIdString, part));
			}
		};

		return JobSummaryUtil.makeDownloadLinks(entityStore, job, linkFactory);
	}

	@RequestMapping(value = "/" + QC_DOWNLOAD + "/{" + QCStatus.JOBID + "}/**", method = RequestMethod.GET)
	public void qcDownload(@PathVariable(QCStatus.JOBID) Long jobId, HttpServletResponse response,
			HttpServletRequest request) throws IOException, AuthenticationException, AnalysisJobNotFoundException {
		IPAddressAuthenticationProvider.checkIfAddressIsValid(request.getRequestURI(), request.getRemoteAddr(),
				GobiiConfiguration.getIpAddressRegex());

		String fullUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

		// This is really horrible
		String fileRequired = fullUrl.replace(String.format("/%s/%d", QC_DOWNLOAD, jobId), "");
		// A simple attempt to prevent access to other parts of the file system
		if (fileRequired.contains("..")) {
			throw new IOException("Invalid access: " + fileRequired);
		}

		AnalysisJob job = entitiesService.getEntityStore().getById(AnalysisJob.class, jobId);
		if (job == null) {
			throw new AnalysisJobNotFoundException(jobId);
		}
		File resultFolder = job.getResultsFolder();

		File file = new File(resultFolder, fileRequired);
		JobSummaryUtil.downloadFile(response, file);
	}

}
