/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Service;

import com.diversityarrays.kdcompute.security.HttpSecurityConfig;

/*
 * // TODO Replace use of  @{link IPAddressAuthenticationProvider.checkIfAddressIsValid} with spring HttpSecurity authorisation
 * QC functions should be visible using a HttpSecurity override. the hasIpAddress does not allow regex patterns. it does allow subnet masks
 * 
 */
@Service
@Configuration
public class GobiiQcHttpSecurityConfig implements HttpSecurityConfig {

	@Value("${gobii.allowedIpRegex}")
	String ipAddress;

	@Override
	public HttpSecurity configure(HttpSecurity http) throws Exception {
		return http.authorizeRequests()
				.antMatchers(QCStart.QCJOBS, QCDownload.QCJOBS_FILES, QCStart.SLASH_QC_START,
						QCDownload.SLASH_QC_DOWNLOAD, QCDownload.SLASH_QC_DOWNLOAD + "/**", QCDownload.SLASH_QC_FILES,
						QCStatus.SLASH_QC_STATUS, QCPurge.SLASH_QC_PURGE)
				.permitAll().and();

	}

}
