/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db.other;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.runtime.KDCDbUtil;

/**
 * @author andrew Any 3rd party database entities must be in the package
 *         {@code com.diversityarrays.kdcompute.db.other}, or in a subpackage,
 *         to be detected.
 */
@Entity
@Table
@Configuration
public class GobiiDataset {

	public static final String ANALYSIS_JOB_ID = "AnalysisJob_Id";

	// FIXME [BSP] why is this a "setter" ???
	@Autowired
	public void setEntitiesService(EntitiesService entitiesService) {
		/*
		 * Removing a plugin-version from KDCompute is a significant action.
		 * Admins should have a very good reason to do so, and should generally
		 * never be done on production.
		 */
		entitiesService.getEntityStore().addDeleteJobsOfPluginResponseList((t) -> {
			List<GobiiDataset> gobiiDatasets = KDCDbUtil.getAllEntities(entitiesService.getEntityStore(),
					GobiiDataset.class);
			for (GobiiDataset gobiiDataset : gobiiDatasets) {
				entitiesService.getEntityStore().remove(gobiiDataset);
			}
		});
	}

	@Id
	private Long gobiiDatasetId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = ANALYSIS_JOB_ID)
	private AnalysisJob analysisJob;

	public GobiiDataset() {
		super();
	}

	public GobiiDataset(Long gobiiDatasetId, AnalysisJob analysisJob) {
		super();
		this.gobiiDatasetId = gobiiDatasetId;
		this.analysisJob = analysisJob;
	}

	public Long getGobiiDatasetId() {
		return gobiiDatasetId;
	}

	public void setGobiiDatasetId(Long gobiiDatasetId) {
		this.gobiiDatasetId = gobiiDatasetId;
	}

	public AnalysisJob getAnalysisJob() {
		return analysisJob;
	}

	public void setAnalysisJob(AnalysisJob analysisJob) {
		this.analysisJob = analysisJob;
	}

}
