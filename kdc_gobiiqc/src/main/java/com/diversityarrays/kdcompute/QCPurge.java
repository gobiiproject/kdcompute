/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.JobStillActiveException;
import com.diversityarrays.kdcompute.db.other.GobiiDataset;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobNotFoundException;

@RestController
public class QCPurge {

	private static final int STATUS_OK_TO_BE_ENCATED = 202;
	private static final int STATUS_OK = 200;
	private static final String QC_PURGE = "qcPurge";
	public static final String SLASH_QC_PURGE = "/" + QC_PURGE;

	@Autowired
	EntitiesService entitiesService;

	@RequestMapping(value = QCStart.QCJOBS, method = RequestMethod.DELETE)
	public void qcPurgeWSAPIBP(@RequestParam(QCStatus.JOBID) Long jobId, HttpServletRequest request,
			HttpServletResponse httpServletResponse) {
		qcPurge(jobId, request, httpServletResponse);
	}

	@RequestMapping(value = SLASH_QC_PURGE, method = RequestMethod.DELETE)
	public void qcPurge(@RequestParam(QCStatus.JOBID) Long jobid, HttpServletRequest request,
			HttpServletResponse httpServletResponse) {

		IPAddressAuthenticationProvider.checkIfAddressIsValid(request.getRequestURI(), request.getRemoteAddr(),
				GobiiConfiguration.getIpAddressRegex());

		AnalysisJob byId = entitiesService.getEntityStore().getById(AnalysisJob.class, jobid);

		if (byId == null) {
			httpServletResponse.setStatus(STATUS_OK);
			return;
		}

		List<GobiiDataset> d = entitiesService.getEntityStore().getEntityByColumnNameMatch("analysisJob",
				GobiiDataset.class, byId);
		d.forEach(e -> {
			entitiesService.getEntityStore().remove(e);
		});

		Consumer<AnalysisJob> terminateFunc = new Consumer<AnalysisJob>() {

			@Override
			public void accept(AnalysisJob t) {
				try {
					entitiesService.getEntityStore().removeAnalysisJob(jobid);
				} catch (JobNotFoundException | IOException | JobStillActiveException e) {
					e.printStackTrace();
				}
			}
		};
		try {
			entitiesService.getJobScheduler().cancelJob(byId, terminateFunc);
			httpServletResponse.setStatus(STATUS_OK_TO_BE_ENCATED);
		} catch (JobNotFoundException e2) {
			terminateFunc.accept(byId);
			httpServletResponse.setStatus(STATUS_OK);
		}

	}

}
