/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

/*
 * This is how you can override KDCompute authentication
 */
//@Service
//public class GobiiQcAuthenticationManagerBuilderProvider extends AuthenticationManagerBuilderProvider
//{
//	@Override
//	public void configureGlobal(AuthenticationManagerBuilder auth) {
//		auth.authenticationProvider(
//				new AbstractUserDetailsAuthenticationProvider() {
//
//					@Override
//					protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
//							throws AuthenticationException {
//						Object password = authentication.getCredentials();
//						List<String> roles;
//						try {
//							roles = login(username, (String)password);
//						} catch (IncorrectUsernameOrPasswordException e) {
//							throw new AuthenticationCredentialsNotFoundException("Bad password for user: " + username);
//						} catch (UserNotFoundException e) {
//							throw new AuthenticationCredentialsNotFoundException("User " + username + " was not found");
//						}
//						List<GrantedAuthority> authorities = roles.stream().map(role->new SimpleGrantedAuthority(role) ).collect(Collectors.toList());
//						User user = new User(username, (String) password,authorities);
//						return user;
//					}
//
//					@Override
//					protected void additionalAuthenticationChecks(UserDetails userDetails,
//							UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
//					}
//				}
//				);
//	}
//	
//	@Override
//	public String loginViaDialgue() throws IncorrectUsernameOrPasswordException, UserNotFoundException, Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<String> login(String username, String password)
//			throws IncorrectUsernameOrPasswordException, UserNotFoundException {
//		if(username.equals("admin")){
//			if(password.equals("password")) {
//				return Arrays.asList("ROLE_ADMIN","ROLE_USER");
//			}else{
//				throw new IncorrectUsernameOrPasswordException("Bad password");
//			}
//		}
//		throw new UserNotFoundException();
//	}
//
//	@Override
//	public boolean getUsingUsernameSplash() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
//	
//}
