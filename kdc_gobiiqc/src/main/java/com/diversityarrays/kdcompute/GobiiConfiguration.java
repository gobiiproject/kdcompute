/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GobiiConfiguration {

	private static String ipAddressRegex;

	private static String pluginName;

	// TODO [BSP] Ask about this style
	// ?? need to create an instance to be able to set the statics ??
	@Autowired
	public void setVariables(@Value("${gobii.allowedIpRegex}") String ipAddressRegex,

			@Value("${gobii.qc.pluginName}") String pluginName) {
		GobiiConfiguration.ipAddressRegex = ipAddressRegex;
		GobiiConfiguration.pluginName = pluginName;
	}

	public static String getIpAddressRegex() {
		return ipAddressRegex;
	}

	public static String getPluginName() {
		return pluginName;
	}

}
