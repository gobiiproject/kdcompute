/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.api.JobIdResponse;
import com.diversityarrays.api.ResponseJob;
import com.diversityarrays.kdcompute.db.other.GobiiDataset;
import com.diversityarrays.kdcompute.gobiiqc.StartResponse;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobNotFoundException;
import com.diversityarrays.kdcompute.submitjob.SubmitJobForPluginService;
import com.diversityarrays.kdcompute.submitjob.SubmitJobResponse;
import com.diversityarrays.kdcompute.util.Check;
import com.google.common.base.Function;

@RestController
@SuppressWarnings("nls")
public class QCStart {

	private static final String FORCE_RESTART = "forcerestart";

	private static final boolean DEBUG = Boolean.getBoolean(QCStart.class.getName() + ".DEBUG");

	// Parameters provided in the qcStart request - both mandatory
	private static final String PARAM_DIRECTORY = "directory";
	private static final String PARAM_DATASET_ID = "datasetId";

	public static final String QC_DEFAULT_USER = "server.admin";

	public static final String QC_START = "qcStart";
	public static final String SLASH_QC_START = "/" + QC_START;
	public static final String QCJOBS = "/qcjobs";

	;

	// The following params are found in kdcp_gobiiqc. Any changes to either
	// must be reflected
	static public final String PARAM_DATASET_FOLDER = "datasetFolder";
	static public final String PARAM_MODE = "mode";
	static public final String MODE_FOLDER = "Folder";

	public static final String QC_USAGE = String.format("/%s?%s=&ltDATASETID&gt&%s=&ltPATH/TO/DATASET/DIR&gt", QC_START,
			PARAM_DATASET_ID, PARAM_DIRECTORY);

	static final boolean PRETTY_PRINT_JSON = true;

	@Autowired
	SubmitJobForPluginService submitJobForPluginService;

	@Autowired
	EntitiesService entitiesService;

	@RequestMapping(value = QCJOBS, method = RequestMethod.POST)
	@ResponseBody
	public SubmitJobResponse qcStartWSAPIBP(@RequestParam(PARAM_DATASET_ID) Long datasetId,
			@RequestParam(value = FORCE_RESTART, required = false, defaultValue = "false") Boolean forceRestart,
			@RequestParam(PARAM_DIRECTORY) String directory, HttpServletRequest request) throws Exception {
		return qcStart(datasetId, forceRestart, directory, request);
	}

	/**
	 * See also:
	 * {@link IPAddressAuthenticationProvider#checkIfAddressIsValid(String, String)}
	 */
	@RequestMapping(value = SLASH_QC_START, method = RequestMethod.GET)
	@ResponseBody
	public SubmitJobResponse qcStart(@RequestParam(PARAM_DATASET_ID) Long datasetId,
			@RequestParam(value = FORCE_RESTART, required = false, defaultValue = "false") Boolean forceRestart,
			@RequestParam(PARAM_DIRECTORY) String directory, HttpServletRequest request) throws Exception {
		// If the auto-configuration of IPAddressAuthenticationProvide does not
		// work
		// then we need to use this code fragment.
		// Note: this throws AuthenticationException if the address is not valid
		IPAddressAuthenticationProvider.checkIfAddressIsValid(request.getRequestURI(), request.getRemoteAddr(),
				GobiiConfiguration.getIpAddressRegex());

		// So - client seems to be acceptable...
		if (Check.isEmpty(directory)) {
			throw new Exception(String.format("%s must not be empty", PARAM_DIRECTORY));
		}
		HashMap<String, String> allRequestParams = new HashMap<>();
		allRequestParams.put(PARAM_DATASET_FOLDER, directory);
		allRequestParams.put(PARAM_MODE, MODE_FOLDER);

		GobiiDataset byId = entitiesService.getEntityStore().getById(GobiiDataset.class, datasetId);
		if (byId != null) {
			// DatasetId already exists, remove if forcerestart
			if (!forceRestart) {
				return new StartResponse(byId.getAnalysisJob().getId());
			} else {
				try {
					entitiesService.getJobScheduler().cancelJob(byId.getAnalysisJob());
				} catch (JobNotFoundException ignore) {

				}
				entitiesService.getEntityStore().remove(byId);
				entitiesService.getEntityStore().remove(byId.getAnalysisJob());
				entitiesService.getEntityStore().remove(byId.getAnalysisJob().getAnalysisRequest());
			}
		}

		SubmitJobResponse result = submitJobForPluginService.submitPluginJob(QC_DEFAULT_USER, allRequestParams,
				GobiiConfiguration.getPluginName(), QC_USAGE,
				(responseJob) -> new JobIdResponse(responseJob.getAnalysisRequest().getId()),
				(analysisJob) -> new Function<ResponseJob, SubmitJobResponse>() {

					@Override
					public SubmitJobResponse apply(ResponseJob input) {
						return null; // Gobii will use qcStatus to continuously
						// query for job completion
					}
				}, (analysisJob) -> {
					GobiiDataset gobiiDataset = new GobiiDataset(datasetId, analysisJob);
					entitiesService.getEntityStore().persist(gobiiDataset);
				});

		return result;
	}

}
