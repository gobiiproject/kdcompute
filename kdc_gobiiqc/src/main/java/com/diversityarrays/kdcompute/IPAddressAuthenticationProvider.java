/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.diversityarrays.kdcompute.util.Check;

@SuppressWarnings("nls")
public class IPAddressAuthenticationProvider implements AuthenticationProvider {

	static private final Logger LOGGER = Logger.getLogger(IPAddressAuthenticationProvider.class.getName());

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Object obj = authentication.getDetails();
		if (obj instanceof WebAuthenticationDetails) {
			WebAuthenticationDetails details = (WebAuthenticationDetails) obj;

			String remoteAddress = details.getRemoteAddress();
			checkIfAddressIsValid("authenticate-session=" + details.getSessionId(), remoteAddress,
					GobiiConfiguration.getIpAddressRegex());
			return authentication;
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return Authentication.class.isAssignableFrom(authentication);
	}

	// static method so that the same logic can be used from multiple places
	/**
	 * return if the address is authorised else throw AuthenticationException
	 * 
	 * @param requestUri
	 *            provide null to not log (i.e. from unit tests)
	 * @param remoteAddress
	 *            the remote address to check
	 * @param regex
	 *            the regular expression to match against
	 * @throws AuthenticationException
	 *             if access should be denied
	 */
	static public void checkIfAddressIsValid(String requestUri, String remoteAddress, String regex)
			throws AuthenticationException {
		if (Check.isEmpty(regex)) {
			throw new MissingRegex();
		}
		if (Check.isEmpty(remoteAddress)) {
			throw new NoRemoteAddress();
		}
		if (!remoteAddress.matches(regex)) {
			if (requestUri != null) {
				LOGGER.log(Level.WARNING, "Access denied for " + requestUri + " from address: " + remoteAddress
						+ ". Allowed REGEX=" + regex);
			}
			throw new UnauthorisedAddress(remoteAddress);
		}
		if (requestUri != null) {
			LOGGER.log(Level.INFO, "Access granted for " + requestUri + " from " + remoteAddress);
		}
	}

	// Concrete sub-classes of AuthenticationException
	static public class MissingRegex extends AuthenticationException {
		public MissingRegex() {
			super("Unable to authenticate - no regex provided");
		}
	}

	static public class NoRemoteAddress extends AuthenticationException {
		public NoRemoteAddress() {
			super("Unable to authenticate - remoteAddress is empty");
		}
	}

	static public class UnauthorisedAddress extends AuthenticationException {
		public UnauthorisedAddress(String addr) {
			super("Unauthorised remoteAddr: " + addr);
		}
	}
}
