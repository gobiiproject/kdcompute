/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.diversityarrays.api.ResponseJob;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.db.PluginVersion;
import com.diversityarrays.kdcompute.db.PostCompletionCall;
import com.diversityarrays.kdcompute.db.PostSubmissionCall;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.AnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.AnalysisJobRunner;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.InMemoryEntityStore;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.JobQueueException;
import com.diversityarrays.kdcompute.runtime.ParameterException;
import com.diversityarrays.kdcompute.runtime.jobscheduler.BashCallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.GroovyCallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.GroovyNotInstalledException;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;
import com.diversityarrays.kdcompute.submitjob.PluginJobParams;
import com.diversityarrays.kdcompute.submitjob.SubmitJobForPluginService;
import com.diversityarrays.kdcompute.submitjob.SubmitJobResponse;
import com.diversityarrays.kdcompute.submitjob.SubmitJobUtils;
import com.diversityarrays.kdcompute.submitjob.SubmitPluginJob;
import com.diversityarrays.kdcompute.util.Either;

@SuppressWarnings("nls")
public class MockSubmitJobForPluginService implements SubmitJobForPluginService {

	private static List<PluginFiles> PLUGIN_FILES = null; // MockProvider.getDropinPluginFiles();

	// private final FileStorage fileStorage;
	private final EntityStore entityStore;
	private final JobQueue jobQueue;
	private final AnalysisJobFactory analysisJobFactory;

	private final BiFunction<String, PluginVersion, Plugin> pluginProvider = new BiFunction<String, PluginVersion, Plugin>() {
		@Override
		public Plugin apply(String pluginName, PluginVersion pluginVersion) {

			Plugin result = null;

			if (entityStore != null) {
				result = entityStore.findPluginFromPluginNameVersion(new PluginNameVersion(pluginName, pluginVersion));
			} else {
				Predicate<Plugin> predicate = (p) -> pluginName.equals(p.getPluginName());
				if (pluginVersion != null) {
					predicate = predicate.and((p) -> pluginVersion.equals(p.getVersion()));
				}
				List<Plugin> plugins = entityStore.getPlugins().stream().filter(predicate).collect(Collectors.toList());

				if (!plugins.isEmpty()) {
					if (plugins.size() > 1) {
						// There is more than one and we want the most recent
						Collections.sort(plugins, InMemoryEntityStore.REVERSE_VERSION_NUMBER);
					}
					result = plugins.get(0);
				}
			}
			return result;
		}
	};

	private final Supplier<List<String>> pluginNamesSupplier = new Supplier<List<String>>() {
		@Override
		public List<String> get() {
			return PLUGIN_FILES.stream().map(pf -> pf.getPlugin().getPluginName()).collect(Collectors.toList());
		}
	};

	private final Function<String, Either<IOException, File>> tmpdirFactory = new Function<String, Either<IOException, File>>() {
		@Override
		public Either<IOException, File> apply(String userName) {
			File result = new File(tmpdir, userName);
			if (result.isDirectory() || result.mkdirs()) {
				return Either.right(result);
			}
			return Either.left(new IOException("Unable to create dir " + result.getPath()));
		}
	};

	private Supplier<String> appVersionSupplier = new Supplier<String>() {
		@Override
		public String get() {
			return "99.99";
		}
	};

	private final Function<AnalysisJob, String> commandBuilder = new Function<AnalysisJob, String>() {
		@Override
		public String apply(AnalysisJob job) {
			return String.format("/%s/%s/%d", MockProvider.JOBSUMMARY, job.getRequestingUser(), job.getId());
		}
	};

	private final Function<PluginJobParams, Either<Exception, AnalysisJob>> jobSubmitter = new Function<PluginJobParams, Either<Exception, AnalysisJob>>() {
		@Override
		public Either<Exception, AnalysisJob> apply(PluginJobParams params) {
			return SubmitJobUtils.submitJob(params, (obj) -> entityStore.persist(obj), (req) -> createJobOrError(req),
					(job) -> doJobQueueSubmit(params, job));
			// SubmittedFormController.submitJob(params);
		}
	};

	private final CallScript bashCallScript = new BashCallScript();
	private final Function<File, Optional<CallScript>> callScriptProvider = new Function<File, Optional<CallScript>>() {
		private String groovyCommand = "/usr/local/bin/groovy";

		@Override
		public Optional<CallScript> apply(File scriptFile) {
			if (scriptFile.getName().endsWith(".sh")) {
				return Optional.of(bashCallScript);
			} else if (scriptFile.getName().endsWith(".groovy")) {
				try {
					return Optional.of(new GroovyCallScript(groovyCommand));
				} catch (GroovyNotInstalledException | InterruptedException | IOException e) {
					return Optional.empty();
				}
			} else {
				throw new RuntimeException("Unsupported script file: " + scriptFile.getName());
			}
		}
	};

	private final Function<AnalysisJob, Exception> statusChangeConsumer = new Function<AnalysisJob, Exception>() {
		@Override
		public Exception apply(AnalysisJob t) {
			System.out.println(String.format("Status changed to %s: jobId=%d", t.getRunState().name(), t.getId()));
			return null;
		}
	};

	private AdditionalVariables additionalVariables = AdditionalVariables.emptyAdditionalVariables();

	private final File tmpdir;
	private final Thread jobRunnerThread;
	private final AnalysisJobRunner jobRunner;

	/* PACKAGE */ MockSubmitJobForPluginService(File tmpdir, EntityStore es, JobQueue jq, AnalysisJobFactory jf) {
		this.tmpdir = tmpdir;
		entityStore = es;
		jobQueue = jq;
		analysisJobFactory = jf;
		jobRunner = new AnalysisJobRunner(callScriptProvider, AnalysisJobRunner.DEFAULT_STDOUT_PROVIDER,
				AnalysisJobRunner.DEFAULT_STDERR_PROVIDER, statusChangeConsumer, additionalVariables);

		jobRunnerThread = new Thread(() -> doRunJobs());
		// jobRunnerThread.setDaemon(true); // TODO make this true again
		jobRunnerThread.start();
	}

	public boolean anyExtantJobs() {
		return runningJob != null || jobQueue.getQueueLength() > 0;
	}

	public Optional<AnalysisJob> getAnalysisJob(long jobId) {
		return Optional.ofNullable(entityStore.getById(AnalysisJob.class, jobId));
	}

	private AnalysisJob runningJob;

	private void doRunJobs() {
		while (true) {
			try {
				runningJob = jobQueue.takeNextJob(1000);
				if (runningJob != null) {
					jobRunner.runJob(runningJob, 10_000, "Job error in Mock");
				}
			} catch (InterruptedException e) {
				System.err.println("jobRunningThread Interrupted");
			}
		}
	}

	private Optional<JobQueueException> doJobQueueSubmit(PluginJobParams params, AnalysisJob job) {
		try {
			jobQueue.submitJob(job);
			params.onSubmission(job);
			return Optional.empty();
		} catch (JobQueueException e) {
			return Optional.of(e);
		}
	}

	private Either<Exception, AnalysisJob> createJobOrError(AnalysisRequest req) {
		try {
			Map<String, String> vars = additionalVariables
					.instantiateAdditionalKDComputeSystemVariables(req.getForUser());
			AnalysisJob job = analysisJobFactory.createAnalysisJob(req, req.getForUser(),
					(file) -> System.out.println(file.getPath()), vars);
			return Either.right(job);
		} catch (IOException | ParameterException | UserIsNotLoggedInException e) {
			return Either.left(e);
		}
	}

	// The production (non-Mock) implementation calls its static method
	// which needs to know about KdcApplication.
	// But we don't have one so we parallel code.
	@Override
	public SubmitJobResponse submitPluginJob(String requestingUser, Map<String, String> allRequestParams,
			String pluginName, String base_usage, Function<ResponseJob, SubmitJobResponse> responseFactory,
			PostCompletionCall postCompletionCall, PostSubmissionCall postSubmissionCall) throws Exception {
		SubmitPluginJob spj = new SubmitPluginJob(pluginProvider, pluginNamesSupplier, tmpdirFactory,
				appVersionSupplier, commandBuilder);

		return spj.submitJobForPlugin(allRequestParams, requestingUser, pluginName, base_usage, responseFactory,
				postCompletionCall, postSubmissionCall, jobSubmitter);
	}
}
