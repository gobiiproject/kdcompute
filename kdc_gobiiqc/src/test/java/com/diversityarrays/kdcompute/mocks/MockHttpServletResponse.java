/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class MockHttpServletResponse implements HttpServletResponse {

    private String contentType;

    private Map<String, List<String>> headers = new LinkedHashMap<>();
    public byte[] getOutputBytes() {
        if (outputStream != null) {
            return outputStream.getOutputBytes();
        }
        return new byte[0];
    }

	@Override
	public String getCharacterEncoding() {
		throw new UnsupportedOperationException();

	}

	@Override
	public String getContentType() {
	    return contentType;
	}

	private MyServletOutputStream outputStream;

    private long contentLength;

    @Override
	public ServletOutputStream getOutputStream() throws IOException {
	    if (outputStream == null) {
	        outputStream = new MyServletOutputStream();
	    }
		return outputStream;
	}

	class MyServletOutputStream extends ServletOutputStream {

//	    private OutputStream out;

	    private ByteArrayOutputStream baos = new ByteArrayOutputStream();
        private byte[] finalOutputBytes;

        MyServletOutputStream() throws IOException {
	        super();
//	        PipedInputStream pipeInput = new PipedInputStream();
//	        out = new BufferedOutputStream(new PipedOutputStream(pipeInput));
	    }

        public byte[] getOutputBytes() {
            if (baos == null) {
                return finalOutputBytes;
            }
            try { baos.flush(); }
            catch (IOException ignore) { }
            return baos.toByteArray();
        }

        @Override
        public boolean isReady() {
            //Return true
            return true;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {
            //Do nothing
        }

        @Override
        public void write(int b) throws IOException {
            baos.write(b);
        }

        @Override
        public void flush() throws IOException {
            baos.flush();
        }

        @Override
        public void close() throws IOException {
            if (baos != null) {
                ByteArrayOutputStream tmp = baos;
                baos = null;
                tmp.close();
                finalOutputBytes = baos.toByteArray();
            }
        }

	}

	@Override
	public PrintWriter getWriter() throws IOException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setCharacterEncoding(String charset) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setContentLength(int len) {
	    contentLength = len;
	}

	@Override
	public void setContentLengthLong(long len) {
	    contentLength = len;
	}

	@Override
	public void setContentType(String type) {
	    contentType = type;
	}

	@Override
	public void setBufferSize(int size) {
		throw new UnsupportedOperationException();

	}

	@Override
	public int getBufferSize() {
//		throw new UnsupportedOperationException();
		return 0;
	}

	@Override
	public void flushBuffer() throws IOException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void resetBuffer() {
		throw new UnsupportedOperationException();

	}

	@Override
	public boolean isCommitted() {
//		throw new UnsupportedOperationException();
		return false;
	}

	@Override
	public void reset() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setLocale(Locale loc) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Locale getLocale() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void addCookie(Cookie cookie) {
		throw new UnsupportedOperationException();

	}

	@Override
	public boolean containsHeader(String name) {
//		throw new UnsupportedOperationException();
		return false;
	}

	@Override
	public String encodeURL(String url) {
		throw new UnsupportedOperationException();

	}

	@Override
	public String encodeRedirectURL(String url) {
		throw new UnsupportedOperationException();

	}

	@Override
	public String encodeUrl(String url) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String encodeRedirectUrl(String url) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void sendError(int sc, String msg) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void sendError(int sc) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void sendRedirect(String location) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDateHeader(String name, long date) {
		setHeader(name, String.valueOf(date));
	}

	@Override
	public void addDateHeader(String name, long date) {
	    addHeader(name, String.valueOf(date));
	}

	@Override
	public void setHeader(String name, String value) {
	    List<String> list = new ArrayList<>();
	    list.add(value);
	    headers.put(name, list);
	}

	@Override
	public void addHeader(String name, String value) {
	    List<String> list = headers.get(name);
	    if (list == null) {
	        list = new ArrayList<>();
	        headers.put(name, list);
	    }
	    list.add(value);
	}

	@Override
	public void setIntHeader(String name, int value) {
	    setHeader(name, String.valueOf(value));
	}

	@Override
	public void addIntHeader(String name, int value) {
        addHeader(name, String.valueOf(value));
	}

	@Override
	public void setStatus(int sc) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setStatus(int sc, String sm) {
		throw new UnsupportedOperationException();

	}

	@Override
	public int getStatus() {
//		throw new UnsupportedOperationException();
		return 0;
	}

	@Override
	public String getHeader(String name) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Collection<String> getHeaders(String name) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Collection<String> getHeaderNames() {
		throw new UnsupportedOperationException();

	}

}
