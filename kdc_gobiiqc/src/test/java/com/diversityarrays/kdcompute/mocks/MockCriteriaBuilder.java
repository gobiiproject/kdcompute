/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Tuple;
import javax.persistence.criteria.CollectionJoin;
import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.MapJoin;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.SetJoin;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.MapAttribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;

import com.diversityarrays.kdcompute.db.other.GobiiDataset;

@SuppressWarnings("nls")
class MockCriteriaBuilder implements CriteriaBuilder {

    /*package*/ MockCriteriaBuilder() {
    }

	@Override
	public CriteriaQuery<Object> createQuery() {
		throw new UnsupportedOperationException();

	}

	/**
	 * Creates mock gobii dataset class
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> CriteriaQuery<T> createQuery(Class<T> resultClass) {
	    if (! GobiiDataset.class.equals(resultClass)) {
	        throw new RuntimeException("Unsupported resultClass=" + resultClass.getName()); //$NON-NLS-1$
	    }
		return (CriteriaQuery<T>) new CriteriaQuery<GobiiDataset>() {

			/**
			 * Returning the mocked Root<X>
			 */
			@Override
			public <X> Root<X> from(Class<X> entityClass) {

			    if (! GobiiDataset.class.equals(entityClass)) {
			        throw new UnsupportedOperationException("Only working for GobiiDataset.class");
			    }

				return (Root<X>) new Root<GobiiDataset>() {

					@Override
					public Set<Join<GobiiDataset, ?>> getJoins() {
						throw new UnsupportedOperationException();

					}

					@Override
					public boolean isCorrelated() {
//						throw new UnsupportedOperationException();
						return false;
					}

					@Override
					public From<GobiiDataset, GobiiDataset> getCorrelationParent() {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Join<GobiiDataset, Y> join(SingularAttribute<? super GobiiDataset, Y> attribute) {
					    throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Join<GobiiDataset, Y> join(SingularAttribute<? super GobiiDataset, Y> attribute,
							JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> CollectionJoin<GobiiDataset, Y> join(
							CollectionAttribute<? super GobiiDataset, Y> collection) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> SetJoin<GobiiDataset, Y> join(SetAttribute<? super GobiiDataset, Y> set) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> ListJoin<GobiiDataset, Y> join(ListAttribute<? super GobiiDataset, Y> list) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <K, V> MapJoin<GobiiDataset, K, V> join(MapAttribute<? super GobiiDataset, K, V> map) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> CollectionJoin<GobiiDataset, Y> join(
							CollectionAttribute<? super GobiiDataset, Y> collection, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> SetJoin<GobiiDataset, Y> join(SetAttribute<? super GobiiDataset, Y> set,
							JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> ListJoin<GobiiDataset, Y> join(ListAttribute<? super GobiiDataset, Y> list,
							JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <K, V> MapJoin<GobiiDataset, K, V> join(MapAttribute<? super GobiiDataset, K, V> map,
							JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> Join<XX, Y> join(String attributeName) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> CollectionJoin<XX, Y> joinCollection(String attributeName) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> SetJoin<XX, Y> joinSet(String attributeName) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> ListJoin<XX, Y> joinList(String attributeName) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, K, V> MapJoin<XX, K, V> joinMap(String attributeName) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> Join<XX, Y> join(String attributeName, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> CollectionJoin<XX, Y> joinCollection(String attributeName, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> SetJoin<XX, Y> joinSet(String attributeName, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> ListJoin<XX, Y> joinList(String attributeName, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, K, V> MapJoin<XX, K, V> joinMap(String attributeName, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public Path<?> getParentPath() {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Path<Y> get(SingularAttribute<? super GobiiDataset, Y> attribute) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <E, C extends Collection<E>> Expression<C> get(
							PluralAttribute<GobiiDataset, C, E> collection) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <K, V, M extends Map<K, V>> Expression<M> get(MapAttribute<GobiiDataset, K, V> map) {
						throw new UnsupportedOperationException();

					}

					@Override
					public Expression<Class<? extends GobiiDataset>> type() {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Path<Y> get(String attributeName) {
//						throw new UnsupportedOperationException();
						return null;
					}

					@Override
					public Predicate isNull() {
						throw new UnsupportedOperationException();

					}

					@Override
					public Predicate isNotNull() {
						throw new UnsupportedOperationException();

					}

					@Override
					public Predicate in(Object... values) {
						throw new UnsupportedOperationException();

					}

					@Override
					public Predicate in(Expression<?>... values) {
						throw new UnsupportedOperationException();

					}

					@Override
					public Predicate in(Collection<?> values) {
						throw new UnsupportedOperationException();

					}

					@Override
					public Predicate in(Expression<Collection<?>> values) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX> Expression<XX> as(Class<XX> type) {
						throw new UnsupportedOperationException();
					}

					@Override
					public Selection<GobiiDataset> alias(String name) {
						throw new UnsupportedOperationException();
					}

					@Override
					public boolean isCompoundSelection() {
//						throw new UnsupportedOperationException();
						return false;
					}

					@Override
					public List<Selection<?>> getCompoundSelectionItems() {
						throw new UnsupportedOperationException();
					}

					@Override
					public Class<? extends GobiiDataset> getJavaType() {
						throw new UnsupportedOperationException();

					}

					@Override
					public String getAlias() {
						throw new UnsupportedOperationException();

					}

					@Override
					public Set<Fetch<GobiiDataset, ?>> getFetches() {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Fetch<GobiiDataset, Y> fetch(SingularAttribute<? super GobiiDataset, Y> attribute) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Fetch<GobiiDataset, Y> fetch(SingularAttribute<? super GobiiDataset, Y> attribute,
							JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Fetch<GobiiDataset, Y> fetch(PluralAttribute<? super GobiiDataset, ?, Y> attribute) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <Y> Fetch<GobiiDataset, Y> fetch(PluralAttribute<? super GobiiDataset, ?, Y> attribute,
							JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> Fetch<XX, Y> fetch(String attributeName) {
						throw new UnsupportedOperationException();

					}

					@Override
					public <XX, Y> Fetch<XX, Y> fetch(String attributeName, JoinType jt) {
						throw new UnsupportedOperationException();

					}

					@Override
					public EntityType<GobiiDataset> getModel() {
						throw new UnsupportedOperationException();

					}

				};
			}

			@Override
			public <X> Root<X> from(EntityType<X> entity) {
				throw new UnsupportedOperationException();

			}

			@Override
			public Set<Root<?>> getRoots() {
				throw new UnsupportedOperationException();

			}

			@Override
			public Selection<GobiiDataset> getSelection() {
				throw new UnsupportedOperationException();

			}

			@Override
			public List<Expression<?>> getGroupList() {
				throw new UnsupportedOperationException();

			}

			@Override
			public Predicate getGroupRestriction() {
				throw new UnsupportedOperationException();

			}

			@Override
			public boolean isDistinct() {
//				throw new UnsupportedOperationException();
				return false;
			}

			@Override
			public Class<GobiiDataset> getResultType() {
				throw new UnsupportedOperationException();

			}

			@Override
			public <U> Subquery<U> subquery(Class<U> type) {
				throw new UnsupportedOperationException();

			}

			@Override
			public Predicate getRestriction() {
				throw new UnsupportedOperationException();

			}

			/**
			 * Simply returns itself for this mock.
			 */
			@Override
			public CriteriaQuery<GobiiDataset> select(Selection<? extends GobiiDataset> selection) {
				return this;
			}

			@Override
			public CriteriaQuery<GobiiDataset> multiselect(Selection<?>... selections) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> multiselect(List<Selection<?>> selectionList) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> where(Expression<Boolean> restriction) {
//				throw new UnsupportedOperationException();
				return this;
			}

			@Override
			public CriteriaQuery<GobiiDataset> where(Predicate... restrictions) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> groupBy(Expression<?>... grouping) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> groupBy(List<Expression<?>> grouping) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> having(Expression<Boolean> restriction) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> having(Predicate... restrictions) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> orderBy(Order... o) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> orderBy(List<Order> o) {
				throw new UnsupportedOperationException();

			}

			@Override
			public CriteriaQuery<GobiiDataset> distinct(boolean distinct) {
				throw new UnsupportedOperationException();

			}

			@Override
			public List<Order> getOrderList() {
				throw new UnsupportedOperationException();

			}

			@Override
			public Set<ParameterExpression<?>> getParameters() {
				throw new UnsupportedOperationException();

			}

		};
	}

	@Override
	public CriteriaQuery<Tuple> createTupleQuery() {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> CriteriaUpdate<T> createCriteriaUpdate(Class<T> targetEntity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> CriteriaDelete<T> createCriteriaDelete(Class<T> targetEntity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> CompoundSelection<Y> construct(Class<Y> resultClass, Selection<?>... selections) {
		throw new UnsupportedOperationException();

	}

	@Override
	public CompoundSelection<Tuple> tuple(Selection<?>... selections) {
		throw new UnsupportedOperationException();

	}

	@Override
	public CompoundSelection<Object[]> array(Selection<?>... selections) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Order asc(Expression<?> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Order desc(Expression<?> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<Double> avg(Expression<N> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> sum(Expression<N> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Long> sumAsLong(Expression<Integer> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Double> sumAsDouble(Expression<Float> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> max(Expression<N> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> min(Expression<N> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X extends Comparable<? super X>> Expression<X> greatest(Expression<X> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X extends Comparable<? super X>> Expression<X> least(Expression<X> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Long> count(Expression<?> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Long> countDistinct(Expression<?> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate exists(Subquery<?> subquery) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> all(Subquery<Y> subquery) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> some(Subquery<Y> subquery) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> any(Subquery<Y> subquery) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate and(Expression<Boolean> x, Expression<Boolean> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate and(Predicate... restrictions) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate or(Expression<Boolean> x, Expression<Boolean> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate or(Predicate... restrictions) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate not(Expression<Boolean> restriction) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate conjunction() {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate disjunction() {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate isTrue(Expression<Boolean> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate isFalse(Expression<Boolean> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate isNull(Expression<?> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate isNotNull(Expression<?> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate equal(Expression<?> x, Expression<?> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate equal(Expression<?> x, Object y) {
//		throw new UnsupportedOperationException();
		return null;
	}

	@Override
	public Predicate notEqual(Expression<?> x, Expression<?> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notEqual(Expression<?> x, Object y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate greaterThan(Expression<? extends Y> x,
			Expression<? extends Y> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate greaterThan(Expression<? extends Y> x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate greaterThanOrEqualTo(Expression<? extends Y> x,
			Expression<? extends Y> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate greaterThanOrEqualTo(Expression<? extends Y> x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate lessThan(Expression<? extends Y> x,
			Expression<? extends Y> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate lessThan(Expression<? extends Y> x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate lessThanOrEqualTo(Expression<? extends Y> x,
			Expression<? extends Y> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate lessThanOrEqualTo(Expression<? extends Y> x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate between(Expression<? extends Y> v, Expression<? extends Y> x,
			Expression<? extends Y> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y extends Comparable<? super Y>> Predicate between(Expression<? extends Y> v, Y x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate gt(Expression<? extends Number> x, Expression<? extends Number> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate gt(Expression<? extends Number> x, Number y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate ge(Expression<? extends Number> x, Expression<? extends Number> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate ge(Expression<? extends Number> x, Number y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate lt(Expression<? extends Number> x, Expression<? extends Number> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate lt(Expression<? extends Number> x, Number y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate le(Expression<? extends Number> x, Expression<? extends Number> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate le(Expression<? extends Number> x, Number y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> neg(Expression<N> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> abs(Expression<N> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> sum(Expression<? extends N> x, Expression<? extends N> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> sum(Expression<? extends N> x, N y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> sum(N x, Expression<? extends N> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> prod(Expression<? extends N> x, Expression<? extends N> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> prod(Expression<? extends N> x, N y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> prod(N x, Expression<? extends N> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> diff(Expression<? extends N> x, Expression<? extends N> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> diff(Expression<? extends N> x, N y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <N extends Number> Expression<N> diff(N x, Expression<? extends N> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Number> quot(Expression<? extends Number> x, Expression<? extends Number> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Number> quot(Expression<? extends Number> x, Number y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Number> quot(Number x, Expression<? extends Number> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> mod(Expression<Integer> x, Expression<Integer> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> mod(Expression<Integer> x, Integer y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> mod(Integer x, Expression<Integer> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Double> sqrt(Expression<? extends Number> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Long> toLong(Expression<? extends Number> number) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> toInteger(Expression<? extends Number> number) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Float> toFloat(Expression<? extends Number> number) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Double> toDouble(Expression<? extends Number> number) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<BigDecimal> toBigDecimal(Expression<? extends Number> number) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<BigInteger> toBigInteger(Expression<? extends Number> number) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> toString(Expression<Character> character) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> Expression<T> literal(T value) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> Expression<T> nullLiteral(Class<T> resultClass) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> ParameterExpression<T> parameter(Class<T> paramClass) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> ParameterExpression<T> parameter(Class<T> paramClass, String name) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <C extends Collection<?>> Predicate isEmpty(Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <C extends Collection<?>> Predicate isNotEmpty(Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <C extends Collection<?>> Expression<Integer> size(Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <C extends Collection<?>> Expression<Integer> size(C collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <E, C extends Collection<E>> Predicate isMember(Expression<E> elem, Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <E, C extends Collection<E>> Predicate isMember(E elem, Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <E, C extends Collection<E>> Predicate isNotMember(Expression<E> elem, Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <E, C extends Collection<E>> Predicate isNotMember(E elem, Expression<C> collection) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <V, M extends Map<?, V>> Expression<Collection<V>> values(M map) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <K, M extends Map<K, ?>> Expression<Set<K>> keys(M map) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate like(Expression<String> x, Expression<String> pattern) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate like(Expression<String> x, String pattern) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate like(Expression<String> x, Expression<String> pattern, Expression<Character> escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate like(Expression<String> x, Expression<String> pattern, char escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate like(Expression<String> x, String pattern, Expression<Character> escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate like(Expression<String> x, String pattern, char escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notLike(Expression<String> x, Expression<String> pattern) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notLike(Expression<String> x, String pattern) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notLike(Expression<String> x, Expression<String> pattern, Expression<Character> escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notLike(Expression<String> x, Expression<String> pattern, char escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notLike(Expression<String> x, String pattern, Expression<Character> escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Predicate notLike(Expression<String> x, String pattern, char escapeChar) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> concat(Expression<String> x, Expression<String> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> concat(Expression<String> x, String y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> concat(String x, Expression<String> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> substring(Expression<String> x, Expression<Integer> from) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> substring(Expression<String> x, int from) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> substring(Expression<String> x, Expression<Integer> from, Expression<Integer> len) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> substring(Expression<String> x, int from, int len) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> trim(Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> trim(Trimspec ts, Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> trim(Expression<Character> t, Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> trim(Trimspec ts, Expression<Character> t, Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> trim(char t, Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> trim(Trimspec ts, char t, Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> lower(Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<String> upper(Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> length(Expression<String> x) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> locate(Expression<String> x, Expression<String> pattern) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> locate(Expression<String> x, String pattern) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> locate(Expression<String> x, Expression<String> pattern, Expression<Integer> from) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Integer> locate(Expression<String> x, String pattern, int from) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<java.sql.Date> currentDate() {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Timestamp> currentTimestamp() {
		throw new UnsupportedOperationException();

	}

	@Override
	public Expression<Time> currentTime() {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> In<T> in(Expression<? extends T> expression) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> coalesce(Expression<? extends Y> x, Expression<? extends Y> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> coalesce(Expression<? extends Y> x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> nullif(Expression<Y> x, Expression<?> y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <Y> Expression<Y> nullif(Expression<Y> x, Y y) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> Coalesce<T> coalesce() {
		throw new UnsupportedOperationException();

	}

	@Override
	public <C, R> SimpleCase<C, R> selectCase(Expression<? extends C> expression) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <R> Case<R> selectCase() {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> Expression<T> function(String name, Class<T> type, Expression<?>... args) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, T, V extends T> Join<X, V> treat(Join<X, T> join, Class<V> type) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, T, E extends T> CollectionJoin<X, E> treat(CollectionJoin<X, T> join, Class<E> type) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, T, E extends T> SetJoin<X, E> treat(SetJoin<X, T> join, Class<E> type) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, T, E extends T> ListJoin<X, E> treat(ListJoin<X, T> join, Class<E> type) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, K, T, V extends T> MapJoin<X, K, V> treat(MapJoin<X, K, T> join, Class<V> type) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, T extends X> Path<T> treat(Path<X> path, Class<T> type) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <X, T extends X> Root<T> treat(Root<X> root, Class<T> type) {
		throw new UnsupportedOperationException();

	}

}
