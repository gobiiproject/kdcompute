/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.kdcompute.runtime.AnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.UsersStore;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobScheduler;

@SuppressWarnings("nls")
public class MockEntitiesService implements EntitiesService {

	private final EntityStore entityStore;

	private JobScheduler jobScheduler;

	/*package*/ MockEntitiesService(EntityStore s) {
	    this.entityStore = s;
	}

    @Override
	public String getVersion() {
		return "Mock-EntitiesService";
	}

	@Override
	public JobScheduler getJobScheduler() {
	    if (jobScheduler == null) {
	        synchronized (this) {
	            if (jobScheduler == null) {

	            }
	        }
	    }
		return jobScheduler;
	}

	@Override
	public UsersStore getUsersStore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntityStore getEntityStore() {
		return entityStore;
	}

	@Override
	public FileStorage getFileStorage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JobQueue getJobQueue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AnalysisJobFactory getAnalysisJobFactory() {
		// TODO Auto-generated method stub
		return null;
	}

}
