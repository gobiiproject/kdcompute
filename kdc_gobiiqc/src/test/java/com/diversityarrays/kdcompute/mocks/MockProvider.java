/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import com.diversityarrays.api.EntitiesService;
import com.diversityarrays.kdcompute.GobiiConfiguration;
import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.QCStart;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.db.other.GobiiDataset;
import com.diversityarrays.kdcompute.plugin.PluginUtil;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.AnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.ParameterException;
import com.diversityarrays.kdcompute.runtime.SimpleAnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.SimpleFileStorage;
import com.diversityarrays.kdcompute.runtime.jobscheduler.BashCallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobScheduler;
import com.diversityarrays.kdcompute.runtime.jobscheduler.SimpleJobScheduler;
import com.diversityarrays.kdcompute.runtime.mock.DummyEntityStore;
import com.diversityarrays.kdcompute.runtime.mock.DummyJobQueue;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;
import com.diversityarrays.kdcompute.util.Check;

@SuppressWarnings("nls")
public class MockProvider {

	public static final String INVALID_REMOTE_ADDRESS = "192.168.11.12";
	public static final String VALID_REMOTE_ADDRESS = "10.1.2.4";

	public static final String IPv4_REGEX_FOR_MISMATCH = "^127\\.0\\.0\\.1$";

	public final static String IPv4_REGEX = "^10\\.1\\.2\\.4$";
	// public final static String IPv4_REGEX =
	// "^\\d{1-3}\\.\\d{1-3}\\.\\d{1-3}\\.\\d{1-3}$";

	static public enum TestRegex {
		NULL_REGEX(null), IPV4("^10\\.1\\.2\\.4$"), MISMATCH("^127\\.0\\.0\\.1$"),;
		public final String value;

		TestRegex(String r) {
			value = r;
		}

		public boolean isValidRegex() {
			return this == IPV4;
		}
	}

	/**
	 * Mock Object provider;
	 */
	public MockProvider() {
	}

	static final String JOBSUMMARY = "/jobsummary";

	// public static final String GOBIIQC_PLUGIN_JSON = "gobiiqc_plugin.json";

	public static final String GOBII_NOTIFICATION_PATH = "/brapi/v1/instructions/qualitycontrol";
	public static final String MOCK_NOTIFICATION_URL = "http://localhost:9090" + GOBII_NOTIFICATION_PATH;

	private static final File PLUGINS_DIR = new File(System.getProperty("user.dir"), "plugins");

	private static final File KDC_PLUGINS_DIR = new File(System.getProperty("user.dir"),
			"UnitTestData" + File.separator + "KDC_Plugins");

	static public enum FolderOption {
		ANALYSIS_JOB_OUTPUTS("AnalysisJobOutputs"), KDC_PLUGINS("KDC_Plugins"), TEMP_DIR("tmp");
		public final String folderName;

		FolderOption(String s) {
			folderName = s;
		}
	}

	static public File getUnitTestDataFolder(FolderOption fo) {
		File userDir = new File(System.getProperty("user.dir"));
		File unitTestData = new File(userDir, "UnitTestData");
		return new File(unitTestData, fo.folderName);
	}

	static public List<PluginFiles> getDropinPluginFiles() {
		File kdcPluginsFolder = getUnitTestDataFolder(FolderOption.KDC_PLUGINS);
		File dropinFolder = new File(kdcPluginsFolder, SimpleFileStorage.DROPIN_DIRNAME);

		if (!dropinFolder.isDirectory()) {
			throw new RuntimeException("Missing directory: " + dropinFolder.getPath());
		}
		List<PluginFiles> result = new ArrayList<>();
		for (File d : dropinFolder.listFiles()) {
			if (PluginFiles.isPluginDir(d)) {
				try {
					result.add(PluginFiles.readPluginFiles(d));
				} catch (IOException e) {
					System.err.println("Unable to load Plugin from " + d.getPath());
				}
			}
		}
		return result;
	}

	static private EntityManager ENTITY_MANAGER = new MockEntityManager();
	static private FileStorage FILE_STORAGE;
	static private JobQueue JOB_QUEUE;

	static private EntityStore ENTITY_STORE;

	static synchronized public FileStorage getFileStorage() {
		if (FILE_STORAGE == null) {
			File resultsRoot = getUnitTestDataFolder(FolderOption.ANALYSIS_JOB_OUTPUTS);
			File pluginsDir = getUnitTestDataFolder(FolderOption.KDC_PLUGINS);
			FILE_STORAGE = new SimpleFileStorage();
			FILE_STORAGE.setResultsRootDir(resultsRoot);
			FILE_STORAGE.setKdcomputePluginsDir(pluginsDir);
		}
		return FILE_STORAGE;
	}

	static synchronized public JobQueue getJobQueue() {
		if (JOB_QUEUE == null) {
			JOB_QUEUE = new DummyJobQueue();
			JOB_QUEUE.setEntityStore(getEntityStore(false));
		}
		return JOB_QUEUE;
	}

	static public synchronized EntityStore getEntityStore(boolean withJobs) {
		if (ENTITY_STORE == null) {
			ENTITY_STORE = new DummyEntityStore();
			ENTITY_STORE.setEntityManager(ENTITY_MANAGER);
			ENTITY_STORE.setFileStorage(getFileStorage());

			populateWithPlugins(ENTITY_STORE);

			if (withJobs) {
				populateWithAnalysisJobs(ENTITY_STORE);
			}
		} else if (withJobs) {
			if (ENTITY_STORE.getAllAnalysisJobs().isEmpty()) {
				populateWithAnalysisJobs(ENTITY_STORE);
			}
		}
		return ENTITY_STORE;
	}

	static private final FileFilter PLUGIN_DIR_FILTER = new FileFilter() {
		@Override
		public boolean accept(File d) {
			return d.isDirectory() && PluginFiles.isPluginDir(d);
		}
	};

	private static void populateWithPlugins(EntityStore entityStore) {
		FileStorage fileStorage = entityStore.getFileStorage();

		List<File> alternatives = Arrays.asList(fileStorage.getDropinPluginsDir(), new File("plugins"));

		int nDropinsFound = 0;
		for (File dropinDir : alternatives) {
			nDropinsFound = copyPluginsToFileStorage(dropinDir, entityStore, fileStorage);
			if (nDropinsFound > 0) {
				break;
			}
		}

		if (nDropinsFound <= 0) {
			throw new RuntimeException(alternatives.stream().map(File::getPath)
					.collect(Collectors.joining("\n  ", "No plugins in : ", "")));
		}

	}

	private static int copyPluginsToFileStorage(File dropinsDir, EntityStore entityStore, FileStorage fileStorage) {
		int result = 0;
		if (dropinsDir.isDirectory()) {
			for (File d : dropinsDir.listFiles(PLUGIN_DIR_FILTER)) {
				try {
					Plugin plugin = PluginUtil.moveOrCopyPluginFrom(PluginUtil.COPY, d, fileStorage, true // can
																											// overwrite
					);
					entityStore.persist(plugin);
					++result;
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return result;
	}

	static private AnalysisJobFactory JOB_FACTORY;

	static private AnalysisJobFactory getAnalysisJobFactory() {
		if (JOB_FACTORY == null) {
			SimpleAnalysisJobFactory f = new SimpleAnalysisJobFactory();
			f.setFileStorage(getFileStorage());
			JOB_FACTORY = f;
		}
		return JOB_FACTORY;
	}

	static private final String TEST_DATA_FOLDER = "gobii_qc/gobiiqc_reader/GOBII_TestData/CornellWiki";
	public static final String PLUGIN_NAME = "kdcp_gobiiqc";
	public static final String MOCK_NOTIFICATION_EXCEPTION_URL = "http://localhost:9090" + GOBII_NOTIFICATION_PATH
			+ "/exception";

	static public List<File> getTestDataDirectories() {
		String path = System.getProperty("MockProvider.TEST_DATA_PATH");
		File dir;
		if (!Check.isEmpty(path)) {
			dir = new File(path);
		} else {
			// try to find the test data folder in gobiiqc/gobiiqc_reader

			File userDir = new File(System.getProperty("user.dir"));

			// ../..
			File p2 = userDir.getParentFile().getParentFile();
			dir = new File(p2, TEST_DATA_FOLDER);
		}
		if (!dir.isDirectory()) {
			throw new RuntimeException("Not a directory: " + dir.getPath());
		}

		return Arrays.asList(dir.listFiles()).stream().filter(File::isDirectory).collect(Collectors.toList());
	}

	private static void populateWithAnalysisJobs(EntityStore entityStore) {

		Plugin plugin = entityStore.findHighestVersionPlugin(PLUGIN_NAME);
		if (plugin == null) {
			RuntimeException ex = new RuntimeException(
					String.format("Failed to find Plugin '%s'", GobiiConfiguration.getPluginName()));
			ex.printStackTrace();
			throw ex;
		}
		Optional<Knob> optional = plugin.getKnobs().stream()
				.filter(k -> QCStart.PARAM_DATASET_FOLDER.equals(k.getKnobName())).findFirst();
		if (!optional.isPresent()) {
			RuntimeException ex = new RuntimeException("Missing KnobName=" + QCStart.PARAM_DATASET_FOLDER);
			ex.printStackTrace();
			throw ex;
		}
		Knob datasetFolderKnob = optional.get();

		optional = plugin.getKnobs().stream().filter(k -> QCStart.PARAM_MODE.equals(k.getKnobName())).findFirst();
		if (!optional.isPresent()) {
			RuntimeException ex = new RuntimeException("Missing KnobName=" + QCStart.PARAM_MODE);
			ex.printStackTrace();
			throw ex;
		}
		Knob modeFolderKnob = optional.get();

		SimpleAnalysisJobFactory jobFactory = new SimpleAnalysisJobFactory();
		jobFactory.setFileStorage(getFileStorage());

		List<File> testDataDirs = getTestDataDirectories();
		int testDataDirIndex = -1;

		AdditionalVariables additionalVariables = AdditionalVariables.emptyAdditionalVariables();
		int nDays = 0;
		for (RunState rs : RunState.values()) {
			++nDays;
			RunBinding runBinding = new RunBinding(plugin);

			if (++testDataDirIndex >= testDataDirs.size()) {
				testDataDirIndex = 0;
			}
			String datasetPath = testDataDirs.get(testDataDirIndex).getPath();

			List<KnobBinding> knobBindings = new ArrayList<>();

			knobBindings.add(new KnobBinding(datasetFolderKnob, datasetPath));
			knobBindings.add(new KnobBinding(modeFolderKnob, QCStart.MODE_FOLDER));

			Map<String, Knob> knobByName = plugin.getKnobs().stream()
					.collect(Collectors.toMap(Knob::getKnobName, Function.identity()));

			knobBindings.forEach(kb -> knobByName.remove(kb.getKnob().getKnobName()));
			try {
				addGobiiDataFileKnobs(datasetPath, knobByName, knobBindings);

				runBinding.setKnobBindings(knobBindings);

				AnalysisRequest ar = new AnalysisRequest(runBinding);
				entityStore.persist(ar);

				AnalysisJob job = jobFactory.createAnalysisJob(ar, QCStart.QC_DEFAULT_USER,
						(file) -> System.out.println("Created ResultsFolder: " + file.getPath()),
						additionalVariables.instantiateAdditionalKDComputeSystemVariables(QCStart.QC_DEFAULT_USER));
				job.setRunState(rs);

				modifyJobForRunState(job, rs, nDays);
				entityStore.persist(job);
			} catch (ParameterException | IOException | UserIsNotLoggedInException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}

	private static void addGobiiDataFileKnobs(String datasetPath, Map<String, Knob> knobByName,
			List<KnobBinding> knobBindings) throws ParameterException {
		Map<String, GobiiDataFile> gdfByKnobName = Arrays.asList(GobiiDataFile.values()).stream()
				.collect(Collectors.toMap(GobiiDataFile::getKnobName, Function.identity()));

		File dir = new File(datasetPath);
		if (!dir.isDirectory()) {
			throw new ParameterException(String.format("Missing datasetPath(%s)", datasetPath));
		}
		for (String knobName : knobByName.keySet()) {
			GobiiDataFile gdf = gdfByKnobName.get(knobName);
			if (gdf == null) {
				throw new ParameterException("No GobiiDataFile mapped to knobName=" + knobName);
			}
			String[] found = dir.list((d, name) -> name.matches(gdf.pattern));
			if (found.length <= 0) {
				throw new ParameterException("Missing file for " + gdf);
			}
			knobBindings.add(new KnobBinding(knobByName.get(knobName), found[0]));
		}
	}

	static private enum GobiiDataFile {
		MARKER_FILE("markerFile", Pattern.quote("marker.file")), SAMPLE_FILE("sampleFile",
				Pattern.quote("sample.file")), SUMMARY_FILE("summaryFile",
						Pattern.quote("summary.file")), HAPMAP_FILE("hapmapFile", "^.*\\.hmp\\.txt$"),;
		public final String knobName;
		public final String pattern;

		GobiiDataFile(String kn, String p) {
			knobName = kn;
			pattern = p;
		}

		@Override
		public String toString() {
			return name() + "(knob=" + knobName + ")";
		}

		public String getKnobName() {
			return knobName;
		}
	}

	private static void modifyJobForRunState(AnalysisJob job, RunState rs, int nDays) {
		Calendar calendar = Calendar.getInstance();
		if (RunState.NEW != rs) {
			// Started a few days before
			calendar.add(Calendar.DATE, -nDays);
		}
		job.setStartDateTime(calendar.getTime());
		calendar.add(Calendar.MINUTE, 10);
		Date endDate = calendar.getTime();

		switch (rs) {
		case CANCELLED:
			job.setEndDateTime(endDate);
			job.setExitCode(-1);
			break;
		case COMPLETED:
			job.setEndDateTime(endDate);
			job.setExitCode(0);
			break;
		case FAILED:
			job.setEndDateTime(endDate);
			job.setExitCode(2);
			job.setError("Mock error");
			break;
		case PAUSED:
		case RUNNING:
			break;
		case NEW:
			// nothing more to do
			break;
		case UNKNOWN:
			job.setEndDateTime(endDate);
			job.setExitCode(2);
			job.setError("Unknown state error");
			break;
		default:
			throw new RuntimeException("RunState=" + rs);
		}

		Date start = job.getStartDateTime();
		if (start != null) {
			Date end = job.getEndDateTime();
			if (end != null) {
				job.setCpuMillisConsumed(end.getTime() - start.getTime());
			}
		}
	}

	static public MockSubmitJobForPluginService getSubmitJobForPluginService() {
		MockSubmitJobForPluginService result = new MockSubmitJobForPluginService(
				getUnitTestDataFolder(FolderOption.TEMP_DIR), getEntityStore(false), getJobQueue(),
				getAnalysisJobFactory());
		return result;
	}

	static public enum ServiceOption {
		WITH_JOBS, WITHOUT_JOBS;

		public boolean wantJobs() {
			return this == WITH_JOBS;
		}
	}

	static public EntitiesService getEntitiesService(ServiceOption option) {
		EntitiesService result = new MockEntitiesService(getEntityStore(option.wantJobs()));
		return result;
	}

	static private JobScheduler JOB_SCHEDULER;

	static synchronized public JobScheduler getJobScheduler() {
		if (JOB_SCHEDULER == null) {
			SimpleJobScheduler simpleJobScheduler = new SimpleJobScheduler(JOB_QUEUE,
					AdditionalVariables.emptyAdditionalVariables());
			Map<String, CallScript> callScriptMap = new HashMap<>();
			callScriptMap.put("sh", new BashCallScript());
			simpleJobScheduler.setCallScriptMap(callScriptMap);
			JOB_SCHEDULER = simpleJobScheduler;

		}
		return JOB_SCHEDULER;
	}

	static public void main(String[] args) {

		int which = 1;
		switch (which) {
		case 1:
			EntityStore entityStore = getEntityStore(false); // no jobs
			List<Plugin> plugins = entityStore.getPlugins();
			System.out.println("nPlugins=" + plugins.size());
			plugins.forEach(p -> System.out.println(p.getPluginName()));
			break;

		case 2:
			List<PluginFiles> pluginsFiles = getDropinPluginFiles();
			System.out.println("Found " + pluginsFiles.size() + " Dropin Plugins");
			pluginsFiles.forEach(p -> System.out.println(p.getPlugin().getPluginName()));
			break;
		}
	}

	public static void addGobiiDataSets(EntityStore entityStore, Function<AnalysisJob, Long> datasetIdProvider) {
		entityStore.getAllAnalysisJobs().stream().map(job -> new Pair<>(datasetIdProvider.apply(job), job))
				.filter(pair -> pair.first != null).forEach(pair -> {
					GobiiDataset gds = new GobiiDataset(pair.first, pair.second);
					entityStore.persist(gds);
				});
	}
}
