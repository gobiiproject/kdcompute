/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;

import org.springframework.web.servlet.HandlerMapping;

import com.diversityarrays.kdcompute.util.Check;

@SuppressWarnings("nls")
public class MockHttpServletRequest implements HttpServletRequest {

    public static HttpServletRequest createValid(String uri) {
        return new MockHttpServletRequest(MockProvider.VALID_REMOTE_ADDRESS, uri);
    }

    public static HttpServletRequest createInvalid(String uri) {
        return new MockHttpServletRequest(MockProvider.INVALID_REMOTE_ADDRESS, uri);
    }


	private String remoteAddr;
    private String requestURI;
    private String servletPath = "/unitTestServlet";
    private Map<String, List<String>> parameterValuesByName = new HashMap<>();
    private final Map<String, Object> attributes = new HashMap<>();

//    public MockHttpServletRequest(String uri) {
//        this(MockHttpProvider.VALID_REMOTE_ADDRESS, uri);
//    }
//
    private MockHttpServletRequest(String addr, String uri) {
        this.remoteAddr = addr;
        this.requestURI = uri.startsWith("/") ? uri : "/" + uri;

        setAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE, requestURI);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + requestURI + " FROM " + remoteAddr + "]";
    }

    @Override
	public Object getAttribute(String name) {
        return attributes.get(name);
	}

	@Override
	public Enumeration<String> getAttributeNames() {
		return Collections.emptyEnumeration();
	}

	@Override
	public String getCharacterEncoding() {
		return null;
	}

	@Override
	public void setCharacterEncoding(String env) throws UnsupportedEncodingException {
	    throw new UnsupportedEncodingException(env);
	}

	@Override
	public int getContentLength() {
		return (servletPath + requestURI).length();
	}

	@Override
	public long getContentLengthLong() {
		return getContentLength();
	}

	@Override
	public String getContentType() {
		return null; // not known
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
	    throw new IOException("not implemented");
//		return null;
	}

	@Override
	public String getParameter(String name) {
	    List<String> list = parameterValuesByName.get(name);
		return Check.isEmpty(list) ? null : list.get(0);
	}

	@Override
	public Enumeration<String> getParameterNames() {
	    return new MyEnumeration<>(parameterValuesByName.keySet());
	}

	@Override
	public String[] getParameterValues(String name) {
	    List<String> list = parameterValuesByName.get(name);
	    if (Check.isEmpty(list)) {
	        return new String[0];
	    }
	    return list.toArray(new String[list.size()]);
	}

	@Override
	public Map<String, String[]> getParameterMap() {
	    Function<Map.Entry<String, List<String>>, String[]> valueMapper = new Function<Map.Entry<String, List<String>>, String[]>() {
            @Override
            public String[] apply(Map.Entry<String, List<String>> e) {
                List<String> list = e.getValue();
                return list.toArray(new String[list.size()]);
            }
        };
        return parameterValuesByName.entrySet().stream()
	        .collect(Collectors.toMap(Map.Entry::getKey, valueMapper));
	}

	@Override
	public String getProtocol() {
		return "HTTP/1.1";
	}

	@Override
	public String getScheme() {
		return "http";
	}

	@Override
	public String getServerName() {
		return "junit-test";
	}

	@Override
	public int getServerPort() {
		return 60000;
	}

	@Override
	public BufferedReader getReader() throws IOException {
	    throw new IOException("not implemented");
	}

	/**
	 * Returns mock IP
	 */
	@Override
	public String getRemoteAddr() {
		return remoteAddr;
	}

	@Override
	public String getRemoteHost() {
		return remoteAddr;
	}

	@Override
	public void setAttribute(String name, Object o) {
	    attributes.put(name, o);
	}

	@Override
	public void removeAttribute(String name) {
	    attributes.remove(name);
	}

	@Override
	public Locale getLocale() {
		return Locale.getDefault();
	}

	static class MyEnumeration<T> implements Enumeration<T> {

	    private final Iterator<T> iterator;

        public MyEnumeration(Collection<T> coll) {
	        this.iterator = coll.iterator();
	    }

        @Override
        public boolean hasMoreElements() {
            return iterator.hasNext();
        }

        @Override
        public T nextElement() {
            return iterator.next();
        }

	}

	@Override
	public Enumeration<Locale> getLocales() {
		List<Locale> locales = Arrays.asList(Locale.getAvailableLocales());
		return new MyEnumeration<>(locales);

	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String path) {
		return null;
	}

	@Override
	public String getRealPath(String path) {
		return null;
	}

	private int remotePort = -1;
	@Override
	public int getRemotePort() {
	    if (remotePort < 0) {
	        remotePort = new Random(System.currentTimeMillis()).nextInt((65535-1024)) + 1024;
	    }
		return remotePort;
	}

	@Override
	public String getLocalName() {
		return "127.0.0.1";
	}

	@Override
	public String getLocalAddr() {
		return getLocalName();
	}

	@Override
	public int getLocalPort() {
		return 8080;
	}

	@Override
	public ServletContext getServletContext() {
	    throw new UnsupportedOperationException();
	}

	@Override
	public AsyncContext startAsync() throws IllegalStateException {
        throw new UnsupportedOperationException();
	}

	@Override
	public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse)
			throws IllegalStateException {
        throw new UnsupportedOperationException();
	}

	@Override
	public boolean isAsyncStarted() {
		return false;
	}

	@Override
	public boolean isAsyncSupported() {
		return false;
	}

	@Override
	public AsyncContext getAsyncContext() {
		return null;
	}

	@Override
	public DispatcherType getDispatcherType() {
	    return DispatcherType.REQUEST;
	}

	@Override
	public String getAuthType() {
		return null; // not authenticated
	}

	@Override
	public Cookie[] getCookies() {
		return null; // no cookies
	}

	@Override
	public long getDateHeader(String name) {
		return -1; // no date header
	}

	private final Map<String, List<String>> headers = new HashMap<>();
	@Override
	public String getHeader(String name) {
		List<String> list = headers.get(name);
		return Check.isEmpty(list) ? null : list.get(0);
	}

	@Override
	public Enumeration<String> getHeaders(String name) {
	    List<String> list = headers.get(name);
	    if (Check.isEmpty(list)) {
	        return new MyEnumeration<>(Collections.emptyList());
	    }
	    return new MyEnumeration<>(list);
	}

	@Override
	public Enumeration<String> getHeaderNames() {
		return new MyEnumeration<>(headers.keySet());
	}

	@Override
	public int getIntHeader(String name) {
	    List<String> list = headers.get(name);
	    if (Check.isEmpty(list)) {
	        return -1;
	    }
		return Integer.parseInt(list.get(0));
	}

	@Override
	public String getMethod() {
		return "GET";
	}

	@Override
	public String getPathInfo() {
		return null;
	}

	@Override
	public String getPathTranslated() {
		return null;
	}

	@Override
	public String getContextPath() {
		return ""; // maybe change
	}

	@Override
	public String getQueryString() {
		return null;
	}

	@Override
	public String getRemoteUser() {
		return null;
	}

	@Override
	public boolean isUserInRole(String role) {
		return false; // not authenticated
	}

	@Override
	public Principal getUserPrincipal() {
		return null; // not authenticated
	}

	@Override
	public String getRequestedSessionId() {
		return null; // no session
	}

	/**
	 * Returns Mock URI for testing
	 */
	@Override
	public String getRequestURI() {
		return requestURI;
	}

	@Override
	public StringBuffer getRequestURL() {
		return new StringBuffer(requestURI);
	}

	@Override
	public String getServletPath() {
		return servletPath;
	}

	@Override
	public HttpSession getSession(boolean create) {
	    if (create) {
	        throw new UnsupportedOperationException("getSession(true)");
	    }
		return null;
	}

	@Override
	public HttpSession getSession() {
	    return getSession(true);
	}

	@Override
	public String changeSessionId() {
	    throw new UnsupportedOperationException();
	}

	@Override
	public boolean isRequestedSessionIdValid() {
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromCookie() {
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromURL() {
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromUrl() {
		return false;
	}

	@Override
	public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        throw new UnsupportedOperationException();
//		return false;
	}

	@Override
	public void login(String username, String password) throws ServletException {
		throw new ServletException("login not supported");
	}

	@Override
	public void logout() throws ServletException {
	}

	@Override
	public Collection<Part> getParts() throws IOException, ServletException {
		return Collections.emptyList();
	}

	@Override
	public Part getPart(String name) throws IOException, ServletException {
		throw new ServletException("No Parts");
	}

	@Override
	public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException {
        throw new UnsupportedOperationException();
	}

}
