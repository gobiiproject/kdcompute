/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.mocks;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.other.GobiiDataset;

public class MockEntityManager implements EntityManager {

    /*package*/ MockEntityManager() {
    }

	@Override
	public void persist(Object entity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> T merge(T entity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void remove(Object entity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> T find(Class<T> entityClass, Object primaryKey) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> T find(Class<T> entityClass, Object primaryKey, Map<String, Object> properties) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> T find(Class<T> entityClass, Object primaryKey, LockModeType lockMode) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> T find(Class<T> entityClass, Object primaryKey, LockModeType lockMode,
			Map<String, Object> properties) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> T getReference(Class<T> entityClass, Object primaryKey) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void flush() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setFlushMode(FlushModeType flushMode) {
		throw new UnsupportedOperationException();

	}

	@Override
	public FlushModeType getFlushMode() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void lock(Object entity, LockModeType lockMode) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void lock(Object entity, LockModeType lockMode, Map<String, Object> properties) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void refresh(Object entity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void refresh(Object entity, Map<String, Object> properties) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void refresh(Object entity, LockModeType lockMode) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void refresh(Object entity, LockModeType lockMode, Map<String, Object> properties) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void detach(Object entity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public boolean contains(Object entity) {
//		throw new UnsupportedOperationException();
		return false;
	}

	@Override
	public LockModeType getLockMode(Object entity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setProperty(String propertyName, Object value) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Map<String, Object> getProperties() {
		throw new UnsupportedOperationException();

	}

	@Override
	public Query createQuery(String qlString) {
        throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
		TypedQuery<GobiiDataset> result = new TypedQuery<GobiiDataset>() {

			private FlushModeType flushMode = FlushModeType.AUTO;

            @Override
			public int executeUpdate() {
//				throw new UnsupportedOperationException();
				return 0;
			}

			@Override
			public int getMaxResults() {
//				throw new UnsupportedOperationException();
				return 0;
			}

			@Override
			public int getFirstResult() {
//				throw new UnsupportedOperationException();
				return 0;
			}

			@Override
			public Map<String, Object> getHints() {
				throw new UnsupportedOperationException();
			}

			@Override
			public Set<Parameter<?>> getParameters() {
				throw new UnsupportedOperationException();
			}

			@Override
			public Parameter<?> getParameter(String name) {
				throw new UnsupportedOperationException();
			}

			@Override
			public <TT> Parameter<TT> getParameter(String name, Class<TT> type) {
				throw new UnsupportedOperationException();
			}

			@Override
			public Parameter<?> getParameter(int position) {
				throw new UnsupportedOperationException();
			}

			@Override
			public <TT> Parameter<TT> getParameter(int position, Class<TT> type) {
				throw new UnsupportedOperationException();
			}

			@Override
			public boolean isBound(Parameter<?> param) {
//				throw new UnsupportedOperationException();
				return false;
			}

			@Override
			public <TT> TT getParameterValue(Parameter<TT> param) {
				throw new UnsupportedOperationException();
			}

			@Override
			public Object getParameterValue(String name) {
				throw new UnsupportedOperationException();
			}

			@Override
			public Object getParameterValue(int position) {
				throw new UnsupportedOperationException();
			}

			@Override
			public FlushModeType getFlushMode() {
			    return flushMode;
			}

			@Override
			public LockModeType getLockMode() {
				throw new UnsupportedOperationException();
			}

			@Override
			public <TT> TT unwrap(Class<TT> cls) {
				throw new UnsupportedOperationException();
			}

			/**
			 * Returns mock datasets for testing.
			 */
			@Override
			public List<GobiiDataset> getResultList() {
			    Function<AnalysisJob, GobiiDataset> factory = new Function<AnalysisJob, GobiiDataset>() {
			        long nextId = 1;
                    @Override
                    public GobiiDataset apply(AnalysisJob job) {
                        return new GobiiDataset(nextId++, job);
                    }
                };

                List<GobiiDataset> datasets = MockProvider.getEntityStore(true).getAllAnalysisJobs()
                        .stream()
                        .map(factory)
                        .collect(Collectors.toList());
				return datasets;
			}

			@Override
			public GobiiDataset getSingleResult() {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setMaxResults(int maxResult) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setFirstResult(int startPosition) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setHint(String hintName, Object value) {
				throw new UnsupportedOperationException();

			}

			@Override
			public <TT> TypedQuery<GobiiDataset> setParameter(Parameter<TT> param, TT value) {
				throw new UnsupportedOperationException();
			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(Parameter<Calendar> param, Calendar value,
					TemporalType temporalType) {
				throw new UnsupportedOperationException();
			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(Parameter<Date> param, Date value,
					TemporalType temporalType) {
				throw new UnsupportedOperationException();
			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(String name, Object value) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(String name, Calendar value, TemporalType temporalType) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(String name, Date value, TemporalType temporalType) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(int position, Object value) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(int position, Calendar value, TemporalType temporalType) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setParameter(int position, Date value, TemporalType temporalType) {
				throw new UnsupportedOperationException();

			}

			@Override
			public TypedQuery<GobiiDataset> setFlushMode(FlushModeType flushMode) {
			    this.flushMode = flushMode;
			    return this;
			}

			@Override
			public TypedQuery<GobiiDataset> setLockMode(LockModeType lockMode) {
				throw new UnsupportedOperationException();

			}

		};

		return (TypedQuery<T>) result;
	}

	@SuppressWarnings("rawtypes")
    @Override
	public Query createQuery(CriteriaUpdate updateQuery) {
		throw new UnsupportedOperationException();
	}

    @SuppressWarnings("rawtypes")
	@Override
	public Query createQuery(CriteriaDelete deleteQuery) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> TypedQuery<T> createQuery(String qlString, Class<T> resultClass) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Query createNamedQuery(String name) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> TypedQuery<T> createNamedQuery(String name, Class<T> resultClass) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Query createNativeQuery(String sqlString) {
		throw new UnsupportedOperationException();
	}

    @SuppressWarnings("rawtypes")
	@Override
	public Query createNativeQuery(String sqlString, Class resultClass) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Query createNativeQuery(String sqlString, String resultSetMapping) {
		throw new UnsupportedOperationException();
	}

	@Override
	public StoredProcedureQuery createNamedStoredProcedureQuery(String name) {
		throw new UnsupportedOperationException();
	}

	@Override
	public StoredProcedureQuery createStoredProcedureQuery(String procedureName) {
		throw new UnsupportedOperationException();
	}

    @SuppressWarnings("rawtypes")
	@Override
	public StoredProcedureQuery createStoredProcedureQuery(String procedureName,
	        Class... resultClasses) {
		throw new UnsupportedOperationException();
	}

	@Override
	public StoredProcedureQuery createStoredProcedureQuery(String procedureName, String... resultSetMappings) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void joinTransaction() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isJoinedToTransaction() {
//		throw new UnsupportedOperationException();
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> cls) {
		throw new UnsupportedOperationException();

	}

	@Override
	public Object getDelegate() {
		throw new UnsupportedOperationException();

	}

	@Override
	public void close() {
		throw new UnsupportedOperationException();

	}

	@Override
	public boolean isOpen() {
//		throw new UnsupportedOperationException();
		return false;
	}

	@Override
	public EntityTransaction getTransaction() {
		throw new UnsupportedOperationException();

	}

	@Override
	public EntityManagerFactory getEntityManagerFactory() {
		throw new UnsupportedOperationException();

	}

	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return new MockCriteriaBuilder();
	}

	@Override
	public Metamodel getMetamodel() {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> EntityGraph<T> createEntityGraph(Class<T> rootType) {
		throw new UnsupportedOperationException();

	}

	@Override
	public EntityGraph<?> createEntityGraph(String graphName) {
		throw new UnsupportedOperationException();

	}

	@Override
	public EntityGraph<?> getEntityGraph(String graphName) {
		throw new UnsupportedOperationException();

	}

	@Override
	public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> entityClass) {
		throw new UnsupportedOperationException();

	}

}
