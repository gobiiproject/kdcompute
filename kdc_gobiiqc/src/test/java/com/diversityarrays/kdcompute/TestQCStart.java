/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.AuthenticationException;

import com.diversityarrays.api.JobIdResponse;
import com.diversityarrays.kdcompute.IPAddressAuthenticationProvider.MissingRegex;
import com.diversityarrays.kdcompute.IPAddressAuthenticationProvider.UnauthorisedAddress;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.mocks.MockHttpServletRequest;
import com.diversityarrays.kdcompute.mocks.MockProvider;
import com.diversityarrays.kdcompute.mocks.MockProvider.ServiceOption;
import com.diversityarrays.kdcompute.mocks.MockSubmitJobForPluginService;
import com.diversityarrays.kdcompute.runtime.mock.DummyJobQueue.JobQueueEvent;
import com.diversityarrays.kdcompute.submitjob.SubmitJobResponse;
import com.google.gson.GsonBuilder;

@SuppressWarnings("nls")
public class TestQCStart {

	private static List<File> TEST_DATA_DIRECTORIES;

	private static final QCStart QC_START = new QCStart();

	// MUST be public to let EventBus access it
	static public class EventBusMonitor {

		EventBusMonitor() {
			EventBus.getDefault().register(this);
		}

		@Subscribe()
		public void jobStatusChanged(JobQueueEvent e) {
			System.out.println("===== JobStatusChanged: " + e);
		}

		public void shutdown() {
			EventBus.getDefault().unregister(this);
		}
	}

	static private MockSubmitJobForPluginService SUBMIT_SERVICE;
	static private EventBusMonitor EVENT_BUS_MONITOR;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TEST_DATA_DIRECTORIES = Collections.unmodifiableList(MockProvider.getTestDataDirectories());

		SUBMIT_SERVICE = MockProvider.getSubmitJobForPluginService();

		QC_START.entitiesService = MockProvider.getEntitiesService(ServiceOption.WITHOUT_JOBS);
		QC_START.submitJobForPluginService = SUBMIT_SERVICE;

		EVENT_BUS_MONITOR = new EventBusMonitor();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		EVENT_BUS_MONITOR.shutdown();
	}

	/**
	 * Setup temp directory for testing. Cannot be done statically.
	 * 
	 * @param regex
	 * @return
	 */
	public String setupAndGetPathToData(String regex, Integer datasetId) {
		GobiiConfiguration config = new GobiiConfiguration();
		config.setVariables(regex, MockProvider.PLUGIN_NAME);

		String result = null;
		if (datasetId != null) {
			if (datasetId > TEST_DATA_DIRECTORIES.size()) {
				fail("Not enough data directories for jobId=" + datasetId);
			}
			File dir = TEST_DATA_DIRECTORIES.get(datasetId.intValue() - 1);
			result = dir.getPath(); // do NOT use toURI
		}
		return result;
	}

	/**
	 * Tests that jobs can be properly submitted.
	 */
	@Test
	public void testJobSubmissionOk() {
		int datasetId = 1;
		String path = setupAndGetPathToData(MockProvider.IPv4_REGEX, datasetId);

		try {
			String uri = String.format("%s/%d/%s", QCStart.SLASH_QC_START, datasetId, path);
			HttpServletRequest request = MockHttpServletRequest.createValid(uri);
			SubmitJobResponse response = QC_START.qcStart((long) datasetId, false, path, request);
			assertNotNull("Non-null response required for " + uri, response);

			assertEquals("Incorrect response class", JobIdResponse.class, response.getClass());

			JobIdResponse jobIdResponse = (JobIdResponse) response;
			String json = new GsonBuilder().setPrettyPrinting().create().toJson(jobIdResponse);
			System.out.println("=== Test using: " + uri + "\nresult=\n" + json + "\n------------");

			long startMillis = System.currentTimeMillis();
			int nSleeps = 0;
			while (SUBMIT_SERVICE.anyExtantJobs() && ++nSleeps < 20) {
				Thread.sleep(200);
			}

			boolean any = SUBMIT_SERVICE.anyExtantJobs();
			if (any) {
				long elapsed = System.currentTimeMillis() - startMillis;
				System.err.println(String.format("Giving up after waiting for %.3f seconds", elapsed / 1000.0));
			} else {
				Optional<AnalysisJob> opt = SUBMIT_SERVICE.getAnalysisJob(jobIdResponse.jobId);
				if (!opt.isPresent()) {
					fail("Unable to find AnalysisJob " + jobIdResponse.jobId);
				}
				AnalysisJob job = opt.get();
				File results = job.getResultsFolder();
				assertTrue("No results folder " + results.getPath(), results.isDirectory());
				List<File> resultFiles = collectResultFiles(results);
				System.out.println("Results in\n" + results.getPath());
				System.out.println(resultFiles.stream().map(File::getName)
						.collect(Collectors.joining("\n  ", "# results=" + resultFiles.size() + "\n  ", "")));
				if (RunState.COMPLETED != job.getRunState()) {
					fail("Incomplete job, state=" + job.getRunState() + "\ncheck files in\n" + results.getPath());
				}
			}
		} catch (Exception e) {
			if (e instanceof NullPointerException) {
				fail("Error starting service on mock dataset (NullPointerException) : " + e.getMessage());
			} else if (e instanceof NumberFormatException) {
				fail("Error starting service on mock dataset (Invalid Dataset Id) : " + e.getMessage());
			} else {
				fail("Error starting service on mock dataset: " + e.getMessage());
			}
			e.printStackTrace();
		}
	}

	static private List<File> collectResultFiles(File dir) {
		List<File> result = new ArrayList<>();
		recurse(dir, result);
		return result;
	}

	static private void recurse(File dir, List<File> list) {
		List<File> dirs = new ArrayList<>();
		for (File f : dir.listFiles()) {
			if (f.isFile()) {
				list.add(f);
			} else {
				dirs.add(f);
			}
		}
		for (File d : dirs) {
			recurse(d, list);
		}
	}

	/**
	 * Tests that empty jobs throw an exception.
	 */
	@Test
	public void testJobSubmissionEmptyFail() {
		setupAndGetPathToData(MockProvider.IPv4_REGEX, null);
		try {
			HttpServletRequest request = MockHttpServletRequest.createValid(QCStart.SLASH_QC_START + "/1");
			QC_START.qcStart(1L, false, null, request);
			fail("Empty dataset proceeded without aborting.");

		} catch (Exception e) {
			if (!"directory must not be empty".equals(e.getMessage())) {
				fail(e.getMessage());
			}
			;
		}
	}

	/**
	 * Test incorrect authentication.
	 */
	@Test
	public void testJobSubmissionBadAuthenticationFail() {
		testJobAuthenticationFail(MockProvider.IPv4_REGEX_FOR_MISMATCH,
				"Bad Authentication completed without error. Should have failed", UnauthorisedAddress.class);
	}

	/**
	 * Test empty authentication.
	 */
	@Test
	public void testJobSubmissionEmptyAuthenticationFail() {
		testJobAuthenticationFail(null, "Empty Authentication completed without error. Should have failed",
				MissingRegex.class);
	}

	private void testJobAuthenticationFail(String regex, String message,
			Class<? extends AuthenticationException> eclass) {
		int datasetId = TEST_DATA_DIRECTORIES.size();
		String path = setupAndGetPathToData(regex, datasetId);

		try {
			String uri = String.format("%s/%d/%s", QCStart.SLASH_QC_START, datasetId, path);
			HttpServletRequest request = MockHttpServletRequest.createValid(uri);
			QC_START.qcStart((long) datasetId, false, path, request);
			fail(message);

		} catch (Exception e) {
			if (e instanceof AuthenticationException) {
				if (eclass.equals(e.getClass())) {
					return; // all is well
				}
			}
			fail(e.getMessage());
		}
	}

	//
	// static private SubmitJobForPluginService SERVICE = new
	// SubmitJobForPluginService() {
	//
	// @Override
	// public Either<ResponseMsg, SubmitJobResponse> submitJobForPluginParser(
	// Map<String, String> allRequestParams, String pluginName, String
	// base_usage,
	// Function<ResponseJob, SubmitJobResponse> responseFactory,
	// PostCompletionCall postCompletionCall, PostSubmissionCall
	// postSubmissionCall)
	// throws Exception
	// {
	// assertEquals("Incorrect pluginName", QCStart.QC_PLUGIN_NAME, pluginName);
	//
	//
	// String postCompletionUrl =
	// allRequestParams.get(SubmitJobConstants.POSTCOMPLETIONURL);
	// if(postCompletionUrl!=null)
	// postCompletionUrl = postCompletionUrl.trim();
	//
	// List<KnobBinding> knobsAndValues = new ArrayList<>();
	//
	// Plugin plugin = null;
	//
	// if (plugin != null) {
	//// FileStorage fileStorage = application.getFileStorage();
	// List<Knob> allKnobs = plugin.getKnobs();
	// for (Knob k : allKnobs) {
	// if(k.isRequired() && !allRequestParams.containsKey(k.getKnobName())) {
	// Either.left(new ResponseMsg(
	// "Required knob("+k.getKnobName()+"), type("+k.getKnobDataType()+")",
	// null, base_usage));
	// }
	// String knobValue = allRequestParams.get(k.getKnobName());
	// KnobBinding knobBinding = null;
	//
	//
	// if (k.getKnobDataType() == KnobDataType.FILE_UPLOAD ) {
	// // Curl file
	// String timeStamp = new SimpleDateFormat("yyyy/MM/dd/hhmmss").format(new
	// Date()); // This will create 3 folders, yyyy / MM / dd
	//
	// File userDir = new File(System.getProperty("user.dir"));
	// File tmpDir = new File(userDir, "UnitTests_Temp");
	// File userTempDir = new File(tmpDir, timeStamp);
	// if(!userTempDir.exists() && !userTempDir.mkdirs() ||
	// !userTempDir.isDirectory()){
	// throw new Exception("Failed to create temp directory
	// "+userTempDir.getAbsolutePath());
	// }
	//
	// if(knobValue==null || knobValue.isEmpty()){
	// if(k.isRequired()){
	// return Either.left(
	// new ResponseMsg("Required knob("+k.getKnobName()+"),
	// type("+k.getKnobDataType()+")",
	// null,
	// base_usage));
	// }else{
	// knobBinding = new KnobBinding(k, "");
	// }
	// }else{
	// File destFile = new File(userTempDir,new File(knobValue).getName());
	// FileOutputStream fileOutputStream = null ;
	// try {
	// fileOutputStream = new FileOutputStream(destFile,true);
	// BufferedReader reader = new BufferedReader(new InputStreamReader(new
	// URL(knobValue).openStream(), "UTF-8"));
	//
	// for(String line;(line=reader.readLine())!=null;){
	// fileOutputStream.write((line+"\n").getBytes());
	// }
	// fileOutputStream.flush();
	// fileOutputStream.close();
	// }finally {
	// fileOutputStream.close();
	// knobBinding = new KnobBinding(k, destFile.getAbsolutePath());
	// }
	// }
	// }else if(k.getKnobDataType().equals(KnobDataType.CHOICE)){
	// List<String> choices = KnobValidationRule.getChoices(k);
	// if(!choices.contains(knobValue)){
	// String error = "Invalid choice "+knobValue;
	// if(knobValue==null || knobValue.isEmpty()){
	// error = "Choice value not provided";
	// }
	// return Either.left(new ResponseMsg(error, null, base_usage));
	// }
	// knobBinding = new KnobBinding(k, knobValue);
	// // System.out.println("knobName = " + k.getKnobName() + ", value = " +
	// knobValue);
	// } else {
	// if(knobValue==null) {
	// knobValue = "";
	// }
	// knobBinding = new KnobBinding(k, knobValue);
	// // System.out.println("knobName = " + k.getKnobName() + ", value = " +
	// knobValue);
	// }
	// knobsAndValues.add(knobBinding);
	// }
	// }
	//
	// AnalysisJob job = SubmittedFormController.submitJob(plugin,
	// knobsAndValues, loggedInUser,
	// postCompletionCall,
	// postSubmissionCall);
	// ResponseJob responseJob = new ResponseJob("TestQCStart-v0.1",
	// JobSummaryController.JOBSUMMARY+"/"+job.getRequestingUser()+"/"+job.getId(),job);
	//
	// return Either.right(responseFactory.apply(responseJob));
	//
	// }
	// };
	//
	// public static AnalysisJob submitJob(
	// Plugin alg,
	// List<KnobBinding> knobsAndValues,
	// String loggedInUser,
	// PostCompletionCall postCompletionCall,
	// PostSubmissionCall postSubmissionCall)
	// {
	// KdcServerApplication application = KdcServerApplication.getInstance();
	// EntityStore store = application.getEntityStore();
	//
	//
	// RunBinding binding = new RunBinding(alg);
	// binding.setKnobBindings(knobsAndValues);
	//
	// AnalysisRequest req = new AnalysisRequest(binding);
	// req.setForUser(loggedInUser);
	// store.persist(binding);
	// store.persist(req);
	//
	// AnalysisJob job = null;
	//
	// try {
	// job = application.getJobRunner()
	// .createAnalysisJob(
	// req, req.getForUser(),
	// (f) System.out.println("Created Job: file=" + f.getPath())
	// ,
	// KdcServerApplication.getInstance().getAdditionalVariables().instantiateAdditionalKDComputeSystemVariables(req.getForUser()));
	// application.getJobQueue().submitJob(job);
	// if(postSubmissionCall!=null){
	// postSubmissionCall.onSubmission(job);
	// }
	// } catch (JobQueueException | UserIsNotLoggedInException e) {
	// e.printStackTrace();
	// }
	//
	// job.setPostCompletionCall(postCompletionCall);
	//
	// return job;
	// }

}
