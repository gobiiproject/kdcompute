/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.AuthenticationException;

@SuppressWarnings("nls")
public class TestIPAuthentication {

    static class TestSet {
        public String regex;
        public String[] addresses;
    }

    private static final boolean DONT_FAIL = false;
    private static final boolean FAIL = true;

    // Explicit null to bypass logging
    private static final String DO_NOT_LOG = null;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testSimpleOk() {
        testOne("192.168.100.1", "192.168.100.\\d+", DONT_FAIL);
    }

    @Test
    public void testComplexOk() {
        String regex = "192\\.168\\.100\\.\\d{1,3}|10\\.1\\.\\d{1,3}\\.\\d{1,3}";
        testOne("192.168.100.1", regex, DONT_FAIL);
        testOne("10.1.0.1", regex, DONT_FAIL);
    }

    @Test
    public void testFail() {
        testOne("10.1.0.1", "192.168.100.\\d+", FAIL);
    }

    @Test
    public void testComplexFail() {
        String regex = "192\\.168\\.100\\.\\d{1,3}|10\\.1\\.\\d{1,3}\\.\\d{1,3}";
        testOne("1.1.1.1", regex, FAIL);
        testOne("192.168.100.1234", regex, FAIL);
        testOne("10.2.0.1", regex, FAIL);
    }

    private void testOne(String addr, String regex, boolean shouldFail) {
        try {
            IPAddressAuthenticationProvider.checkIfAddressIsValid(DO_NOT_LOG, addr, regex);
            if (shouldFail) {
                fail(String.format("%s should not have matched %s", addr, regex));
            }
        }
        catch (AuthenticationException e) {
            if (! shouldFail) {
                fail(String.format("%s should not matched %s:\n%s",
                        addr, regex, e.getMessage()));
            }
        }
    }

}
