/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.AuthenticationException;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisJobNotFoundException;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.gobiiqc.GobiiDatasetIdNotFoundException;
import com.diversityarrays.kdcompute.gobiiqc.containers.StatusResponse;
import com.diversityarrays.kdcompute.mocks.MockHttpServletRequest;
import com.diversityarrays.kdcompute.mocks.MockProvider;
import com.diversityarrays.kdcompute.mocks.MockProvider.ServiceOption;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.EntityStore.RunStateChoice;
import com.diversityarrays.kdcompute.util.Check;

@SuppressWarnings("nls")
public class TestQCStatus {

	public final static boolean FAIL = true;
	public final static boolean DONT_FAIL = false;

	private static QCStatus QC_STATUS;
	private static EntityStore ENTITY_STORE;

	private static Predicate<AnalysisJob> runningJobPredicate = (j) -> RunState.RUNNING.equals(j.getRunState());
	private static Predicate<AnalysisJob> runningOrCompletedJobPredicate = (
			j) -> RunState.RUNNING.equals(j.getRunState()) || RunState.COMPLETED.equals(j.getRunState());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		initGobiiConfiguration(MockProvider.IPv4_REGEX);

		ENTITY_STORE = MockProvider.getEntityStore(true);

		String datasetFolderKnobName = getDatasetFolderKnobName();

		List<String> testDataPaths = MockProvider.getTestDataDirectories().stream().map(File::getPath)
				.collect(Collectors.toList());

		Function<AnalysisJob, Long> datasetIdProvider = new Function<AnalysisJob, Long>() {
			@Override
			public Long apply(AnalysisJob job) {
				Long result = null;
				if (runningOrCompletedJobPredicate.test(job)) {
					// if (runningJobPredicate.test(job)) {
					AnalysisRequest ar = job.getAnalysisRequest();
					// Find the KnobBinding with the correct knob bame
					Optional<KnobBinding> opt = ar.getRunBindings().stream()
							.flatMap(rb -> rb.getKnobBindings().stream())
							.filter(kb -> kb.getKnob().getKnobName().equals(datasetFolderKnobName)).findFirst();
					if (opt.isPresent()) {
						// Yup - the knobName is in this AnalysisJob's Request
						String datasetPath = opt.get().getKnobValue();
						int index = testDataPaths.indexOf(datasetPath);
						if (index >= 0) {
							result = new Long(index + 1);
						}
					}
				}
				return result;
			}
		};
		MockProvider.addGobiiDataSets(ENTITY_STORE, datasetIdProvider);
		QC_STATUS = new QCStatus();
		QC_STATUS.entitiesService = MockProvider.getEntitiesService(ServiceOption.WITH_JOBS);
	}

	private static String getDatasetFolderKnobName() {
		Plugin plugin = ENTITY_STORE.findHighestVersionPlugin(GobiiConfiguration.getPluginName());
		if (plugin == null) {
			throw new RuntimeException(String.format("Failed to find Plugin '%s'", GobiiConfiguration.getPluginName()));
		}
		Optional<Knob> optional = plugin.getKnobs().stream()
				.filter(k -> QCStart.PARAM_DATASET_FOLDER.equals(k.getKnobName())).findFirst();
		if (!optional.isPresent()) {
			throw new RuntimeException("Missing KnobName=" + QCStart.PARAM_DATASET_FOLDER);
		}
		Knob datasetFolderKnob = optional.get();
		String datasetFolderKnobName = datasetFolderKnob.getKnobName();
		return datasetFolderKnobName;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Initialise REGEX and URL. Cannot be done statically.
	 *
	 * @param regex
	 * @return
	 */
	private static void initGobiiConfiguration(String regex) {
		GobiiConfiguration config = new GobiiConfiguration();
		config.setVariables(regex, MockProvider.PLUGIN_NAME);
	}

	@Test
	public void testInvalidJobId() {
		initGobiiConfiguration(MockProvider.IPv4_REGEX);

		HttpServletRequest req = MockHttpServletRequest.createValid(QCStatus.SLASH_QC_STATUS);
		try {
			StatusResponse response = QC_STATUS.qcStatus(Long.valueOf(Integer.MAX_VALUE), req);
			System.out.println(response);
		} catch (AnalysisJobNotFoundException expected) {
			// that's ok
		} catch (Exception e) {
			String msg = e.getMessage();
			if (Check.isEmpty(msg)) {
				e.printStackTrace();
				fail(e.getClass().getName());
			}
			fail(e.getMessage());
		}

	}

	/**
	 * Test QCStatus command functions correctly.
	 */
	@Test
	public void testStatusOk() {
		initGobiiConfiguration(MockProvider.IPv4_REGEX);

		try {
			// waitForRunningJobsToComplete();

			AnalysisJob job = getTestJob(EntityStore.COMPLETED_JOBS);
			// Not actually running the jobs so we just want the first (only) COMPLETED one

			HttpServletRequest req = MockHttpServletRequest.createValid(QCStatus.SLASH_QC_STATUS);
			StatusResponse response = QC_STATUS.qcStatus(job.getId(), req);

			assertNotNull("response GobiiDataSetId was null", response.getGobiiDatasetId());
			// TODO the job id does not necessarily match the gobii dataset id
			// assertEquals("ID mismatch: job versus response",
			// job.getId().longValue(),
			// response.getGobiiDatasetId().longValue());

			assertNotNull("response resultUrls should not be null", response.getResultsUrls());

			// TODO re-instate after getting expected count from the results
			// folder
			// should it even happen for
			// assertEquals("Invalid URLs count returned",
			// MockProvider.MOCK_DATASET_FILESIZE,
			// response.getResultsUrls().size());

			assertEquals("response status is incorrect", RunState.COMPLETED, response.getStatus());

		} catch (Exception e) {
			if (e instanceof AuthenticationException) {
				fail("Authentication failed incorrectly: " + e.getMessage());

			} else {
				handleUnexpectedException(e);
			}
		}
	}

	@Test
	public void testGetStatusEmptyDatasetFail() {
		initGobiiConfiguration(MockProvider.IPv4_REGEX);

		try {
			HttpServletRequest req = MockHttpServletRequest.createValid(QCStatus.SLASH_QC_STATUS);
			QC_STATUS.qcStatus(null, req);
			fail("Failed to detect empty dataset id for status request");

		} catch (Exception e) {
			if (e instanceof AnalysisJobNotFoundException) {
				// As expected.
			} else {
				handleUnexpectedException(e);
			}
		}
	}

	/**
	 * Test for response to an bad authentication regex for status.
	 */
	@Test
	public void testGetStatusBadAuthenticationFail() {
		initGobiiConfiguration(MockProvider.IPv4_REGEX_FOR_MISMATCH);

		try {
			AnalysisJob job = getTestJob(null); // any job will do
			HttpServletRequest req = MockHttpServletRequest.createValid(QCStatus.SLASH_QC_STATUS);
			QC_STATUS.qcStatus(job.getId(), req);
			fail("Failed to detect bad authentication for status request");

		} catch (AuthenticationException | GobiiDatasetIdNotFoundException | AnalysisJobNotFoundException | IOException
				| NullPointerException e) {

			if (e instanceof AuthenticationException) {
				// As Expected
			} else {
				handleUnexpectedException(e);
			}
		}
	}

	private AnalysisJob getTestJob(RunStateChoice rsc) {
		List<AnalysisJob> list = ENTITY_STORE.getAnalysisJobs(rsc);
		return list.isEmpty() ? null : list.get(0);
	}

	/**
	 * Test for response to an empty authentication regex for status.
	 */
	@Test
	public void testGetStatusEmptyAuthenticationFail() {
		initGobiiConfiguration(null);

		try {
			AnalysisJob job = getTestJob(null);
			HttpServletRequest req = MockHttpServletRequest.createValid(QCStatus.SLASH_QC_STATUS);
			QC_STATUS.qcStatus(job.getId(), req);
			fail("Failed to detect empty authentication for status request");

		} catch (AuthenticationException | GobiiDatasetIdNotFoundException | AnalysisJobNotFoundException | IOException
				| NullPointerException e) {

			if (e instanceof AuthenticationException) {
				// As Expected

			} else {
				handleUnexpectedException(e);
			}
		}
	}

	/**
	 * Save repeated code.
	 *
	 * @param e
	 */
	private void handleUnexpectedException(Exception e) {
		e.printStackTrace();
		if (e instanceof GobiiDatasetIdNotFoundException) {
			fail("Gobii dataset not found when present: " + e.getMessage());

		} else if (e instanceof AnalysisJobNotFoundException) {
			fail("Analysis job not found when present: " + e.getMessage());

		} else if (e instanceof IOException) {
			fail("Fatal IOException error (likely from file manipulation): " + e.getMessage());
		} else if (e instanceof NullPointerException) {
			fail("Fatal NullPointerException from: " + e.getMessage());
		}
	}

	private void waitForRunningJobsToComplete() {

		List<AnalysisJob> runningJobs = ENTITY_STORE.getAnalysisJobs(
		        RunStateChoice.create("Running", RunState.RUNNING));

		if (runningJobs.isEmpty()) {
			return;
		}

		// Job RunState may change (and the EntityManager will update the in-memory)
		Optional<AnalysisJob> anyRunning = runningJobs.stream()
				.filter(job -> RunState.RUNNING.equals(job.getRunState())).findAny();
		long startMillis = System.currentTimeMillis();
		while (anyRunning.isPresent()) {
			if ((System.currentTimeMillis() - startMillis) > 2000) {
				System.err.println("Waiting for RUNNING jobs to finish");
				startMillis = System.currentTimeMillis();
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			anyRunning = runningJobs.stream().filter(job -> RunState.RUNNING.equals(job.getRunState())).findAny();
		}

	}

}
