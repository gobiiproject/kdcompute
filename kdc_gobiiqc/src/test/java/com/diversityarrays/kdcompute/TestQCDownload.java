/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.AuthenticationException;

import com.diversityarrays.kdcompute.IPAddressAuthenticationProvider.MissingRegex;
import com.diversityarrays.kdcompute.IPAddressAuthenticationProvider.UnauthorisedAddress;
import com.diversityarrays.kdcompute.QCDownload.LinksList;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisJobNotFoundException;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.mocks.MockHttpServletRequest;
import com.diversityarrays.kdcompute.mocks.MockHttpServletResponse;
import com.diversityarrays.kdcompute.mocks.MockProvider;
import com.diversityarrays.kdcompute.mocks.MockProvider.ServiceOption;
import com.diversityarrays.kdcompute.mocks.MockProvider.TestRegex;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.EntityStore.RunStateChoice;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

@SuppressWarnings("nls")
public class TestQCDownload {

	// == Copied from com.diversityarrays.gobiiqc.summary.Report
	// == in Project: gobiiqc_summary
	static public enum SheetName {

		PARENTAGE("F1"),
		REPRODUCIBILITY("Reproducibility"),
		SUMMARY_SAMPLES("Summary_samples"),
		SUMMARY_MARKERS("Summary_markers"),
		DATASET_SUMMARY("dataset_summary");

		public final String sheetName;

		SheetName(String s) {
			this.sheetName = s;
		}
	}

	private static final String CSV_EXTENSION = ".csv";
	// === End Copy ===

	// == Copied from com.diversityarrays.gobiiqc.summary.Application
	// == in Project: gobiiqc_summary

	private static final String REPORT_XLSX = "Report.xlsx";

	// === End Copy ===

	private static QCDownload QC_DOWNLOAD;
	private static EntityStore ENTITY_STORE;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ENTITY_STORE = MockProvider.getEntityStore(true);
		QC_DOWNLOAD = new QCDownload();
		QC_DOWNLOAD.entitiesService = MockProvider.getEntitiesService(ServiceOption.WITH_JOBS);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Tests whether file URL's are returned correctly.
	 *
	 * @throws IOException
	 * @throws AuthenticationException
	 * @throws JsonSyntaxException
	 * @throws AnalysisJobNotFoundException
	 */
	@Test
	public void testListURLsOk()
			throws JsonSyntaxException, AuthenticationException, IOException, AnalysisJobNotFoundException {
		GobiiConfiguration config = new GobiiConfiguration();
		config.setVariables(MockProvider.TestRegex.IPV4.value, MockProvider.PLUGIN_NAME);

		for (AnalysisJob job : ENTITY_STORE.getAllAnalysisJobs()) {

			String uri = String.format("/%s/%d", QCDownload.QC_DOWNLOAD, job.getId());
			HttpServletRequest request = MockHttpServletRequest.createValid(uri);
			LinksList linksList = new Gson().fromJson(QC_DOWNLOAD.qcFiles(job.getId(), request), LinksList.class);
			// // this was the old test
			// List<String> urls =
			// QCDownload.getDownloadLinksForQcJob(ENTITY_STORE, job);

			String path = job.getResultsFolder().getPath();
			assertNotNull("results folder path null for jobId=" + job.getId(), path);

			File results = new File(path);
			assertTrue("Nonex results folder: " + results.getPath(), results.isDirectory());

			List<File> files = new ArrayList<>();
			listFilesRecursively(results, files);

			Function<String, String> f = new Function<String, String>() {
				@Override
				public String apply(String s) {
					return String.format("/%s/%d%s", QCDownload.QC_DOWNLOAD, job.getId(),
							s.startsWith(path) ? s.substring(path.length()) : s);
				}

			};
			Set<String> possibles = files.stream().map(File::getPath).map(f).collect(Collectors.toSet());

			assertEquals("incorrect URL count", possibles.size(), linksList.links.size());
			for (String url : linksList.links) {
				if (!possibles.contains(url)) {
					String msg = possibles.stream()
							.collect(Collectors.joining("  \n", "Unexpected url=" + url + "\nNot one of:\n  ", ""));
					fail(msg);
				}
			}
		}
	}

	static private void listFilesRecursively(File dir, List<File> results) {
		for (File f : dir.listFiles()) {
			if (f.isFile()) {
				results.add(f);
			} else if (f.isDirectory()) {
				listFilesRecursively(f, results);
			}
		}
	}

	/**
	 * Tests whether file download process functions correctly.
	 */
	@Test
	public void testFileDownloadOk() {

		doFileDownload(MockProvider.TestRegex.IPV4,
		        RunStateChoice.create("Completed Jobs", RunState.COMPLETED),
		        (job) -> {
		            try {
		                return makeValidRemoteRequest(job);
		            } catch (IOException e) {
		                throw new RuntimeException(e);
		            }
		        },
		        null); // no error expected
	}

	@Test
	public void testInvalidJobId() {

		Long invalidId = Long.valueOf(Integer.MAX_VALUE);
		MockHttpServletResponse response = new MockHttpServletResponse();
		String uri = String.format("/%s/%d/Invalid_algorithm/NonexistenFile", QCDownload.QC_DOWNLOAD, invalidId);

		HttpServletRequest request = MockHttpServletRequest.createValid(uri);

		try {
			QC_DOWNLOAD.qcDownload(invalidId, response, request);
		} catch (AnalysisJobNotFoundException expected) {
			// That's ok
		} catch (AuthenticationException | IOException e) {
			fail(e.getMessage());
		}

	}

	/**
	 * Tests whether file download process functions correctly.
	 */
	@Test
	public void testFileDownloadNullRegexFail() {
		doFileDownload(TestRegex.NULL_REGEX,
		        EntityStore.ALL_JOBS,
		        (job) -> {
        			try {
        				return makeValidRemoteRequest(job);
        			} catch (IOException e) {
        				throw new RuntimeException(e);
        			}
        		},
		        MissingRegex.class);
	}

	/**
	 * Tests whether file download authentication functions correctly.
	 */
	@Test
	public void testFileDownloadIncorrectAuthenticationFail() {
		doFileDownload(MockProvider.TestRegex.IPV4,
		        EntityStore.ALL_JOBS,
		        (job) -> makeInvalidRemoteRequest(job),
				UnauthorisedAddress.class);
	}

	/**
	 * I think the request is supposed to look like: /qcDownload/ID/FILENAME
	 *
	 * @param job
	 * @return
	 * @throws IOException
	 */
	static private HttpServletRequest makeValidRemoteRequest(AnalysisJob job) throws IOException {

		long id = job.getId();

		String fileName;
		int index = (int) (id - 1);
		if (index < SheetName.values().length) {
			fileName = SheetName.values()[index].sheetName + CSV_EXTENSION;
		} else {
			fileName = REPORT_XLSX;
		}

		File resultsFolder = job.getResultsFolder();
		String pluginName = GobiiConfiguration.getPluginName();
		Plugin plugin = ENTITY_STORE.findHighestVersionPlugin(pluginName);
		File pluginResultsFolder = new File(resultsFolder, plugin.getAlgorithmName().replace(' ', '_'));
		File dummyResultFile = new File(pluginResultsFolder, fileName);
		try {
			PrintWriter pw = new PrintWriter(dummyResultFile);
			pw.println("Dummy content for " + dummyResultFile.getPath());
			pw.close();
		} catch (FileNotFoundException e) {
			fail("Unable to create content for " + dummyResultFile.getPath());
		}

		String uri = String.format("/%s/%d/%s/%s", QCDownload.QC_DOWNLOAD, id, job.getDisplayName().replace(' ', '_'),
				fileName);

		return MockHttpServletRequest.createValid(uri);
	}

	static private HttpServletRequest makeInvalidRemoteRequest(AnalysisJob job) {
		String fmt = (0 == (System.currentTimeMillis() & 1)) ? "/%s/%d/not_valid_filename" : "/%s/%d";
		String uri = String.format(fmt, QCDownload.QC_DOWNLOAD, job.getId());
		return MockHttpServletRequest.createInvalid(uri);
	}

	/**
	 * Tests whether file download functions correctly.
	 */
	private void doFileDownload(TestRegex testRegex,
	        RunStateChoice runStateChoice,
			Function<AnalysisJob, HttpServletRequest> requestFactory,
			Class<? extends AuthenticationException> expectedError) {
		GobiiConfiguration config = new GobiiConfiguration();
		config.setVariables(testRegex.value, MockProvider.PLUGIN_NAME);

		List<AnalysisJob> allJobs = ENTITY_STORE.getAnalysisJobs(runStateChoice);
		while (allJobs.isEmpty()) {
			System.err.println("Waiting for jobs matching predicate: " + runStateChoice.getDescription());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			    System.err.println("TestQCDownload: Interrupted");
			}
		}

		if (testRegex.isValidRegex()) {
			for (AnalysisJob mockJob : allJobs) {
				testOneJob(mockJob, requestFactory, expectedError);
				break; // only need one
			}
		} else {
			// Only need to do one
			testOneJob(allJobs.get(0), requestFactory, expectedError);
		}
	}

	private void testOneJob(AnalysisJob job, Function<AnalysisJob, HttpServletRequest> requestFactory,
			Class<? extends AuthenticationException> expectedError) {
		try {
			MockHttpServletResponse response = new MockHttpServletResponse();
			HttpServletRequest request = requestFactory.apply(job);

			QC_DOWNLOAD.qcDownload(job.getId(), response, request);

			if (expectedError == null) {
				byte[] outputBytes = response.getOutputBytes();
				if (RunState.COMPLETED == job.getRunState()) {
					// This one is supposed to work
					assertTrue(String.format("No output available for COMPLETED job: %s", request.getRequestURI()),
							outputBytes.length > 0);
				} else {
					assertTrue("Unexpected output", outputBytes.length <= 0);
				}
			} else {
				fail("Authentication proceeded when incorrect!");
			}
		} catch (AuthenticationException | IOException | AnalysisJobNotFoundException e) {
			if (expectedError == null) {
				fail("download failed: " + e.getMessage());
			}

			if (e instanceof AuthenticationException) {
				// Worked as expected
				if (!expectedError.equals(e.getClass())) {
					fail("Incorrect Authentication Failure: " + e.getMessage());
				}
			} else {
				fail("download failed: " + e.getMessage());
			}
		}
	}

}
