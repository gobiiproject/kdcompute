/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.kdcpluginstudio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Properties;

import com.diversityarrays.kdcompute.util.Check;
import com.diversityarrays.kdcompute.util.MsgBox;

@SuppressWarnings("nls")
public class PluginStudioSettings {

	private static final String ANSIBLE_PLAYBOOK_PATH = "ansiblePlaybookPath";

	private static final String CURRENT_PLUGIN_DIR = "currentPluginDir";

	private static final String PLUGIN_STUDIO_PROPERTIES = "PluginStudio.properties";

	private static final String GROOVY_BINARY_PATH = "groovyBinaryPath";

	static private PluginStudioSettings SINGLETON = null;

	public static PluginStudioSettings getSettings() {
		if (SINGLETON == null) {
			throw new RuntimeException("PluginStudioSettings has not been initialised");
		}
		return SINGLETON;
	}

	public static void initialise(File applicationFolder) {
		if (SINGLETON != null) {
			throw new IllegalStateException("Already initialised");
		}
		SINGLETON = new PluginStudioSettings(applicationFolder);
	}

	private final File applicationFolder;

	private Properties properties;

	private Charset charset;
	private File workspaceFolder;

	private PluginStudioSettings(File appFolder) {
		applicationFolder = appFolder;
		this.properties = new Properties();
		try {
			File propFile = new File(applicationFolder, PLUGIN_STUDIO_PROPERTIES);
			this.properties.load(new FileReader(propFile));
		} catch (IOException ignore) {
		}
	}

	public Charset getCharset() {
		if (charset == null) {
			synchronized (this) {
				if (charset == null) {
					Charset cs = null;
					String csname = this.properties.getProperty("charset");
					if (!Check.isEmpty(csname)) {
						try {
							cs = Charset.forName(csname);
						} catch (IllegalCharsetNameException e) {
							System.err.println("Illegal charset name in properties: " + csname);
						}
					}
					if (cs == null) {
						cs = Charset.defaultCharset();
					}
					charset = cs;
				}
			}
		}
		return charset;
	}

	public void setWorkspaceFolder(File dir) {
		workspaceFolder = dir;
		properties.setProperty("workspacePath", dir.getPath());
		try {
			properties.store(new FileOutputStream(new File(applicationFolder, PLUGIN_STUDIO_PROPERTIES)),
					"Changed " + "workspacePath");
		} catch (IOException e) {
			MsgBox.error(null, e, "setWorkspaceFolder");
		}
	}

	private void saveSettings(String comment) {
		try {
			properties.store(new FileOutputStream(new File(applicationFolder, PLUGIN_STUDIO_PROPERTIES)), comment);
		} catch (IOException e) {
			MsgBox.error(null, e, "saveSettings(" + comment + ")");
		}
	}

	public File getWorkspaceFolder() {
		if (workspaceFolder == null) {
			String path = properties.getProperty("workspacePath");
			if (!Check.isEmpty(path)) {
				workspaceFolder = new File(path);
			}

			if (workspaceFolder == null || !workspaceFolder.isDirectory()) {
				workspaceFolder = new File(applicationFolder, "Workspace");
				workspaceFolder.mkdirs();
			}
		}
		return workspaceFolder;
	}

	public String getLinuxTerminalCommand() {
		String result = properties.getProperty("linuxTerminalCommand", "gnome-terminal");
		return result;
	}

	public boolean getUseFileDialogForDirectoryChooser() {
		if (System.getProperty("os.name").toLowerCase().indexOf("mac") == 0) {
			String tmp = properties.getProperty("useNativeFileDialog");
			if (Check.isEmpty(tmp)) {
				tmp = "true";
			}
			return Boolean.parseBoolean(tmp);
		} else {
			return false;
		}
	}

	public int getIntProperty(String key, int defaultValue) {
		int result = defaultValue;
		String s = properties.getProperty(key);
		if (!Check.isEmpty(s)) {
			try {
				result = Integer.parseInt(s);
			} catch (NumberFormatException e) {
				result = defaultValue;
			}
		}
		return result;
	}

	public void setIntProperty(String key, int value) {
		properties.setProperty(key, Integer.toString(value));
		saveSettings(key);
	}

	public void setCurrentPluginDir(File dir) {
		if (dir == null) {
			properties.remove(CURRENT_PLUGIN_DIR);
		} else {
			properties.setProperty(CURRENT_PLUGIN_DIR, dir.getPath());
		}
		saveSettings("setCurrentPluginDir");
	}

	public File getCurrentPluginDir() {
		File result = null;
		String path = properties.getProperty(CURRENT_PLUGIN_DIR);
		if (!Check.isEmpty(path)) {
			result = new File(path);
			if (!result.isDirectory()) {
				result = null;
			}
		}
		return result;
	}

	public void setGitSupported(Boolean b) {
		if (b == null) {
			properties.remove("useGit");
		} else {
			properties.setProperty("useGit", b ? "true" : "false");
		}
		saveSettings("setGitSupported");
	}

	public Boolean getGitSupported() {
		Boolean b = null;
		String s = properties.getProperty("useGit");
		if (!Check.isEmpty(s)) {
			b = Boolean.parseBoolean(s);
		}
		return b;
	}

	public String getAnsiblePlaybookPath() {
		String ansiblePlaybookPath = properties.getProperty(ANSIBLE_PLAYBOOK_PATH);
		if (Check.isEmpty(ansiblePlaybookPath)) {
			ansiblePlaybookPath = null;
		}
		return ansiblePlaybookPath;
	}

	public void setAnsiblePlaybookPath(String ansiblePlaybookPath) {
		properties.setProperty(ANSIBLE_PLAYBOOK_PATH, ansiblePlaybookPath);
		saveSettings(ANSIBLE_PLAYBOOK_PATH);
	}

	public String getGroovyCommand() {
		return properties.getProperty(GROOVY_BINARY_PATH);
	}

	public void setGroovyBinaryPath(String groovyBinaryPath) {
		properties.setProperty(GROOVY_BINARY_PATH, groovyBinaryPath);
		saveSettings(GROOVY_BINARY_PATH);
	}

}
