/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugindeployment;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import com.diversityarrays.kdcompute.PluginDependencyScript;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.ScriptNotSupportedException;
import com.diversityarrays.kdcompute.script.AdditionalVariables;

import thatkow.ansible.AnsiblePlaybookNotFoundException;
import thatkow.ansible.AnsibleScript;
import thatkow.ansible.AnsibleScriptBase;
import thatkow.ansible.AnsibleScriptException;

public class DeployPluginScript extends DeployPlugin {

	private PluginDependencyScript pluginDependencyScript;

	public DeployPluginScript(PluginFiles pluginFiles, PrintStream outputPrintStream, PrintStream errorPrintStream,
			File unitTestRoot, AdditionalVariables additionalVariables, PluginDependencyScript pluginDependencyScript) {
		super(pluginFiles, outputPrintStream, errorPrintStream, unitTestRoot, additionalVariables);
		this.pluginDependencyScript = pluginDependencyScript;
	}

	public void deploy(EntityStore entityStore)
			throws PluginAlreadyExistsInDatabaseException, FailedPluginTestsException, ScriptNotSupportedException,
			IOException, InterruptedException, AnsiblePlaybookNotFoundException, AnsibleScriptException {

		AnsibleScriptBase ansibleScript = pluginDependencyScript.getAnsibleScript();
		if (!(ansibleScript instanceof AnsibleScript)) {
			throw new RuntimeException("Expected PluginDependencyScript to contain a AnsibleScript type by found "
					+ ansibleScript.getClass().getName());
		} else {
			int exitCode = ((AnsibleScript) ansibleScript).install(outputPrintStream, errorPrintStream);
			if (exitCode != 0) {
				throw new AnsibleScriptException(exitCode);
			}
		}

		super.runTestsBuildAndInsertIntoDb(entityStore);
	}
}
