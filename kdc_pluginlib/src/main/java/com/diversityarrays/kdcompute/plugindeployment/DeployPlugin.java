/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugindeployment;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginGroup;
import com.diversityarrays.kdcompute.help.EnglishGenerateHelpPage;
import com.diversityarrays.kdcompute.plugin.PluginUtil;
import com.diversityarrays.kdcompute.plugin.example.Example;
import com.diversityarrays.kdcompute.plugin.example.RunExample;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.ScriptNotSupportedException;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.script.AdditionalVariables;

public abstract class DeployPlugin {

	protected final PluginFiles pluginFiles;
	protected final PrintStream outputPrintStream;
	protected final PrintStream errorPrintStream;
	protected final File unitTestRoot;
	protected final AdditionalVariables additionalVariables;

	public DeployPlugin(PluginFiles pluginFiles, PrintStream outputPrintStream, PrintStream errorPrintStream,
			File unitTestRoot, AdditionalVariables additionalVariables) {
		super();
		this.pluginFiles = pluginFiles;
		this.outputPrintStream = outputPrintStream;
		this.errorPrintStream = errorPrintStream;
		this.unitTestRoot = unitTestRoot;
		this.additionalVariables = additionalVariables;
	}

	protected void runTestsBuildAndInsertIntoDb(EntityStore entityStore) throws PluginAlreadyExistsInDatabaseException,
			FailedPluginTestsException, ScriptNotSupportedException, IOException {

		if (entityStore.getPlugins().contains(pluginFiles.getPlugin())) {
			throw new PluginAlreadyExistsInDatabaseException();
		}

		CallScript callScriptForPlugin = entityStore.getCallScriptForPlugin(pluginFiles.getPlugin());
		List<String> failedTests = runTests(callScriptForPlugin);
		if (!failedTests.isEmpty()) {
			throw new FailedPluginTestsException(failedTests);
		}

		buildAndMergeIntoDb(pluginFiles, entityStore);

	}

	private List<String> runTests(CallScript callScript) throws IOException {
		/*
		 * Load examples
		 */
		List<Example> examples = pluginFiles.getExamples();

		/*
		 * Launch all unit tests
		 */
		Map<RunExample, Optional<File>> resultByRunExample = new LinkedHashMap<>();
		for (Example e : examples) {
			if (e.isUnitTest()) {

				final RunExample runExample = new RunExample(new Supplier<CallScript>() {

					@Override
					public CallScript get() {
						return callScript;
					}
				}, e, pluginFiles, outputPrintStream, errorPrintStream, pluginFiles.getDataRoot(), additionalVariables);
				synchronized (resultByRunExample) {
					resultByRunExample.put(runExample, Optional.empty());
				}
				try {
					File tempRoot = unitTestRoot;
					if (unitTestRoot == null) {
						tempRoot = KDCFileUtils.getExampleTempDir();
					}
					File resultsFolder = new File(tempRoot, runExample.getExample().getName());
					resultsFolder.mkdirs();
					boolean success = runExample.execute("kdcompute.deployment.user", resultsFolder,
							null /* analysisJobConsumer */);
					synchronized (resultByRunExample) {
						resultByRunExample.put(runExample, Optional.ofNullable(success ? resultsFolder : null));
					}
				} catch (IOException ex) {
					errorPrintStream.println(ex);
				}
			}

		}

		/*
		 * Check results of unit test
		 */
		List<String> testsFailed = new ArrayList<>();
		for (RunExample runExample : resultByRunExample.keySet()) {
			Optional<File> optional = resultByRunExample.get(runExample);
			if (!optional.isPresent()) {
				testsFailed.add(runExample.getExample().getName());
			}
		}

		return testsFailed;

	}

	protected void buildAndMergeIntoDb(PluginFiles pluginFiles, EntityStore entityStore) throws IOException {

		// Ensure directory path is correctly set
		Plugin moveOrCopyPluginFrom = PluginUtil.moveOrCopyPluginFrom(true, pluginFiles.getRootDir(),
				entityStore.getFileStorage(), true);
		pluginFiles = PluginFiles.readPluginFiles(entityStore.getFileStorage().getPluginFolder(moveOrCopyPluginFrom));

		// Generate Help Page
		EnglishGenerateHelpPage englishGenerateHelpPage = new EnglishGenerateHelpPage(pluginFiles);
		String meta = englishGenerateHelpPage.getHtml();
		englishGenerateHelpPage.copyResourcesTo(pluginFiles.getHelpResourcesFolder());

		// Generate Help Page
		// String meta = new EnglishGenerateHelpPage(pluginFiles).getHtml();

		final Plugin plugin = pluginFiles.getPlugin();
		String htmlHelp = plugin.getHtmlHelp();
		String extra = "";
		if (htmlHelp != null && htmlHelp != "") {
			File helpFile = new File(pluginFiles.getHelpDir(), htmlHelp);
			extra = "\n\n<h2> Missing help file </h2>\n"
					+ "<p>I was told there was extra information, but I couldn't find any</p>\n"
					+ "<p>Please report this to: " + pluginFiles.getContact().getEmail() + "</p>";
			if (helpFile.exists())
				extra = FileUtils.readFileToString(helpFile, Charset.defaultCharset());
		} else {
			extra = "";
			htmlHelp = "default";
		}
		String mergedHelpFile = plugin.getPluginName() + "_v" + plugin.getVersion() + "_"
				+ FilenameUtils.getName(htmlHelp);

		FileUtils.write(new File(pluginFiles.getHelpDir(), mergedHelpFile), meta + extra, Charset.defaultCharset());

		Optional<Plugin> findFirst = entityStore.getPlugins().stream().filter(e -> e.equals(plugin)).findFirst();
		findFirst = entityStore.getPlugins().stream().filter(e -> e.equals(plugin)).findFirst();
		if (findFirst.isPresent()) {
			throw new RuntimeException(
					"Plugin " + pluginFiles.getPluginNameWithVersion() + " already exists in database");
		}
		plugin.setHtmlHelp(pluginFiles.getHelpDir().getName() + File.separator + mergedHelpFile);
		List<PluginGroup> pluginGroups = entityStore.getPluginGroups();
		String pluginGroup = pluginFiles.getDefaults().getGroupName();
		Optional<PluginGroup> pluginGroupInDb = pluginGroups.stream().filter(e -> e.getGroupName().equals(pluginGroup))
				.findFirst();
		entityStore.merge(plugin);

		Optional<Plugin> pluginInDbOpt = entityStore.getPlugins().stream().filter(e -> e.equals(plugin)).findFirst();
		if (findFirst.isPresent()) {
			throw new RuntimeException("Couldn't find recently added plugin!");
		}
		Plugin pluginInDb = pluginInDbOpt.get();
		if (pluginGroupInDb.isPresent()) {
			pluginGroupInDb.get().addPlugin(pluginInDb);
			entityStore.merge(pluginGroupInDb.get());
		} else {
			PluginGroup newPluginGroup = new PluginGroup(pluginGroup);
			newPluginGroup.addPlugin(pluginInDb);
			entityStore.merge(newPluginGroup);
		}
	}

}
