/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugindeployment;

import java.io.File;
import java.io.PrintStream;

import com.diversityarrays.kdcompute.PluginDependencyScript;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.script.AdditionalVariables;

import thatkow.ansible.AnsibleScript;
import thatkow.ansible.AnsibleSudoScript;

public class PluginDeploymentInspector {

	final PluginFiles pluginFiles;
	final PluginDependencyScript pluginDependencyScript;

	public PluginDeploymentInspector(PluginFiles pluginFiles, PluginDependencyScript pluginDependencyScript) {
		super();
		this.pluginFiles = pluginFiles;
		this.pluginDependencyScript = pluginDependencyScript;
	}

	public DeployPlugin create(PrintStream outputPrintStream, PrintStream errorPrintStream, File unitTestRoot,
			AdditionalVariables additionalVariables) {
		if (pluginDependencyScript == null) {
			return new DeployPluginNoscript(pluginFiles, outputPrintStream, errorPrintStream, unitTestRoot,
					additionalVariables);
		} else if (pluginDependencyScript.getAnsibleScript() instanceof AnsibleSudoScript) {
			return new DeployPluginSudoScript(pluginFiles, outputPrintStream, errorPrintStream, unitTestRoot,
					additionalVariables, pluginDependencyScript);
		} else if (pluginDependencyScript.getAnsibleScript() instanceof AnsibleScript) {
			return new DeployPluginScript(pluginFiles, outputPrintStream, errorPrintStream, unitTestRoot,
					additionalVariables, pluginDependencyScript);
		}
		return null;
	}

}
