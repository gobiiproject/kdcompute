/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugindeployment;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.ScriptNotSupportedException;
import com.diversityarrays.kdcompute.script.AdditionalVariables;

public class DeployPluginNoscriptNoTest extends DeployPlugin {

	public DeployPluginNoscriptNoTest(PluginFiles pluginFiles, PrintStream outputPrintStream,
			PrintStream errorPrintStream, File unitTestRoot, AdditionalVariables additionalVariables) {
		super(pluginFiles, outputPrintStream, errorPrintStream, unitTestRoot, additionalVariables);
	}

	public void deploy(EntityStore entityStore) throws PluginAlreadyExistsInDatabaseException,
			FailedPluginTestsException, ScriptNotSupportedException, IOException {
		super.buildAndMergeIntoDb(pluginFiles, entityStore);
	}

}
