/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.customhtml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.w3c.tidy.Tidy;

import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.Plugin;

public class HtmlForm {

	private String knobs;

	public String getKnobs() {
		return knobs;
	}

	public void setKnobs(String knobs) {
		this.knobs = knobs;
	}

	public HtmlForm() {
		super();
	}

	public static HtmlForm createFromFile(File file) throws IOException {
		HtmlForm htmlForm = new HtmlForm();
		String readFileToString = FileUtils.readFileToString(file, Charset.defaultCharset());
		htmlForm.setKnobs(readFileToString);
		return htmlForm;
	}

	public HtmlForm(Plugin plugin) throws Exception {

		List<String> s = new ArrayList<>();
		s.add(Fragments.getPreFragment());
		for (Knob knob : plugin.getKnobs()) {
			switch (knob.getKnobDataType()) {
			case CHOICE:
				String[] split = knob.getValidationRule().split("\\|");
				s.add(Fragments.getChoiceFragment(knob.getVisibleName(), knob.getKnobName(), knob.getDefaultValue(),
						Arrays.asList(split)));
				break;
			case DECIMAL:
				s.add(Fragments.getDecimalFragment(knob.getVisibleName(), knob.getKnobName(),
						!knob.getDefaultValue().isEmpty() ? Double.parseDouble(knob.getDefaultValue()) : 0, 0.0000001));
				break;
			case TEXT:
				s.add(Fragments.getTextFragment(knob.getVisibleName(), knob.getKnobName(), knob.getDefaultValue()));
				break;
			case FILE_UPLOAD:
				s.add(Fragments.getUploadFragment(knob.getVisibleName(), knob.getKnobName()));
				break;
			case FORMULA:
				throw new Exception("NYI: Formula");
			case INTEGER:
				s.add(Fragments.getIntegerFragment(knob.getVisibleName(), knob.getKnobName(),
						!knob.getDefaultValue().isEmpty() ? Integer.parseInt(knob.getDefaultValue()) : 0));
				break;
			default:
				break;
			}

		}
		s.add(Fragments.getPostFragment());
		String join = String.join("\n", s);
		String finalStr = formatHtml(join);
		this.knobs = finalStr;
	}

	public void write(File dest) throws IOException {
		FileUtils.write(dest, formatHtml(knobs), Charset.defaultCharset());
	}

	public static String formatHtml(String data) throws UnsupportedEncodingException {
		Tidy tidy = new Tidy();
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.setWraplen(Integer.MAX_VALUE);
		tidy.setPrintBodyOnly(true);
		tidy.setXmlOut(true);
		tidy.setSmartIndent(true);
		tidy.setShowErrors(0);
		tidy.setQuiet(true);
		tidy.setShowWarnings(false);

		ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes("UTF-8"));
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		tidy.parseDOM(inputStream, outputStream);
		return outputStream.toString("UTF-8");
	}

	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

}
