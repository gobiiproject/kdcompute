/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.customhtml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.pluginform.PluginForm;

public class Fragments {

	@SuppressWarnings("nls")
	static public enum FragmentName {

		// if choices.length == 1
		CHECKBOX("checkboxFragment.html"),

		// if choices.length < 4
		CHOICELONGPRE("choiceLongFragmentPre.html"),
		CHOICELONGCATAGORY("choiceLongFragmentCatagory.html"),
		CHOICELONGPOST("choiceLongFragmentPost.html"),

		// if choices.length >=4
		CHOICESHORTPRE("choiceShortFragmentPre.html"),
		CHOICESHORTCATAGORY("choiceShortFragmentCatagory.html"),
		CHOICESHORTPOST("choiceShortFragmentPost.html"),

		TEXT("textFragment.html"),
		DECIMAL("decimalFragment.html"),
		FILEUPLOAD("fileUploadFragment.html"),
		FORMULA("formulaFragment.html"),
		INTEGER("integerFragment.html"),

		PRE("formPre.html"),
		POST("formPost.html"),
		POSTFORM("formPostForm.html"),

		TABLEPREFRAGMENT("tablePreFragment.html"),
		TABLEPOSTFRAGMENT("tablePostFragment.html"),


		;

		public final String name;
		FragmentName(String s) {
			this.name = s;
		}
	}


	static public String getFragnentRaw(FragmentName fragmentName) {
		InputStream is = FragmentName.class.getResourceAsStream(fragmentName.name);
		if (is == null) {
			throw new RuntimeException("Missing resource " + fragmentName); //$NON-NLS-1$
		}
		try {
			return  IOUtils.toString(is, Charset.defaultCharset());
		} catch (IOException e) {
			throw new RuntimeException("Missing resource " + fragmentName); //$NON-NLS-1$
		}
	}

	static private String replaceCommon(String rawFragment,String title,String id,String _default){
		return rawFragment.replaceAll("<%=TITLE%>", title).replaceAll("<%=ID%>", id).replaceAll("<%=DEFAULT%>", _default);
	}



	static public String getChoiceFragment(String title, String id,String _default,List<String> choices) throws IOException {
		if(choices.size()==1) {
			return replaceCommon(getFragnentRaw(FragmentName.CHECKBOX), title, id,_default);
		}else if(choices.size()<4) {

			String fragnentRaw = getFragnentRaw(FragmentName.CHOICESHORTCATAGORY);
			List<String> collect = choicesReplace(id, _default, choices, fragnentRaw);
			return String.join("\n",
					replaceCommon(getFragnentRaw(FragmentName.CHOICESHORTPRE),title,id,_default),
					String.join("\n", collect),
					replaceCommon(getFragnentRaw(FragmentName.CHOICESHORTPOST),title,id,_default)
					);
		}else{
			String fragnentRaw = getFragnentRaw(FragmentName.CHOICELONGCATAGORY);
			List<String> collect = choicesReplace(id, _default, choices, fragnentRaw);
			return String.join("\n",
					replaceCommon(getFragnentRaw(FragmentName.CHOICELONGPRE),title,id,_default),
					String.join("\n", collect),
					replaceCommon(getFragnentRaw(FragmentName.CHOICELONGPOST),title,id,_default)
					);
		}

	}

	private static List<String> choicesReplace(String id, String _default, List<String> choices, String fragnentRaw) {
		List<String> collect = new ArrayList<>();
		for(String e : choices) {
			String s = fragnentRaw
			.replaceAll("<%=ID%>", id)
			.replaceAll("<%=VALUE%>", e);
			if(e.equals(_default)) {
				s = s.replaceAll("<%=CHECKED%>", "checked=\"checked\"");
			}else{
				s = s.replaceAll("<%=CHECKED%>", "");
			}
			collect.add(s);
		}
		return collect;
	}

	static public String getDecimalFragment(String title, String id,Double _default,Double step) {
		return replaceCommon(getFragnentRaw(FragmentName.DECIMAL), title, id,Double.toString(_default)).replaceAll("<%=STEP%>", Double.toString(step));
	}

	static public String getUploadFragment(String title, String id) {
		return replaceCommon(getFragnentRaw(FragmentName.FILEUPLOAD), title, id,"");
	}

	static public String getIntegerFragment(String title, String id,Integer _default) {
		return replaceCommon(getFragnentRaw(FragmentName.INTEGER), title, id,Integer.toString(_default));
	}

	static public String getTextFragment(String title, String id,String _default) {
		return replaceCommon(getFragnentRaw(FragmentName.TEXT), title, id,_default);
	}

	static public String getPre(){
		return getFragnentRaw(FragmentName.PRE);
	}

	static public String getPost(Plugin plugin){
		String joinKnobMap = buildMap(plugin);
		return String.join("\n", joinKnobMap,getFragnentRaw(FragmentName.POST));
	}

	static public String getPreFragment(){
		return getFragnentRaw(FragmentName.TABLEPREFRAGMENT);
	}

	static public String getPostFragment(){
		return getFragnentRaw(FragmentName.TABLEPOSTFRAGMENT);
	}

	static public String buildMap(Plugin plugin) {
		String buildJsonKnobMap = PluginForm.buildJsonKnobMap(plugin.getKnobs(),true);
		String replaceAll = buildJsonKnobMap.replaceAll("\n", "' + \n'");
		String joinKnobMap = String.join("\n",
				"<script>",
				"var jsonConfig = "+
				"'"+ replaceAll+"'"
				+";",
				"var jsonKnobMap = parseJsonValidationRule(jsonConfig);",
				"validateForSubmitButton();",
				"findChoiceKnobs(jsonKnobMap);",
				"</script>");
		return joinKnobMap;
	}

	static public String createHtmlPage(String knobs2,Plugin plugin) {
		return String.join("\n",
				knobs2,
				buildMap(plugin));
	}
}
