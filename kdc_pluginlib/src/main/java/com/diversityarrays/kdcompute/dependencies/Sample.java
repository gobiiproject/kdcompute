/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.dependencies;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;

import com.diversityarrays.kdcompute.plugin.files.ScriptType;

public class Sample {
	public static String createNew(String title, String filename, String description)
			throws MalformedURLException, IOException {

		String resourceName = "ansible_template.yml";
		;
		if (SystemUtils.IS_OS_MAC_OSX) {
			resourceName = "ansible_template_osx.yml";
			;
		}
		InputStream resourceAsStream = ScriptType.class.getResourceAsStream(resourceName);
		String string;
		string = IOUtils.toString(resourceAsStream, Charset.defaultCharset());
		String replace = string.replace("[[title]]", title).replace("[[filename]]", filename).replace("[[description]]",
				description);
		return replace;
	}
}
