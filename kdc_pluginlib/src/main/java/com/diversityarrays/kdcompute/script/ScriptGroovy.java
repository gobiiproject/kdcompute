/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.script;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

public class ScriptGroovy implements Script {

	private String kdcomputeKnobsAutogenComment;
	private String kdcomputeKnobsText;
	private String knobsAutogenComment;
	private String knobsText;
	private String logicAutogenComment;
	private String logicText;

	private static final String END_COMMENT = "*/";
	private static final String AUTOGEN_TAG = "AUTOGENERATED_BY_KDCPLUGINSTUDIO";
	private static final String START_COMMENT = "/*";

	public ScriptGroovy(File file) throws IOException {
		this(FileUtils.readFileToString(file,Charset.defaultCharset()));
	}

    static private int findAutogenCommentLine(List<String> lines, int startIndex) throws IOException {
        int result = -1;
        for (int index = startIndex; index < lines.size(); ++index) {
            if (lines.get(index).contains(AUTOGEN_TAG)) {
                result = index;
                while (result >= 0) {
                    if (lines.get(result).trim().startsWith(START_COMMENT)) {
                        return result;
                    }
                    --result;
                }
                throw new IOException("Missing expected START_COMMENT (" + START_COMMENT + ")");
            }
        }
        if (result < 0) {
            throw new IOException("Missing expected START_COMMENT (" + START_COMMENT + ")");
        }
        return result;
    }
    static private int findEndCommentLine(List<String> lines, int startIndex) throws IOException {
        for (int index = startIndex; index < lines.size(); ++index) {
            if (lines.get(index).trim().startsWith(END_COMMENT)) {
                return index;
            }
        }
        throw new IOException("Missing an expect END_COMMENT (" + END_COMMENT + ")");
    }

	public ScriptGroovy(String script) throws IOException {
        List<String> lines = Arrays.asList(script.split("\n"));
		if(3 != lines.stream().filter(s -> s.contains(AUTOGEN_TAG)).count()) {
			throw new IOException("Failed to load Plugin Script, could not find 3 '"+AUTOGEN_TAG+"' tokens");
		}

	    int temp_idx = findAutogenCommentLine(lines, 0);
        int kdc_knobs_autogen_end_idx = findEndCommentLine(lines, temp_idx+1);

        int kdc_knobs_start_idx = kdc_knobs_autogen_end_idx + 1;

        int knobs_autogen_start_idx = findAutogenCommentLine(lines, kdc_knobs_start_idx+1);
        int knobs_autogen_end_idx = findEndCommentLine(lines, knobs_autogen_start_idx);

        int kdc_knobs_end_idx = knobs_autogen_start_idx - 1;

        int knobs_start_idx = knobs_autogen_end_idx + 1;
        int logic_autogen_start_idx = findAutogenCommentLine(lines, knobs_start_idx+1);

        int knobs_end_idx = logic_autogen_start_idx - 1;
        int logic_autogen_end_idx = findEndCommentLine(lines, logic_autogen_start_idx+1);

        kdcomputeKnobsAutogenComment = collectLines(lines, 0, kdc_knobs_autogen_end_idx);
        kdcomputeKnobsText = collectLines(lines, kdc_knobs_start_idx, kdc_knobs_end_idx);

        knobsAutogenComment = collectLines(lines, knobs_autogen_start_idx, knobs_autogen_end_idx);
        knobsText = collectLines(lines, knobs_start_idx, knobs_end_idx);

        logicAutogenComment = collectLines(lines, logic_autogen_start_idx, logic_autogen_end_idx);
        logicText = collectLines(lines, logic_autogen_end_idx+1, lines.size() - 1);
	}

	private String collectLines(List<String> lines, int fromIndex, int toIndexInclusive) {
	    StringBuilder sb = new StringBuilder();
	    for (String s : lines.subList(fromIndex, toIndexInclusive+1)) {
	        sb.append(s).append('\n');
	    }
        return sb.toString();
    }

    @Override
    public void setKnobsText(String knobsText) {
		this.knobsText = knobsText;
	}

	@Override
    public String getKdcomputeKnobsAutogenComment() {
		return kdcomputeKnobsAutogenComment;
	}

	@Override
    public String getKdcomputeKnobsText() {
		return kdcomputeKnobsText;
	}

	@Override
    public String getKnobsAutogenComment() {
		return knobsAutogenComment;
	}

	@Override
    public String getKnobsText() {
		return knobsText;
	}

	@Override
    public String getLogicAutogenComment() {
		return logicAutogenComment;
	}

	@Override
    public String getLogicText() {
		return logicText;
	}

	@Override
	public String toString() {
	    StringBuilder sb = new StringBuilder();
        Consumer<String> appender = (s) -> {
            sb.append(s);
            if (! s.endsWith("\n")) {
                sb.append('\n');
            }
        };
        appender.accept(kdcomputeKnobsAutogenComment);
        appender.accept(kdcomputeKnobsText);
        appender.accept(knobsAutogenComment);
        appender.accept(knobsText);
        appender.accept(logicAutogenComment);
        appender.accept(logicText);
        return sb.toString();
	}

	@Override
	public List<String> getSupportedExtensions() {
		return Arrays.asList("groovy");
	}

	@Override
	public String getRSyntaxTextAreaStyle() {
		return SyntaxConstants.SYNTAX_STYLE_JAVA;
	}

	@Override
	public Script createFromContent(String context) throws Exception {
		return new ScriptGroovy(context);
	}

	@Override
	public void setKdcomputeKnobsText(String kdcomputeKnobsText) {
		this.kdcomputeKnobsText = kdcomputeKnobsText;
	}





}
