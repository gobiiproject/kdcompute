/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.help;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.db.DataSet;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.plugin.Contact;
import com.diversityarrays.kdcompute.plugin.Defaults;
import com.diversityarrays.kdcompute.plugin.files.FilesBase;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;

public class EnglishGenerateHelpPage implements HtmlGenerator {

	private PluginFiles pluginFiles;
	private static final String LOGO = "logo";
	private static final String RETURN = "\n";
	private static final String BR = "<br/>";

	public EnglishGenerateHelpPage(PluginFiles pluginFiles) {
		super();
		this.pluginFiles = pluginFiles;
	}

	@Override
	public String getHtml() {
		Plugin plugin = pluginFiles.getPlugin();
		Defaults defaults = pluginFiles.getDefaults();
		Contact contact = pluginFiles.getContact();

		/*
		 *
		 */
		File sourceResourcesFolder = pluginFiles.getHelpResourcesFolder();
		File logoFile = getLogoFile(pluginFiles.getHelpDir(), sourceResourcesFolder);
		String page = "";
		if (logoFile != null) {
			try {
				page = wrapLogoInHtml(pluginFiles, logoFile);
			} catch (Exception e) {
				// Just forget about the img
			}
		}

		page += "<a name=\"" + PluginCollectionMeta.pluginRef(pluginFiles.getPlugin()) + "\">";
		page += "<h2>" + plugin.getAlgorithmName() + "</h2>\n\n";
		page += "<h3>" + "Attributes" + "</h3>\n\n";
		page += "<p>Plugin Name    : " + plugin.getPluginName() + "</p>\n\n";
		page += "<p>Plugin Group    : " + defaults.getGroupName() + "</p>\n\n";
		page += "<p>Version      : " + plugin.getVersion() + "</p>\n\n";
		;
		if (plugin.getVersionString() != null)
			page += " ( " + plugin.getVersionString() + " )" + "</p>\n\n";
		page += "<p>Author       : " + plugin.getAuthor() + "</p>\n\n";
		page += "<p>Maintainer's email       : " + contact.getEmail() + "</p>\n\n";
		Date whenLastUpdated = plugin.getWhenLastUpdated();
		if (whenLastUpdated == null) {
			page += "<p>Last Updated : " + "Not Provided" + "</p>\n\n";
		} else {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			page += "<p>Last Updated : " + formatter.format(whenLastUpdated) + "</p>\n\n";
		}
		page += "\n\n<h3>Description</h3>";
		page += plugin.getDescription().replaceAll(Character.toString('\n'), "\n<br/>") + RETURN;

		page += "\n\n<h3>Arguments</h3>";

		page += "<table  cellspacing=\"20\">" + RETURN;
		for (DataSet dataset : plugin.getInputDataSets()) {
			page += "	<tr>\n";
			page += "		<td valign=\"top\">" + dataset.getVisibleName() + "</td>\n";
			page += "		<td valign=\"top\">" + dataset.getDataSetType() + "</td>\n";
			page += "		<td valign=\"top\">" + dataset.getTooltip().replaceAll(RETURN, BR) + "</td>\n";
			page += "	</tr>\n";
		}
		for (Knob knob : plugin.getKnobs()) {
			page += "	<tr>\n";
			page += "		<td valign=\"top\">" + knob.getVisibleName() + "</td>\n";
			page += "		<td valign=\"top\">" + knob.getKnobDataType() + "</td>\n";
			page += "		<td valign=\"top\">" + knob.getTooltip().replaceAll(RETURN, BR) + "</td>\n";
			page += "	</tr>\n";
		}
		page += "</table>" + RETURN;
		return page;
	}

	final static String resources = "resources";

	/**
	 * Logo must be in a subforlder of
	 * <pluginroot>/help/resource/path/to/logo.png
	 * 
	 * @param helpDir
	 * @param logoFolder
	 * @param logo
	 * @return
	 * @throws Exception
	 */
	public static String wrapLogoInHtml(FilesBase filesBase, File logoFile) throws Exception {
		File helpResourcesFolder = filesBase.getHelpResourcesFolder();
		if (helpResourcesFolder.getPath().contains(logoFile.getPath())) {
			throw new Exception(logoFile + " is not subfile of help resource folder " + helpResourcesFolder);
		}
		String page = "";
		if (logoFile != null) {
			String logoFilePath = logoFile.getPath().replaceAll(filesBase.getHelpDir() + File.separator, "");
			page += "<img src=\"" + logoFilePath + "\" style=\"float:right;max-height: 330px;max-width:60%;\">\n\n";
		}
		return page;
	}

	public static File getLogoFile(File helpFolder, File resourceFolder) {

		for (ImgTypes imgTypes : ImgTypes.values()) {
			String logoName = LOGO + "." + imgTypes.toString();
			File file = new File(resourceFolder.getPath(), logoName);
			if (file.exists()) {
				return file;
			}
			File file2 = new File(helpFolder.getPath(), logoName);
			if (file2.exists()) {
				return file2;
			}
		}
		return null;
	}

	@Override
	public void copyResourcesTo(File dest) throws IOException {
		File helpResourcesFolder = pluginFiles.getHelpResourcesFolder();
		File destDir = new File(dest, helpResourcesFolder.getName());
		if (helpResourcesFolder.exists()) {
			FileUtils.copyDirectory(helpResourcesFolder, destDir);
		} else {
			destDir.mkdirs();
		}
	}

}
