/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.help;

import java.io.File;
import java.io.IOException;

public interface HelpRetriever {

	File getHelpDir();

	void setDefaultHelp(String name) throws IOException;

	/**
	 * Create the "resources" folder.
	 * @return result of <code>mkdirs()</code>
	 */
	boolean makeHelpResourcesFolder();

	/**
	 * This identifies where the resources are stored.
	 * It used to be the name of the plugin, lower-cased and with invalid filename chars
	 * changed to "_".
	 * <p>
	 * As of Jan 2017, this is now a constant "resources" sub-directory which improves the
	 * maintainability of the packages and simplifies code that has to use it (e.g. in KDXplore)
	 * - at the cost of a little complexity in getting to the "logo" files
	 * (see {@link EnglishGenerateHelpPage#getHtml}).
	 * @return
	 */
	File getHelpResourcesFolder();


}
