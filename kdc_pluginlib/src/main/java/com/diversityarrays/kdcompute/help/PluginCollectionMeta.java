/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.help;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.plugin.files.collection.PluginCollectionFiles;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.google.gson.JsonSyntaxException;


public class PluginCollectionMeta implements HtmlGenerator {

	private static final String BR = "<hr/>";
	private static final String NEWLINE = "\n";
	PluginCollectionFiles commonFiles;
	Supplier<String> htmlBody;

	public PluginCollectionMeta(PluginCollectionFiles commonFiles, Supplier<String> htmlBody) {
		super();
		this.commonFiles = commonFiles;
		this.htmlBody = htmlBody;
	}

	@Override
	public String getHtml() {
		return String.join(NEWLINE, getHeaderHTML(),htmlBody.get(),getPluginsHTML());

	}

	public Supplier<String> getHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(Supplier<String> htmlBody) {
		this.htmlBody = htmlBody;
	}

	public void setCommonFiles(PluginCollectionFiles commonFiles) {
		this.commonFiles = commonFiles;
	}

	public String getPluginsHTML() {
		List<String> pluginsBodyL = new ArrayList<>();
		for(String plugin : commonFiles.getPluginCollection().getPlugins()) {
			try {
				PluginFiles pluginFiles = PluginFiles.readPluginFiles(new File(commonFiles.getRootDir(),plugin));
				EnglishGenerateHelpPage englishGenerateHelpPage = new EnglishGenerateHelpPage(pluginFiles);
				pluginsBodyL.add(englishGenerateHelpPage.getHtml());
				pluginsBodyL.add(BR);
			} catch (JsonSyntaxException | IOException e) {
				pluginsBodyL.add("<h2>"+plugin+"</h2>");
				pluginsBodyL.add("<p>Failed to parse help page</p>");
			}
		}
		return String.join(NEWLINE, pluginsBodyL);
	}

	public String getHeaderHTML() {
		File logoFile = EnglishGenerateHelpPage.getLogoFile(commonFiles.getHelpDir(), commonFiles.getHelpResourcesFolder());
		String page ="";
		if(logoFile!=null && logoFile.exists()) {
			try {
				page = EnglishGenerateHelpPage.wrapLogoInHtml(commonFiles, logoFile);
			} catch (Exception e) {
				// Just forget about the img
			}
		}

		String meta= String.join(NEWLINE,
				page,
				"<h1> Plugin Collection: "+commonFiles.getPluginCollection().getPluginGroup()+ "</h1>",
				"",
				"<h2>Attributes</h2>",
				"",
				"<p>Version: "+commonFiles.getPluginCollection().getVersion()+"</p>",
				"<p>Author: "+commonFiles.getPluginCollection().getAuthor()+"</p>",
				"<p>Maintainer's email: "+commonFiles.getPluginCollection().getContact()+"</p>",
				"<p>Last Updated: "+commonFiles.getPluginCollection().getLastUpdated()+"</p>"
				);

		List<String> pluginsRef = new ArrayList<>();

		pluginsRef.add("<h2>Plugins</h2>");
		pluginsRef.add("<ul>");
		for(String plugin : commonFiles.getPluginCollection().getPlugins()) {
			try {
				PluginFiles pluginFiles = PluginFiles.readPluginFiles(new File(commonFiles.getRootDir(),plugin));
				pluginsRef.add("<li><a href=\"#"+ pluginRef(pluginFiles.getPlugin()) + "\">"+pluginFiles.getPlugin().getAlgorithmName()+"</a></li>");
			} catch (JsonSyntaxException | IOException e) {
			}
		}
		pluginsRef.add("</ul>");

		return String.join(NEWLINE, meta,String.join(NEWLINE, pluginsRef));
	}

	public static String pluginRef(Plugin plugin) {
		return "pluginref-"+plugin.getPluginName();
	}

	@Override
	public void copyResourcesTo(File dest) throws JsonSyntaxException, IOException{

		for(String pluginName : commonFiles.getPluginCollection().getPlugins()) {
			File pluginFolder = new File(commonFiles.getRootDir(), pluginName);
			PluginFiles pluginFiles = PluginFiles.readPluginFiles(pluginFolder);
			FileUtils.copyDirectory(pluginFiles.getHelpResourcesFolder(), dest);
		}

		File resourcesFolder = commonFiles.getHelpResourcesFolder();
		FileUtils.copyDirectory(resourcesFolder,new File(dest, resourcesFolder.getName()));
	}

}
