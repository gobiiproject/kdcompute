/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.submitjob;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.RequiredKnobNotProvidedException;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.JobQueueException;
import com.diversityarrays.kdcompute.util.Either;

public class SubmitJobUtils {
	/**
	 * @param request
	 * @param file
	 * @param pluginFiles
	 * @param userTempDir
	 *            Directory to place uploaded input files
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws RequiredKnobNotProvidedException
	 */
	public static Pair<List<File>, List<KnobBinding>> getKnobsAndValues(HttpServletRequest request,
			MultipartFile[] file, PluginFiles pluginFiles, File userTempDir)
			throws IOException, RequiredKnobNotProvidedException {
		List<Knob> allKnobs = pluginFiles.getPlugin().getKnobs();
		int i = 0; // TODO HACK

		List<KnobBinding> knobsAndValues = new ArrayList<>();

		// Store input files if example is used
		List<File> inputFilesToCopy = new ArrayList<>();

		for (Knob k : allKnobs) {

			if (k != null) { // Most definitely not null just being safe
				KnobBinding knobBinding = null;

				if (k.getKnobDataType() == KnobDataType.FILE_UPLOAD && file != null) {

					// Check if plugin example submitted
					if (file.length == 0) {
						String parsedKnobValue = request.getParameter(k.getKnobName());
						if (parsedKnobValue == null) {
							if (k.isRequired()) {
								throw new RequiredKnobNotProvidedException("Missing required file knob " + k);
							} else {
								knobBinding = new KnobBinding(k, "");
							}
						} else {
							File finalFile = null;

							File fileAsIfOnServer = new File(parsedKnobValue);
							if (fileAsIfOnServer.exists()) {
								finalFile = fileAsIfOnServer;
							} else {
								finalFile = new File(pluginFiles.getDataRoot(), parsedKnobValue);
							}
							knobBinding = new KnobBinding(k, finalFile.getAbsolutePath());
							inputFilesToCopy.add(finalFile);
						}
					} else {
						MultipartFile multipartFile = file[i];
						if (!multipartFile.isEmpty()) {

							File fileReceived = new File(userTempDir, multipartFile.getOriginalFilename());

							InputStream fileInputStream = null;
							FileOutputStream fileOutputStream = null;

							try {
								fileInputStream = multipartFile.getInputStream();
								fileOutputStream = new FileOutputStream(fileReceived);

								IOUtils.copy(multipartFile.getInputStream(), new FileOutputStream(fileReceived));

								// System.out.println("Knob Name : " +
								// k.getKnobName() + ", fileName "
								// + multipartFile.getOriginalFilename());
								knobBinding = new KnobBinding(k, fileReceived.getAbsolutePath());
								i++;
							}

							finally {
								fileInputStream.close();
								fileOutputStream.close();
							}
						} else if (k.isRequired()) {
							throw new RequiredKnobNotProvidedException("Missing required knob file " + k.getKnobName());
						} else {
							// Not required
							knobBinding = new KnobBinding(k, "");
						}
					}
				} else {
					String parsedKnobValue = request.getParameter(k.getKnobName());
					if (parsedKnobValue == null) {
						parsedKnobValue = "";
					}
					if (k.getKnobDataType() == KnobDataType.CHOICE) {
						knobBinding = new KnobBinding(k, parsedKnobValue);
						// System.out.println("knobName = " + k.getKnobName() +
						// ", value = " + parsedKnobValue);
					} else {
						knobBinding = new KnobBinding(k, parsedKnobValue);
						// System.out.println("knobName = " + k.getKnobName() +
						// ", value = " + parsedKnobValue);
					}
				}
				knobsAndValues.add(knobBinding);
			}
		}
		return new Pair<>(inputFilesToCopy, knobsAndValues);
	}

	static public Either<Exception, AnalysisJob> submitJob(PluginJobParams params, Consumer<Object> entityStore,
			Function<AnalysisRequest, Either<Exception, AnalysisJob>> jobFactory,
			Function<AnalysisJob, Optional<JobQueueException>> jobQueue) {
		RunBinding binding = params.getRunBinding();

		AnalysisRequest req = new AnalysisRequest(binding);
		req.setForUser(params.userName);
		entityStore.accept(binding);
		entityStore.accept(req);

		Either<Exception, AnalysisJob> either = jobFactory.apply(req);
		if (either.isRight()) {
			AnalysisJob job = either.right();
			job.setPostCompletionCall(params.postCompletionCall);

			Optional<JobQueueException> opt = jobQueue.apply(job);
			if (opt.isPresent()) {
				either = Either.left(opt.get());
			}
		}
		return either;
	}
}
