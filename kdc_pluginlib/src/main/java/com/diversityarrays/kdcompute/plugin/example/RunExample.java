/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.example;

import java.awt.Color;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.plugin.OutputRedirect;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.AnalysisJobRunner;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.ParameterException;
import com.diversityarrays.kdcompute.runtime.SimpleAnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;
import com.diversityarrays.kdcompute.util.Either;

public class RunExample {

	private final Example example;
	private final OutputRedirect outputRedirect;
	private final PluginFiles pluginFiles;
	private final File pluginDirectory;
	private final AdditionalVariables additionalVariables;

	private AnalysisJob analysisJob;

	private final List<KnobBinding> knobBinding; // copied to handle file
	private final long timeoutMillis;
	// uplaods

	private final Supplier<CallScript> callScript;

	public RunExample(Supplier<CallScript> callScript, Example example, PluginFiles pluginFiles,
			OutputRedirect outputRedirect, File inputFileRoot, AdditionalVariables additionalVariables,
			long timeoutMillis) {

		this.example = example;
		this.pluginFiles = pluginFiles;
		this.pluginDirectory = pluginFiles.getRootDir();
		this.outputRedirect = outputRedirect;
		this.additionalVariables = additionalVariables;
		this.timeoutMillis = timeoutMillis;
		this.callScript = callScript;

		this.knobBinding = new ArrayList<>();

		// Adjust input datasets paths
		for (KnobBinding binding : this.example.getBindings()) {
			String value = binding.getKnobValue();
			if (binding.getKnob().getKnobDataType().equals(KnobDataType.FILE_UPLOAD)) {
				if (inputFileRoot != null) {
					value = new File(inputFileRoot, binding.getKnobValue()).getAbsolutePath();
				} else {
					value = binding.getKnobValue();
				}
			}
			this.knobBinding.add(new KnobBinding(binding.getKnob(), value));
		}

	}

	public RunExample(Supplier<CallScript> callScript, Example example, PluginFiles pluginFiles2, OutputRedirect split,
			File dataRoot, AdditionalVariables additionalVariables2) {
		this(callScript, example, pluginFiles2, split, dataRoot, additionalVariables2, 10 * 60 * 1000);
	}

	public RunExample(Supplier<CallScript> callScript2, Example e, PluginFiles pluginFiles2,
			PrintStream outputPrintStream, PrintStream errorPrintStream, File dataRoot,
			AdditionalVariables additionalVariables2) {
		this(callScript2, e, pluginFiles2, new OutputRedirect() {

			@Override
			public void appendStdout(String msg) {
				outputPrintStream.println(msg);
			}

			@Override
			public void appendStderr(String msg) {
				errorPrintStream.println(msg);
			}

			@Override
			public void append(String msg) {
				outputPrintStream.println(msg);
			}

			@Override
			public void append(List<Pair<String, Color>> msgsWithColors) {
				for (Pair<String, Color> p : msgsWithColors) {
					outputPrintStream.println(p.getFirst());
				}
			}

			@Override
			public void append(String msg, Color color) {
				outputPrintStream.println(msg);
			}
		}, dataRoot, additionalVariables2);
	}

	public Example getExample() {
		return example;
	}

	static private Map<String, CallScript> buildCallScriptMap(CallScript... callScripts) {
		Map<String, CallScript> result = new HashMap<>();
		for (CallScript cs : callScripts) {
			for (String ext : cs.getSupportedExtensions()) {
				result.put(ext, cs);
			}
		}
		return result;
	}

	/**
	 * Thread safe
	 * 
	 * @return true if ran ok
	 * @throws IOException
	 */
	public boolean execute(String forUser, File tempResultsFolder, Consumer<AnalysisJob> onAnalysisJobCreated)
			throws IOException {
		if (analysisJob != null) {
			throw new IllegalStateException("Already run or running");
		}
		// Collecting messages before outputing. This is done so because these
		// can be run concurrently

		RunBinding runBinding = new RunBinding(pluginFiles.getPlugin());
		runBinding.setKnobBindings(knobBinding);
		runBinding.setInputDataSetBindings(example.getInputDatasets());

		runBinding.setOutputFolderPath(tempResultsFolder.getAbsolutePath());
		AnalysisRequest analysisRequest = new AnalysisRequest(runBinding);

		SimpleAnalysisJobFactory analysisJobFactory = new SimpleAnalysisJobFactory();
		analysisJobFactory.setFileStorage(new FileStorageImpl(tempResultsFolder));
		analysisRequest.setId(0L);

		try {
			analysisJob = analysisJobFactory.createAnalysisJob(analysisRequest, forUser, (file) -> {
			}, // resultsFolderCreated
					additionalVariables.instantiateAdditionalKDComputeSystemVariables(forUser));
		} catch (ParameterException | UserIsNotLoggedInException | IOException e1) {
			outputRedirect.append(e1.getMessage(), Color.RED.darker());
			return false;
		}

		if (onAnalysisJobCreated != null) {
			onAnalysisJobCreated.accept(analysisJob);
		}

		Function<File, Optional<CallScript>> callScriptProvider = new Function<File, Optional<CallScript>>() {
			@Override
			public Optional<CallScript> apply(File scriptFile) {
				return Optional.of(callScript.get());
			}
		};

		Function<AnalysisJob, Exception> statusChangeConsumer = new Function<AnalysisJob, Exception>() {
			@Override
			public Exception apply(AnalysisJob t) {
				// do nothing for now
				return null;
			}
		};
		AnalysisJobRunner jobRunner = new AnalysisJobRunner(callScriptProvider,
				AnalysisJobRunner.DEFAULT_STDOUT_PROVIDER, AnalysisJobRunner.DEFAULT_STDERR_PROVIDER,
				statusChangeConsumer, additionalVariables);

		boolean success = false;
		try {
			jobRunner.runJob(analysisJob, timeoutMillis, "Please report to Plugin Developer");
		} finally {
			success = report(analysisJob);
		}

		return success;
	}

	private boolean report(AnalysisJob analysisJob) throws IOException {
		boolean result = true;
		File tempResultsFolder = analysisJob.getResultsFolder();
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
		outputRedirect.append("\n" + sdf.format(new Date()) + "\tCompleted Test: " + example.getName(),
				Color.YELLOW.darker().darker());

		File dataRoot = pluginFiles.getDataRoot();
		// Check exp exists
		for (ExpExists expOut : example.getExpExists()) {
			try {
				String actualFilepath = expOut.getActualFilepath();
				if (actualFilepath.isEmpty())
					throw new IOException("Actual file (" + actualFilepath + ") not provided");
				File actual = regexFile(tempResultsFolder, actualFilepath);
				if (actual == null || !actual.exists()) {
					outputRedirect.append("Actual file (" + actualFilepath + ") does not exist:", Color.RED.darker());
					result = false;
				}
			} catch (IOException e) {
				result = false;
				outputRedirect.append(e.getMessage() + "\n", Color.RED.darker());
			}
		}

		// Check exp identical
		for (ExpIdentical expOut : example.getExpIdentical()) {
			if (!doChecker(tempResultsFolder, expOut, dataRoot, checkExpIdentical)) {
				result = false;
			}
		}

		// Check exp percentage identical
		for (ExpPercentageIdentical expOut : example.getExpPercentageIdentical()) {
			if (!doChecker(tempResultsFolder, expOut, dataRoot, checkExpPercentageIdentical)) {
				result = false;
			}
		}

		// Check exp size
		for (ExpSize expOut : example.getExpSize()) {
			if (!doChecker(tempResultsFolder, expOut, dataRoot, checkExpSize)) {
				result = false;
			}
		}

		if (analysisJob.getRunState() != RunState.COMPLETED) {
			outputRedirect.append("Job did not finish with COMPLETED status, but with " + analysisJob.getRunState(),
					Color.RED.darker());
			result = false;
		}

		if (result) {
			outputRedirect.append("ALL TESTS PASSED: " + example.getName(), Color.GREEN.darker());
			outputRedirect.append("Job folder: " + tempResultsFolder.getPath(), Color.GREEN.darker());
		} else {
			outputRedirect.append("TEST(S) FAILED: " + example.getName() + ". See previous errors", Color.RED.darker());
			outputRedirect.append("Job folder: " + tempResultsFolder.getPath(), Color.RED.darker());
		}

		return result;
	}

	private Function<CheckParam<ExpIdentical>, Either<IOException, Boolean>> checkExpIdentical = new Function<CheckParam<ExpIdentical>, Either<IOException, Boolean>>() {
		@Override
		public Either<IOException, Boolean> apply(CheckParam<ExpIdentical> param) {
			boolean result = true;
			try {
				if (!FileUtils.contentEquals(param.expected, param.actual)) {
					outputRedirect.append("ACTUAL FILE DIFFERS FROM EXPECTED:", Color.RED.darker());
					outputRedirect.append("> EXPECTED FILE: " + param.expectedFilepath, Color.CYAN.darker());
					outputRedirect.append(param.expected.getPath().trim(), Color.MAGENTA.darker());
					outputRedirect.append("> ACTUAL FILE: " + param.actualFilepath, Color.CYAN.darker());
					outputRedirect.append(param.actual.getPath().trim(), Color.RED);
					result = false;
				}
			} catch (IOException e) {
				return Either.left(e);
			}
			return Either.right(result);
		}
	};

	private <T extends ActualExp> boolean doChecker(File tempDir, T ae, File dataRoot,
			Function<CheckParam<T>, Either<IOException, Boolean>> func) {
		try {
			CheckParam<T> param = new CheckParam<>(dataRoot, ae, tempDir);
			return checker(param, func);
		} catch (IOException e) {
			outputRedirect.append(e.getMessage() + "\n", Color.RED.darker());
			return false;
		}

	}

	private <T extends ActualExp> boolean checker(CheckParam<T> param,
			Function<CheckParam<T>, Either<IOException, Boolean>> func) throws IOException {
		boolean result = true;
		if (param.actualFilepath.isEmpty())
			throw new IOException("Actual file not provided");
		if (param.expectedFilepath.isEmpty())
			throw new IOException("Expected file not provided");
		if (!param.expected.exists()) {
			outputRedirect.append("Expected file (" + param.expectedFilepath + ") does not exist:", Color.RED.darker());
			outputRedirect.append(" " + param.expected.getAbsolutePath(), Color.CYAN.darker());
			result = false;
		} else if (param.actual == null || !param.actual.exists()) {
			outputRedirect.append("Actual(" + param.actualFilepath + ") file does not exist:", Color.RED.darker());
			result = false;
		} else {
			Either<IOException, Boolean> either = func.apply(param);
			if (either.isLeft()) {
				throw either.left();
			}
			result = either.right();
		}
		return result;
	}

	private Function<CheckParam<ExpSize>, Either<IOException, Boolean>> checkExpSize = new Function<CheckParam<ExpSize>, Either<IOException, Boolean>>() {

		@Override
		public Either<IOException, Boolean> apply(CheckParam<ExpSize> param) {
			boolean result = true;

			long actualBytes = param.actual.length();
			long expectedBytes = param.expected.length();

			double percentageDiff = (double) actualBytes / (double) expectedBytes * 100.0;

			if (percentageDiff < 100.0 - param.expOut.getTolleranceDiff()
					|| percentageDiff > 100.0 + param.expOut.getTolleranceDiff()) {
				outputRedirect.append("ACTUAL FILE SIZE DIFFERS FROM EXPECTED SIZE BEYOND TOLLERANCE:",
						Color.RED.darker());
				outputRedirect.append("> EXPECTED FILE: " + param.expectedFilepath, Color.CYAN.darker());
				outputRedirect.append(Long.toString(expectedBytes) + " bytes", Color.MAGENTA.darker());
				outputRedirect.append("> ACTUAL FILE: " + param.actualFilepath, Color.CYAN.darker());

				if (percentageDiff < 100.0 - param.expOut.getTolleranceDiff()) {
					outputRedirect.append(String.format("%d bytes(%6.2f%% < 100.0%% - %6.2f%% )", actualBytes,
							percentageDiff, param.expOut.getTolleranceDiff()), Color.RED);
				} else {
					outputRedirect.append(String.format("%d bytes (%6.2f%% > 100.0%% + %6.2f%%", actualBytes,
							percentageDiff, param.expOut.getTolleranceDiff()), Color.RED);
				}
				result = false;
			}

			return Either.right(result);
		}

	};

	class CheckParam<T extends ActualExp> {
		public final String expectedFilepath;
		public final File expected;

		public final String actualFilepath;
		public final File actual;

		public final T expOut;

		public CheckParam(File dataRoot, T t, File tempDir) throws IOException {
			actualFilepath = t.getActualFilepath();
			expectedFilepath = t.getExpectedFilepath();
			expOut = t;

			if (actualFilepath == null || actualFilepath.isEmpty()) {
				throw new IOException("Actual file not provided");
			}
			actual = regexFile(tempDir, actualFilepath);

			if (expectedFilepath == null || expectedFilepath.isEmpty()) {
				throw new IOException("Expected file not provided");
			}
			expected = new File(dataRoot, expectedFilepath);
		}

		public CheckParam(File dataRoot, String ep,

				File tempDir, String ap,

				T t) throws IOException {
			actualFilepath = ap;
			expectedFilepath = ep;
			expOut = t;

			if (actualFilepath.isEmpty()) {
				throw new IOException("Actual file not provided");
			}
			actual = regexFile(tempDir, actualFilepath);

			if (expectedFilepath.isEmpty()) {
				throw new IOException("Expected file not provided");
			}
			expected = new File(dataRoot, expectedFilepath);
		}
	}

	private Function<CheckParam<ExpPercentageIdentical>, Either<IOException, Boolean>> checkExpPercentageIdentical = new Function<CheckParam<ExpPercentageIdentical>, Either<IOException, Boolean>>() {

		@Override
		public Either<IOException, Boolean> apply(CheckParam<ExpPercentageIdentical> checkParam) {
			boolean result = true;

			String actualString = null;
			String expectedString = null;
			try {
				actualString = FileUtils.readFileToString(checkParam.actual, "UTF-8");
				expectedString = FileUtils.readFileToString(checkParam.expected, "UTF-8");
			} catch (IOException e) {
				outputRedirect.append(e.getMessage(), Color.RED.darker());
				return Either.right(Boolean.FALSE);
			}

			String[] actualStringList = actualString.split("\n");
			String[] expectedStringList = expectedString.split("\n");

			int nDiff = 0;
			for (int i = 0; i < Math.min(actualStringList.length, expectedStringList.length); i++) {
				String act = actualStringList[i];
				String exp = expectedStringList[i];
				nDiff += !act.equals(exp) ? 1 : 0;
			}
			int totalLines = expectedString.split("\n").length;

			double percent = (1.0 * (totalLines - nDiff) / totalLines);
			if (percent < checkParam.expOut.getPercentageIdentical() / 100.0) {
				String idenitcal = " % IDENTICAL < % REQUIRED = ( "
						+ Double.toString(100.0 * (totalLines - nDiff) / totalLines) + "% < "
						+ Double.toString(checkParam.expOut.getPercentageIdentical()) + "% )";
				outputRedirect.append("ACTUAL FILE DIFFERS FROM EXPECTED ABOVE THRESHOLD:", Color.RED.darker());

				outputRedirect.append("> EXPECTED FILE: " + checkParam.expectedFilepath, Color.CYAN.darker());
				outputRedirect.append(checkParam.expected.getPath().trim(), Color.MAGENTA.darker());

				outputRedirect.append("> ACTUAL FILE: " + checkParam.actualFilepath, Color.CYAN.darker());
				outputRedirect.append(checkParam.actual.getPath().trim(), Color.RED);
				outputRedirect.append(idenitcal, Color.RED);
				result = false;
			}

			return Either.right(result);
		}
	};

	private static File[] listFilesMatching(File root, String regex) throws IOException {
		if (!root.isDirectory()) {
			throw new IOException(root + " is no directory.");
		}
		final Pattern p = Pattern.compile(regex); // careful: could also throw
		// an exception!
		return root.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return p.matcher(file.getName()).matches();
			}
		});
	}

	private File regexFile(File dir, String expectedFilepath) throws IOException {
		File f = new File(dir, expectedFilepath);

		String filename = f.getName();
		File parentFile = f.getParentFile();
		File[] files = listFilesMatching(parentFile, filename);

		if (files.length > 0) {
			return files[0];
		} else {
			return null;
		}
	}

	class FileStorageImpl implements FileStorage {

		File tempResultsFolder;

		FileStorageImpl(File tmpdir) {
			tempResultsFolder = tmpdir;
		}

		@Override
		public void setResultsRootDir(File d) {

		}

		@Override
		public void setKdcomputePluginsDir(File d) {

		}

		@Override
		public void removePluginFile(Plugin algorithm, String filename) throws IOException {

		}

		@Override
		public boolean isPathForUser(String path, String userName) {
			return tempResultsFolder.getAbsolutePath().contains(path);
		}

		@Override
		public File getUserTempDir(String userName) throws IOException {
			return tempResultsFolder;
		}

		@Override
		public File getUserDir(String userName) throws IOException {
			return tempResultsFolder;
		}

		@Override
		public File getTemplateFile(Plugin alg, String path) throws IOException {
			return new File(pluginDirectory, alg.getScriptTemplateFilename());
		}

		@Override
		public File getSubmittedJobsDir(String userName) throws IOException {
			return tempResultsFolder;
		}

		@Override
		public File getPluginInstanceFolder(File resultsFolder, String algorithmName, Integer index)
				throws IOException {
			return tempResultsFolder;
		}

		@Override
		public File getPluginFolder(Plugin algorithm) throws IOException {
			return pluginDirectory;
		}

		@Override
		public File getNewResultsFolder(String forUser, long jobId) throws IOException {
			return tempResultsFolder;
		}

		@Override
		public File getLegacyPluginsFolder() {

			return null;
		}

		@Override
		public File getKdcomputePluginsDir() {
			return tempResultsFolder.getParentFile();
		}

		@Override
		public File getPluginsDir() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public File getDropinPluginsDir() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public File getPluginFolder(PluginNameVersion pluginNameVersion) {
			return pluginDirectory;
		}

		@Override
		public File getUserStorage() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public File getPluginFolderAsStaged(PluginNameVersion pluginNameVersion) throws IOException {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
