/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.example;

import java.util.ArrayList;
import java.util.List;

import com.diversityarrays.kdcompute.db.DataSetBinding;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.Plugin;

/**
 * @author andrew
 * @deprecated Replaced by Example
 * 
 */
public class OldExample {
	String name;
	boolean unitTest;
	String description;
	List<DataSetBinding> inputBinding;
	List<KnobBinding> binding;
	List<ExpOut> expOut;

	public OldExample(String name, boolean unitTest, String description, List<DataSetBinding> inputBinding,
			List<KnobBinding> binding, List<ExpOut> expOut) {
		super();
		this.name = name;
		this.unitTest = unitTest;
		this.description = description;
		this.inputBinding = inputBinding;
		this.binding = binding;
		this.expOut = expOut;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isUnitTest() {
		return unitTest;
	}

	public void setUnitTest(boolean unitTest) {
		this.unitTest = unitTest;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DataSetBinding> getInputBinding() {
		return inputBinding;
	}

	public void setInputBinding(List<DataSetBinding> inputBinding) {
		this.inputBinding = inputBinding;
	}

	public List<KnobBinding> getBinding() {
		return binding;
	}

	public void setBinding(List<KnobBinding> binding) {
		this.binding = binding;
	}

	public List<ExpOut> getExpOut() {
		return expOut;
	}

	public void setExpOut(List<ExpOut> expOut) {
		this.expOut = expOut;
	}

	public Example toExample(Plugin plugin) {
		Example example = new Example(plugin);
		example.setBinding(this.getBinding());
		example.setDescription(this.getDescription());
		List<ExpIdentical> expIdentical = new ArrayList<>();
		if (this.getExpOut() != null)
			for (ExpOut expOut : this.getExpOut()) {
				expIdentical.add(new ExpIdentical(expOut.getActualFilepath(), expOut.getExpectedFilepath()));
			}
		example.setExpIdentical(expIdentical);
		example.setName(this.getName());
		example.setUnitTest(this.isUnitTest());
		return example;
	}

}
