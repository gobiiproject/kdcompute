/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.example;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.util.Either;

import net.pearcan.util.CancelChecker;

public class ExampleRunner {

	private final RunExample runExample;
	private final String userName;

	private Thread thread;
	private boolean running;
	private boolean hasBeenRun;

	public ExampleRunner(RunExample runExample, String runAsUser) {
		this.runExample = runExample;
		userName = runAsUser;
	}

	public void stopRunning() {
		running = false;
		if (thread != null) {
			thread.interrupt();
		}
	}

	/**
	 * Return true if job proceeded to completion, false if it was cancelled.
	 * 
	 * @param onComplete
	 * @param cancelChecker
	 * @return boolean
	 */
	public boolean startRunningAndWait(Consumer<Either<IOException, Pair<File, CompleteState>>> onComplete,
			CancelChecker cancelChecker, File exampleRoot) {
		if (hasBeenRun) {
			throw new IllegalStateException("Can't run more than once");
		}
		thread = new Thread(() -> runTheExample(onComplete, null, exampleRoot));
		thread.setDaemon(true);
		running = true;
		thread.start();

		while (running) {
			try {
				Thread.sleep(500);
				if (cancelChecker.isCancelRequested()) {
					running = false;
					break;
				}
				if (!thread.isAlive()) {
					break;
				}
			} catch (InterruptedException ignore) {
			}
		}
		return running;
	}

	public void startRunningAndReturn(Consumer<Either<IOException, Pair<File, CompleteState>>> onComplete,
			Consumer<AnalysisJob> onAnalysisJobCreated, File exampleRoot) {
		if (hasBeenRun) {
			throw new IllegalStateException("Can't run more than once");
		}
		thread = new Thread(() -> runTheExample(onComplete, onAnalysisJobCreated, exampleRoot));
		thread.setDaemon(true);
		running = true;
		thread.start();
	}

	/**
	 *
	 * @param onComplete
	 * @param onAnalysisJobCreated
	 */
	private void runTheExample(Consumer<Either<IOException, Pair<File, CompleteState>>> onComplete,
			Consumer<AnalysisJob> onAnalysisJobCreated, File exampleRoot) {

		try {
			File resultsFolder = KDCFileUtils.createTempDirectory(exampleRoot);
			CompleteState completeState;
			if (runExample.execute(userName, resultsFolder, onAnalysisJobCreated)) {
				completeState = CompleteState.SUCCESS;
			} else {
				completeState = CompleteState.FAILED;
			}

			if (!running) {
				completeState = CompleteState.STOPPED;
			}
			onComplete.accept(Either.right(new Pair<>(resultsFolder, completeState)));
		} catch (IOException e) {
			onComplete.accept(Either.left(e));
		}
	}

	static public enum CompleteState {
		SUCCESS, FAILED, STOPPED
	}
}
