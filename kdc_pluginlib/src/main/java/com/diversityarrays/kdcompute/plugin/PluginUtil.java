/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.google.common.io.Files;

public class PluginUtil {

	public static final String PLUGIN_JSON = "plugin.json";

	private PluginUtil() {
	} // No instances

	static public final boolean MOVE = true;
	static public final boolean COPY = false;

	static public Plugin moveOrCopyPluginFromDropin(boolean move, String dropinFolderName, FileStorage fileStorage)
			throws IOException {
		return moveOrCopyPluginFromDropin(move, dropinFolderName, fileStorage, false);
	}

	static public Plugin moveOrCopyPluginFromDropin(boolean move, String dropinFolderName, FileStorage fileStorage,
			boolean okToOverwrite) throws IOException {
		File srcDir = new File(fileStorage.getDropinPluginsDir(), dropinFolderName);
		return moveOrCopyPluginFrom(move, srcDir, fileStorage, okToOverwrite);
	}

	static public Plugin moveOrCopyPluginFrom(boolean move, File srcDir, FileStorage fileStorage, boolean okToOverwrite)
			throws IOException {
		File spec = new File(srcDir, PLUGIN_JSON);
		Plugin plugin = Plugin.createPluginFromFile(spec);
		File destDir = new File(fileStorage.getPluginsDir(), PluginUtil.getPluginNameWithVersion(plugin));
		if (destDir.exists() && !okToOverwrite) {
			throw new IOException("Plugin " + destDir.getName() + " already exists");
		}
		String srcDirPath = srcDir.getPath();
		String destDirPath = destDir.getPath();
		if (!srcDirPath.equals(destDirPath)) {
			if (move) {
				FileUtils.moveDirectory(srcDir, destDir);
			} else {
				FileUtils.copyDirectory(srcDir, destDir);
			}
		}
		return plugin;
	}

	static public String getPluginNameWithVersion(Plugin plugin) {
		return plugin.getPluginName() + "-v" + plugin.getVersion();
	}

	static public String getPluginNameWithVersion(String pluginName, Integer versionCode) {
		return pluginName + "-v" + Integer.toString(versionCode);
	}

	public static void moveOrCopyFromDropin(boolean move, String name, FileStorage fileStorage) throws IOException {

		File src = new File(fileStorage.getDropinPluginsDir(), name);

		PluginFiles pluginFiles = PluginFiles.readPluginFiles(src);

		File dest = new File(fileStorage.getPluginsDir(), pluginFiles.getPluginNameWithVersion());
		if (move) {
			Files.move(src, dest);
		} else {
			Files.copy(src, dest);
		}
	}

}
