/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import com.diversityarrays.kdcompute.Pair;

public class OutputCapture implements OutputRedirect {

	ByteArrayOutputStream osOut = new ByteArrayOutputStream();
	PrintStream psOut = new PrintStream(osOut);
	ByteArrayOutputStream osErr = new ByteArrayOutputStream();
	PrintStream psErr = new PrintStream(osErr);

	@Override
	public void appendStdout(String msg) {
		psOut.println(msg);
	}

	@Override
	public void appendStderr(String msg) {
		psOut.println(msg);
	}

	public PrintStream getPsOut() {
		return psOut;
	}

	public PrintStream getPsErr() {
		return psErr;
	}

	@Override
	public void append(String msg, Color color) {
		appendStdout(msg);
	}

	@Override
	public void append(String msg) {
		appendStdout(msg);
	}

	public String getStdout() {
		return osOut.toString();
	}

	public String getStderr() {
		return osErr.toString();
	}

	@Override
	public void append(List<Pair<String, Color>> msgsWithColors) {
		for (Pair<String, Color> msg : msgsWithColors) {
			append(msg.getFirst());
		}
	}
}
