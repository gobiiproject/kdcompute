/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.diversityarrays.kdcompute.dependencies.DepedenciesRetriever;
import com.diversityarrays.kdcompute.help.HelpRetriever;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.readme.RetrieveReadme;

public abstract class FilesBase implements RetrieveReadme, HelpRetriever, DepedenciesRetriever {

	public static final String DEPENDENCIES = "dependencies"; //$NON-NLS-1$
	public static final String RESOURCES = "resources"; //$NON-NLS-1$
	public static final String DEPENDENCIES_SUFFIX = ".yml"; //$NON-NLS-1$
	public static final String PLUGIN_HELP = "help"; //$NON-NLS-1$
	protected static final Charset CHARSET = StandardCharsets.UTF_8;

	private File rootDir;
	private String label;

	public FilesBase(File rootDir) {
		super();
		this.rootDir = rootDir;
	}

	public String getLabel() {
		return label;
	}

	protected void setLabel(String label) {
		this.label = label;
	}

	@Override
	public File getHelpDir() {
		return new File(rootDir, PLUGIN_HELP);
	}

	@Override
	public File getDependenciesRoot() {
		return new File(getRootDir(), DEPENDENCIES);
	}

	public static File makeDependenciesDir(File pluginDir) {
		return new File(pluginDir, DEPENDENCIES);
	}

	public static File makeScriptFile(File pluginDir, String selectedDep) {

		String child = selectedDep;

		if (!child.endsWith(DEPENDENCIES_SUFFIX)) {
			child += DEPENDENCIES_SUFFIX;
		}

		return new File(makeDependenciesDir(pluginDir), child);
	}

	@Override
	public File getReadmeFile() {
		return new File(rootDir, RetrieveReadme.README);
	}

	@Override
	public String getAnsibleScriptTitle() {
		return label;
	}

	/**
	 * Provides the location where help resources are stored in a plugin.
	 */
	@Override
	public File getHelpResourcesFolder() {
		return new File(getHelpDir(), RESOURCES);
	}

	@Override
	public boolean makeHelpResourcesFolder() {
		// This is the new standard location
		File folder = new File(getHelpDir(), RESOURCES);
		return folder.mkdirs();
	}

	public List<File> getListOfDepedencies() {
		File[] files = getDependenciesRoot().listFiles();
		List<File> deps = new ArrayList<>();
		if (files != null)
			for (File file : files) {
				if (file.isFile()) {
					if (file.getName().endsWith(PluginFiles.DEPENDENCIES_SUFFIX)) {
						deps.add(file);
					}
				}
			}
		return deps;
	}

	// For debugging
	@Override
	public String toString() {
		return FilesBase.class.getSimpleName() + "[" + rootDir.getPath() + "]";
	}

	public File getRootDir() {
		return rootDir;
	}

	protected boolean createStructure() {
		return getDependenciesRoot().mkdirs() && getHelpDir().mkdirs() && makeHelpResourcesFolder();
	}

}
