/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files.single;

import java.util.List;
import java.util.Map;

public class ExamplesListAndKnobBindings {
	List<PluginExample> examples;
	List<Map<String,String>> bindings;
	List<String> failedExamples;
	public ExamplesListAndKnobBindings(List<PluginExample> examples, List<Map<String, String>> bindings,
			List<String> failedExamples) {
		super();
		this.examples = examples;
		this.bindings = bindings;
		this.failedExamples = failedExamples;
	}
	public List<PluginExample> getExamples() {
		return examples;
	}
	public List<Map<String, String>> getBindings() {
		return bindings;
	}
	public List<String> getFailedExamples() {
		return failedExamples;
	}
	
}
