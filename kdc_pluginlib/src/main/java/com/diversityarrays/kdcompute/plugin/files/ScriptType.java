/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files;

import java.io.InputStream;

@SuppressWarnings("nls")
public enum ScriptType {
	GROOVY("groovy", "groovy.template"),
	BASH("sh", "bash.template");
	
	public final String ext;
	/**
	 * This is the name of the resource in main/src/resource/...
	 */
	private final String templateResourceName;

	private ScriptType(String ext, String trn) {
		this.ext = ext;
		this.templateResourceName = trn;
	}
	
	public InputStream getTemplateResource() {
	    return ScriptType.class.getResourceAsStream(templateResourceName);
	}

	public String getExt() {
		return ext;
	}

	public String getTemplateResourceName() {
		return templateResourceName;
	}
	
}
