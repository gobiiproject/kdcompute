/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files.single;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.diversityarrays.kdcompute.PluginDependencyScript;
import com.diversityarrays.kdcompute.customhtml.HtmlForm;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.plugin.Contact;
import com.diversityarrays.kdcompute.plugin.Defaults;
import com.diversityarrays.kdcompute.plugin.PluginUtil;
import com.diversityarrays.kdcompute.plugin.example.Example;
import com.diversityarrays.kdcompute.plugin.example.OldExample;
import com.diversityarrays.kdcompute.plugin.files.FilesBase;
import com.diversityarrays.kdcompute.plugin.files.ScriptType;
import com.diversityarrays.kdcompute.plugin.files.collection.PluginCollectionFiles;
import com.diversityarrays.kdcompute.script.Script;
import com.diversityarrays.kdcompute.script.ScriptBash;
import com.diversityarrays.kdcompute.script.ScriptGroovy;
import com.diversityarrays.kdcompute.script.SystemVariable;
import com.diversityarrays.kdcompute.util.Either;
import com.diversityarrays.kdcompute.util.ZipUtils;
import com.google.common.io.Files;
import com.google.gson.Gson;

import thatkow.ansible.AnsiblePlaybookNotFoundException;
import util.JsonUtil;

@SuppressWarnings("deprecation")
public class PluginFiles extends FilesBase {
	private static final String CONTACT_FILENAME = "contact.json";
	private static final String RETURN = "\n"; //$NON-NLS-1$

	private static final String DEFAULTS_FILENAME = "defaults.json"; //$NON-NLS-1$
	private static final String PLUGIN_EXAMPLES = "examples"; //$NON-NLS-1$
	public static final String EXAMPLES_SUFFIX = ".json"; //$NON-NLS-1$

	private static final String PLUGIN_DIR = "plugins"; //$NON-NLS-1$
	private static final String PLUGIN_DATA = "data"; //$NON-NLS-1$
	public static final String CUSTOM_HTML_FORM = "form.html"; //$NON-NLS-1$

	public static final FileFilter SPECIFICATION_FILE_FILTER = new FileFilter() {
		@Override
		public String getDescription() {
			return "Plugin Specification File (" + PluginUtil.PLUGIN_JSON + ")"; //$NON-NLS-1$//$NON-NLS-2$
		}

		@Override
		public boolean accept(File f) {
			return f.isDirectory() || f.getName().equals(PluginUtil.PLUGIN_JSON);
		}
	};

	public static final java.io.FileFilter PLUGIN_DIRECTORY_FILTER = new java.io.FileFilter() {
		@Override
		public boolean accept(File f) {
			return PluginFiles.isPluginDirectory(f);
		}
	};

	public static boolean isPluginDirectory(File d) {
		try {
			PluginFiles.readPluginFiles(d);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public static List<File> listPlugins(File dir) {
		List<File> result = Arrays.asList(dir.listFiles(PluginFiles.PLUGIN_DIRECTORY_FILTER)).stream()
				.collect(Collectors.toList());
		return result;
	}

	private Plugin plugin;
	private Contact contact;
	private Defaults defaults;

	public Plugin getPlugin() {
		return plugin;
	}

	public static Either<Exception, PluginFiles> createNew(File rootDir, ScriptType scriptType,
			List<SystemVariable> additionalSystemKnobs) {
		Plugin plugin = new Plugin();
		Contact contact = new Contact("you@example.com");
		Defaults defaults = new Defaults();
		plugin.setAlgorithmName(rootDir.getName() + "_algorithm");
		plugin.setPluginName(rootDir.getName());
		plugin.setVersionString("Pilot");
		plugin.setAuthor("");
		plugin.setDescription("");
		plugin.setLegacy(false);
		ArrayList<Knob> knobs = new ArrayList<>();
		Knob knobA = new Knob("arg1", "A detailed description of argument 1", KnobDataType.TEXT, "");
		knobA.setDefaultValue("");
		knobs.add(knobA);
		Knob knobB = new Knob("arg2", "A detailed description of argument 2", KnobDataType.TEXT, "");
		knobB.setDefaultValue("");
		knobs.add(knobB);
		plugin.setKnobs(knobs);

		plugin.setScriptTemplateFilename("script." + scriptType.getExt() + ".tmpl");

		PluginFiles pluginFiles = null;
		try {
			pluginFiles = new PluginFiles(rootDir, plugin, contact, defaults);
			pluginFiles.createStrcture();
			InputStream is = scriptType.getTemplateResource();
			if (is == null) {
				throw new IOException("Resource " + scriptType.getTemplateResourceName() + " does not exist");
			}
			if (additionalSystemKnobs != null && !additionalSystemKnobs.isEmpty()) {
				String isAsString = IOUtils.toString(is, Charset.defaultCharset());
				Script script = null;
				if (scriptType.equals(ScriptType.BASH)) {
					script = new ScriptBash(isAsString);
				} else if (scriptType.equals(ScriptType.GROOVY)) {
					script = new ScriptGroovy(isAsString);
				} else {
					throw new Exception("Unsupported ScriptType: " + scriptType);
				}
				List<String> l = new ArrayList<>();
				l.add(script.getKdcomputeKnobsText());
				l.addAll(additionalSystemKnobs.stream().map(e -> e.isSurroundWithQuotes()
						? e.getKey() + "='<%= " + e.getKey() + " %>'" : e.getKey() + "=<%= " + e.getKey() + " %>")
						.collect(Collectors.toList()));
				script.setKdcomputeKnobsText(String.join("\n", l));
				FileUtils.writeStringToFile(PluginFiles.getScriptFile(rootDir, plugin), script.toString(),
						Charset.defaultCharset());
			} else {
				FileUtils.copyInputStreamToFile(is, PluginFiles.getScriptFile(rootDir, plugin));
			}
		} catch (Exception e) {
			return Either.left(e);
		}

		for (String s : new String[] { DEPENDENCIES, PLUGIN_EXAMPLES, PLUGIN_DATA }) {
			File file = new File(rootDir, s);
			file.mkdir();

			if (!file.exists() || !file.isDirectory()) {
				return Either.left(new IOException("Unable to create directory " + file.getAbsolutePath()));
			}
		}

		String text = String.join(RETURN, "# KDCompute Plugin for " + plugin.getPluginName(), "", "## About", "",
				"*This is a KDCompute Plugin wrapper to your algorithm. It can be directly deployed onto a KDCompute Server via the Admin tab*",
				"", "## Installation", "",
				"*Installation of basic plugins and their underlying algorithms can be managed via the ansible script (See Depedencies in KDCompute Plugin Studio)*",
				"", "*Additional installation requirements should be documented here*");

		File outfile = new File(rootDir, "README.md");
		try {
			FileUtils.writeStringToFile(outfile, text, CHARSET);
		} catch (IOException e) {
			return Either.left(e);
		}
		try {
			pluginFiles.writePlugin();
		} catch (IOException e) {
			return Either.left(e);
		}
		return Either.right(pluginFiles);
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public PluginFiles(File rootDir, Plugin plugin, Contact contact, Defaults defaults) {
		super(rootDir);
		this.plugin = plugin;
		this.contact = contact;
		this.defaults = defaults;
		super.setLabel(this.plugin.getPluginName());
	}

	public void writePlugin() throws IOException {
		this.plugin.setWhenLastUpdated(new Date());
		FileUtils.writeStringToFile(getSpecificationFile(), JsonUtil.makeJsonPretty(plugin), CHARSET);
		FileUtils.writeStringToFile(getContactsFile(), JsonUtil.makeJsonPretty(contact), CHARSET);
		FileUtils.writeStringToFile(getDefaultsFile(), JsonUtil.makeJsonPretty(defaults), CHARSET);
		this.plugin = Plugin.createPluginFromFile(getSpecificationFile());
		this.contact = Contact.fromFile(getContactsFile());
		this.defaults = Defaults.fromFile(getDefaultsFile());
	}

	private File getSpecificationFile() {
		return PluginFiles.getSpecificationFile(getRootDir());
	}

	public static File getSpecificationFile(File rootDir) {
		return new File(rootDir, PluginUtil.PLUGIN_JSON);
	}

	public static boolean isPluginDir(File dir) {
		return PluginFiles.getSpecificationFile(dir).exists();
	}

	public static PluginFiles readPluginFiles(File rootDir) throws IOException {

		File pluginFile = PluginFiles.getSpecificationFile(rootDir);
		if (!pluginFile.exists()) {
			throw new FileNotFoundException(
					"Could not find expected file " + pluginFile.getPath() + " does not exist!");
		}

		Plugin plugin = Plugin.createPluginFromFile(pluginFile);
		Contact contact;
		try {
			contact = Contact.fromFile(PluginFiles.getContactsFile(rootDir));
		} catch (Exception e) {
			contact = new Contact("you@example.com");
		}

		Defaults defaults;
		try {
			defaults = Defaults.fromFile(PluginFiles.getDefaultsFile(rootDir));
		} catch (Exception e) {
			defaults = new Defaults();
		}

		PluginFiles pluginFiles = new PluginFiles(rootDir, plugin, contact, defaults);
		pluginFiles.createStrcture();
		return pluginFiles;
	}

	private File getDefaultsFile() {
		return getDefaultsFile(getRootDir());
	}

	public static File getDefaultsFile(File root) {
		return new File(root, DEFAULTS_FILENAME);
	}

	private File getContactsFile() {
		File rootDir = getRootDir();
		return getContactsFile(rootDir);
	}

	public static File getContactsFile(File rootDir) {
		return new File(rootDir, CONTACT_FILENAME);
	}

	public File getScriptFile() {
		File rootDir = getRootDir();
		return getScriptFile(rootDir, plugin);
	}

	public static File getScriptFile(File rootDir, Plugin plugin) {
		return new File(rootDir, plugin.getScriptTemplateFilename());
	}

	public Contact getContact() {
		return contact;
	}

	public Defaults getDefaults() {
		return defaults;
	}

	public File getExamplesRoot() {
		return new File(getRootDir(), PLUGIN_EXAMPLES);
	}

	public File getDataRoot() {
		return new File(getRootDir(), PLUGIN_DATA);
	}

	public static File makePluginDir(File dir) {
		return new File(dir, PLUGIN_DIR);
	}

	public static File makeExamplesDir(File pluginDir) {
		return new File(pluginDir, PLUGIN_EXAMPLES);
	}

	@Override
	public void setDefaultHelp(String name) throws IOException {
		plugin.setHtmlHelp(name);
		writePlugin();
	}

	public String readScript() throws IOException {
		return FileUtils.readFileToString(getScriptFile(), CHARSET);
	}

	private boolean createStrcture() {
		return super.createStructure() && getExamplesRoot().mkdirs() && getDataRoot().mkdirs();
	}

	public List<Example> getExamples() throws IOException {
		List<Example> examples = new ArrayList<>();
		for (File f : Arrays.asList(getExamplesRoot().listFiles())) {
			if (f.getName().endsWith(PluginFiles.EXAMPLES_SUFFIX)) {
				String exampleStr = FileUtils.readFileToString(f, StandardCharsets.UTF_8);
				Gson gson = new Gson();
				Example example = gson.fromJson(exampleStr, Example.class);
				if (example.getExpExists() == null || example.getExpIdentical() == null
						|| example.getExpPercentageIdentical() == null) {
					// Might be depreciated form
					OldExample oldExample = gson.fromJson(exampleStr, OldExample.class);
					example = oldExample.toExample(getPlugin());
				}
				if (example.getExpSize() == null) {
					// Legacy, just add it
					example.setExpSize(new ArrayList<>());
				}
				examples.add(example);
			}
		}
		return examples;
	}

	public String getPluginNameWithVersion() {
		return getPlugin().getPluginName() + "-v" + getPlugin().getVersion().toString();
	}

	public File getCustomHtmlForm() {
		String htmlFormTemplateFilename = plugin.getHtmlFormTemplateFilename();
		if (htmlFormTemplateFilename == null || htmlFormTemplateFilename.isEmpty()) {
			return null;
		} else {
			return new File(getRootDir(), htmlFormTemplateFilename);
		}
	}

	public File createCustomHtmlForm() throws Exception {
		File customHtmlForm = getCustomHtmlForm();
		if (customHtmlForm == null || !customHtmlForm.exists()) {
			plugin.setHtmlFormTemplateFilename(CUSTOM_HTML_FORM);
			File f = getCustomHtmlForm();
			new HtmlForm(plugin).write(f);
			return f;
		} else {
			return customHtmlForm;
		}
	}

	public ExamplesListAndKnobBindings getExamplesListAndKnobBindings() throws IOException {
		List<PluginExample> examples = new ArrayList<>();
		List<Map<String, String>> examplesKnobbindings = new ArrayList<>();

		File[] listOfExampleFiles = getExamplesRoot().listFiles();
		List<String> failedToLoad = new ArrayList<>();
		for (File e : listOfExampleFiles) {
			try {
				Example createExampleFromFile = Example.createExampleFromFile(e);
				if (createExampleFromFile.isUnitTest()) {
					Map<String, String> knobValues = new HashMap<>();
					for (KnobBinding binding : createExampleFromFile.getBindings()) {
						knobValues.put(binding.getKnob().getKnobName(), binding.getKnobValue());
					}
					examplesKnobbindings.add(knobValues);

					examples.add(
							new PluginExample(createExampleFromFile.getName(), createExampleFromFile.getDescription()));
				}
			} catch (Exception ex) {
				failedToLoad.add(e.getName());
			}
		}
		return new ExamplesListAndKnobBindings(examples, examplesKnobbindings, failedToLoad);
	}

	public List<PluginDependencyScript> getAvailableDependencyScripts() {
		List<File> asList = Arrays.asList(this.getDependenciesRoot().listFiles());
		return asList.stream().filter(e -> e.getPath().endsWith(DEPENDENCIES_SUFFIX)).map(e -> {
			try {
				return new PluginDependencyScript(e);
			} catch (IOException | AnsiblePlaybookNotFoundException | InterruptedException e1) {
				return null;
			}
		}).filter(e -> e != null).collect(Collectors.toList());
	}

	/**
	 * Replaces any local copy
	 * 
	 * @param file
	 * @param removeZipIfUncompressed
	 * @return
	 * @throws IOException
	 */
	public static File getUnzippedForm(File file, boolean removeZipIfUncompressed) throws IOException {
		if (file.getPath().endsWith(".zip")) {
			File fu = new File(file.getPath().replaceAll("\\.zip$", ""));
			ZipUtils.unzip(file, fu);
			if (removeZipIfUncompressed) {
				file.delete();
			}
			try {
				List<File> containingFiles = Arrays.asList(fu.listFiles());
				Optional<File> findAny = containingFiles.stream().filter(e -> {
					String filename = e.getName();
					return fu.getName().contains(filename);
				}).findAny();
				if (findAny.isPresent()) {
					File fo = findAny.get();
					File[] listFiles = fo.listFiles();
					for (File f : listFiles) {
						if (f.isDirectory()) {
							FileUtils.moveDirectoryToDirectory(f, fu, false);
						} else {
							FileUtils.moveFileToDirectory(f, fu, false);
						}
					}
				}
			} catch (IOException e) {
				fu.delete();
				throw e;
			}

			String pluginName = null;

			try {
				PluginCollectionFiles createFromDir = PluginCollectionFiles.createFromDir(fu);
				pluginName = createFromDir.getPluginCollection().getPluginGroup();
			} catch (IOException e) {
				PluginFiles readPluginFiles = PluginFiles.readPluginFiles(fu);
				pluginName = readPluginFiles.getPlugin().getPluginName();
			}

			File to = new File(fu.getParentFile(), pluginName);
			if(!to.equals(fu)) {
				FileUtils.deleteDirectory(to);
				Files.move(fu, to);
			}
			return to;
		}
		return file;
	}

}
