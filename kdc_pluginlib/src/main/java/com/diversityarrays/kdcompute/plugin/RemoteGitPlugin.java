/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;

public class RemoteGitPlugin implements GitPlugin {
	private URL remoteUrl;
	private OutputRedirect outputRedirect;
	private UsernamePasswordCredentialsProvider credentialsProvider;
	private boolean cloned = false;

	private File tempDir;

	private CloneCommand tmpRepo;

	public RemoteGitPlugin() {
		super();
	}

	public URL getRemoteUrl() {
		return remoteUrl;
	}

	public RemoteGitPlugin(URL remoteUrl, OutputRedirect outputRedirect) {
		this(remoteUrl, outputRedirect, null);
	}

	public RemoteGitPlugin(URL remoteUrl, OutputRedirect outputRedirect,
			UsernamePasswordCredentialsProvider credentialsProvider) {
		super();
		this.remoteUrl = remoteUrl;
		this.outputRedirect = outputRedirect;
		this.credentialsProvider = credentialsProvider;
	}

	public static void pull(File localRepo, UsernamePasswordCredentialsProvider credentialsProvider)
			throws IOException {
		Git.open(localRepo).pull().setCredentialsProvider(credentialsProvider);
	}

	/**
	 * @return Destination of clone
	 * @throws IOException
	 * @throws InvalidRemoteException
	 * @throws TransportException
	 * @throws GitAPIException
	 */
	public File gitClone(String branch)
			throws IOException, InvalidRemoteException, TransportException, GitAPIException {
		tempDir = KDCFileUtils.getExampleTempDir();
		tmpRepo = Git.cloneRepository();
		if (credentialsProvider != null) {
			if (branch == null || branch.isEmpty()) {
				tmpRepo.setCredentialsProvider(credentialsProvider).setURI(remoteUrl.toString()).setDirectory(tempDir)
						.call();
			} else {
				tmpRepo.setCredentialsProvider(credentialsProvider).setURI(remoteUrl.toString()).setDirectory(tempDir)
						.call().checkout().setName(branch).call();
			}
		} else {
			if (branch == null || branch.isEmpty()) {
				tmpRepo.setURI(remoteUrl.toString()).setDirectory(tempDir).call();
			} else {
				tmpRepo.setURI(remoteUrl.toString()).setDirectory(tempDir).call().checkout().setName(branch).call();
			}
		}

		cloned = true;
		return tempDir;
	}

	/**
	 * Clone into final destination
	 * 
	 * @return Dest folder of plugin
	 * @param Destination
	 *            directory
	 * @throws Exception
	 */
	@Override
	public File migrate(File pluginsDir) throws Exception {
		if (!cloned) {
			throw new Exception("Must clone plugin first");
		}
		PluginFiles pluginFiles = PluginFiles.readPluginFiles(this.tempDir);
		File destFolderVersioned = new File(pluginsDir, pluginFiles.getPluginNameWithVersion());
		FileUtils.moveDirectory(this.tempDir, destFolderVersioned);

		outputRedirect.appendStdout("Cloned to " + destFolderVersioned.getAbsolutePath());
		return destFolderVersioned;
	}

}
