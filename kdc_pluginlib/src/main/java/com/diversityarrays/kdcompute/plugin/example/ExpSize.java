/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.example;

public class ExpSize extends ActualExp {
	private Double tolleranceDiff;

	public ExpSize(String act, String exp, Double tolleranceDiff) {
		super(act, exp);
		this.tolleranceDiff = tolleranceDiff;
	}

	public ExpSize() {
		super("", "");
		tolleranceDiff = 5.0;
	}

	public Double getTolleranceDiff() {
		return tolleranceDiff;
	}

	public void setTolleranceDiff(Double tolleranceDiff) {
		this.tolleranceDiff = tolleranceDiff;
	}

	@Override
	public String toString() {
		return "tolleranceDiff=" + Double.toString(tolleranceDiff) + ", " + super.toString();
	}

}
