/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.example;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.db.DataSet;
import com.diversityarrays.kdcompute.db.DataSetBinding;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.google.gson.Gson;

public class Example {

	String name;
	boolean unitTest;
	String description;
	List<DataSetBinding> inputBinding;
	List<KnobBinding> binding;
	List<ExpIdentical> expIdentical = new ArrayList<>();
	List<ExpSize> expSize = new ArrayList<>();
	List<ExpPercentageIdentical> expPercentageIdentical = new ArrayList<>();
	List<ExpExists> expExists = new ArrayList<>();


	public static Example createExampleFromFile(File file) throws IOException {
		if(!file.exists()) {
			throw new FileNotFoundException("File "+file.getAbsolutePath()+" does not exist!");
		}
		String readFileToString = FileUtils.readFileToString(file,"UTF-8");
		return new Gson().fromJson(readFileToString, Example.class);
	}


	public Example(Plugin plugin) {
		super();
		this.name = "";
		this.unitTest = false;
		this.description = "";

		this.binding = new ArrayList<>();
		this.inputBinding = new ArrayList<>();
		for(Knob k : plugin.getKnobs()) {
			if(k.getKnobDataType().equals(KnobDataType.FILE_UPLOAD)){
//				this.inputBinding.add(new DataSetBinding(new DataSet(k.getBriefString(),k.getDescription())));
				this.binding.add(new KnobBinding(k, k.getDefaultValue()));
			}else{
				this.binding.add(new KnobBinding(k, k.getDefaultValue()));
			}
		}

		for(DataSet binding : plugin.getInputDataSets()) {
			this.inputBinding.add(new DataSetBinding(binding));
		}

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<ExpIdentical> getExpIdentical() {
		return expIdentical;
	}


	public void setExpIdentical(List<ExpIdentical> expIdentical) {
		this.expIdentical = expIdentical;
	}

	public List<ExpPercentageIdentical> getExpPercentageIdentical() {
		return expPercentageIdentical;
	}


	public void setExpPercentageIdentical(List<ExpPercentageIdentical> expPercentageIdentical) {
		this.expPercentageIdentical = expPercentageIdentical;
	}

	public List<ExpExists> getExpExists() {
		return expExists;
	}

	public List<ExpSize> getExpSize() {
		return expSize;
	}


	public void setExpSize(List<ExpSize> expSize) {
		this.expSize = expSize;
	}


	public void setExpExists(List<ExpExists> expExists) {
		this.expExists = expExists;
	}


	public void setBinding(List<KnobBinding> binding) {
		this.binding = binding;
	}

	public boolean isUnitTest() {
		return unitTest;
	}

	public void setUnitTest(boolean unitTest) {
		this.unitTest = unitTest;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<KnobBinding> getBindings() {
		return binding;
	}

	public List<DataSetBinding> getInputDatasets() {
		return inputBinding;
	}




}
