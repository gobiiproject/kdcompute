/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.diversityarrays.kdcompute.db.PluginVersion;

public class PluginCollection {
	public static final String PLUGIN_COLLECTION = "plugin_collection";

	private String pluginGroup;
	private Date lastUpdated;
	private Long version;
	private String author;
	private String contact;
	private List<String> plugins;

	public PluginCollection() {

		this.plugins = new ArrayList<>();
		this.pluginGroup = "Ungrouped";
		this.lastUpdated = new Date();
		this.version = 0L;
		this.author = "Not provided";
		this.contact = "nobody@example.com";
	}

	public List<String> getPlugins() {
		return plugins;
	}

	public void setPlugins(List<String> plugins) {
		this.plugins = plugins;
	}

	public String getPluginGroup() {
		return pluginGroup;
	}

	public void setPluginGroup(String pluginGroup) {
		this.pluginGroup = pluginGroup;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public PluginVersion getVersion() {
		int v = (int) version.longValue();
		return new PluginVersion(0, v, 0);
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public void setDefaultHelp(String name) {
	}

	public String getPluginCollectionNameVersion() {
		return String.join("-v", Arrays.asList(getPluginGroup(), getVersion().toString()));
	}

}
