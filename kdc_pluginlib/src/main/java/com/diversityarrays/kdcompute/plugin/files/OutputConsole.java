/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.plugin.OutputRedirect;

public class OutputConsole extends JPanel{

	private OutputRedirect outputRedirect;

	private final JTextPane jTextPane = new JTextPane();
	private Action clearAction = new AbstractAction("Clear") {
        /**
		 * 
		 */

		@Override
        public void actionPerformed(ActionEvent e) {
            jTextPane.setText("");
        }
	};
    
	
	public OutputRedirect getOutputRedirect() {
		return outputRedirect;
	}

	public OutputConsole() {
	    super(new BorderLayout());

		EmptyBorder eb = new EmptyBorder(new Insets(10, 10, 10, 10));
		jTextPane.setBorder(eb);
		//setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		jTextPane.setMargin(new Insets(5, 5, 5, 5));
		outputRedirect = new OutputRedirect() {
			
			@Override
			public void appendStdout(String msg) {
				append(msg,Color.BLUE.darker());
			}
			
			@Override
			public void appendStderr(String msg) {
				append(msg,Color.RED.darker());
			}
			
			@Override
			public void append(String msg) {
				append(msg,Color.BLACK);
			}
			
			@Override
			public void append(String msg, Color color) {
				StyleContext sc = StyleContext.getDefaultStyleContext();
				AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);

				aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
				aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

				int len = jTextPane.getDocument().getLength();
				jTextPane.setCaretPosition(len);
				jTextPane.setCharacterAttributes(aset, false);
				jTextPane.replaceSelection("\n"+msg);
			}

			@Override
			public void append(List<Pair<String, Color>> msgsWithColors) {
				int len = jTextPane.getDocument().getLength();
				jTextPane.setCaretPosition(len);
				jTextPane.replaceSelection("\n");
				for(Pair<String, Color> e : msgsWithColors){
					StyleContext sc = StyleContext.getDefaultStyleContext();
					AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, e.second);

					aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
					aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

					len = jTextPane.getDocument().getLength();
					jTextPane.setCaretPosition(len);
					jTextPane.setCharacterAttributes(aset, false);
					jTextPane.replaceSelection(e.first);
				}				
			}
		};
		
		JScrollPane scrollPane = new JScrollPane(jTextPane);
		Box buttons =Box.createHorizontalBox();
		buttons.add(Box.createHorizontalGlue());
		buttons.add(new JButton(clearAction));

		this.add(scrollPane, BorderLayout.CENTER);
		this.add(buttons, BorderLayout.SOUTH);
	}
}
