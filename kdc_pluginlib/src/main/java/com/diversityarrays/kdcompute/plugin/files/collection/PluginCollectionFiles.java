/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.plugin.files.collection;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.plugin.files.FilesBase;
import com.diversityarrays.kdcompute.plugin.files.single.PluginFiles;
import com.diversityarrays.kdcompute.util.Either;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import util.JsonUtil;

public class PluginCollectionFiles extends FilesBase {

	private static final String RETURN = "\n"; //$NON-NLS-1$

	private static final String DEPENDENCIES = "dependencies"; //$NON-NLS-1$

	public static final String JSON_SUFFIX = ".json"; //$NON-NLS-1$

	public static final String PLUGINCOLLECTION_JSON = PluginCollection.PLUGIN_COLLECTION + JSON_SUFFIX;

	public static final String DEPENDENCIES_SUFFIX = ".yml"; //$NON-NLS-1$

	private static final String HELP = "help"; //$NON-NLS-1$

	private static final Charset CHARSET = StandardCharsets.UTF_8;

	private File pluginCollectionFile;

	private PluginCollection pluginCollection;

	public static Either<Exception, PluginCollectionFiles> createNew(File root, String pluginGroup) {
		PluginCollection pluginCollection = new PluginCollection();
		pluginCollection.setPluginGroup(pluginGroup);
		pluginCollection.setLastUpdated(new Date());

		for (String s : new String[] { DEPENDENCIES, HELP }) {
			File file = new File(root, s);
			if (!file.mkdir()) {
				return Either.left(new IOException("Unable to create directory " + file.getAbsolutePath()));
			}
		}

		String text = String.join(RETURN, "# KDCompute Plugin Group **" + pluginGroup + "**", "", "## About", "",
				"*This is a KDCompute Plugin Group. It can be directly deployed onto a KDCompute Server via the Admin tab*",
				"", "## Installation", "",
				"*Installation of basic plugins and their underlying algorithms can be managed via the ansible script (See Depedencies in KDCompute Plugin Studio)*",
				"", "*Additional installation requirements should be documented here*");

		File outfile = new File(root, README);
		try {
			FileWriter fileWriter = new FileWriter(outfile);
			fileWriter.write(text);
			fileWriter.close();
		} catch (IOException e) {
			return Either.left(e);
		}

		PluginCollectionFiles commonFiles = new PluginCollectionFiles(root, pluginCollection);
		commonFiles.createStrcture();
		try {
			commonFiles.write();
		} catch (IOException e) {
			return Either.left(e);
		}
		return Either.right(commonFiles);
	}

	// For debugging
	@Override
	public String toString() {
		return PluginCollectionFiles.class.getSimpleName() + "[" + getRootDir().getPath() + "]";
	}

	public PluginCollectionFiles(File rootdir) throws JsonSyntaxException, IOException {
		this(rootdir, readPluginCollection(rootdir));
	}

	public PluginCollectionFiles(File rootdir, PluginCollection pluginCollection) {
		super(rootdir);
		this.setLabel(rootdir.getName());
		this.pluginCollection = pluginCollection;
		this.pluginCollectionFile = new File(rootdir, PLUGINCOLLECTION_JSON);
	}

	public void write() throws IOException {
		this.pluginCollection.setLastUpdated(new Date());
		FileWriter fileWriter = new FileWriter(pluginCollectionFile);
		String makeJsonPretty = JsonUtil.makeJsonPretty(pluginCollection);
		fileWriter.write(makeJsonPretty);
		fileWriter.close();
	}

	private static PluginCollection readPluginCollection(File root) throws IOException {
		String readPluginCollectionFromFile = FileUtils.readFileToString(new File(root, PLUGINCOLLECTION_JSON),
				CHARSET);
		return new Gson().fromJson(readPluginCollectionFromFile, PluginCollection.class);
	}

	public PluginCollection getPluginCollection() {
		return pluginCollection;
	}

	public static File makeDependencyScript(File pluginDir, String selectedDep) {
		return new File(makeDependenciesDir(pluginDir), selectedDep + DEPENDENCIES_SUFFIX);
	}

	public static boolean isPluginCollectionDir(File dir) {
		return getCollectionFile(dir).exists();
	}

	private static File getCollectionFile(File dir) {
		return new File(dir, PluginCollectionFiles.PLUGINCOLLECTION_JSON);
	}

	public static PluginCollectionFiles createFromDir(File root) throws IOException {
		File file = getCollectionFile(root);
		if (!file.exists()) {
			throw new IOException("Could not find plugin collection file " + file.getPath());
		}
		String contents = new String(Files.readAllBytes(Paths.get(file.getPath())));
		PluginCollection pluginCollection = new Gson().fromJson(contents, PluginCollection.class);

		PluginCollectionFiles pluginCollectionFiles = new PluginCollectionFiles(root, pluginCollection);
		pluginCollectionFiles.createStrcture();
		return pluginCollectionFiles;
	}

	@Override
	public void setDefaultHelp(String name) throws IOException {
		this.pluginCollection.setDefaultHelp(name);
	}

	private boolean createStrcture() {
		return super.createStructure() && getDependenciesRoot().mkdirs() && getHelpDir().mkdirs();
	}

	public static File navigateToCollectionRoot(PluginFiles pluginFiles) throws IOException {
		if (isPluginPartOfPluginCollection(pluginFiles.getRootDir())) {
			String path = new File(pluginFiles.getRootDir(), "..").getCanonicalPath();
			return new File(path);
		} else {
			return null;
		}
	}

	public static boolean isPluginPartOfPluginCollection(File pluginFolder) throws IOException {
		return isPluginCollection(new File(new File(pluginFolder, "..").getCanonicalPath()));
	}

	public static boolean isPluginCollection(File folder) throws IOException {
		File file = new File(folder, PLUGINCOLLECTION_JSON);
		if (file.exists()) {
			String contents;
			contents = new String(Files.readAllBytes(file.toPath()));
			PluginCollection fromJson = new Gson().fromJson(contents, PluginCollection.class);
			if (fromJson != null) {
				return true;
			}
		}
		return false;
	}

	public List<Plugin> getPlugins() throws IOException {
		List<Plugin> plugins = new ArrayList<>();
		for (String pluginName : getPluginCollection().getPlugins()) {
			plugins.add(PluginFiles.readPluginFiles(new File(getRootDir(), pluginName)).getPlugin());
		}
		return plugins;
	}

}
