/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.swingutils;

import java.awt.Component;

import javax.swing.JOptionPane;

import com.diversityarrays.kdcompute.util.MsgBox;

public class SwingUtils {
	public static boolean prompt(Component c, String title, String message, String yes, String no) {
		Object[] options = { yes, no };
		int n = JOptionPane.showOptionDialog(c, message, title, JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
		return n == 0;
	}

	public static boolean confirm(Component c, String message) {
		return confirm(c, "Please Confirm", message);
	}

	public static boolean confirm(Component c, String title, String message) {
		return JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(c, message, title, JOptionPane.YES_NO_OPTION);
	}

	public static void error(Component c, String title, String message) {
		MsgBox.error(c, message, title);
	}

	public static void notify(Component c, String title, String message) {
		MsgBox.info(c, message, title);
	}

	public static String promptText(Component c, String title, String message) {
		return JOptionPane.showInputDialog(c, message, title, JOptionPane.PLAIN_MESSAGE);
	}

	public static String promptTextWithPlaceholder(Component c, String message, String placeholder) {
		return JOptionPane.showInputDialog(c, message, placeholder);
	}

	public static String select(Component c, String title, String message, String[] options) {
		return (String) JOptionPane.showInputDialog(c, message, title, JOptionPane.PLAIN_MESSAGE, null, options,
				options[0]);
	}
}
