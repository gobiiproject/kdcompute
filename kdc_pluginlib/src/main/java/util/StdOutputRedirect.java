/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package util;

import java.awt.Color;
import java.util.List;
import java.util.stream.Collectors;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.plugin.OutputRedirect;

public class StdOutputRedirect implements OutputRedirect{
	@Override
	public void append(String msg) {
		System.out.println(msg);
	}

	@Override
	public void appendStdout(String msg) {
		System.out.println(msg);
	}

	@Override
	public void appendStderr(String msg) {
		System.err.println(msg);
	}

	@Override
	public void append(String msg, Color color) {
		System.out.println(msg);
	}

	@Override
	public void append(List<Pair<String, Color>> msgsWithColors) {
		System.out.println(msgsWithColors.stream().map(e -> e.first).collect(Collectors.toList()));
	}
}
