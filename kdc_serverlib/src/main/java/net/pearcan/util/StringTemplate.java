/**
 * 
 */
/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.pearcan.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Provides a template mechanism for token replacement in a String.
 * <p>
 * Usage:<br>
 * <pre>
 * StringTemplate2 st = new StringTemplate2("The ${animal} sat on the ${object}.");
 * Map&lt;String,String&gt; tokenValues = new HashMap&lt;String,String&gt;();
 * tokenValues.put("animal", "cat");
 * tokenValues.put("object", "mat");
 * System.out.println(st.replaceTokens(tokenValues));
 * 
 * </pre>
 * should produce:
 * <pre>
 * The cat sat on the mat.
 * </pre>
 * <p>
 * Alternatively, use:<pre>
 * String result = StringTemplate.buildString("The ${animal} sat on the ${object}.")
 *      .replace("animal", "mat")
 *      .replace("object", "mat")
 *      .build();
 *      
 * </pre>
 * @author brianp
 */
@SuppressWarnings("nls")
public class StringTemplate {
    
    private static final String TOKEN_PATTERN_STR = "\\$\\{([a-zA-Z0-9][^}]*)\\}";

    /**
     * Provides a Fluent style for using StringTemplate.
     * @author brian
     *
     */
    static public class Builder {
        
        private final StringTemplate template;
        private final Map<String,Object> tokenValues = new HashMap<String,Object>();

        public Builder(String t, boolean forSQL) {
            this.template = new StringTemplate(t);
            template.setDoubleUpSingleQuotesForStrings(forSQL);
        }

        public Builder(String t) {
            this.template = new StringTemplate(t);
        }

        public Builder(StringTemplate st) {
            this.template = st;
        }
        
        public Builder(StringTemplate st, boolean forSQL) {
            this.template = st;
            template.setDoubleUpSingleQuotesForStrings(forSQL);
        }
        
        public Builder replace(String token, Object value) {
            tokenValues.put(token, value.toString());
            return this;
        }
        
        public Builder replace(Map<String,?> map) {
            for (String token : map.keySet()) {
                tokenValues.put(token, map.get(token));
            }
            return this;
        }
        
        public String build() {
            return template.replaceTokens(tokenValues);
        }
    }
    
    /**
     * Create a Builder for a StringTemplate that is not intended to
     * construct an SQL command. 
     * @param template
     * @return the Builder
     */
    static public Builder buildString(String template) {
        return new Builder(template);
    }
    
    /**
     * Create a Builder for a StringTemplate that is intended to be
     * used to construct an SQL command.
     * @param template
     * @return
     */
    static public Builder buildSql(String template) {
        return new Builder(template, true);
    }
    
    private final String template;
    private boolean doubleUpSingleQuotesForStrings = false;
    private final Pattern pattern;
    
    public StringTemplate(String template) {
        this.template = template;
        this.pattern = Pattern.compile(TOKEN_PATTERN_STR);
    }
    
    /**
     * Create a Builder for non-SQL.
     * @return
     */
    public Builder build() {
        return new Builder(this);
    }
    
    /**
     * Create a Builder for SQL.
     * @param forSQL
     * @return
     */
    public Builder build(boolean forSQL) {
        return new Builder(this, forSQL);
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName()+":"+template; //$NON-NLS-1$
    }
    
    /**
     * Retrieve the set of expected tokens in this template.
     * @return a Set of String
     */
    public Set<String> getTokens() {
        Set<String> result = new HashSet<String>();

        Matcher m = pattern.matcher(template);

        while (m.find()) {
            String p = m.group(1);
            result.add(p);
        }
        return result;
    }
    
    /**
     * Set the parameter to provide SQL command construction.
     * @param b
     */
    public void setDoubleUpSingleQuotesForStrings(boolean b) {
        this.doubleUpSingleQuotesForStrings = b;
    }
    
    /**
     * Using the supplied map of tokens to values, replace all of the
     * tokens in the template. If the <code>doubleUpSingleQuotesForStrings</code>
     * parameter has been set then any tokenValues that are strings will have
     * single quote characters (<code>'</code>) doubled. This is to facilitate
     * building SQL commands and have textual substitution for SQL values in 
     * a "safe" manner.
     * @param tokenValues
     * @return a String
     * @throws MissingVariablesException
     */
    public String replaceTokens(Map<String,?> tokenValues)
    throws MissingVariablesException 
    {
        Set<String> missing = null;
        
        Matcher m = pattern.matcher(template);
        
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String p = m.group(1);
            Object v = tokenValues.get(p);
            if (v==null) {
                if (missing==null) {
                    missing = new TreeSet<String>();
                }
                missing.add(p);
            }
            else {
                if ((v instanceof String) && doubleUpSingleQuotesForStrings) {
                    String s = (String) v;
                    if (s.indexOf('\'') >= 0) {
                        s = s.replaceAll("'", "''");
                    }
                    m.appendReplacement(sb, s);
                }
                else {
                    m.appendReplacement(sb, v.toString());
                }
            }
        }
        
        if (missing!=null) {
            String msg = missing.stream()
                    .collect(Collectors.joining("','", "Missing value(s) for '", "'"));
            throw new MissingVariablesException(msg, missing);
        }
        
        m.appendTail(sb);

        return sb.toString();
    }

    /**
     * Replace the single token with value and return the generated String.
     * The template is expected to <b>only</b> have a single token - otherwise
     * you will get a RuntimeException about the missing token values.
     * @param token
     * @param value
     * @return a String
     * @throws MissingVariablesException
     */
    public String replaceToken(String token, Object value) 
    throws MissingVariablesException
    {
        Map<String, Object> tokenValues = new HashMap<String, Object>();
        tokenValues.put(token, value);
        return replaceTokens(tokenValues);
    }
    
//  /**
//   * Find each of the tokens and return it to the TokenWriter
//   * along with the text that precedes it in the template.
//   * @param tokenWriter
//   * @throws TokenWriteException
//   */
//  public void replaceTokens(TokenWriter tokenWriter) throws TokenWriteException {
//      Matcher m = pattern.matcher(template);
//      
//      int startPos = 0;
//      while (m.find()) {
//          String token = m.group(1);
//          String prefix = template.substring(startPos, m.start());
//          tokenWriter.writeToken(prefix, token);
//          startPos = m.end();
//      }
//  
//      tokenWriter.writeToken(startPos==0 ? template : template.substring(startPos), null);
//  }
    

}
