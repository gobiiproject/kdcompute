/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.api;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.submitjob.SubmitJobResponse;

public class ResponseJob implements SubmitJobResponse {

	final String _kdcVersion;
	final String jobStatusUrl;
	final AnalysisJob analysisRequest;
	public ResponseJob(String _kdcVersion, String jobStatusUrl, AnalysisJob analysisRequest) {
		super();
		this._kdcVersion = _kdcVersion;
		this.jobStatusUrl = jobStatusUrl;
		this.analysisRequest = analysisRequest;
	}
	public String get_kdcVersion() {
		return _kdcVersion;
	}
	public String getJobStatusUrl() {
		return jobStatusUrl;
	}
	public AnalysisJob getAnalysisRequest() {
		return analysisRequest;
	}



}
