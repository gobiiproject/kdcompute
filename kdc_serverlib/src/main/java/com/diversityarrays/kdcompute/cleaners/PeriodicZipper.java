/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.cleaners;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.zeroturnaround.zip.ZipUtil;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.EntityStore.RunStateChoice;
import com.diversityarrays.kdcompute.util.Check;

public class PeriodicZipper extends FilesDaemon<Void> {

	private static final long PERIODIC_ZIPPER_FREQ_MILLIS;
	static {
		String s = System.getProperty("PERIODIC_ZIPPER_FREQ_MILLIS");
		long millis = -1;
		if (!Check.isEmpty(s)) {
			try {
				millis = Long.parseLong(s);
			} catch (NumberFormatException e) {
				Logger.getLogger(PeriodicZipper.class.getSimpleName()).log(Level.WARNING,
						"Invalid PERIODIC_ZIPPER_FREQ_MILLIS: " + s);
			}
		}
		if (millis <= 0) {
			millis = 60_000;
		}
		PERIODIC_ZIPPER_FREQ_MILLIS = millis;
	}

	static private boolean doit(File file, long maxAgeMillis) {

		if (file != null && file.exists() && !hasSuffix(file, "zip")) {

			BasicFileAttributes attrs;
			try {
				attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
				FileTime time = attrs.lastAccessTime();
				long elapsed = System.currentTimeMillis() - time.toMillis();
				if (elapsed > maxAgeMillis) {
					if (file.listFiles().length == 0) {
						FileUtils.deleteDirectory(file);
						return true;
					} else {
						zipFolder(file);
						FileUtils.deleteDirectory(file);
						return true;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public PeriodicZipper(Function<RunStateChoice, Consumer<Predicate<File>>> visitResultsFolderWithPredicate,
			Duration zipAfterNonAccessSeconds) {
		super(visitResultsFolderWithPredicate.apply(EntityStore.INACTIVE_JOBS), 0, PERIODIC_ZIPPER_FREQ_MILLIS,
				file -> doit(file, zipAfterNonAccessSeconds.toMillis()));
	}

	public static File zipFolder(File t) {
		File zipFile = AnalysisJob.toZipFile(t);
		if (zipFile.exists()) {
			zipFile.delete();
		}
		ZipUtil.pack(t, zipFile);
		return zipFile;
	}

}
