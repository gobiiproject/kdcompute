/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.cleaners;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.commons.io.FilenameUtils;

/**
 * Iterates all sub-files and sub-folders of a directory and applys an action to
 * each file found
 *
 * @author andrewkowalczyk, brianp
 *
 */
public class FilesDaemon<R> implements Runnable {
	static public boolean hasSuffix(File file, String suffix) {
		return FilenameUtils.getExtension(file.getPath()).equals(suffix);
	}

	private final long frequencyMillis;
	private final long initialMillis;

	private final TimerTask timerTask;
	private Timer timer;

	public FilesDaemon(Consumer<Predicate<File>> filesToIterateOver, long initialMillis, long frequencyMillis,
			Predicate<File> action) {
		super();
		this.frequencyMillis = frequencyMillis;
		this.initialMillis = initialMillis;
		// Logger logger = Logger.getLogger(getClass().getSimpleName());
		this.timerTask = new TimerTask() {
			@Override
			public void run() {
				// logger.log(Level.DEBUG, "processing files");
				filesToIterateOver.accept(action);
			}
		};
	}

	@Override
	public void run() {
		timer = new Timer(true);
		timer.schedule(timerTask, initialMillis, frequencyMillis);
	}

}
