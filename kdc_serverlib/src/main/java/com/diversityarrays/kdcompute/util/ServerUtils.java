/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.diversityarrays.kdcompute.Pair;

public class ServerUtils {
	public static String buildParamUrl(String url, Map<String, String> params) {
		return url + "?" + String.join("&",
				params.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.toList()));
	}

	public static String buildParamUrl(String url, List<Pair<String, String>> params) {
		return url + "?" + String.join("&",
				params.stream().map(e -> e.getFirst() + "=" + e.getSecond()).collect(Collectors.toList()));
	}

	@SafeVarargs
	public static String buildParamUrl(String url, Pair<String, String>... params) {
		return buildParamUrl(url, Arrays.asList(params));
	}
}
