/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.script.AdditionalEnvironmentVariables;
import com.diversityarrays.kdcompute.util.Check;

public class AnalysisJobRunner {

	static public final String STDOUT_FILENAME = "stdout.txt";
	static public final String STDERR_FILENAME = "stderr.txt";

	static public final Function<File, File> DEFAULT_STDOUT_PROVIDER = new Function<File, File>() {
		@Override
		public File apply(File dir) {
			return new File(dir, STDOUT_FILENAME);
		}
	};

	static public final Function<File, File> DEFAULT_STDERR_PROVIDER = new Function<File, File>() {
		@Override
		public File apply(File dir) {
			return new File(dir, STDERR_FILENAME);
		}
	};

	private final Function<File, Optional<CallScript>> callScriptProvider;
	private final Function<File, File> stdoutProvider;
	private final Function<File, File> stderrProvider;
	private final Function<AnalysisJob, Exception> statusChangeConsumer;
	private final AdditionalEnvironmentVariables userEnvironmentVariables;

	public AnalysisJobRunner(Function<File, Optional<CallScript>> callScriptProvider,
			Function<File, File> stdoutProvider, Function<File, File> stderrProvider,
			Function<AnalysisJob, Exception> statusChangeConsumer,
			AdditionalEnvironmentVariables userEnvironmentVariables) {
		super();
		this.callScriptProvider = callScriptProvider;
		this.stdoutProvider = stdoutProvider;
		this.stderrProvider = stderrProvider;
		this.statusChangeConsumer = statusChangeConsumer;
		if (userEnvironmentVariables == null) {
			throw new RuntimeException("AdditionalEnvironmentVariables is null");
		}
		this.userEnvironmentVariables = userEnvironmentVariables;
	}

	public void runJob(AnalysisJob job, long timeoutMillis, String errorPrefix) {
		String shellFilePath = job.getShellFilePath();
		File scriptFile = new File(shellFilePath);

		File scriptDir = scriptFile.getParentFile();

		File stdout = stdoutProvider.apply(scriptDir);
		File stderr = stderrProvider.apply(scriptDir);

		try {
			job.setStartDateTime(new Date());
			setState(job, RunState.RUNNING);
			if (statusChangeConsumer != null) {
				statusChangeConsumer.apply(job);
			}

			Optional<CallScript> callScriptOptional = callScriptProvider.apply(scriptFile);
			if (!callScriptOptional.isPresent()) {
				FileUtils.write(stderr, "The script type (" + FilenameUtils.getExtension(scriptFile.getPath())
						+ ") of plugin " + job.getDisplayName() + " is not supported.", Charset.defaultCharset());
				return;
			}
			CallScript callScript = callScriptOptional.get();
			int exitCode = callScript.call(scriptFile, scriptDir, stdout, stderr, timeoutMillis,
					userEnvironmentVariables.instantiateEnvironmentVariables(job.getRequestingUser()));

			job.setExitCode(exitCode);

			job.setEndDateTime(new Date());
			if (exitCode != 0) {
				FileUtils.write(stderr, "Plugin exit with non zero value: " + exitCode, Charset.defaultCharset(), true);
				setState(job, RunState.FAILED);
			} else {
				setState(job, RunState.COMPLETED);
			}

			/*
			 * Delete stdout/stderr if empty
			 */
			deleteIfEmpty(stdout);
			deleteIfEmpty(stderr);
		} catch (InterruptedException e) {
			job.setError(e.getMessage());
			job.setEndDateTime(new Date());
			setState(job, RunState.CANCELLED);
			try {
				String msg;
				if (Check.isEmpty(e.getMessage())) {
					msg = e.getMessage();
				} else {
					msg = errorPrefix + "\n" + e.getMessage();
				}
				FileUtils.write(stderr, msg, Charset.defaultCharset(), true);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			job.setError(e.getMessage());
			setState(job, RunState.FAILED);
			try {
				String msg;
				if (Check.isEmpty(e.getMessage())) {
					msg = e.getMessage();
				} else {
					msg = errorPrefix + "\n" + e.getMessage();
				}
				FileUtils.write(stderr, msg, Charset.defaultCharset(), true);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} finally {
			job.setEndDateTime(new Date());
			if (job.getPostCompletionCall() != null)
				job.getPostCompletionCall().onFinish(job);
		}
	}

	private void setState(AnalysisJob job, RunState newState) {
		job.setRunState(newState);
		if (statusChangeConsumer != null) {
			statusChangeConsumer.apply(job);
		}
	}

	public static void deleteIfEmpty(File file) throws FileNotFoundException, IOException {
		if (file.length() <= 0) {
			file.delete();
		}
	}
}
