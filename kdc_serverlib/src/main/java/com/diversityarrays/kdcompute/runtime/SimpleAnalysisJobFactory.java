/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import com.diversityarrays.fileutils.KDCFileUtils;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.DataSet;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.legacy.LegacyTemplate;
import com.diversityarrays.kdcompute.util.Check;

import net.pearcan.util.StringTemplate;

public class SimpleAnalysisJobFactory implements AnalysisJobFactory {

	private static final String TMPL = ".tmpl";

	private static final String EMPTY_SCRIPT_TEMPLATE = "empty-script.template";

	/**
	 * Single algorithm shell script filename.
	 */
	public static final String RUNME_SH = "runme.sh"; //$NON-NLS-1$
	public static final String RUNME_GROOVY = "runme.groovy"; //$NON-NLS-1$

	/**
	 * Script file name for the "master" script for multiple algorithm runs.
	 */
	private static final String MULTIRUN_SH = "multirun.sh"; //$NON-NLS-1$

	private FileStorage fileStorage;

	public SimpleAnalysisJobFactory() {
		super();
	}

	public void setFileStorage(FileStorage fileStorage) {
		this.fileStorage = fileStorage;
	}

	@Override
	public AnalysisJob createAnalysisJob(AnalysisRequest request, String userName, Consumer<File> resultsFolderCreated,
			Map<String, String> additionalSystemVariables) throws IOException, ParameterException {

		Date now = new Date();

		String displayName = "Non-tagged";
		if (!request.getAlgorithms().isEmpty())
			displayName = request.getAlgorithms().get(0).getAlgorithmName();
		AnalysisJob result = new AnalysisJob(request, now, displayName);
		result.setRequestingUser(userName);
		request.setDateFirstSubmitted(now);

		File resultsFolder = fileStorage.getNewResultsFolder(userName, request.getId());

		if (resultsFolderCreated != null) {
			resultsFolderCreated.accept(resultsFolder);
		}
		result.setResultsFolderPath(resultsFolder.getPath().replace(" ", "\\ "));

		// First = separate the RunBindings by Algorithm so we can work out if
		// we need
		// to use instance numbers for the RunBindings for an Algorithm.

		Map<Plugin, List<RunBinding>> rblistByPlugin = request.getRunBindings().stream()
				.collect(Collectors.groupingBy(RunBinding::getPlugin));

		// This lets us later re-order the scripts by the runBinding order as
		// provided in the AnalysisJob
		Map<File, RunBinding> runBindingByFile = new HashMap<>();

		for (Plugin alg : rblistByPlugin.keySet()) {
			if (Check.isEmpty(alg.getAlgorithmName())) {
				throw new ParameterException("One of the Algorithms has no name");
			}
			List<RunBinding> rblist = rblistByPlugin.get(alg);
			ensureRunBindingsAreFullyBound(alg, rblist);
		}

		for (Plugin alg : rblistByPlugin.keySet()) {

			List<RunBinding> rblist = rblistByPlugin.get(alg);

			if (rblist.size() == 1) {
				RunBinding rb = rblist.get(0);

				File scriptFile = generateScriptFile(userName, alg, rb, null, resultsFolder, additionalSystemVariables);
				runBindingByFile.put(scriptFile, rb);
			} else {
				int instanceNumber = 0;
				for (RunBinding rb : rblist) {
					++instanceNumber;

					File scriptFile = generateScriptFile(userName, alg, rb, instanceNumber, resultsFolder,
							additionalSystemVariables);
					runBindingByFile.put(scriptFile, rb);
				}
			}
		}

		List<File> scriptFiles = new ArrayList<>(runBindingByFile.keySet());
		if (scriptFiles.size() == 1) {
			result.setShellFilePath(scriptFiles.get(0).getPath());
		} else {
			// Create a "master" shell script that invokes all of the others
			File shellFile = createMultiScriptShellFile(request, runBindingByFile, scriptFiles, resultsFolder);
			result.setShellFilePath(shellFile.getPath());
		}

		return result;
	}

	public void ensureRunBindingsAreFullyBound(Plugin alg, List<RunBinding> rblist) throws ParameterException {
		for (RunBinding rb : rblist) {
			if (!rb.isFullyBound()) {
				StringBuilder sb = new StringBuilder(alg.getAlgorithmName());
				sb.append(": ");
				int sblen = sb.length();
				Set<Knob> unboundKnobs = rb.getUnboundKnobs();
				if (!unboundKnobs.isEmpty()) {
					sb.append("Unbound knobs: ");
					sb.append(unboundKnobs.stream().map(Knob::getKnobName).collect(Collectors.joining(", ")));
				}

				Set<DataSet> unboundDataSets = rb.getUnboundDataSets();
				if (!unboundDataSets.isEmpty()) {
					if (sb.length() > sblen) {
						sb.append(" and ");
					}
					sb.append("Unbound DataSets: ");
					sb.append(unboundDataSets.stream().map(DataSet::getDataSetName).collect(Collectors.joining(", ")));
				}
				throw new ParameterException(sb.toString());
			}

		}
	}

	private File createMultiScriptShellFile(AnalysisRequest job, Map<File, RunBinding> runBindingByFile,
			List<File> scriptFiles, File resultsFolder) throws IOException {
		Map<RunBinding, Integer> orderByRunBinding = new HashMap<>();
		int order = 0;
		for (RunBinding rb : job.getRunBindings()) {
			orderByRunBinding.put(rb, ++order);
		}

		scriptFiles.sort(new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				RunBinding rb1 = runBindingByFile.get(o1);
				RunBinding rb2 = runBindingByFile.get(o2);

				Integer i1 = orderByRunBinding.get(rb1);
				Integer i2 = orderByRunBinding.get(rb2);
				return i1.compareTo(i2);
			}

		});

		// Now ... one file to rule them all...
		File tmp = new File(resultsFolder, MULTIRUN_SH);
		FileWriter fw = new FileWriter(tmp);
		try {
			PrintWriter pw = new PrintWriter(fw);
			pw.println("#!/bin/sh"); //$NON-NLS-1$
			int index = 0;
			String fmt = "# Script %d\nsource %s\n# --------\n"; //$NON-NLS-1$
			for (File scriptFile : scriptFiles) {
				++index;
				pw.println();
				pw.println(String.format(fmt, index, scriptFile.getPath()));
			}
			pw.close();
		} finally {
			fw.close();
		}
		return tmp;
	}

	private File generateScriptFile(String userName, Plugin alg, RunBinding rb, Integer instanceNumber,
			File resultsFolder, Map<String, String> additionalSystemVariables) throws IOException {
		File algorithmInstanceFolder = fileStorage.getPluginInstanceFolder(resultsFolder, alg.getAlgorithmName(),
				instanceNumber);

		File result = null;

		rb.setOutputFolderPath(algorithmInstanceFolder.getAbsolutePath());

		boolean success = false;
		String path = alg.getScriptTemplateFilename(); // a/b/c.sh
		if (Check.isEmpty(path)) {
			result = new File(algorithmInstanceFolder, RUNME_GROOVY);
			// There is no supplied template so we will create a dummy one

			InputStream is = getClass().getResourceAsStream(EMPTY_SCRIPT_TEMPLATE);
			if (is == null) {
				throw new IOException("missing resource: " + EMPTY_SCRIPT_TEMPLATE);
			}

			Map<String, String> varmap = buildVariableMap(alg, rb);

			varmap = checkVariables(varmap);

			String str = IOUtil.readContents(new InputStreamReader(is));
			StringTemplate template = new StringTemplate(str);
			String scriptLines = template.replaceTokens(varmap);

			success = IOUtil.writeFileContents(result, scriptLines);
		} else {
			File templateFile = fileStorage.getTemplateFile(alg, path);
			String name = templateFile.getName();
			result = new File(algorithmInstanceFolder, getScriptInstance(name));
			Map<String, String> variableMap = buildVariableMap(alg, rb);
			String templateLines = IOUtil.readContents(new FileReader(templateFile));

			variableMap.putAll(additionalSystemVariables);

			String scriptLines;
			if (alg.isLegacy()) {
				LegacyTemplate template = new LegacyTemplate(templateLines);

				/*
				 * Legacy plugins required fullpaths from the server root to work, e.g. binary:
				 * $DIRPATH/algorithms/kdcplug_standalone_distMatrixCalculator/
				 * distmatrixcalculator result_dir
				 * $DIRPATH/public/userdata/$USERNAME/home/submitted_jobs/ $DALJOBID Below, we
				 * make the directories required in /tmp to handle this brain damage
				 */
				File tempMockDirs = KDCFileUtils.getExampleTempDir();
				variableMap.put("DIRPATH", tempMockDirs.getAbsolutePath());
				variableMap.put("USERNAME", userName);
				String daljobid = new File(rb.getOutputFolderPath()).getName();
				variableMap.put("DALJOBID", daljobid);
				File pluginRoot = new File(tempMockDirs, "algorithms"
				// +File.separator+alg.getPluginName() // Last one is done in
				// softlink
				);
				File userRoot = new File(tempMockDirs, "public" + File.separator + "userdata" + File.separator
						+ userName + File.separator + "home" + File.separator + "submitted_jobs" + File.separator
				// +daljobid + File.separator // Last one is done in softlink
				);

				pluginRoot.mkdirs();
				userRoot.mkdirs();

				// new File(tempMockDirs,
				// "public"+File.separator+
				// "userdata"+File.separator+
				// userName+File.separator +
				//

				Files.createSymbolicLink(new File(pluginRoot, alg.getPluginName()).toPath(),
						templateFile.getParentFile().toPath());
				Files.createSymbolicLink(new File(userRoot, daljobid).toPath(),
						new File(rb.getOutputFolderPath()).toPath());

				// Remove fullpath from any input files
				for (Map.Entry<String, String> e : variableMap.entrySet()) {
					variableMap.put(e.getKey(),
							e.getValue().replaceAll(fileStorage.getUserDir(userName).getAbsolutePath(), "."));
				}

				variableMap = checkVariables(variableMap);

				scriptLines = template.replaceVariables(variableMap);
			} else {
				scriptLines = templateLines;
				variableMap = checkVariables(variableMap);
				for (Map.Entry<String, String> token : variableMap.entrySet()) {
					String key = "<%=\\s*" + token.getKey() + "\\s*%>";
					String value = "";
					if (token.getValue() != null) {
						value = token.getValue();
					}
					String escapedValue = Matcher.quoteReplacement(value);
					scriptLines = scriptLines.replaceAll(key, escapedValue);
				}
			}

			success = IOUtil.writeFileContents(result, scriptLines);
		}

		if (success) {
			return result;
		}
		result.delete();
		return result;
	}

	public static String getScriptInstance(String templateName) {
		return templateName.replaceAll(TMPL, "");
	}

	public Map<String, String> buildVariableMap(Plugin alg, RunBinding rb) throws IOException {
		Map<String, String> varmap = new HashMap<>();

		if (alg.isLegacy()) {
			for (String vname : LegacyTemplate.OLD_TEMPLATE_CORE_VARNAMES) {
				varmap.put(vname, vname);
			}
			for (KnobBinding kb : rb.getKnobBindings()) {
				varmap.put(kb.getKnob().getKnobName(), kb.getKnobValue());
			}
		} else {
			String versionString = alg.getVersionString();
			if (Check.isEmpty(versionString)) {
				versionString = "unknown";
			}

			String when = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			varmap.put("className", getClass().getName());
			varmap.put("when", when);
			varmap.put("algorithmName", alg.getAlgorithmName());
			varmap.put("versionString", versionString);
			varmap.put("versionCode", alg.getVersion().toString());
			varmap.put("algorithmDir", fileStorage.getPluginFolder(alg).getAbsolutePath());
			varmap.put("pluginDir", fileStorage.getPluginFolder(alg).getAbsolutePath());
			varmap.put("jobDir", rb.getOutputFolderPath());

			for (String name : "DALCOOKIEPATH DALBASEURL DALWRITETOKEN DIRPATH JOBID USERNAME USEREMAIL knobValues"
					.split(" ")) {
				varmap.put(name, "value-of-" + name);
			}
			/*
			 * # Generated by ${className} on ${when} # Template for ${algorithmName} #
			 * Version: ${versionString} [${versionCode}]
			 * 
			 * DALCOOKIEPATH=${DALCOOKIEPATH} DALBASEURL=${DALBASEURL}
			 * DALWRITETOKEN=${DALWRITETOKEN} DIRPATH=${DIRPATH} JOBID=${JOBID}
			 * USERNAME=${USERNAME} USEREMAIL=${USEREMAIL}
			 * 
			 * # Algorithm Knob Values ${knobValues}
			 * 
			 */

			for (KnobBinding kb : rb.getKnobBindings()) {
				varmap.put(kb.getKnob().getKnobName(), kb.getKnobValue());
			}
		}

		// for (Map.Entry<String, String> token : varmap.entrySet()) {
		// String newtoken = StringEscapeUtils.escapeJava(token.getValue());
		// if (System.getProperty("os.name").toLowerCase().contains("window")) {
		// newtoken = StringEscapeUtils.escapeJava(newtoken);
		// }
		// varmap.put(token.getKey(), newtoken);
		// }

		return varmap;
	}

	final static Function<String, String> replaceBackslash;
	static {
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			replaceBackslash = new Function<String, String>() {

				@Override
				public String apply(String arg0) {
					String replaceAll = arg0.replaceAll("\\\\", "/");
					return replaceAll;
				}
			};
		} else {
			replaceBackslash = new Function<String, String>() {

				@Override
				public String apply(String t) {
					return t;
				}
			};
		}
	}

	private static String variableCheck(String str) {
		return replaceBackslash.apply(str);
	}

	private static Map<String, String> checkVariables(Map<String, String> varmap) {
		return varmap.entrySet().stream()
				.collect(Collectors.toMap(Entry::getKey, e -> variableCheck(String.valueOf(e.getValue()))));
	}
}
