/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Deprecated
public class EntityQuery<T> {

    private Class<T> entityClass;
    private EntityStore entityStore;

    public EntityQuery(Class<T> entityClass, EntityStore entityStore) {
        this.entityClass = entityClass;
        this.entityStore = entityStore;
    }

    public List<T> getAll() {
        EntityManager manager = entityStore.getEntityManager();
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = criteriaBuilder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);
        criteria = criteria.select(root);
        TypedQuery<T> query = manager.createQuery(criteria);
        return query.getResultList();
    }

    public List<T> getAllWhere(String attributeName, Object attributeValue) {
        EntityManager manager = entityStore.getEntityManager();
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = criteriaBuilder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);

        criteria = criteria.select(root)
                .where(criteriaBuilder.equal(root.get(attributeName), attributeValue));
        TypedQuery<T> query = manager.createQuery(criteria);
        return query.getResultList();
    }
}
