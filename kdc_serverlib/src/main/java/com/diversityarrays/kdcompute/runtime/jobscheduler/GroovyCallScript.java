/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Used by JobScheduler to call a groovy plugin script
 * 
 * @author andrew
 *
 */
public class GroovyCallScript implements CallScript {

	final String groovyCommand;

	public GroovyCallScript(String groovyCommand)
			throws GroovyNotInstalledException, InterruptedException, IOException {
		super();
		this.groovyCommand = groovyCommand;
		if (groovyCommand == null) {
			throw new GroovyNotInstalledException("Groovy command not provided. Please set in applicaiton.properties");
		}
		final Process proc = Runtime.getRuntime().exec(groovyCommand);
		int waitFor = proc.waitFor();
		if (waitFor != 0) {
			throw new GroovyNotInstalledException("Groovy command failed with exit code " + waitFor
					+ ". Please correct and set in applicaiton.properties");
		}
	}

	@Override
	public int call(File script, File workingDirectory, File stdoutFile, File stderrFile, long timeout_milliseconds,
			Map<String, String> enviromentVariables) throws Exception {

		String groovyCmd = groovyCommand;
		List<String> envCommand = new ArrayList<>();
		for (Map.Entry<String, String> envVar : enviromentVariables.entrySet()) {
			String var = envVar.getKey();
			String value = envVar.getValue();
			envCommand.addAll(Arrays.asList("export " + var + "=\'" + value + "\'"));
		}

		/*
		 * Run the command, capturing the variable $DALCOOKIE in an echo
		 */
		String[] full_command = new String[] { groovyCmd, new File(script.getPath()).getAbsolutePath() };

		@SuppressWarnings("unused")
		String cmd = String.join(" ", full_command); // 4 debugging

		ProcessBuilder builder = new ProcessBuilder(full_command).directory(workingDirectory).inheritIO()
				.redirectOutput(stdoutFile).redirectError(stderrFile)

		;

		Process process = builder.start();

		if (!process.waitFor(timeout_milliseconds, TimeUnit.MILLISECONDS)) {
			throw new StderrException("");
		}

		return process.exitValue();
	}

	protected String getOs() {
		return System.getProperty("os.name").toLowerCase();
	}

	@Override
	public List<String> getSupportedExtensions() {
		return Arrays.asList("groovy");
	}

	public String getCommand() {
		return groovyCommand;
	}

}
