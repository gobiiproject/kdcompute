/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Id;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.JobStillActiveException;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginGroup;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.db.User;
import com.diversityarrays.kdcompute.db.UserRole;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobNotFoundException;
import com.diversityarrays.kdcompute.util.Check;
import com.diversityarrays.kdcompute.util.Either;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;

@SuppressWarnings("nls")
public class InMemoryEntityStore implements EntityStore {

	private static final String ALGORITHM_SAVED_JSON = "algorithm-saved.json";

	private EntityManager entityManager;

	private static final String currentVersionNumber = "KDCDatabase_1.0";

	public static final Comparator<Plugin> REVERSE_VERSION_NUMBER = new Comparator<Plugin>() {
		@Override
		public int compare(Plugin o1, Plugin o2) {
			return o1.getVersion().compareTo(o2.getVersion());
		}
	};

	static private Set<String> ADMIN_NAMES = Collections.unmodifiableSet(
			new HashSet<>(Arrays.asList("admin", "administrator", UserRoleSpecification.ROLE_ADMIN.name())));

	private long nextId = 1;
	private final IdFieldStore idFieldStore = new IdFieldStore();

	protected Logger logger = Logger.getLogger(getClass().getName());
	protected FileStorage fileStorage;
	protected JobQueue dummyJobQueue;

	private Consumer<Class<?>> onDataChanged;

	public JobQueue getDummyJobQueue() {
		return dummyJobQueue;
	}

	private final List<User> allUsers = new ArrayList<>();

	public InMemoryEntityStore() {
		allUsers.add(newUsers("admin", "password", ADMIN_ROLE));
		allUsers.add(newUsers("user", "pass", USER_ROLE));
		allUsers.add(newUsers("dev", "password", ADMIN_ROLE));
	}

	protected void setOnDataChanged(Consumer<Class<?>> onDataChanged) {
		this.onDataChanged = onDataChanged;
	}

	@Override
	public void setFileStorage(FileStorage fileStorage) {
		this.fileStorage = fileStorage;
	}

	/**
	 * Make an instance managed and persistent.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object persist(Object record) {
		Class<?> tclass = record.getClass();
		Field idField = idFieldStore.getIdField(tclass);

		try {
			Object idValue = idField.get(record);
			if (idValue != null) {
				throw new EntityExistsException(
						"ID(" + idValue + ") Already assigned: " + tclass.getName() + "." + idField.getName());
			}

			Long id = nextId++;
			idField.set(record, id);

			logger.info("created id=" + id + " for " + tclass.getName());
			Map<Long, Object> map = getRecordMap(tclass);
			map.put(id, record);

			if (record instanceof Plugin) {
				writeAlgorithmToFileStorage((Plugin) record);
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}

		if (onDataChanged != null) {
			onDataChanged.accept(tclass);
		}
		return record;
	}

	private void removeAlgorithmFromFileStorage(Plugin alg) {
		if (fileStorage != null) {
			try {
				fileStorage.removePluginFile(alg, ALGORITHM_SAVED_JSON);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void writeAlgorithmToFileStorage(Plugin plugin) {
		if (!plugin.isLegacy() && fileStorage != null) {
			FileWriter w = null;
			try {
				File algFolder = fileStorage.getPluginFolder(plugin);
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				File jsonFile = new File(algFolder, ALGORITHM_SAVED_JSON);

				w = new FileWriter(jsonFile);
				gson.toJson(plugin, w);
			} catch (JsonIOException | IOException e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			} finally {
				if (w != null) {
					try {
						w.close();
					} catch (IOException ignore) {
					}
				}
			}
		}
	}

	/**
	 * Merge the state of the given entity into the current persistence context.
	 *
	 * @return the managed instance that the state was merged to
	 */
	@Override
	public <T> T merge(T record) {
		@SuppressWarnings("unchecked")
		T persisted = (T) getPersistedInstance(record);
		if (persisted == null) {
			throw new IllegalArgumentException("no persisted instance for " + record);
		}
		if (persisted != record) {
			copyFieldValuesFromTo(record, persisted);
		}

		if (persisted instanceof Plugin) {
			writeAlgorithmToFileStorage((Plugin) persisted);
		}
		return persisted;
	}

	/**
	 * Refresh the state of the instance from the database, overwriting changes
	 * made to the entity, if any.
	 */
	@Override
	public void refresh(Object record) {
		Object persisted = getPersistedInstance(record);
		if (persisted == null) {
			throw new IllegalArgumentException("no persisted instance for " + record);
		}
		if (persisted != record) {
			copyFieldValuesFromTo(persisted, record);
		}
	}

	@Override
	public void remove(Object record) {
		Class<?> tclass = record.getClass();
		Field idField = idFieldStore.getIdField(tclass);

		try {
			Long id = idField.getLong(record);

			Map<Long, Object> map = recordsByClass.get(tclass);
			if (map != null) {
				map.remove(id);
			}

			if (record instanceof Plugin) {
				removeAlgorithmFromFileStorage((Plugin) record);
			}

		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Plugin> getPlugins() {
		return getListOfEntities(Plugin.class);
		// Map<Long, Object> map = getRecordMap(Plugin.class);
		// if (map == null) {
		// return Collections.emptyList();
		// }
		// List<Plugin> result = new ArrayList<>();
		// for (Object obj : map.values()) {
		// result.add((Plugin) obj);
		// }
		// return result;
	}

	@SuppressWarnings("unchecked")
	protected <T> List<T> getListOfEntities(Class<T> tclass) {
		Map<Long, Object> map = getRecordMap(tclass);
		if (map == null) {
			return Collections.emptyList();
		}
		return map.values().stream().map(e -> (T) e).collect(Collectors.toList());
	}

	@Override
	public List<PluginGroup> getPluginGroups() {
		Map<Long, Object> map = getRecordMap(PluginGroup.class);
		if (map == null) {
			return Collections.emptyList();
		}
		List<PluginGroup> result = new ArrayList<>();
		for (Object obj : map.values()) {
			result.add((PluginGroup) obj);
		}
		return result;
	}

	@Override
	public Throwable doTransaction(Consumer<EntityTransaction> worker) {
		worker.accept(null);
		return null;
	}

	// ======================

	private final Map<Class<?>, List<Field>> fieldsByClass = new HashMap<>();

	private void copyFieldValuesFromTo(Object from, Object to) {
		Class<?> clz = to.getClass();
		if (!clz.equals(from.getClass())) {
			throw new RuntimeException(
					"Different classes: " + from.getClass().getName() + " <> " + to.getClass().getName());
		}
		List<Field> fields = fieldsByClass.get(clz);
		if (fields == null) {
			fields = new ArrayList<>();
			for (Field f : clz.getDeclaredFields()) {
				int mods = f.getModifiers();
				if (!Modifier.isStatic(mods)) {
					f.setAccessible(true);
					fields.add(f);
				}
			}
			fieldsByClass.put(clz, fields);
		}

		boolean anyChanged = false;
		for (Field f : fields) {
			try {
				Object fromValue = f.get(from);
				Object toValue = f.get(to);
				f.set(to, fromValue);

				if (fromValue == null) {
					if (toValue != null) {
						anyChanged = true;
					}
				} else if (!fromValue.equals(toValue)) {
					anyChanged = true;
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}

		if (anyChanged && onDataChanged != null) {
			onDataChanged.accept(clz);
		}
	}

	private Object getPersistedInstance(Object record) {
		Object result = null;
		Class<?> tclass = record.getClass();
		Map<Long, Object> map = getRecordMap(tclass);
		if (map != null) {
			Field idField = idFieldStore.getIdField(tclass);
			try {
				Long id = (Long) idField.get(record);
				result = map.get(id);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		return result;
	}

	private final Map<Class<?>, Map<Long, Object>> recordsByClass = new HashMap<>();

	protected Map<Long, Object> getRecordMap(Class<?> tclass) {
		Map<Long, Object> map = recordsByClass.get(tclass);
		if (map == null) {
			map = new HashMap<>();
			recordsByClass.put(tclass, map);
		}
		return map;
	}

	static class IdFieldStore {

		private final Map<Class<?>, Field> idFieldByClass = new HashMap<>();

		public Field getIdField(Class<?> tclass) {
			Field idField = idFieldByClass.get(tclass);
			if (idField == null) {
				for (Field f : tclass.getDeclaredFields()) {
					if (Modifier.isStatic(f.getModifiers())) {
						continue;
					}
					Id id = f.getAnnotation(Id.class);
					if (id != null) {
						if (idField != null) {
							throw new RuntimeException("Multiple @Id annotations in " + tclass.getName());
						}
						idField = f;
					}
				}
				if (idField == null) {
					throw new RuntimeException("No @Id field in " + tclass.getName());
				}
				if (Long.class != idField.getType()) {
					throw new RuntimeException("@Id " + tclass.getName() + "." + idField.getName() + " is not a Long");
				}
				idField.setAccessible(true);
				idFieldByClass.put(tclass, idField);
			}
			return idField;
		}
	}

	@Override
	public boolean isInMemory() {
		return true;
	}

	@Override
	public PluginGroup getPluginGroupBy(String groupName) {
		PluginGroup result = null;
		Map<Long, Object> map = getRecordMap(PluginGroup.class);

		if (map != null) {
			for (Object obj : map.values()) {
				PluginGroup g = (PluginGroup) obj;
				if (groupName.equalsIgnoreCase(g.getGroupName())) {
					result = g;
					break;
				}
			}
		}
		return result;
	}

	@Override
	public boolean isPluginUsedInAnyRunBindings(Plugin algorithm) {
		Map<Long, Object> map = getRecordMap(RunBinding.class);
		if (map == null) {
			return false;
		}
		for (Object obj : map.values()) {
			RunBinding rb = (RunBinding) obj;
			if (rb.getPlugin().equals(algorithm)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getById(Class<T> rClass, Long id) {
		T result = null;
		if (id != null) {
			Map<Long, Object> map = getRecordMap(rClass);
			if (map != null) {
				Object r = map.get(id);
				if (r != null) {
					if (rClass.isAssignableFrom(r.getClass())) {
						result = (T) r;
					} else {
						throw new IllegalStateException(String.format("Incorrect class in map: expected=%s, actual=%s",
								rClass.getName(), r.getClass().getName()));
					}
				}
			}
		}
		return result;
	}

	@Override
	public List<String> getListOfPluginNames() {
		List<String> names = new ArrayList<>();
		List<Plugin> plugins = getPlugins();
		plugins.stream().forEach(e -> names.add(e.getAlgorithmName()));
		return names;
	}

	@Override
	public FileStorage getFileStorage() {
		return this.fileStorage;
	}

	@Override
	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public void setEntityManager(EntityManager manager) {
		this.entityManager = manager;
	}

    @Override
    public List<AnalysisJob> getAnalysisJobs(RunStateChoice runStateChoice) {
        if (runStateChoice.isEmpty()) {
            return getAllAnalysisJobs();
        }

        Set<RunState> runStates = new HashSet<>();
        Collections.addAll(runStates, runStateChoice.runStates);

        Class<?> tclass = AnalysisJob.class;
        idFieldStore.getIdField(tclass);
        List<AnalysisJob> jobs = new ArrayList<>();

        try {
            Map<Long, Object> map = getRecordMap(tclass);

            if (map != null) {
                for (Long id : map.keySet()) {
                    if (map.get(id) instanceof AnalysisJob) {
                        AnalysisJob job = (AnalysisJob) map.get(id);
                        if (runStates.contains(job.getRunState())) {
                            jobs.add((AnalysisJob) map.get(id));
                        }
                    }
                }
            }
            return jobs;
        }
        catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        }
    }


	@Override
	public List<AnalysisJob> getAllAnalysisJobs() {

		Class<?> tclass = AnalysisJob.class;
		idFieldStore.getIdField(tclass);
		List<AnalysisJob> jobs = new ArrayList<>();

		try {
			Map<Long, Object> map = getRecordMap(tclass);

			if (map != null) {
				for (Long id : map.keySet()) {

					if (map.get(id) instanceof AnalysisJob) {
						jobs.add((AnalysisJob) map.get(id));
					}
				}
			}
			return jobs;
		}

		catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getDatabaseVersion() {
		return currentVersionNumber;
	}

	@Override
	public void setJobQueue(JobQueue jobQ) {
		this.dummyJobQueue = jobQ;

	}

	// public static List<InMemoryUser> getInMemUsers() {
	// return Arrays.asList(new InMemoryUser("admin","password","ADMIN"),
	// new InMemoryUser("user","pass","USER"),
	// new InMemoryUser("dev","password","ADMIN"));
	// }

	static private User newUsers(String name, String pass, UserRole r) {
		User u = new User();
		u.setUserName(name);
		u.setPassword(pass);

		u.setUserRole(r);
		return u;
	}

	static private final List<UserRole> USER_ROLES;

	static private final UserRole ADMIN_ROLE;
	static private final UserRole USER_ROLE;
	static {
		UserRole r = new UserRole();
		r.setId(1L);
		r.setUserName("ADMIN");
		r.setUserRole(UserRoleSpecification.ROLE_ADMIN);
		r.setUserRoleSpecString(UserRoleSpecification.ROLE_ADMIN.name());
		ADMIN_ROLE = r;

		r = new UserRole();
		r.setId(1L);
		r.setUserName("USER");
		r.setUserRole(UserRoleSpecification.ROLE_USER);
		r.setUserRoleSpecString(UserRoleSpecification.ROLE_USER.name());
		USER_ROLE = r;

		USER_ROLES = Arrays.asList(ADMIN_ROLE, USER_ROLE);
	}

	@Override
	public List<User> getUsers() {
		return Collections.unmodifiableList(allUsers);
	}

	@Override
	public void addUser(User user, UserRole userRole) {
		user.setUserRole(userRole);
		allUsers.add(user);
	}

	@Override
	public boolean isUserAnAdmin(String loggedInUser) {
		return ADMIN_NAMES.stream().filter(s -> s.equalsIgnoreCase(loggedInUser)).findFirst().isPresent();
	}

	@Override
	public void addDeleteJobsOfPluginResponseList(Consumer<Plugin> c) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean supportsVisitListOfEntities() {
		return false;
	}

	@Override
	public <T> Optional<T> visitListOfEntities(Class<? extends T> tclass, Predicate<T> predicate) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Plugin findHighestVersionPlugin(String pluginName) {
		Plugin result = null;
		Predicate<Plugin> predicate = (p) -> pluginName.equals(p.getPluginName());

		List<Plugin> allPlugins = getPlugins();
		List<Plugin> plugins = allPlugins.stream().filter(predicate).collect(Collectors.toList());

		if (!plugins.isEmpty()) {
			if (plugins.size() > 1) {
				// There is more than one and we want the most recent
				Collections.sort(plugins, REVERSE_VERSION_NUMBER);
			}
			result = plugins.get(0);
		}
		return result;
	}

	@Override
	public void removeJobsOfPlugin(Plugin plugin) {
		Map<Long, Object> recordMap = getRecordMap(Plugin.class);
		if (!Check.isEmpty(recordMap)) {
			Set<Long> toRemove = recordMap.entrySet().stream().filter(e -> ((Plugin) e.getValue()).equals(plugin))
					.map(Map.Entry::getKey).collect(Collectors.toSet());
			toRemove.forEach(id -> recordMap.remove(id));
		}
	}

	@Override
	public Plugin findPluginFromPluginNameVersion(PluginNameVersion pluginNameVersion) {
		Plugin result = null;
		Predicate<Plugin> predicate = (p) -> pluginNameVersion.getPluginName().equals(p.getPluginName())
				&& pluginNameVersion.getPluginVersion().equals(p.getVersion());

		List<Plugin> allPlugins = getPlugins();
		List<Plugin> plugins = allPlugins.stream().filter(predicate).collect(Collectors.toList());

		if (!plugins.isEmpty()) {
			if (plugins.size() != 1) {
				throw new RuntimeException("Found more then one plugin with pluginNameVersion=" + pluginNameVersion);
			}
			result = plugins.get(0);
		}
		return result;
	}

	//@Override
	public void visitFoldersOfAnalysisJobs(Predicate<AnalysisJob> predicate,
			Consumer<Either<Exception, File>> visitor) {
		throw new RuntimeException("NYI");
	}

	@Override
	public void removeAnalysisJob(Long jobid) throws JobNotFoundException, IOException, JobStillActiveException {
		throw new RuntimeException("NYI");
	}

	@Override
	public <T> List<T> getEntityByColumnNameMatch(String columnNameSearch, Class<T> clazz, Object value) {
		throw new RuntimeException("NYI");
	}

	@Override
	public void addCallScript(CallScript callScript) {
		// TODO Auto-generated method stub

	}

	@Override
	public CallScript getCallScriptForPlugin(Plugin plugin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, CallScript> getCallScriptMap() {
		// TODO Auto-generated method stub
		return null;
	}

}
