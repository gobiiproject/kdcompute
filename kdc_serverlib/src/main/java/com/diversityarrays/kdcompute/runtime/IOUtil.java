/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

@SuppressWarnings("nls")
public class IOUtil {

    /*
     * No instances allowed.
     */
    private IOUtil() { }
    
    public static void closeQuietly(InputStream is, OutputStream os) {
        if (is != null) {
            try { is.close(); } catch (IOException ignore) {}
        }
        if (os != null) {
            try { os.close(); } catch (IOException ignore) {}
        }
    }

    public static void closeQuietly(Reader r, Writer w) {
        if (r != null) {
            try { r.close(); } catch (IOException ignore) {}
        }
        if (w != null) {
            try { w.close(); } catch (IOException ignore) {}
        }
    }

    public static String readContents(Reader rdr) throws IOException {
        StringBuilder sb = new StringBuilder();
        try {
            char[] cbuf = new char[8192];
            int nb;
            while (-1 != (nb = rdr.read(cbuf))) {
                sb.append(cbuf, 0, nb);
            }
        }
        finally {
            closeQuietly(rdr, null);
        }
        return sb.toString();
    }

    public static String readContentLines(Reader rdr) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;        
        try {
            br = new BufferedReader(rdr);
            String line;
            while (null != (line = br.readLine())) {
                sb.append(line).append('\n');
            }
        }
        finally {
            closeQuietly(br, null);
        }
        return sb.toString();
    }

    public static boolean writeFileContents(File file, String content) throws IOException {
        FileWriter fw = new FileWriter(file);
        boolean success = false;
        try {
            fw.write(content);
            success = true;
        }
        finally {
            try { fw.close(); } catch (IOException ignore) {
                success = false;
            }
            
            if (! success) {
                file.delete();
            }
        }
        return success;
    }

    public static String getStackTrace(Throwable error) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        pw.println("Error: " + error.getClass().getName());
        error.printStackTrace(pw);
        pw.close();
        return sw.toString();
    }

    public static void ensureDirectoryOrIOException(File dir) throws IOException {
        if (dir.exists()) {
            if (! dir.isDirectory()) {
                throw new IOException("Not a directory: " + dir.getPath());
            }
        }
        else if (! dir.mkdir()) {
            throw new IOException("Unable to create: " + dir.getPath());
        }
    }

    public static void recursivelyRemove(File file) {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                recursivelyRemove(f);
            }
        }
        file.delete();
        System.err.println("Deleted " + file.getPath());
    }
}
