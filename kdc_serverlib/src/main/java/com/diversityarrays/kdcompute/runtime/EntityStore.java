/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.EntityTransaction;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.JobStillActiveException;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginGroup;
import com.diversityarrays.kdcompute.db.PluginNameVersion;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.db.User;
import com.diversityarrays.kdcompute.db.UserRole;
import com.diversityarrays.kdcompute.runtime.jobscheduler.CallScript;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobNotFoundException;

public interface EntityStore extends KdcomputeManagerProvider {

	public static final int LATEST_PLUGIN_VERSION = -1;

	static public class RunStateChoice {

		static public RunStateChoice create(String desc, RunState... runStates) {
			if (runStates == null || runStates.length <= 0) {
				throw new IllegalArgumentException("null or empty runStates");
			}
			return new RunStateChoice(desc, runStates);
		}

		public final RunState[] runStates;
		private final String description;
		private Object[] runStateNames;

		private RunStateChoice(String d, RunState... array) {
			description = d;
			runStates = array;
		}

		private RunStateChoice(String d, List<RunState> list) {
			description = d;
			if (list == null || list.isEmpty()) {
				runStates = null;
			} else {
				runStates = list.toArray(new RunState[list.size()]);
			}
		}

		public String getDescription() {
			return description;
		}

		public boolean isEmpty() {
			return runStates.length <= 0;
		}

		public Object[] getRunStateNames() {
			if (runStateNames == null) {
				if (runStates == null) {
					runStateNames = new Object[0];
				} else {
					runStateNames = new Object[runStates.length];
					for (int i = runStates.length; --i >= 0;) {
						runStateNames[i] = runStates[i].name();
					}
				}
			}
			return runStates;// runStateNames;
		}
	}

	static public RunStateChoice ALL_JOBS = new RunStateChoice("All Jobs", (RunState[]) null);

	static public RunStateChoice COMPLETED_JOBS = new RunStateChoice("Completed Jobs", RunState.COMPLETED);

	static public RunStateChoice ACTIVE_JOBS = new RunStateChoice("Active Jobs",
			Arrays.asList(RunState.values()).stream().filter(RunState::isActive).collect(Collectors.toList()));

	static public RunStateChoice INACTIVE_JOBS = new RunStateChoice("Inactive Jobs",
			Arrays.asList(RunState.values()).stream().filter(rs -> !rs.isActive()).collect(Collectors.toList()));

	void setJobQueue(JobQueue jobQ);

	String getDatabaseVersion();

	/**
	 * Make an instance managed and persistent.
	 */
	<T> T persist(T record);

	/**
	 * Merge the state of the given entity into the current persistence context.
	 *
	 * @return the managed instance that the state was merged to
	 */
	<T> T merge(T record);

	/**
	 * Refresh the state of the instance from the database, overwriting changes made
	 * to the entity, if any.
	 */
	void refresh(Object record);

	/**
	 * Remove from the database.
	 *
	 * @param record
	 */
	void remove(Object record);

	<T> T getById(Class<T> rClass, Long id);

	List<PluginGroup> getPluginGroups();

	List<Plugin> getPlugins();

	List<User> getUsers();

	void addUser(User user, UserRole userRole);

	Throwable doTransaction(Consumer<EntityTransaction> worker);

	boolean isInMemory();

	PluginGroup getPluginGroupBy(String groupName);

	/**
	 * Return true if the supplied Algorithm is in use by any RunBindings. If it is
	 * then any UI's must not allow any changes unless the version number is
	 * incremented.
	 *
	 * @param plugin
	 * @return
	 */
	boolean isPluginUsedInAnyRunBindings(Plugin plugin);

	List<String> getListOfPluginNames();

	List<AnalysisJob> getAnalysisJobs(RunStateChoice runStateChoice);

	List<AnalysisJob> getAllAnalysisJobs();

	/**
	 * Return true if the method <code>visitListOfEntities</code> is supported.
	 * Note: this is only there for testing because we don't want to do all of the
	 * Criteria support code.
	 *
	 * @return boolean
	 */
	boolean supportsVisitListOfEntities();

	/**
	 * Return the first T for which predicate tests true
	 *
	 * @param tclass
	 * @param predicate
	 * @return
	 */
	<T> Optional<T> visitListOfEntities(Class<? extends T> tclass, Predicate<T> predicate);

	boolean isUserAnAdmin(String loggedInUser);

	/**
	 * When a plugin is deleted from KDCompute, all it's assosiated
	 * analysisJobs/requests/joins must go too. Actions can be added to this list
	 *
	 * @return
	 */
	void addDeleteJobsOfPluginResponseList(Consumer<Plugin> consumer);

	void removeJobsOfPlugin(Plugin plugin);

	Plugin findPluginFromPluginNameVersion(PluginNameVersion pluginNameVersion);

	Plugin findHighestVersionPlugin(String pluginName);

	// Commented out because I can find no usages (brianp)
	// void visitFoldersOfAnalysisJobs(Predicate<AnalysisJob> predicate,
	// Consumer<Either<Exception, File>> visitor);

	void removeAnalysisJob(Long jobid) throws JobNotFoundException, IOException, JobStillActiveException;

	<T> List<T> getEntityByColumnNameMatch(String columnNameSearch, Class<T> clazz, Object value);

	void addCallScript(CallScript callScript);

	CallScript getCallScriptForPlugin(Plugin plugin) throws ScriptNotSupportedException;

	Map<String, CallScript> getCallScriptMap();
}
