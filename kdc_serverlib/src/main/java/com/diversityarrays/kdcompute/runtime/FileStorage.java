/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.File;
import java.io.IOException;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginNameVersion;

public interface FileStorage {

	public void setResultsRootDir(File d);

	public void setKdcomputePluginsDir(File d);

	public File getKdcomputePluginsDir();

	public File getPluginsDir();

	/**
	 * Return the Directory assigned for the given username.
	 * 
	 * @param userName
	 * @return File that is a directory
	 * @throws IOException
	 */
	File getUserDir(String userName) throws IOException;

	/**
	 * Return the Directory for SubmittedJobs for the user.
	 * 
	 * @param userName
	 * @return directory
	 * @throws IOException
	 */
	File getSubmittedJobsDir(String userName) throws IOException;

	/**
	 * Return a new folder (that hasn't existed before) in the working folder
	 * for the specified user.
	 * 
	 * @param forUser
	 * @param jobId
	 * @return a File that is a directory.
	 * @throws IOException
	 */
	File getNewResultsFolder(String forUser, long jobId) throws IOException;

	/**
	 * Return the Temp directory assigned for the given username; for the
	 * specified user.
	 * 
	 * @param userName
	 * @return a File that is a directory.
	 * @throws IOException
	 */

	File getUserTempDir(String userName) throws IOException;

	/**
	 * Return a folder for the instance of the algorithm. The folder should be
	 * in the resultsFolder and the name of the folder should be derived from
	 * the algorithm name and the index.
	 * 
	 * @param resultsFolder
	 * @param algorithmName
	 * @param index
	 *            if non-null, append this to the folder name
	 * @return File
	 */
	File getPluginInstanceFolder(File resultsFolder, String algorithmName, Integer index) throws IOException;

	/**
	 * Retrieve the file for the
	 * 
	 * @param alg
	 * @param path
	 * @return
	 */
	File getTemplateFile(Plugin alg, String path) throws IOException;

	/**
	 * Return the folder that contains all of the core Algortihm information
	 * 
	 * @param algorithm
	 * @return File
	 * @throws IOException
	 */
	File getPluginFolder(Plugin plugin) throws IOException;

	/**
	 * Remove the specified file from the Algorithms folder.
	 * 
	 * @param filename
	 * @throws IOException
	 */
	void removePluginFile(Plugin algorithm, String filename) throws IOException;

	/**
	 * Return the folder containing the legacy "algorithms" sub-folder.
	 * 
	 * @return File
	 */
	File getLegacyPluginsFolder();

	public boolean isPathForUser(String path, String userName);

	public static String makeFileSafe(String filename) {
		return filename.replaceAll("[^a-zA-Z0_9_]", "_");
	}

	public File getDropinPluginsDir();

	File getPluginFolder(PluginNameVersion pluginNameVersion);

	public File getUserStorage();

	public File getPluginFolderAsStaged(PluginNameVersion pluginNameVersion) throws IOException;

}
