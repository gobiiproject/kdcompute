/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CallScript {
	String COMMAND_SEP = " && ";
	String TMPL = "tmpl";

	/**
	 * @param scriptFile
	 * @param workingDirectory
	 * @param stdoutFile
	 * @param stderrFile
	 * @param timeout_milliseconds
	 * @param enviromentVariables
	 * @return Exit code
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws StderrException
	 * @throws Exception
	 */
	public int call(File scriptFile, File workingDirectory, File stdoutFile, File stderrFile, long timeout_milliseconds,
			Map<String, String> enviromentVariables)
			throws IOException, InterruptedException, StderrException, Exception;

	public List<String> getSupportedExtensions();

}
