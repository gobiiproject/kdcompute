/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.diversityarrays.jobscheduler.Job;
import com.diversityarrays.jobscheduler.slurm.SlurmController;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.JobQueue;

public class SlurmJobScheduler extends JobScheduler {

	static private final FileFilter SLURM_ERR_FILTER = new FileFilter() {
		@Override
		public boolean accept(File file) {
			String loname = file.getName().toLowerCase();
			return loname.startsWith("slurm") && loname.endsWith(".err");
		}
	};

	static private final FileFilter SLURM_OUT_FILTER = new FileFilter() {
		@Override
		public boolean accept(File file) {
			String loname = file.getName().toLowerCase();
			return loname.startsWith("slurm") && loname.endsWith(".out");
		}
	};

	private JobQueue jobQueue = null;

	private SlurmController slurmController;

	private Map<Long, AnalysisJob> active_jobs;

	public SlurmJobScheduler(JobQueue jobQueue) {
		this.jobQueue = jobQueue;
		this.slurmController = new SlurmController();
		this.active_jobs = new HashMap<>();
	}

	public void setJobQueue(JobQueue q) {
		this.jobQueue = q;
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			/*
			 * Compare and update active_jobs
			 */
			List<Job> slurm_jobs = null;
			try {
				slurm_jobs = slurmController.squeue();
			} catch (Exception e2) {
				Logger.getGlobal().log(Level.SEVERE, "Slurm Error", e2);
			}
			for (Map.Entry<Long, AnalysisJob> active_job : active_jobs.entrySet()) {
				Long slurm_jobid = active_job.getKey();

				AnalysisJob analysis_job = active_job.getValue();

				/*
				 * Check if our active job is in the queue list. If not, it's no
				 * longer running: either COMPLETED or FAILED
				 */
				boolean found = false;
				for (Job slurm_job : slurm_jobs) {
					if (slurm_job.getId() == slurm_jobid) {
						found = true;
					}
				}
				if (!found) {
					active_jobs.remove(slurm_jobid);
					analysis_job.setRunState(RunState.COMPLETED);
					try {
						jobQueue.jobStatusChanged(analysis_job);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			/*
			 * Check we have a free node
			 */
			String free_node_name = null;
			try {
				free_node_name = slurmController.getFreePartition();
			} catch (JobSchedulerError e1) {
				Logger.getGlobal().log(Level.SEVERE, "Slurm Error", e1);
				return;
			}

			if (free_node_name == null) {
				continue;
			}

			/*
			 * Check for new jobs
			 */
			try {
				AnalysisJob job = jobQueue.takeNextJob();
				if (job == null) {
					Thread.sleep(1000);
					continue;
				}
				System.out.println("SlurmJobScheduler has taken job:\n" + job);

				long slurm_jobid = slurmController.sbatch(job.getShellFilePath()); // sbatch
																					// to
																					// any
																					// node
				if (active_jobs.containsKey(slurm_jobid)) {
					throw new Exception(
							"SLURM returned jobid (" + Long.toString(slurm_jobid) + " which is already registered...\n"
									+ "Exisitng:\n" + active_jobs.get(slurm_jobid) + "\n" + "New:\n" + job);
				} else {
					job.setRunState(RunState.RUNNING);
					jobQueue.jobStatusChanged(job);
					active_jobs.put(slurm_jobid, job);
				}
			} catch (Exception e) {

			}
		}
		Logger.getGlobal().log(Level.INFO, "SlurmJobScheduler loop ended");
	}

	@Override
	public File getStdout_file(File dir) {
		File[] files = dir.listFiles(SLURM_OUT_FILTER);
		if (files.length > 0) {
			return files[0];
		} else {
			return null;
		}
	}

	@Override
	public File getStderr_file(File dir) {
		File[] files = dir.listFiles(SLURM_ERR_FILTER);
		if (files.length > 0) {
			return files[0];
		} else {
			return null;
		}
	}

	@Override
	public void cancelJob(AnalysisJob job, Consumer<AnalysisJob> callBack) throws JobNotFoundException {
		// TODO Auto-generated method stub
		throw new RuntimeException("NYI");
	}

}
