/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.io.FilenameUtils;
import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.AnalysisJobRunner;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.script.AdditionalEnvironmentVariables;

public class SimpleJobScheduler extends JobScheduler {

	private static final int TIMEOUT_YEAR_IN_MILLISECONDS = 365 * 24 * 60 * 60 * 1000;
	private JobQueue jobQueue;
	private final String stdout_filename;
	private final String stderr_filename;
	private Map<String, CallScript> callScripts; // Extension to Callscript
	private long timeout_milliseconds;
	private final ExecutorService service;
	private final Map<Long, Future<?>> runningJobMap = new ConcurrentHashMap<>();

	private final Function<File, Optional<CallScript>> callScriptProvider = new Function<File, Optional<CallScript>>() {
		@Override
		public Optional<CallScript> apply(File scriptFile) {
			String extension = FilenameUtils.getExtension(scriptFile.getName());
			CallScript callScript = null;

			if (extension.isEmpty()) {
				return Optional.empty();
			} else {
				callScript = callScripts.get(extension);
				if (callScript == null) {
					return Optional.empty();
				}
				return Optional.of(callScript);
			}
		}
	};

	private final Function<File, File> stdoutProvider = new Function<File, File>() {
		@Override
		public File apply(File dir) {
			return getStdout_file(dir);
		}
	};

	private final Function<File, File> stderrProvider = new Function<File, File>() {
		@Override
		public File apply(File dir) {
			return getStderr_file(dir);
		}
	};
	private final AnalysisJobRunner analysisJobRunner;
	private final Function<AnalysisJob, Exception> statusChangeConsumer = new Function<AnalysisJob, Exception>() {
		@Override
		public Exception apply(AnalysisJob job) {
			Exception result = null;
			try {
				jobQueue.jobStatusChanged(job);
			} catch (Exception e) {
				result = e;
			}
			return result;
		}
	};

	private SimpleJobScheduler(JobQueue jobQueue, String stdout_filename, String stderr_filename,
			long timeout_milliseconds, AdditionalEnvironmentVariables additionalEnvironmentVariables, int nthreads) {
		super();
		this.jobQueue = jobQueue;
		this.stdout_filename = stdout_filename;
		this.stderr_filename = stderr_filename;
		this.callScripts = new HashMap<>();
		this.timeout_milliseconds = timeout_milliseconds;
		this.analysisJobRunner = new AnalysisJobRunner(callScriptProvider, stdoutProvider, stderrProvider,
				statusChangeConsumer, additionalEnvironmentVariables);
		this.service = Executors.newFixedThreadPool(nthreads);
		Logger.getLogger(SimpleJobScheduler.class.getSimpleName()).log(Level.INFO,
				"Simple Job Scheduler running with " + nthreads + " threads");
	}

	// Used by Test
	public SimpleJobScheduler(JobQueue jobQueue, String stdout_filename, String stderr_filename,
			AdditionalEnvironmentVariables additionalEnvironmentVariables) {
		this(jobQueue, stdout_filename, stderr_filename, TIMEOUT_YEAR_IN_MILLISECONDS, additionalEnvironmentVariables,
				Thread.activeCount());
	}

	// Used by KdcServerApplication
	public SimpleJobScheduler(JobQueue djobQueue, AdditionalEnvironmentVariables additionalEnvironmentVariables) {
		this(djobQueue, AnalysisJobRunner.STDOUT_FILENAME, AnalysisJobRunner.STDERR_FILENAME,
				TIMEOUT_YEAR_IN_MILLISECONDS, additionalEnvironmentVariables, Thread.activeCount());
	}

	public SimpleJobScheduler(JobQueue djobQueue, AdditionalEnvironmentVariables additionalEnvironmentVariables,
			int nThreads) {
		this(djobQueue, AnalysisJobRunner.STDOUT_FILENAME, AnalysisJobRunner.STDERR_FILENAME,
				TIMEOUT_YEAR_IN_MILLISECONDS, additionalEnvironmentVariables, nThreads);
	}

	public void setCallScriptMap(Map<String, CallScript> callScripts) {
		this.callScripts = callScripts;
	}

	@Override
	public void setJobQueue(JobQueue q) {
		this.jobQueue = q;
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {

			AnalysisJob job = jobQueue.takeNextJob();
			if (job == null) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return;
				}
				continue;
			}
			synchronized (runningJobMap) {
				runningJobMap.put(job.getId(), service.submit(new Runnable() {

					@Override
					public void run() {
						Logger.getLogger(SimpleJobScheduler.class.getSimpleName()).log(Level.INFO,
								"Staring job " + job.getId());
						analysisJobRunner.runJob(job, timeout_milliseconds, "Please report to plugin developer");
						synchronized (runningJobMap) {
							runningJobMap.remove(job.getId());
						}
					}

				}));
			}

		}
	}

	@Override
	public File getStdout_file(File dir) {
		return new File(dir, stdout_filename);
	}

	@Override
	public File getStderr_file(File dir) {
		return new File(dir, stderr_filename);
	}

	@Override
	public void cancelJob(AnalysisJob job, Consumer<AnalysisJob> callBack) throws JobNotFoundException {
		Future<?> future = runningJobMap.get(job.getId());
		if (future == null) {
			throw new JobNotFoundException();
		}
		new Runnable() {

			@Override
			public void run() {
				future.cancel(true);
				runningJobMap.remove(job.getId());
				job.setRunState(RunState.CANCELLED);
				job.setEndDateTime(new Date());
				statusChangeConsumer.apply(job);
				callBack.accept(job);
			}
		}.run();
	}

}
