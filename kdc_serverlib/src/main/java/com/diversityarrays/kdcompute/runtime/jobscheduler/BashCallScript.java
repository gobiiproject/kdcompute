/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

public class BashCallScript implements CallScript {

	@Override
	public int call(File script, File workingDirectory, File stdoutFile, File stderrFile, long timeout_milliseconds,
			Map<String, String> enviromentVariables)
			throws IOException, InterruptedException, StderrException, Exception {
		String[] cmd = { "bash " + script.getAbsolutePath() };

		/*
		 * Add additional environment variables, such as the DALCOOKIE
		 */
		List<String> jobCommand = new ArrayList<>();
		for (Map.Entry<String, String> envVar : enviromentVariables.entrySet()) {
			String var = envVar.getKey();
			String value = envVar.getValue();
			jobCommand.addAll(Arrays.asList("export", var + "=\'" + value + "\'"));
			jobCommand.add(COMMAND_SEP);
		}

		jobCommand.addAll(Arrays.asList(cmd));

		/*
		 * Run the command, capturing the variable $DALCOOKIE in an echo
		 */

		List<String> jobCommandList = Arrays.asList("bash", "-c", String.join(" ", jobCommand));
		ProcessBuilder pb = new ProcessBuilder(jobCommandList);
		pb.directory(workingDirectory);

		pb.redirectOutput(stdoutFile);
		pb.redirectError(stderrFile);
		Process process = null;
		try {
			process = pb.start();
			String cmdJoined = String.join(" ", jobCommandList);

			if (!process.waitFor(timeout_milliseconds, TimeUnit.MILLISECONDS)) { // 1
				// day
				// timeout,
				// for
				// now
				process.destroy();
				throw new InterruptedException("Command " + cmdJoined + " timedout");
			}

		} catch (InterruptedException e) {
			process.destroy();
			FileUtils.write(stderrFile, "\nJob Interrupted, process PID: " + getPidOfProcess(process),
					Charset.defaultCharset(), true);
			throw e;
		}
		return process.exitValue();
	}

	@Override
	public List<String> getSupportedExtensions() {
		return Arrays.asList("sh");
	}

	public static synchronized long getPidOfProcess(Process p) {
		long pid = -1;

		try {
			if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
				Field f = p.getClass().getDeclaredField("pid");
				f.setAccessible(true);
				pid = f.getLong(p);
				f.setAccessible(false);
			}
		} catch (Exception e) {
			pid = -1;
		}
		return pid;
	}

}
