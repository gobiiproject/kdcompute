/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import java.io.File;
import java.util.function.Consumer;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.runtime.JobQueue;

/**
 * Responsible for monitoring a JobQueue and scheduling actual processes on
 * compute nodes. IT MUST ALSO MONITOR THE STATE OF EACH IF THE RUNNING JONS (EG
 * VIA SQUEUE) and update the status of the jobs in the JobQueue and/r the
 * database (entityStore).
 * 
 * @author andrew
 *
 */

public abstract class JobScheduler extends Thread {

	public abstract File getStdout_file(File dir);

	public abstract File getStderr_file(File dir);

	public abstract void setJobQueue(JobQueue q);

	public void cancelJob(AnalysisJob job) throws JobNotFoundException {
		cancelJob(job, new Consumer<AnalysisJob>() {

			@Override
			public void accept(AnalysisJob t) {

			}
		});
	}

	public abstract void cancelJob(AnalysisJob job, Consumer<AnalysisJob> callBack) throws JobNotFoundException;

}
