/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.util.List;

//import javax.persistence.EntityManager;

import org.greenrobot.eventbus.EventBus;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunState;

public class JobProcessor implements Runnable {

    /**
     * Used to communicate when something happens to an AnalysiJobRun
     * @author brianp
     *
     */
    static public class JobProcessorEvent {

        public final AnalysisJob job;

        public JobProcessorEvent(AnalysisJob job) {
            this.job = job;
        }
    }

    private final JobQueue jobQueue;
    private final EntityStore entityManager;


    private final Thread thread;

    private boolean keepRunning;

    public JobProcessor(JobQueue jq, EntityStore em) {
        jobQueue = jq;
        entityManager = em;

        thread = new Thread(this);
        thread.setDaemon(true);

        keepRunning = true;
        thread.start();
    }

    public void stopRunning() {
        keepRunning = false;
        thread.interrupt();
    }

    @Override
    public void run() {
        while (keepRunning) {
            try {
                AnalysisJob job = jobQueue.takeNextJob(5000);
                if (job == null) {
                    EventBus.getDefault().post(new JobProcessorEvent(null));
                    continue;
                }
                AnalysisRequest analysisJob = job.getAnalysisRequest();
                List<Plugin> algorithms = analysisJob.getAlgorithms();
                StringBuilder sb = new StringBuilder("Running ");
                sb.append(" for ").append(analysisJob.getForUser());
                if (1 == algorithms.size()) {
                    sb.append(algorithms.get(0).getAlgorithmName());
                }
                else {
                    sb.append(algorithms.size()).append(" algorithms");
                }
                System.out.println(sb.toString());

                job.setRunState(RunState.RUNNING);
                entityManager.refresh(job);
                EventBus.getDefault().post(new JobProcessorEvent(job));

                try {
                    Thread.sleep(5000);

                    job.setRunState(RunState.COMPLETED);
                    entityManager.refresh(job);
                }
                catch (InterruptedException e) {
                    job.setError("interrupted while running job");
                    entityManager.refresh(job);
                }
                finally {
                    EventBus.getDefault().post(new JobProcessorEvent(job));
                }
            }
            catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
    }

    public int getJobCount() {
        return jobQueue.getQueueLength();
    }
}
