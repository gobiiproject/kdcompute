/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import com.diversityarrays.kdcompute.db.KdcDatabaseVersion;

//TODO create all static level additions.

public class KDCDbUtil {

	private final static Map<Class<?>, Field> fieldByClass = new HashMap<>();

	/**
	 *
	 * @param idColumnName:
	 *            id column Name of the table.
	 * @param idValue
	 *            : id value of the
	 * @param clazz
	 *            : Class of the Entity to be searched for
	 */

	public static <T> T searchEntityById(EntityManager manager, Long idValue, Class<T> clazz) {

		T entity = manager.find(clazz, idValue);
		return entity;
	}

	public static <T> List<T> getAllEntities(EntityStore store, Class<T> clazz) {
		List<T> result = new ArrayList<>();
		if (store.supportsVisitListOfEntities()) {
			store.visitListOfEntities(clazz, (t) -> {
				result.add(t);
				return false;
			}); // false coz we want to get them all
		} else {
			String cmd = String.format("select a from %s a", clazz.getSimpleName());
			TypedQuery<T> qry = store.getEntityManager().createQuery(cmd, clazz);

			List<T> list = qry.getResultList();
			if (list != null) {
				result.addAll(list);
			}
		}
		return result;
	}

	/**
	 * Find id field of the class
	 *
	 * @param clazz
	 *            : Class type of the class to be searched for.
	 * @return: Entity found
	 */

	@SuppressWarnings("unused")
	private static <T> Field getIdFieldName(Class<T> clazz) {
		Field idField = fieldByClass.get(clazz);
		if (idField == null) {
			for (Field f : clazz.getDeclaredFields()) {
				if (Modifier.isStatic(f.getModifiers())) {
					continue;
				}
				Id id = f.getAnnotation(Id.class);
				if (id != null) {
					if (idField != null) {
						throw new RuntimeException("Multiple @Id annotations in " + clazz.getName());
					}
					idField = f;
				}
			}
			if (idField == null) {
				throw new RuntimeException("No @Id field in " + clazz.getName());
			}
			if (Long.class != idField.getType()) {
				throw new RuntimeException("@Id " + clazz.getName() + "." + idField.getName() + " is not a Long");
			}
			idField.setAccessible(true);
			fieldByClass.put(clazz, idField);
		}
		return idField;
	}


	/**
	 *
	 * @param manager
	 *            : EntityManager.
	 * @param columnNameSearch
	 *            : ColumnName to be search for.
	 * @param clazz
	 *            : Class type.
	 * @param value
	 *            : Value to be searched for.
	 * @return : List of entities: if found any or empty list, never null;
	 */
	public static <T> List<T> getEntityByColumnNameMatch(
	        EntityManager manager,
	        String columnName,
	        Class<T> clazz,
			Object ... values)
	{
	    if (values == null || values.length <= 0) {
	        Logger.getLogger(KDCDbUtil.class.getSimpleName()).log(Level.WARNING, "No values provided");
	        return Collections.emptyList();
	    }
		List<T> entities = new ArrayList<>();

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(clazz);

		Root<T> root = criteria.from(clazz);
		criteria.select(root);

		Expression<Boolean> expr = null;
		for (Object value : values) {
		    if (expr == null) {
		        expr = builder.equal(root.get(columnName), value);
		    }
		    else {
		        expr = builder.or(expr, builder.equal(root.get(columnName), value));
		    }
		}

		criteria.where(expr);

		TypedQuery<T> query = manager.createQuery(criteria);
		List<T> list = query.getResultList();

		if (list != null) {
			entities.addAll(list);
		}

		return entities;

	}

	public static String checkDatabaseVersionIfExist(EntityStore store) {
		// Never be null;
		KdcDatabaseVersion versionString = getAllEntities(store, KdcDatabaseVersion.class).get(0);
		String version = null;

		version = versionString.getVersionString();

		return version;

	}

}
