/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;

import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginNameVersion;

@SuppressWarnings("nls")
public class SimpleFileStorage implements FileStorage {

	static public final String PLUGINS_DIRNAME = "plugins";
	static public final String LEGACY_DIRNAME = "legacy";
	static public final String DROPIN_DIRNAME = "dropin";

	private File resultsRoot;
	private File kdcomputePlugins;
	private File pluginsDir;
	private File dropinPluginsDir;

	public SimpleFileStorage() {
	}

	@Override
	public File getPluginsDir() {
		return pluginsDir;
	}

	@Override
	public void setResultsRootDir(File d) {
		this.resultsRoot = d;
		try {
			IOUtil.ensureDirectoryOrIOException(resultsRoot);
		} catch (IOException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	@Override
	public void setKdcomputePluginsDir(File d) {
		kdcomputePlugins = d;
		pluginsDir = new File(kdcomputePlugins, PLUGINS_DIRNAME);
		dropinPluginsDir = new File(kdcomputePlugins, DROPIN_DIRNAME);
		try {
			IOUtil.ensureDirectoryOrIOException(pluginsDir);
		} catch (IOException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	public void report(PrintStream out) {
		out.println();
		out.println("***** ***** ***** ***** ***** ***** ***** ***** ***** *****");
		out.println("***** ***** ***** " + this.getClass().getName());
		out.println();

		out.println("Results Directory");
		reportDirContent(out, resultsRoot, "");

		out.println("Algorithms Directory");
		reportDirContent(out, pluginsDir, "");
	}

	private void reportDirContent(PrintStream out, File dir, String depth) {

		String down = "  " + depth;
		out.println(depth + "/" + dir.getName());
		List<File> dirs = new ArrayList<>();

		for (File file : dir.listFiles()) {
			if (file.isDirectory()) {
				dirs.add(file);
			} else {
				out.println(down + file.getName());
			}
		}

		for (File d : dirs) {
			reportDirContent(out, d, down);
		}
	}

	@Override
	public File getUserDir(String userName) throws IOException {
		File result = new File(resultsRoot, userName);
		IOUtil.ensureDirectoryOrIOException(result);
		return result;
	}

	@Override
	public File getUserTempDir(String userName) throws IOException {
		File tempDir = new File(getUserDir(userName), "temp");
		IOUtil.ensureDirectoryOrIOException(tempDir);
		return tempDir;
	}

	@Override
	public File getNewResultsFolder(String userName, long jobId) throws IOException {
		String filename = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File submittedJobs = getSubmittedJobsDir(userName);
		File result = new File(submittedJobs, filename + "_" + jobId);
		IOUtil.ensureDirectoryOrIOException(result);
		return result;
	}

	@Override
	public File getSubmittedJobsDir(String userName) throws IOException {
		File userDir = getUserDir(userName);
		File submittedJobs = new File(userDir, "SubmittedJobs");
		if (!submittedJobs.isDirectory()) {
			if (!submittedJobs.mkdir()) {
				throw new IOException("Unable to create " + submittedJobs.getPath());
			}
		}
		return submittedJobs;
	}

	@Override
	public File getPluginInstanceFolder(File resultsFolder, String pluginName, Integer index) throws IOException {
		StringBuilder sb = new StringBuilder(pluginName);
		if (index != null) {
			sb.append('_').append(index);
		}

		String filename = sb.toString();
		filename = FileStorage.makeFileSafe(filename);

		File result = new File(resultsFolder, filename);
		IOUtil.ensureDirectoryOrIOException(result);
		return result;
	}

	@Override
	public File getTemplateFile(Plugin plugin, String filename) throws IOException {
		// String pluginName = alg.getPluginName();
		// if (Check.isEmpty(pluginName)) {
		// throw new IOException("Algorithm '" + alg.getAlgorithmName() + "'
		// does not have a pluginName");
		// }
		File algFolder = getPluginFolder(plugin);
		if (!algFolder.exists()) {
			throw new IOException("Missing algortihm folder: " + algFolder.getPath());
		}
		return new File(algFolder, filename);
	}

	@Override
	public File getPluginFolder(Plugin plugin) throws IOException {
		PluginNameVersion pluginNameVersion = plugin.getPluginNameVersion();
        File result = pluginNameVersion.getPluginFolder(pluginsDir);
		if (result == null) {
		    throw new IOException("Missing directory for Plugin: " + pluginNameVersion);
		}
		IOUtil.ensureDirectoryOrIOException(result);
		return result;
	}

	/**
	 * Get the folder for the specified Algorithm but do NOT ensure that it
	 * exists.
	 *
	 * @param plugin
	 * @return
	 * @throws IOException
	 */
	private File getPluginFolderInternal(Plugin plugin) {
		String folderName;
		if (plugin.isLegacy()) {
			folderName = ".." + File.separator + LEGACY_DIRNAME + File.separator + plugin.getPluginName();
		} else {
			folderName = plugin.getPluginName() + "-v" + plugin.getVersion();
		}
		File result = new File(pluginsDir, folderName);
		return result;
	}

	@Override
	public void removePluginFile(Plugin plugin, String filename) throws IOException {
		File folder = getPluginFolderInternal(plugin);
		if (folder.exists() && folder.isDirectory()) {
			File file = new File(folder, filename);
			file.delete();
		}
	}

	@Override
	public File getLegacyPluginsFolder() {
		return new File(kdcomputePlugins, LEGACY_DIRNAME);
	}

	@Override
	public boolean isPathForUser(String path, String userName) {
		return path.contains(File.separator + userName + File.separator);
	}

	@Override
	public File getKdcomputePluginsDir() {
		return kdcomputePlugins;
	}

	@Override
	public File getDropinPluginsDir() {
		return dropinPluginsDir;
	}

	@Override
	public File getPluginFolder(PluginNameVersion pluginNameVersion) {
		return pluginNameVersion.getPluginFolder(pluginsDir);
	}

	@Override
	public File getUserStorage() {
		return resultsRoot;
	}

	@Override
	public File getPluginFolderAsStaged(PluginNameVersion pluginNameVersion) throws IOException {
		File asPluginDir = new File(pluginsDir, pluginNameVersion.toString());
		if (pluginNameVersion.getPluginVersion() == null) {
			// Check if in dropin
			File asDropIn = new File(dropinPluginsDir, pluginNameVersion.toString());
			if (asDropIn.exists()) {
				FileUtils.moveDirectory(asDropIn, asPluginDir);
				return asPluginDir;
			} else {
				// Might be in staged but already renamed with version, find
				// highest version

				List<File> asList = Arrays.asList(pluginsDir.listFiles());
				List<File> collect = asList.stream()
						.filter(e -> e.getName().contains(pluginNameVersion.getPluginName())).sorted()
						.collect(Collectors.toList());
				Collections.reverse(collect);
				if (!collect.isEmpty()) {
					return collect.get(0);
				}
			}
		} else if (asPluginDir.exists()) {
			return asPluginDir;
		}
		return null;
	}

}
