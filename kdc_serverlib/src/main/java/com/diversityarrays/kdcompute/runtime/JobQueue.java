/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import java.time.Duration;
import java.util.List;
import java.util.function.Predicate;

import com.diversityarrays.kdcompute.db.AnalysisJob;

public interface JobQueue {

    /**
     * Return the number of jobs in the queue.
     * @return
     */
    int getQueueLength();

    /**
     * @param analysisJob
     * @throws JobQueueException
     */
    void submitJob(AnalysisJob analysisJob) throws JobQueueException;

    /*
     * Perform an operation of each AnalysisJobRun
     */
    void visitJobs(Predicate<AnalysisJob> visitor);

    /*
     * Used by the JobScheduler
     */
    AnalysisJob takeNextJob();
    AnalysisJob takeNextJob(int millis) throws InterruptedException;

    /*
     * The entity store which JobQueue stores new, processing and finished jobs.
     */
	void setEntityStore(EntityStore entityStore);

	/*
	 * Active jobs are defined as those pending or running
	 */
	List<AnalysisJob> getActiveJobs();

	/*
	 * Used by JobScehduler
	 */
	void jobStatusChanged(AnalysisJob run) throws Exception;

	/*
	 * Active jobs are defined as those pending or running
	 */
	List<AnalysisJob> getRecentJobs(Duration duration);

	List<AnalysisJob> getAllAnalysisJobs();

  }
