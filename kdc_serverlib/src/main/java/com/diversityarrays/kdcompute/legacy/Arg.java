/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.legacy;

import com.diversityarrays.kdcompute.db.DataSetType;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.Plugin;

/**
 * Provides a structure that holds the components
 * of a legacy plugin specification's <code>Args</code>.
 * <p>
 * In the new system, these will be translated into a <code>Knob</code>
 * and added to an <code>Algorithm</code>
 * @see {@link AlgorithmSpec}
 * @see {@link Plugin}
 * @see {@link Knob}
 * @author brianp
 */
public class Arg {
    
    String ArgName;
    String ArgLabel;
    String DataType; // int, file, string
    String HTMLType; // inputbox, file, checkbox, dropdownlist
    String Order; // "1" TODO not sure what this is for
    /**
     * This seems to provide a DAL list command but it doesn't cater
     * for when there could be thousands of responses or provide a search
     * capability
     * e.g. "list/analysisgroup/100/page/1?ctype=json&callback=?"
     *      "list/type/markerdataset/active?ctype=json&callback=?",
     * At the very list it needs to provide for features of the
     * entity that can be used for filtering - or joined ones
     * @see {@link DataSetType}
     */
    String SrcURL; // TODO translate to "KDDArt listable entity"
    String SrcType;  // TODO work out what this is. Values seen dare "local", "external"
    
//    String SrcLocal; // TODO what is this ??? [ { "OptionValue" : "Enable" } ]
    
//    String HTMLSrcDisplay; // TODO again - what is this ???[ { "FieldName" : "AnalysisGroupName", "Separator" : "-" }, { "FieldName" : "AnalysisGroupId", "Separator" : "" } ]

    String HTMLSrcId; // none
    String DefaultValue;
    String Tooltip;
    String TmplVarName;
    String Validation; // DAL style rule: eg. BOOLEX(x>0) ,BOOLEX(x>=0 && x<=1)
    String Required; // 1, 0

    @Override
    public String toString() {
        return "Arg(" + ArgName + ": " + DataType + ")";
    }
}
