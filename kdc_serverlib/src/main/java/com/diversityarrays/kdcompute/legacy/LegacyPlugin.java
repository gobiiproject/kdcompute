/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.legacy;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Provides a POJO that corresponds to a Plugin in the old KDCompute style.
 * These plugins are provided as a folder with:
 * <ul>
 *   <li>
 *     a specification file: <code><i>plugin_name</i>_spec.json</code>
 *     @see {@link AlgorithmSpec}
 *     @see {@link Arg}
 *   </li>
 *   <li>
 *     a bash template: <code><i>plugin_name</i>_bash.tmp</code>
 *     <br>this template file 
 *   </li>
 *   <li>an executable </li>
 *   <li></li>
 *   <li></li>
 * </ul>
 * @see {@link LegacyConverter}
 * @author brianp
 *
 */
@SuppressWarnings("nls")
public class LegacyPlugin {

    static public LegacyPlugin createFromPluginFolder(File pluginFolder) throws IOException {
        String pluginName = pluginFolder.getName();
        File specFile = new File(pluginFolder, getSpecFilepath(pluginName));
        
        Gson gson = new GsonBuilder().create();
        AlgorithmSpec spec = gson.fromJson(new FileReader(specFile), AlgorithmSpec.class);
        
        return new LegacyPlugin(pluginName, spec);
    }

    public final String pluginName;
    
    public final AlgorithmSpec algorithmSpec;
    
    public LegacyPlugin(String name, AlgorithmSpec spec) {
        pluginName = name;
        algorithmSpec = spec;
    }

    static public String getBashTemplateFilepath(String pluginName) {
        return /* pluginName + "/" + */ pluginName + "_bash.tmpl";
    }
    
    static public String getSpecFilepath(String pluginName) {
        return /*pluginName + "/" + */ pluginName + "_spec.json";
    }
}
