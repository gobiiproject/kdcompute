/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.legacy;

import java.util.ArrayList;
import java.util.List;

import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.Plugin;

/**
 * Provides a structure that holds all the components
 * of a legacy plugin specification.
 *  <p>
 * In the new system, these will be translated into an <code>Algorithm</code>
 * and the <code>Args</code> added to the <code>Algorithm</code> as <code>Knob</code>s.
 * @see {@link Arg}
 * @see {@link Plugin}
 * @see {@link Knob}
 * @author brianp
 */
public class AlgorithmSpec {

    public String AlgorithmName;
    public String AlgorithmGroup;
    public String AlgorithmDescription;
    public String AlgorithmHelp;
    public String AlgorithmVersion;
    public String Author;
    public String BashTmplPath;
    public String FormType;
    public String FormTmplPath;
    public String LastUpdate;

    public List<Arg> Args = new ArrayList<>();
    
    @Override
    public String toString() {
        return "AlgorithmSpec(" + AlgorithmName + ": " + Args.size() + " Args, group=" + AlgorithmGroup + ")";
    }
}
