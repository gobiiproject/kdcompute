/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.legacy;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.db.DataSet;
import com.diversityarrays.kdcompute.db.DataSetType;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.helper.InvalidRuleException;
import com.diversityarrays.kdcompute.db.helper.KnobValidationRule;
import com.diversityarrays.kdcompute.db.helper.KnobValidationRule.Range;
import com.diversityarrays.kdcompute.util.Check;

@SuppressWarnings("nls")
public class LegacyConverter {

	static public final String ALGORITHMS_FOLDER_NAME = "algorithms";

	static private final String[] DATE_FORMATS = { "d MMM yyyy", "dd/MM/yyyy", "yyyy/MM/dd", "yyyy-MM-dd", };

	/**
	 * Returns a Pair with the <i>algorithmGroupName</i> and the
	 * <i>Algorithm</i>.
	 * 
	 * @param pluginName
	 * @param spec
	 * @param warnings
	 *            a List in which any warnings will be returned
	 * @return
	 * @throws InvalidRuleException
	 */
	static public Pair<String, Plugin> createFrom(String pluginName, AlgorithmSpec spec, List<String> warnings)
			throws LegacyConversionException {

		// ?? make different versions ?
		int version = 1;
		if (!Check.isEmpty(spec.AlgorithmVersion)) {
			try {
				String[] parts = spec.AlgorithmVersion.split("\\.");
				version = Integer.parseInt(parts[0]);
			} catch (NumberFormatException e) {
			}
		}

		Plugin result = new Plugin(spec.AlgorithmName, version, 0, 0);

		result.setLegacy(true);
		// need to have a numeric "versionCode" that is mon
		result.setPluginName(pluginName);
		result.setVersionString(spec.AlgorithmVersion);
		result.setAuthor(spec.Author);
		result.setDescription(spec.AlgorithmDescription);
		result.setHtmlHelp(spec.AlgorithmHelp);

		if (!Check.isEmpty(spec.LastUpdate)) {
			Date update = null;
			for (String fmt : DATE_FORMATS) {
				try {
					update = new SimpleDateFormat(fmt).parse(spec.LastUpdate);
					break;
				} catch (ParseException ignore) {
				}
			}
			if (update == null) {
				result.setWhenLastUpdated(update);
			} else if (warnings != null) {
				warnings.add("Unable to parse LastUpdate='" + spec.LastUpdate + "'");
			}
		}

		String pathPrefix = ALGORITHMS_FOLDER_NAME + "/" + pluginName + "/";

		// "FormType" : "dynamic",
		// "FormTmplPath" : "none",
		// Note that in the new Algorithm a form is dynamic if it does NOT
		// have a form
		String formTmplPath = spec.FormTmplPath;
		if (Check.isEmpty(formTmplPath) || "none".equals(formTmplPath)) {
			if (warnings != null) {
				warnings.add("No FormTmplPath");
			}
		} else {
			if (formTmplPath.startsWith(pathPrefix)) {
				formTmplPath = formTmplPath.substring(pathPrefix.length());
			}
			if (formTmplPath.contains("/")) {
				warnings.add("FormTmplPath contains / after the plugin_name");
			}
			result.setHtmlFormTemplateFilename(formTmplPath);
		}

		// "BashTmplPath" :
		// "algorithms/dist_matrix_calculator/dist_matrix_calculator_bash.tmpl",
		String bashTmplPath = spec.BashTmplPath;
		if (bashTmplPath.startsWith(pathPrefix)) {
			bashTmplPath = bashTmplPath.substring(pathPrefix.length());
		}
		result.setScriptTemplateFilename(bashTmplPath);

		List<DataSet> inputDataSets = new ArrayList<>();
		List<Knob> knobs = new ArrayList<>();

		int argIndex = -1;
		for (Arg arg : spec.Args) {
			++argIndex;

			String name = arg.ArgName;
			String desc = arg.ArgLabel;
			if (!Check.isEmpty(arg.TmplVarName)) {
				name = arg.TmplVarName;
				if (Check.isEmpty(desc)) {
					desc = arg.ArgName;
				}
			}

			if (!Check.isEmpty(arg.SrcURL)) {
				DataSet ds = new DataSet(name, desc);
				ds.setTooltip(arg.Tooltip);
				DataSetType dst = DataSetType.classify(arg.SrcURL, DataSetType.ANY);
				ds.setDataSetType(dst);
				inputDataSets.add(ds);
			} else {
				Knob k = new Knob(name);
				k.setDescription(desc);
				// k.setScriptVariableName(arg.TmplVarName);

				k.setRequired(1 == asInt(arg.Required, 1));

				if ("file".equalsIgnoreCase(arg.DataType)) {
					k.setKnobDataType(KnobDataType.FILE_UPLOAD);
				} else if ("string".equalsIgnoreCase(arg.DataType)) {
					k.setKnobDataType(KnobDataType.TEXT);
				} else if ("int".equalsIgnoreCase(arg.DataType)) {
					k.setKnobDataType(KnobDataType.INTEGER);
				} else if ("float".equalsIgnoreCase(arg.DataType)) {
					// HTMLType should be "input"
					k.setKnobDataType(KnobDataType.DECIMAL);
				}

				if (!Check.isEmpty(arg.DefaultValue)) {
					k.setDefaultValue(arg.DefaultValue);
				}

				// ... but HTMLType might modify the data type
				if ("checkbox".equalsIgnoreCase(arg.HTMLType)) {
					k.setKnobDataType(KnobDataType.CHOICE);
					k.setValidationRule("yes");
				}
				// What about any others?

				// TODO SrcType: seems to be either 'local' or 'external'
				/// The 'external' ones I've seen have been DAL urls for listing
				// "things"
				// TODO SrcURL this seems to be the 'external' URL to use for
				// the query
				// TODO SrcLocal seems to be when we have a HTMLType=='checkbox'
				// TODO HTMLSrcDisplay seems to be:
				// 1) the JSON keys to retrieve SrcURL's response
				// 2) the "column headings" to display the "table" from the
				// response
				// TODO HTMLSrcId is the "ID" value from the SrcURL response
				// rows

				k.setDefaultValue(arg.DefaultValue);
				k.setTooltip(arg.Tooltip);

				// FIXME: translate from DAL validation rule
				// so far I've only seen BOOLEXs.
				if (!Check.isEmpty(arg.Validation)) {
					try {
						Pair<KnobDataType, KnobValidationRule> pair = KnobValidationRule.create(arg.Validation);
						KnobDataType kdt = pair.first;
						if (KnobDataType.TEXT == kdt && KnobDataType.FILE_UPLOAD == k.getKnobDataType()) {
							// That's ok then
						} else if (!k.getKnobDataType().equals(kdt)) {

							String zeroes = null;
							if (KnobDataType.INTEGER == kdt) {
								String defaultValue = arg.DefaultValue;
								if (!Check.isEmpty(defaultValue)) {
									int dotPos = defaultValue.lastIndexOf('.');
									if (dotPos >= 0) {
										try {
											Double.parseDouble(defaultValue);
											// it is a number
											Pattern pattern = Pattern.compile("^.*\\.([0-9]+)$");
											Matcher m = pattern.matcher(defaultValue);
											if (m.matches()) {
												int ndecs = m.group(1).length();
												StringBuilder sb = new StringBuilder(".");
												for (int i = 0; i < ndecs; ++i) {
													sb.append('0');
												}
												zeroes = sb.toString();
											}
										} catch (NumberFormatException ignore) {
											// so - it isn't a number
										}
									}
								}
							}

							if (zeroes == null) {
								String msg = MessageFormat.format(
										"Arg#{0,number,integer} Validation ''{1}'' does not match data type {2}",
										argIndex, arg.Validation, k.getKnobDataType().getDisplayName());
								throw new LegacyConversionException(msg);
							}

							KnobValidationRule kvr = pair.second;
							if (!(kvr instanceof Range)) {
								throw new IllegalStateException();
							}
							Range range = (Range) kvr;
							Double[] limits = range.get_Limits();
							StringBuilder sb = new StringBuilder();
							if (range.getIncludesLowerBound()) {
								sb.append("[ ");
							} else {
								sb.append("( ");
							}
							if (limits[0] != null) {
								sb.append(limits[0].intValue()).append(zeroes);
							}
							sb.append(" .. ");
							if (limits[1] != null) {
								sb.append(limits[1].intValue()).append(zeroes);
							}
							if (range.getIncludesUpperBound()) {
								sb.append(" ]");
							} else {
								sb.append(" )");
							}

							String exp = sb.toString();

							k.setKnobDataType(KnobDataType.DECIMAL);
							k.setValidationRule(exp);
						} else {
							k.setKnobDataType(pair.first);
							k.setValidationRule(pair.second.getExpression());
						}
					} catch (InvalidRuleException e) {
						throw new LegacyConversionException(e);
					}
				}

				knobs.add(k);
			}
		}
		result.setKnobs(knobs);
		result.setInputDataSets(inputDataSets);

		// caller needs to use the AlgorithmGroup
		return new Pair<>(spec.AlgorithmGroup, result);
	}

	static public int asInt(String input, int defalt) {
		int result = defalt;
		if (!Check.isEmpty(input)) {
			try {
				result = Integer.parseInt(input);
			} catch (NumberFormatException ignore) {
			}
		}
		return result;
	}
}
