/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.legacy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import net.pearcan.util.MissingVariablesException;

@SuppressWarnings("nls")
public class LegacyTemplate {

    public static final String USER_EMAIL = "USEREMAIL";


    static public final List<String> OLD_TEMPLATE_CORE_VARNAMES;
    static {
        OLD_TEMPLATE_CORE_VARNAMES = Arrays.asList(
                "DALCOOKIEPATH",
                "DALBASEURL",
                "DALWRITETOKEN",
                "DIRPATH",
                "JOBID",
                "USERNAME",
                USER_EMAIL );
    }
    
    static public Map<String,String> makeOldTemplateVariablesMap() {
        Map<String,String> result = new HashMap<>();
        
        for (String v : OLD_TEMPLATE_CORE_VARNAMES) {
            result.put(v,  v.toLowerCase());
        }
        
        return result;
    }
    
    private static final String TMPL_VAR_PATTERN = "<TMPL_VAR NAME=([a-zA-Z0-9][a-zA-Z0-9_]+)>";

    private static final Predicate<String> NON_BASH_COMMENT_LINES = new Predicate<String>() {
        @Override
        public boolean test(String line) {
            return ! line.isEmpty() && ! line.trim().startsWith("#");
        }
    };

    private final String template;

    private final Set<String> ignore = new HashSet<>();
    private final Predicate<String> shouldProcessLine;
    private final Pattern pattern;
    
    /**
     * A convenience constructor creating a LegacyTemplate that only processes
     * non-blank and non-bash-comment lines (i.e. beginning with "#").
     * @param template
     */
    public LegacyTemplate(String template) {
        this(template, NON_BASH_COMMENT_LINES);
    }
    /**
     * Create a new LegacyTemplate that will either process each template line
     * separately or all of them 
     * @param stemplate
     * @param byLine
     */
    public LegacyTemplate(String template, Predicate<String> processLine) {
        this.template = template;
        this.shouldProcessLine = processLine;
        
        pattern = Pattern.compile(TMPL_VAR_PATTERN);
    }
    
    /**
     * Add a specific template variable that you want to ignore as "missing".
     * @param v
     */
    public void addIgnore(String v) {
        ignore.add(v);
    }
    
    /**
     * Return a set of all the variable names in the template.
     * @return
     */
    public Set<String> getVariableNames() {
        Set<String> result = new HashSet<>();

        if (shouldProcessLine != null) {
            for (String line : template.split("\n")) {
                if (shouldProcessLine.test(line)) {
                    Matcher m = pattern.matcher(line);
                    while (m.find()) {
                        String p = m.group(1);
                        result.add(p);
                    }
                }
            }
        }
        else {
            Matcher m = pattern.matcher(template);
            while (m.find()) {
                String p = m.group(1);
                result.add(p);
            }
        }
        
        result.removeAll(ignore);
        
        return result;
    }
    
    /**
     * Replace all of the variables in the template with the values from the variableMap.
     * If there are any missing variable names in the map a MissingVariablesException will be thrown.
     * @param variableMap
     * @return String
     * @throws MissingVariablesException
     */
    public String replaceVariables(Map<String,String> variableMap)  throws MissingVariablesException {
        return replaceVariables(variableMap, null);
    }
    
    
    /**
     * Replace all of the variables in the template with the values from the variableMap.
     * Behaviour on vsariables being missing depends on whether the Consumer is null;
     * if it is then a MissingVariablesException will be thrown. Otherwise 
     * the Consumer will be called with the set of variableNames that were not found in the variableMap.
     * @param variableMap
     * @param missingVariables optional Consumer for missing variable names
     * @return String
     * @throws MissingVariablesException
     */
    public String replaceVariables(Map<String,String> variableMap, Consumer<Set<String>> missingVariables)
    throws MissingVariablesException
    {
        String result;

        Set<String> missing = new TreeSet<>();

        if (shouldProcessLine != null) {
            StringBuilder lines = new StringBuilder();
            for (String line : template.split("\n")) {
                if (shouldProcessLine.test(line)) {
                    lines.append(applyVariableMap(variableMap, missing, line));
                }
                else {
                    lines.append(line);
                }
                lines.append('\n');
            }
            result = lines.toString();
        }
        else {
            String input = template;
            result = applyVariableMap(variableMap, missing, input);
        }
        
        if (! missing.isEmpty()) {
            if (missingVariables != null) {
                missingVariables.accept(missing);
            }
            else {
                String msg = missing.stream()
                    .collect(Collectors.joining("','", "Missing value(s) for '", "'"));
            
                throw new MissingVariablesException(msg, missing);
            }
        }
        return result;
    }

    public String applyVariableMap(Map<String, String> variableMap, Set<String> missing,
            String input) {
        String result;
        Matcher m = pattern.matcher(input);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String p = m.group(1);
            String v = variableMap.get(p);
            if (v == null) {
                if (! ignore.contains(p)) {
                    missing.add(p);
                }
            }
            else {
                m.appendReplacement(sb, v);
            }
        }
        m.appendTail(sb);
        result = sb.toString();
        return result;
    }
}
