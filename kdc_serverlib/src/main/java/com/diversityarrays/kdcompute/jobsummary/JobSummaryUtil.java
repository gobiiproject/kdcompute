/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.jobsummary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.EntityStore;

public class JobSummaryUtil {
	public static final String getJobDownloadUrlRoot(String user, Long jobid, String alg) {

		return String.join("/",
				Arrays.asList("", "download", "jobsummary", user, Long.toString(jobid), alg.replace(" ", "%20")));

	}

	private static void getFilesRecursively(File directory, List<File> files) {
		// get all the files from a directory
		if (directory != null && directory.exists()) {
			File[] fList = directory.listFiles();
			for (File file : fList) {
				if (file.isFile()) {
					files.add(file);
				} else if (file.isDirectory()) {
					getFilesRecursively(file, files);
				}
			}
		}
	}

	public static List<String> makeDownloadLinks(EntityStore entityStore, AnalysisJob job,
			Function<String, String> linkFactory) throws IOException {
		String URL_SEP = "/";
		File resultsFolder = job.getResultsFolder();
		if (resultsFolder == null) {
			if (job.getRunState().equals(RunState.ERASED)) {
				return Arrays.asList();
			}
			throw new IOException("Results folder missing");
		}
		String replaceThis = resultsFolder.getPath() + URL_SEP;

		List<File> files = JobSummaryUtil.getFilesOfJob(entityStore, job);

		return files.stream().map(file -> linkFactory.apply(file.getPath().replaceAll(replaceThis, "")))
				.collect(Collectors.toList());
	}

	public static HashMap<String, String> makeName2DownloadLinks(EntityStore entityStore, AnalysisJob job,
			Function<String, String> linkFactory) throws IOException {
		String URL_SEP = "/";
		String replaceThis = job.getResultsFolder().getPath() + URL_SEP;

		List<File> files = JobSummaryUtil.getFilesOfJob(entityStore, job);

		HashMap<String, String> map = new HashMap<>();
		for (File f : files) {
			String strippedFilepath = f.getPath().replaceAll(replaceThis, "");
			map.put(strippedFilepath, linkFactory.apply(strippedFilepath));
		}
		return map;
	}

	public static List<File> getFilesOfJob(EntityStore entityStore, AnalysisJob job) throws IOException {
		File resultFolderPath = job.getResultsFolder();
		List<File> files = new ArrayList<>();
		getFilesRecursively(resultFolderPath, files);
		return files;
	}

	public static void downloadFile(HttpServletResponse response, File file) throws FileNotFoundException, IOException {

		String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		if (mimeType == null) {
			mimeType = "application/octet-stream";
		}
		response.setContentType(mimeType);

		
		OutputStream out = response.getOutputStream();
		FileInputStream in = new FileInputStream(file);
		byte[] buffer = new byte[4096];
		int length;
		while ((length = in.read(buffer)) > 0) {
			out.write(buffer, 0, length);
		}
		in.close();
		out.flush();

		// if (!file.exists()) {
		// // FIXME [BSP] should probably return 404 error
		// return;
		// }
		//
		// String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		// if (mimeType == null) {
		// System.out.println("mimetype is not detectable, will take default");
		// mimeType = "application/octet-stream";
		// }
		//
		// // System.out.println("mimetype : "+mimeType);
		//
		// response.setContentType(mimeType);
		//
		// /*
		// * "Content-Disposition : inline" will show viewable types [like
		// * images/text/pdf/anything viewable by browser] right on browser while
		// * others(zip e.g) will be directly downloaded [may provide save as
		// * popup, based on your browser setting.]
		// */
		// response.setHeader("Content-Disposition", String.format("inline;
		// filename=\"%s\"", file.getName()));
		//
		// /*
		// * "Content-Disposition : attachment" will be directly download, may
		// * provide save as popup, based on your browser setting
		// */
		// // response.setHeader("Content-Disposition", String.format("attachment;
		// // filename=\"%s\"", file.getName()));
		//
		// int length = (int) file.length();
		// response.setContentLength(length);
		//
		// InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		//
		// // Copy bytes from source to destination(outputstream in this example),
		// // closes both streams.
		// IOUtils.copy(inputStream, response.getOutputStream());
		// try {
		// inputStream.close();
		// } catch (IOException ignore) {
		// // IOUtils.copy does not close the streams.
		// // We need to close the inputStream but do NOT close
		// // the response's outputStream as the Servlet Environment
		// // may need to do more with it.
		// }
	}
}
