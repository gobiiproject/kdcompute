/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.pluginform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.helper.InvalidRuleException;
import com.diversityarrays.kdcompute.db.helper.KnobValidationRule;
import com.diversityarrays.kdcompute.db.helper.KnobValidationRule.TextRule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PluginForm {

	public static double calculateIncrementValue(int numberOfDecimalPlaces) {
	
		double divisor =  Math.pow(10, numberOfDecimalPlaces);
		double result = 1 / divisor;
		return result;
	}

	public  static String buildJsonKnobMap(List<Knob> allKnobs){
		return buildJsonKnobMap(allKnobs,false);
	}
	
	public  static String buildJsonKnobMap(List<Knob> allKnobs,boolean pretty) {
		List<JsonKnob> jsonKnobs = new ArrayList<>();
		
	
		GsonBuilder gsonBuilder  = new GsonBuilder();
		
		if(pretty) {
			 gsonBuilder.setPrettyPrinting();
		}
		
		Gson gson = gsonBuilder.create();
		
		
		for (Knob k :  allKnobs) {
			String validationRule = k.getValidationRule();
			
			try {
				Pair<KnobDataType, KnobValidationRule> rulePair = null;
	
				if(validationRule !=null && validationRule.isEmpty()) {
					rulePair  = new Pair<>(KnobDataType.TEXT, new TextRule());
				}
	
				else if (validationRule !=null) {
					rulePair = KnobValidationRule.create(validationRule);
				}
	
				JsonKnob knob = new JsonKnob();
				knob.setKnobDataType(k.getKnobDataType());
	
				if (rulePair.second instanceof KnobValidationRule.Range) {
					KnobValidationRule.Range rangeKnob = (com.diversityarrays.kdcompute.db.helper.KnobValidationRule.Range)rulePair.second;
					
	
					knob.setShouldCompareMin(rangeKnob.get_Limits()[0] !=null);
					knob.setShouldCompareMax(rangeKnob.get_Limits()[1] !=null);
					knob.setMax(rangeKnob.get_Limits()[1]);
					knob.setMin(rangeKnob.get_Limits()[0]);
					knob.setNoOfDecimalPlaces(rangeKnob.getNumberOfDecimalPlaces());
					knob.setMaxIncluded(rangeKnob.getIncludesUpperBound());
					knob.setMinIncluded(rangeKnob.getIncludesLowerBound());
					knob.setIncrementVal(calculateIncrementValue(rangeKnob.getNumberOfDecimalPlaces()));
					
				}
				
				else if (rulePair.second instanceof KnobValidationRule.Choice)  {
					KnobValidationRule.Choice choiceKnob = (com.diversityarrays.kdcompute.db.helper.KnobValidationRule.Choice)rulePair.second;
					knob.setMultipleChoiceAllowed(false); // to start with
					List<String> choices = choiceKnob.getChoices();
					knob.setChoices(choices);
				}
				
				knob.setDefaultValue(k.getDefaultValue());
				knob.setName(k.getKnobName());	
				knob.setKnobValueNeeded(k.isRequired());
				jsonKnobs.add(knob);
				
			}
			catch (InvalidRuleException e) { 
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		Map<String,JsonKnob> knobValByName = new HashMap<>();
		for (JsonKnob jk : jsonKnobs) {
			knobValByName.put(jk.getName(), jk);
		}
	
		String mapstr = gson.toJson(knobValByName);
		return mapstr;
	}

}
