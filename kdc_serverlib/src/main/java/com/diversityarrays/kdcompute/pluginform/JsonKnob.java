/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.pluginform;
import java.util.ArrayList;
import java.util.List;

import com.diversityarrays.kdcompute.db.KnobDataType;

public class JsonKnob {

	private String name = "";
	private Double min;
	private Double max;
	private int noOfDecimalPlaces = 0;
	boolean isMaxIncluded = false;
	boolean isMinIncluded = false;
	private double incrementVal = 1.0;
	private KnobDataType knobDataType = KnobDataType.TEXT;
	private boolean shouldCompareMin = false;
	private boolean shouldCompareMax = false;

	private boolean isMultipleChoiceAllowed = false;
	private boolean isKnobValueNeeded = false;
	
	private String defaultVal = "";

	public boolean getKnobValueNeeded() {
		return isKnobValueNeeded;
	}

	public void setKnobValueNeeded(boolean isKnobValueNeeded) {
		this.isKnobValueNeeded = isKnobValueNeeded;
	}

	private List<String> choices = new ArrayList<>();

	public boolean isMultipleChoiceAllowed() {
		return isMultipleChoiceAllowed;
	}

	public void setMultipleChoiceAllowed(boolean isMultipleChoiceAllowed) {
		this.isMultipleChoiceAllowed = isMultipleChoiceAllowed;
	}

	public double getIncrementVal() {
		return this.incrementVal;
	}

	public boolean isShouldCompareMin() {
		return shouldCompareMin;
	}

	public void setShouldCompareMin(boolean shouldCompareMin) {
		this.shouldCompareMin = shouldCompareMin;
	}

	public boolean isShouldCompareMax() {
		return shouldCompareMax;
	}

	public void setShouldCompareMax(boolean shouldCompareMax) {
		this.shouldCompareMax = shouldCompareMax;
	}

	public void setIncrementVal(double increment){
		this.incrementVal = increment;
	}
	public double getMin() {
		return min;
	}
	public void setMin(Double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public void setMax(Double max) {
		this.max = max;
	}
	public boolean isMaxIncluded() {
		return isMaxIncluded;
	}
	public void setMaxIncluded(boolean isMaxIncluded) {
		this.isMaxIncluded = isMaxIncluded;
	}
	public boolean isMinIncluded() {
		return isMinIncluded;
	}
	public void setMinIncluded(boolean isMinIncluded) {
		this.isMinIncluded = isMinIncluded;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public KnobDataType getKnobDataType() {
		return knobDataType;
	}
	public void setKnobDataType(KnobDataType knobDataType) {
		this.knobDataType = knobDataType;
	}
	public int getNoOfDecimalPlaces() {
		return noOfDecimalPlaces;
	}
	public void setNoOfDecimalPlaces(int noOfDecimalPlaces) {
		this.noOfDecimalPlaces = noOfDecimalPlaces;
	}

	public void setChoices(List<String> choices) {
		this.choices.clear();
		this.choices.addAll(choices);
	}

	public List<String> getChoices() {
		return this.choices;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultVal = defaultValue;
	}
	public String getDefaultValue() {
		return this.defaultVal;
	}
}
