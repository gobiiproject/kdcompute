/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.submitjob;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.diversityarrays.api.ResponseJob;
import com.diversityarrays.api.ResponseMsg;
import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PluginVersion;
import com.diversityarrays.kdcompute.db.PostCompletionCall;
import com.diversityarrays.kdcompute.db.PostSubmissionCall;
import com.diversityarrays.kdcompute.db.helper.KnobValidationRule;
import com.diversityarrays.kdcompute.util.Check;
import com.diversityarrays.kdcompute.util.Either;

public class SubmitPluginJob {

	// Passes the folder path from QCStart
	// See the Plugin named QcStart.QC_PLUGIN_NAME

	private final BiFunction<String, PluginVersion, Plugin> pluginStore;
	private final Supplier<List<String>> pluginNamesSupplier;
	private final Function<String, Either<IOException, File>> tmpdirFactory;

	private final Supplier<String> applicationVersionSupplier;
	private final Function<AnalysisJob, String> commandBuilder;

	public SubmitPluginJob(BiFunction<String, PluginVersion, Plugin> pluginStore,
			Supplier<List<String>> pluginNamesSupplier, Function<String, Either<IOException, File>> tmpdirFactory,
			Supplier<String> appVersionSupplier, Function<AnalysisJob, String> commandBuilder) {
		this.pluginStore = pluginStore;
		this.pluginNamesSupplier = pluginNamesSupplier;
		this.tmpdirFactory = tmpdirFactory;
		this.applicationVersionSupplier = appVersionSupplier;
		this.commandBuilder = commandBuilder;
	}

	public SubmitJobResponse submitJobForPlugin(Map<String, String> allRequestParams, String loggedInUser,
			String pluginName, String base_usage, Function<ResponseJob, SubmitJobResponse> responseFactory,
			PostCompletionCall postCompletionCall, PostSubmissionCall postSubmissionCall,

			Function<PluginJobParams, Either<Exception, AnalysisJob>> jobSubmitter) throws Exception {
		Plugin plugin = pluginStore.apply(pluginName, null);

		if (plugin == null) {
			return new ResponseMsg("Plugin does not exist: " + pluginName,
					pluginNamesSupplier.get().stream().collect(Collectors.joining(", ", "Available plugins: ", "")),
					base_usage);
		}

		String postCompletionUrl = allRequestParams.get(SubmitJobConstants.POSTCOMPLETIONURL);
		if (postCompletionUrl != null)
			postCompletionUrl = postCompletionUrl.trim();

		List<KnobBinding> knobBindings = new ArrayList<>();

		List<Knob> allKnobs = plugin.getKnobs();
		for (Knob k : allKnobs) {
			String knobValue = allRequestParams.get(k.getKnobName());
			if (Check.isEmpty(knobValue) && k.isRequired()) {
				return buildUsageResponse(String.format("Missing value for required knob(%s), type(%s)",
						k.getKnobName(), k.getKnobDataType().name()), base_usage, allKnobs, k);
			}
			KnobBinding knobBinding = null;

			switch (k.getKnobDataType()) {
			case CHOICE:
				List<String> choices = KnobValidationRule.getChoices(k);
				if (!choices.contains(knobValue)) {
					String error = "Invalid choice " + knobValue;
					if (knobValue == null || knobValue.isEmpty()) {
						error = "Choice value not provided";
					}
					return buildUsageResponse(error, base_usage, allKnobs, k);
				}
				knobBinding = new KnobBinding(k, knobValue);
				break;

			case FILE_UPLOAD:
				// Curl file
				String timeStamp = new SimpleDateFormat("yyyy/MM/dd/hh_mm_ss").format(new Date()); // This
																									// will
																									// create
																									// 3
																									// folders,
																									// yyyy
																									// /
																									// MM
																									// /
																									// dd
				Either<IOException, File> either = tmpdirFactory.apply(loggedInUser);
				if (either.isLeft()) {
					throw either.left();
				}
				File tmpdir = either.right();
				File userTempDir = new File(tmpdir, timeStamp);
				if (!userTempDir.exists() && !userTempDir.mkdirs() || !userTempDir.isDirectory()) {
					throw new Exception("Failed to create temp directory " + userTempDir.getAbsolutePath());
				}

				if (Check.isEmpty(knobValue)) {
					// Note: required check was done above
					knobBinding = new KnobBinding(k, "");
				} else {
					File destFile = new File(userTempDir, new File(knobValue).getName());
					FileOutputStream fileOutputStream = null;
					try {
						fileOutputStream = new FileOutputStream(destFile, true);
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(new URL(knobValue).openStream(), "UTF-8"));

						for (String line; (line = reader.readLine()) != null;) {
							fileOutputStream.write((line + "\n").getBytes());
						}
						fileOutputStream.flush();
						fileOutputStream.close();
					} finally {
						if (fileOutputStream != null) {
							try {
								fileOutputStream.close();
							} catch (IOException ignore) {
							}
						}
						knobBinding = new KnobBinding(k, destFile.getAbsolutePath());
					}
				}
				break;

			case TEXT:
			case DECIMAL:
			case FORMULA:
			case INTEGER:
				knobBinding = new KnobBinding(k, knobValue == null ? "" : knobValue);
				break;
			}

			knobBindings.add(knobBinding);
		}

		PluginJobParams jobParams = new PluginJobParams(plugin, knobBindings, loggedInUser, postCompletionCall,
				postSubmissionCall);
		Either<Exception, AnalysisJob> either = jobSubmitter.apply(jobParams);

		// SubmittedFormController.submitJob(
		// plugin,
		// knobsAndValues,
		// loggedInUser,
		// postCompletionCall,
		// postSubmissionCall);

		if (either.isLeft()) {
			return new ResponseMsg(either.left().getMessage(), null, base_usage);
		}

		AnalysisJob job = either.right();
		ResponseJob responseJob = new ResponseJob(applicationVersionSupplier.get(), commandBuilder.apply(job), job);

		return responseFactory.apply(responseJob);
	}

	private ResponseMsg buildUsageResponse(String error, String base_usage, List<Knob> allKnobs, Knob k) {
		String alternative = "";
		if (k.getKnobDataType().equals(KnobDataType.CHOICE)) {
			alternative = String.join(", ", k.getValidationRule().split("\\" + KnobDataType.CHOICE_SEP));
		}
		return new ResponseMsg(error, alternative, constructUsageOmitting_PLUGIN_NAME(base_usage, allKnobs));
	}

	private static String constructUsageOmitting_PLUGIN_NAME(String base_usage, List<Knob> allKnobs) {
		StringBuilder sb = new StringBuilder("usage: ");
		sb.append(base_usage);
		allKnobs.stream().filter(k -> !SubmitJobConstants.PLUGIN_NAME.equals(k.getKnobName())).forEach(
				k -> sb.append('&').append(k.getKnobName()).append("=[").append(k.getKnobDataType()).append(']'));
		return sb.toString();
	}

}
