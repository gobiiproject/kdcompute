/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.submitjob;

import java.util.List;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.PostCompletionCall;
import com.diversityarrays.kdcompute.db.PostSubmissionCall;
import com.diversityarrays.kdcompute.db.RunBinding;

public class PluginJobParams {

    public final Plugin plugin;
    public final String userName;
    public final List<KnobBinding> knobBindings;
    public final PostCompletionCall postCompletionCall;
    private final PostSubmissionCall postSubmissionCall;

    public PluginJobParams(Plugin p,
            List<KnobBinding> bindings,
            String user,
            PostCompletionCall completion,
            PostSubmissionCall submission)
    {
        plugin = p;
        knobBindings = bindings;
        userName = user;
        postCompletionCall = completion;
        postSubmissionCall = submission;
    }

    public RunBinding getRunBinding() {
        RunBinding result = new RunBinding(plugin);
        result.setKnobBindings(knobBindings);
        return result;
    }

    public void onSubmission(AnalysisJob job) {
        if (postSubmissionCall!=null) {
            postSubmissionCall.onSubmission(job);
        }
    }
}
