/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.jobscheduler;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.diversityarrays.kdcompute.db.RunState;

public class Job {

	public static final int EXPECTED_TOKENS = 8; // JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
	
	@NotNull
	Long id;
	@NotNull
	String name;
	@NotNull
	String user;
	@NotNull
	RunState state;
	@NotNull
	Date started;

	public Job(Long id, String name, String user, RunState state, Date started) {
		this.id = id;
		this.name = name;
		this.user = user;
		this.state = state;
		this.started = started;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getUser() {
		return user;
	}

	public RunState getState() {
		return state;
	}

	public Date getStarted() {
		return started;
	}

	

}
