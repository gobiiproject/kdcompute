/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.jobscheduler.slurm;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.diversityarrays.jobscheduler.Job;
import com.diversityarrays.jobscheduler.Node;
import com.diversityarrays.jobscheduler.NodeState;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.jobscheduler.JobSchedulerError;
import com.diversityarrays.kdcompute.runtime.jobscheduler.StderrException;

public class SlurmController {

	private static long timeout = 100L;

    static private String getLine(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is);
        s.useDelimiter("\\A");
        String result = "";
        try {
            result = s.hasNext() ? s.next() : "";
        }
        finally {
            s.close();
        }
        return result;
    }

//	private static String execCmd(String[] cmd) throws java.io.IOException, InterruptedException {
//		Process exec = Runtime.getRuntime().exec(cmd);
//		return getLine(exec.getInputStream());
//	}

//	private static String convertStreamToString(java.io.InputStream is) {
//	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
//	    return s.hasNext() ? s.next() : "";
//	}

//	private static String execCmd(String cmd) throws java.io.IOException, InterruptedException {
//		Process exec = Runtime.getRuntime().exec(cmd);
//        return getLine(exec.getInputStream());
//	}
	
	public String execCmdTimeout(String[] cmd, long timeout_milliseconds) throws java.io.IOException, InterruptedException, StderrException {
	    ProcessBuilder pb = new ProcessBuilder(cmd); 
	    //pb.redirectErrorStream(true);
	    Process p = pb.start();

	    if(!p.waitFor(timeout_milliseconds, TimeUnit.MILLISECONDS)) {
	        p.destroy();
	        throw new InterruptedException("Command "+String.join(" ",cmd)+ " timedout");
	    }
	    String errmsg = getLine(p.getErrorStream());
	    if(!errmsg.isEmpty()) {
	        throw new StderrException(errmsg);
	    }

	    InputStream inputStream = p.getInputStream();
	    if (inputStream==null) {
	        return "";
	    }
	    return getLine(inputStream);
	}

	public String execCmdTimeout(String cmd, long timeout_milliseconds) throws java.io.IOException, InterruptedException, StderrException {
		String[] cmds = cmd.split(" +"); 
		return execCmdTimeout(cmds, timeout_milliseconds);
	}

	public void runSlurmDiagnositcs() throws JobSchedulerError, InterruptedException, IOException {
		if(Runtime.getRuntime().exec("pidof slurmctld").waitFor() != 0) {
			throw new JobSchedulerError("Could not find running process of SLURM CONTROLLER. Is slurmctld running?");
		}
		try {
			execCmdTimeout( "sinfo -h -r" ,1000);
		} catch (StderrException e) {
			throw new JobSchedulerError(e);
		}		
	}

	public List<Job> squeue() throws JobSchedulerError {	
		String squeue_output;
		try {
			squeue_output = execCmdTimeout("squeue -h",timeout);
		} catch (InterruptedException e) {
			throw new JobSchedulerError("Could not execute squeue. Is slurmctld running?");
		} catch (IOException | StderrException e) {
			throw new JobSchedulerError(e.getMessage());
		} 

		List<Job> slurm_jobs = new ArrayList<>();
		if(squeue_output.trim().isEmpty()) {
			return slurm_jobs;	
		}

		String[] lines = squeue_output.trim().split("\n");


		for( String line : lines ) {
			String[] split = line.trim().split(" +");
			if(split.length != Job.EXPECTED_TOKENS ){
				throw new JobSchedulerError( "Error processing squeue, line did not have expected number of tokens (" + Job.EXPECTED_TOKENS +"): " + line );
			}else{

				String[] time_elements = split[5].split(":");
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(Calendar.MINUTE, -Integer.parseInt(time_elements[0]));
				cal.add(Calendar.SECOND, -Integer.parseInt(time_elements[1]));
				RunState jobState = null;
				switch (split[4].trim()) {
				case "PD":
					jobState = RunState.NEW;
					break;
				case "R":
					jobState = RunState.RUNNING;
					break;
				case "CA":
					jobState = RunState.CANCELLED;
					break;
				case "F":
					jobState = RunState.FAILED;
					break;
				default:
					jobState = RunState.UNKNOWN;
					break;
				}
				slurm_jobs.add(new Job(Long.parseLong(split[0]), split[2], split[3], jobState, cal.getTime()));
			}
		}

		return slurm_jobs;
	}

	public String getFreePartition() throws JobSchedulerError {
		String squeue_output;
		try {
			squeue_output = execCmdTimeout("sinfo -h",timeout);
		} catch (IOException |StderrException  | InterruptedException e) {
			throw new JobSchedulerError(e);
		} 

		List<Node> nodes_list = new ArrayList<>();
		if(squeue_output.trim().isEmpty()) {
			return null;	
		}

		String[] lines = squeue_output.trim().split("\n");
		for( String line : lines ) {
			String[] split = line.trim().split(" +");
			if(split.length != Node.EXPECTED_TOKENS){
				throw new JobSchedulerError( "Error processing sinfo, line did not have expected number of tokens (" + Node.EXPECTED_TOKENS +"): " + line );
			}else{
				nodes_list.add(new Node(split[0].trim(), split[1].equalsIgnoreCase("up"), java.time.Duration.ZERO, Integer.parseInt(split[3]), NodeState.fromString(split[4]), split[5]));
			}
		}
		for(Node n : nodes_list) {
			if ( n.getState() == NodeState.IDLE ) {
				return n.getPartition();
			}
		}
		return null;
	}

	public int sbatch(String shellFilePath)  throws JobSchedulerError {
		String filePath = shellFilePath.
				substring(0,shellFilePath.lastIndexOf(File.separator));
		String script = new File(shellFilePath).getName();
		Logger.getGlobal().log(Level.INFO, "Job submitted to slurm. Directory: " + filePath);
		try {
			String[] cmd = { "/bin/sh", "-c", "cd \""+filePath + "\"; sbatch "+script };
			String result = execCmdTimeout(cmd,timeout);
			String[] tokens = result.toString().split(" +");
			return Integer.parseInt(tokens[tokens.length - 1].trim());
		}catch(Exception e){
			e.printStackTrace();
			throw new JobSchedulerError(e);
		}		
	}

	public boolean isSlurmWorking() throws JobSchedulerError {
		try {
			if(Runtime.getRuntime().exec("pidof slurmctld").waitFor() == 0) {
				String execCmdTimeout = execCmdTimeout( "sinfo -h -r" ,timeout);
				if(!execCmdTimeout.isEmpty()) {
					return true;	
				}
			}
		} catch (InterruptedException | StderrException  | IOException e) {
			throw new JobSchedulerError(e);
		}		
		return false;
	}

}
