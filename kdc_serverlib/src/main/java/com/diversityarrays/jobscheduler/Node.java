/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.jobscheduler;

import java.time.Duration;

import javax.validation.constraints.NotNull;

public class Node {
	
	public static final int EXPECTED_TOKENS = 6;
	
	@NotNull
	String partition;
	@NotNull
	boolean available;
	@NotNull
	Duration timelimit;
	@NotNull
	int nodes;
	@NotNull
	NodeState state;
	@NotNull
	String nodelist;
	public Node(String partition, boolean available, Duration timelimit, int nodes, NodeState state,
			String nodelist) {
		super();
		this.partition = partition;
		this.available = available;
		this.timelimit = timelimit;
		this.nodes = nodes;
		this.state = state;
		this.nodelist = nodelist;
	}
	public String getPartition() {
		return partition;
	}
	public boolean isAvailable() {
		return available;
	}
	public Duration getTimelimit() {
		return timelimit;
	}
	public int getNodes() {
		return nodes;
	}
	public NodeState getState() {
		return state;
	}
	public String getNodelist() {
		return nodelist;
	}


}
