/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.jobscheduler;

import java.util.Vector;

import com.diversityarrays.kdcompute.db.RunState;

public class Jobs extends Vector<Job>{
	
	public Jobs(Vector<Job> jobs) {
		super(jobs);
	}
	
	public Jobs filterUser(String user) {
		Jobs filtered = new Jobs(new Vector<Job>());
		
		for(Job j : this) {
			if(j.getUser().equalsIgnoreCase(user)){
				filtered.addElement(j);
			}
		}
		return filtered;
	}
	
	public Jobs filterState(RunState jobState) {
		Jobs filtered = new Jobs(new Vector<Job>());
		for(Job j : this) {
			if(j.getState().equals(jobState)){
				filtered.addElement(j);
			}
		}
		return filtered;
	}
	
	
	
}
