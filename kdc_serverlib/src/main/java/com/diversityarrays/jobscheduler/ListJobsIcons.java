/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.jobscheduler;



public class ListJobsIcons {
	public  boolean displayDownload(Job j) {
		switch(j.getState()) {
			case RUNNING:
				return false;
			case CANCELLED:
				return true;
			case FAILED:
				return true;
			default:
				return false;
		}
	}
	
	public static boolean displayView(Job j) {
		switch(j.getState()) {
			case RUNNING:
				return true;
			case CANCELLED:
				return true;
			case FAILED:
				return true;
			default:
				return false;
		}
	}
	
	public static boolean displayCancel(Job j) {
		switch(j.getState()) {
			case RUNNING:
				return true;
			case CANCELLED:
				return false;
			case FAILED:
				return false;
			default:
				return false;
		}
	}
	
	public static boolean displayDelete(Job j) {
		switch(j.getState()) {
			case RUNNING:
				return true;
			case CANCELLED:
				return true;
			case FAILED:
				return true;
			default:
				return false;
		}
	}
}
