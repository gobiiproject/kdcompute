/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.fileutils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.io.FileUtils;

public class KDCFileUtils {
	public static File createTempDirectory(String prefix) throws IOException {
		File temp;

		if (prefix == null || prefix.isEmpty()) {
			prefix = "tmp";
		}

		temp = File.createTempFile(prefix, Long.toString(System.nanoTime()));

		if (!(temp.delete())) {
			throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
		}

		if (!(temp.mkdirs())) {
			throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
		}

		return (temp);
	}

	public static String escape(String str) {
		return String.join("\\ ", str.split(" ")); // For
		// some
		// reason,
		// String.replaceAll
		// doesnt
		// do
		// the
		// job
	}

	public static String fileSafeUrl(File file) {
		String path = file.getPath();
		String replaceAll = path.replaceAll("\\\\", "/");
		return escape(replaceAll);
	}

	public static File getTempDirLocation() throws IOException {
		String absolutePath = File.createTempFile("test", "test").getAbsolutePath();
		return new File(absolutePath.substring(0, absolutePath.lastIndexOf(File.separator)));
	}

	public static File getExampleTempDir(Long deleteFoldersSecondsOld) {
		File exampleRoot = getExamplesDir();
		exampleRoot.mkdirs();

		Arrays.asList(exampleRoot.listFiles()).forEach(file -> {
			long diff = new Date().getTime() - file.lastModified();
			if (diff > deleteFoldersSecondsOld * 1000) {
				try {
					FileUtils.deleteDirectory(file);
				} catch (Exception ignore) {
				}
			}
		});

		File file = new File(exampleRoot, Long.toString(nextLong()));
		file.mkdir();
		return file;
	}

	private static long nextLong() {
		return ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
	}

	public static File getExamplesDir() {
		return new File(FileUtils.getTempDirectory(), "kdcExampleTempDir");
	}

	public static File getExampleTempDir() {
		return getExampleTempDir(3600L);
	}

	public static File createTempDirectory(File root) throws IOException {
		final File temp = new File(root, Long.toString(nextLong()));

		temp.delete();

		if (!(temp.mkdir())) {
			throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
		}

		return (temp);
	}
}
