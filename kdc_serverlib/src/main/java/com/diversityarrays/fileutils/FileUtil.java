/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.fileutils;

import java.awt.GraphicsEnvironment;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtil {

	public static List<String> partialReadFile(final Path path, final int numLines, final int numCols)
			throws IOException {
		try (final BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
			final List<String> lines = new ArrayList<>(numLines);
			int lineNum = 0;
			String line;
			while ((line = reader.readLine()) != null && lineNum < numLines) {
				lines.add(line.substring(0, Math.min(numCols, line.length())));
				lineNum++;
			}
			return lines;
		}
	}

	private static String serverWorkingDirectory;

	public static String getServerWorkingDirectory() {
		return serverWorkingDirectory;
	}

	public static void setServerWorkingDirectory(String d) {
		Logger.getGlobal().log(Level.FINE, "Server working directory set to: " + d);
		FileUtil.serverWorkingDirectory = d;
	}

	/**
	 * Wrapper function implemented to handle duel functionality of executing as an
	 * executable jar (As a desktop application)
	 * 
	 * @return
	 */
	public static String getJarLocation() {
		if (serverWorkingDirectory != null)
			return serverWorkingDirectory;

		if (System.getProperty("os.name").toLowerCase().contains("nux") && !GraphicsEnvironment.isHeadless()) { // linux

			String s = System.getProperty("java.class.path").split(":")[0];

			if (new File(s).exists()) {
				String path = new File(s).getParent();
				return path;
			}
		}
		return System.getProperty("user.dir");
	}

	// public static File getTempDirectory() throws IOException {
	// return File.createTempFile("", "");
	// }
	//
	// public static File createTempDirectory(File root) throws IOException {
	// final File temp = new File(root, "temp" + Long.toString(System.nanoTime()));
	//
	// temp.delete();
	//
	// if (!(temp.mkdir())) {
	// throw new IOException("Could not create temp directory: " +
	// temp.getAbsolutePath());
	// }
	//
	// return (temp);
	// }
	//
	// public static File createTempDirectory() throws IOException {
	// final File temp;
	//
	// temp = File.createTempFile("temp", Long.toString(System.nanoTime()));
	//
	// if (!(temp.delete())) {
	// throw new IOException("Could not delete temp file: " +
	// temp.getAbsolutePath());
	// }
	//
	// if (!(temp.mkdir())) {
	// throw new IOException("Could not create temp directory: " +
	// temp.getAbsolutePath());
	// }
	//
	// return (temp);
	// }

	public static void usage(String args) {
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		StackTraceElement main = stack[stack.length - 1];
		String mainClass = main.getClassName();
		System.err.println(mainClass + " " + args);
	}
}
