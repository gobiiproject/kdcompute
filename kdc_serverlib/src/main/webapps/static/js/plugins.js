//TODO write a jquery function to on every input element inside table and use the id to assign it a step value.
//TODO may be even change evalValRule to use the same approach

/*<![CDATA[*/

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].onclick = function() {
		this.classList.toggle("active");
		this.nextElementSibling.classList.toggle("show");
	}
}

var acc = document.getElementsByClassName("accordionVersion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].onclick = function() {
		this.classList.toggle("active");
		this.nextElementSibling.classList.toggle("show");
	}
}

var shouldBeSubmitable = true;

function assignChangeListener(knobName) {
	// Only called by File Input Boxes.
	var inputBox = document.getElementById(knobName);
	var errorMessageId = "message" + knobName;
	var errorInputBox = document.getElementById(errorMessageId);

	var text = inputBox.value;

	if (text) {
		errorInputBox.innerText = "";
	}
	validateForSubmitButton();
}

function validateForSubmitButton() {
	// Initial check for submit button based on Validation Rule and on every input on input boxes.
	var submitButtonId = "submit_button";
	var submitButton = document.getElementById(submitButtonId);
	shouldBeSubmitable = true;

	$("input")
	.each(
			function() {

				var knobName = this.id;
				var isKnob = findValidationRuleByKnobName(knobName);

				if (isKnob != null) {
					var isKnobInputBox = document
					.getElementById(knobName);
					var errorMessageInputId = "message"
						+ knobName;

					var errorMessageInput = document
					.getElementById(errorMessageInputId);

					var errorValue = errorMessageInput.innerText;
					var knobInputBoxValue = isKnobInputBox.value;

					var isValueNeeded = isKnob.isKnobValueNeeded;

					if (isValueNeeded === true) {
						if (!knobInputBoxValue) {
							errorMessageInput.innerText = "Value Needed";
							shouldBeSubmitable = false;
						}
					}

					if (typeof variable !== 'undefined'
						&& errorValue
						&& 0 !== errorValue.length) {
						shouldBeSubmitable = false;
					}

				}
			});
	if (shouldBeSubmitable === false) {
		submitButton.disabled = true;
	} else {
		submitButton.disabled = false;
	}

}

$("#algorithm_content")
.on(
		'input',
		"input",
		function() {
			var submitButtonId = "submit_button";
			var submitButton = document
			.getElementById(submitButtonId);

			var knobName = this.id;
			var ifKnob = findValidationRuleByKnobName(knobName);

			if (ifKnob != null) {
				var input = document.getElementById(knobName);

				var errorMessageInputId = "message" + knobName;
				var errorMessageAsterick = "warning" + knobName;
				var inputBox = document
				.getElementById(knobName);

				var shouldBeSubmitable = true;

				var knobDataType = ifKnob.knobDataType;
				var enteredValue = input.value;

				if (knobDataType === "INTEGER") {
					var result = integerEval(ifKnob,
							enteredValue);

					if (result) {
						document
						.getElementById(errorMessageAsterick).innerHTML = "*";
						inputBox.style.color = "red";
					} else {
						document
						.getElementById(errorMessageAsterick).innerHTML = "";
						inputBox.style.color = "black";
					}

					document
					.getElementById(errorMessageInputId).innerHTML = result;

				} else if (knobDataType === "DECIMAL") {
					var result = decimalEval(ifKnob,
							enteredValue);

					if (result) {
						document
						.getElementById(errorMessageAsterick).innerHTML = "*";
						inputBox.style.color = "red";
					} else {
						document
						.getElementById(errorMessageAsterick).innerHTML = "";
						inputBox.style.color = "black";
					}

					document
					.getElementById(errorMessageInputId).innerHTML = result;

				}
			}

			// We check every input box on any input box entry.
			validateForSubmitButton();

		});

function findChoiceKnobs(jsonKnobMap) {

	for ( var key in jsonKnobMap) {
		if (jsonKnobMap.hasOwnProperty(key)) {
			valRule = jsonKnobMap[key];

			if (valRule.knobDataType == "CHOICE") {

				var inputBox = document.getElementById(key);
				var divBox = document.getElementById(key).parentElement;
				inputBox.className += " knobE";

				choices = valRule.choices;
				if (choices.length == 1) {
					var label = document.createElement('label');
					inputBox.type = "checkbox";
					var value = choices[0];
					inputBox.value = value;
					inputBox.name = key;

					// No need of a label if only one choice.
					/* 							label.htmlFor = key;
							 label.appendChild(document.createTextNode(value));
							 divBox.appendChild(label); */

				} else if (choices.length < 4) {
					$(inputBox).remove();
					for (i = 0; i < choices.length; i++) {
						var label = document.createElement('label');
						var newInputBox = document
						.createElement("INPUT");
						newInputBox.id = (key + i);
						newInputBox.type = "radio";
						newInputBox.value = choices[i];
						newInputBox.name = key; //knobName
						newInputBox.className += " knobE";

						label.htmlFor = (key + i);
						label.appendChild(document
								.createTextNode(choices[i]));
						divBox.appendChild(newInputBox);
						divBox.appendChild(label);
					}
					var defaultVal = valRule.defaultVal;
					var allRadios = document.getElementsByName(key);

					if (defaultVal) {
						for (i = 0; i < allRadios.length; i++) {
							if (allRadios[i].value.toLowerCase() == defaultVal
									.toLowerCase()) {
								var radio = allRadios[i];
								radio.checked = true;
							}
						}
					} else {
						var firstRadio = allRadios[0];
						firstRadio.checked = true;
					}

				} else if (choices.length > 4) {
					$(inputBox).remove();
					var selectList = document.createElement("select");
					selectList.id = key;
					selectList.name = key;
					selectList.className += " knobE";

					for (var i = 0; i < choices.length; i++) {
						var option = document.createElement("option");
						option.value = choices[i];
						option.text = choices[i];
						selectList.appendChild(option);
					}
					divBox.appendChild(selectList);
				}
			}
		}
	}
}

function getEventTarget(e) {
	e = e || window.event;
	return e.target || e.srcElement;
}

var div = document.getElementById("plugins_listview");

function getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}

div.onclick = function(event) {
	var target = getEventTarget(event);
	var tagName = $(event.target).get(0).tagName;

	if (tagName == "A") {
		var context = getBaseUrl();//@{\/};
		alert(context);
		var algoId = $(event.target).get(0).id;
		var url = context + 'showalgo/' + algoId;
		$('#algorithm_content').load(url);

		var helpUrl = 'plugin/help/' + algoId +'/';

		$('#includedContent').empty();
		$('<iframe>', {
			src: helpUrl,
			frameborder: 0,
			scrolling: 'yes',
			style: 'border-style: solid',
		}).appendTo('#includedContent');

	}
};

$("#algorithm_content").on("focus", "input", function() {

	var knobName = this.id;
	var ifKnob = findValidationRuleByKnobName(knobName);

	if (ifKnob != null) {

		var knobDataType = ifKnob.knobDataType;
		var incrementVal = ifKnob.incrementVal;

		if (knobDataType === "DECIMAL") {
			//this is definately an knob input box
			this.step = incrementVal;
		} else if (knobDataType === "INTEGER") {
			this.step = parseInt(incrementVal, 10);
		}
	}
});



function decimalEval(thisKnobValRule, enteredValue) {
	var result = false;

	var includeMin = thisKnobValRule.isMinIncluded;
	var includeMax = thisKnobValRule.isMaxIncluded;
	var min = thisKnobValRule.min;
	var max = thisKnobValRule.max;

	var shouldCompareMin = thisKnobValRule.shouldCompareMin;
	var shouldCompareMax = thisKnobValRule.shouldCompareMax;

	if (shouldCompareMin === true && shouldCompareMax === true) {
		return compareBoth(min, max, includeMin, includeMax,
				enteredValue);
	} else if (shouldCompareMin === false && shouldCompareMax === true) {
		return compareMax(max, includeMax, enteredValue);
	} else if (shouldCompareMin === true && shouldCompareMax === false) {
		return compareMin(min, includeMin, enteredValue);
	}
}

function integerEval(thisKnobValRule, enteredValue) {
	var result = false;

	var includeMin = thisKnobValRule.isMinIncluded;
	var includeMax = thisKnobValRule.isMaxIncluded;
	var min = thisKnobValRule.min;
	var max = thisKnobValRule.max;

	var intMin = parseInt(min, 10);
	var intMax = parseInt(max, 10);
	var shouldCompareMin = thisKnobValRule.shouldCompareMin;
	var shouldCompareMax = thisKnobValRule.shouldCompareMax;

	if (shouldCompareMin === true && shouldCompareMax === true) {
		return compareBoth(intMin, intMax, includeMin, includeMax,
				enteredValue);
	} else if (shouldCompareMin === false && shouldCompareMax === true) {
		return compareMax(intMax, includeMax, enteredValue);
	} else if (shouldCompareMin === true && shouldCompareMax === false) {
		return compareMin(intMin, includeMin, enteredValue);
	}

}

function compareBoth(min, max, includeMin, includeMax, enteredValue) {

	switch (includeMin) {
	case true:
		switch (includeMax) {
		case true:
			if (min <= enteredValue && enteredValue <= max) {
				result = true;
			} else {
				result = false;
			}
			break;
		case false:
			if (min <= enteredValue && enteredValue < max) {
				result = true;
			} else {
				result = false;
			}
			break;
		}
		break;
	case false:
		switch (includeMax) {
		case true:
			if (min < enteredValue && enteredValue <= max) {
				result = true;
			} else {
				result = false;
			}
			break;
		case false:
			if (min < enteredValue && enteredValue < max) {
				result = true;
			} else {
				result = false;
			}
			break;
		}
		break;

	}

	var errorMessage = "";
	switch (result) {
	case false:
		errorMessage = "Invalid value " + enteredValue;
		break;
	case true:
		errorMessage = "";
		break;
	}
	return errorMessage;
}

function compareMin(min, includeMin, enteredValue) {

	switch (includeMin) {
	case true:
		if (min <= enteredValue) {
			result = true;
		} else {
			result = false;
		}
		break;
	case false:
		if (min < enteredValue) {
			result = true;
		} else {
			result = false;
		}
		break;
	}

	var errorMessage = "";

	switch (result) {

	case false:
		errorMessage = "Invalid value " + enteredValue;
		break;
	case true:
		errorMessage = "";
		break;
	}

	return errorMessage;
}

function compareMax(max, includeMax, enteredValue) {

	switch (includeMax) {
	case true:
		if (enteredValue <= max) {
			result = true;
		} else {
			result = false;
		}
		break;
	case false:
		if (enteredValue < max) {
			result = true;
		} else {
			result = false;
		}
		break;
	}

	var errorMessage = "";

	switch (result) {

	case false:
		errorMessage = "Invalid value " + enteredValue;
		break;
	case true:
		errorMessage = "";
		break;
	}

	return errorMessage;
}

/*]]>*/