/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
	document.getElementById("myDropdown").classList.toggle("show");
}

//Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
	if (!event.target.matches('.dropbtn')) {

		var dropdowns = document.getElementsByClassName("dropdown-content");
		var i;
		for (i = 0; i < dropdowns.length; i++) {
			var openDropdown = dropdowns[i];
			if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
			}
		}
	}
}

function replaceTargetWith( targetID, html ){
	/// find our target
	var i, div, elm, last, target = document.getElementById(targetID);
	/// create a temporary div
	div = document.createElement('td');
	/// fill that div with our html, this generates our children
	div.innerHTML = html;
	/// step through the temporary div's children and insertBefore our target
	i = div.childNodes.length;
	/// the insertBefore method was more complicated than I first thought so I 
	/// have improved it. Have to be careful when dealing with child lists as they 
	/// are counted as live lists and so will update as and when you make changes.
	/// This is why it is best to work backwards when moving children around, 
	/// and why I'm assigning the elements I'm working with to `elm` and `last`
	last = target;
	while(i--){
		target.parentNode.insertBefore((elm = div.childNodes[i]), last);
		last = elm;
	}
	/// remove the target.
	target.parentNode.removeChild(target);
}


