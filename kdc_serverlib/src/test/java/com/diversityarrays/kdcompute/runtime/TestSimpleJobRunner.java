/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.DataSet;
import com.diversityarrays.kdcompute.db.DataSetBinding;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.runtime.mock.DummyEntityStore;
import com.diversityarrays.kdcompute.runtime.mock.DummyJobQueue;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;

public class TestSimpleJobRunner {

	private static final boolean REMOVE_FILES = false;

	private static EntityStore entityManager;
	private static SimpleFileStorage fileStorage;
	private static DummyJobQueue jobQueue;
	private static AdditionalVariables additionalVariables;

	@BeforeClass
	static public void setup() {

		entityManager = new DummyEntityStore();

		File userDir = new File(System.getProperty("user.dir"));
		File resultsDir = new File(userDir, "TestOutput_UserDirs");
		File kdc_server = new File(userDir.getParentFile(), "kdc_server");

		File kdcomputePlugins = new File(kdc_server, "KDCompute_Plugins");

		fileStorage = new SimpleFileStorage();
		fileStorage.setResultsRootDir(resultsDir);
		fileStorage.setKdcomputePluginsDir(kdcomputePlugins);

		// DON'T give the entityManager a FileStorage object

		jobQueue = new DummyJobQueue();

		additionalVariables = AdditionalVariables.emptyAdditionalVariables();
	}

	@AfterClass
	static public void tearDown() {
		System.out.println("==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==");
		System.out.println("==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==");
		System.out.println("==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==  ==");
		// if (entityManager instanceof DummyEntityManager) {
		// ((DummyEntityManager) entityManager).report(System.out);
		// }
		if (jobQueue != null) {
			jobQueue.report(System.out);
		}
		if (fileStorage != null) {
			fileStorage.report(System.out);
		}
	}

	@Test
	public void test() {

		SimpleAnalysisJobFactory jobRunner = new SimpleAnalysisJobFactory();
		jobRunner.setFileStorage(fileStorage);

		Knob knob_a = new Knob("a", "simple knob", KnobDataType.INTEGER, "0..100");
		// knob_a.setScriptVariableName("VALUE_FOR_KNOB_A");

		Knob knob_b = new Knob("another_parameter", "simple knob", KnobDataType.INTEGER, "0..100");

		Plugin algorithm = new Plugin("Dummy Algorithm", 1, 0, 0);
		algorithm.setKnobs(Arrays.asList(knob_a, knob_b));

		DataSet dataset_1 = new DataSet("Input#1", "a simple input");
		algorithm.setInputDataSets(Arrays.asList(dataset_1));
		entityManager.persist(algorithm);

		RunBinding binding = new RunBinding(algorithm);
		binding.addKnobBinding(new KnobBinding(knob_a, "42"));
		binding.addKnobBinding(new KnobBinding(knob_b, "90"));

		// DataSet dataset = new DataSet("Input#1", "");
		binding.addInputDataSetBinding(new DataSetBinding(dataset_1, "not really a file"));

		entityManager.persist(binding);

		AnalysisRequest request = new AnalysisRequest(binding);
		entityManager.persist(request);

		File[] folder = new File[1];
		Consumer<File> resultsFolderCreated = new Consumer<File>() {
			@Override
			public void accept(File f) {
				folder[0] = f;
			}
		};
		String forUser = System.getProperty("user.name");
		try {
			AnalysisJob job = jobRunner.createAnalysisJob(request, forUser, resultsFolderCreated,
					additionalVariables.instantiateAdditionalKDComputeSystemVariables(forUser));
			jobQueue.submitJob(job);

		} catch (JobQueueException | UserIsNotLoggedInException | IOException | ParameterException e) {
			if (folder[0] != null) {
				if (REMOVE_FILES) {
					System.err.println("--- Removing files after error ---");
					IOUtil.recursivelyRemove(folder[0]);
				} else {
					File stacktraceFile = new File(folder[0], "stacktrace.txt");
					String stacktrace = IOUtil.getStackTrace(e);
					try {
						IOUtil.writeFileContents(stacktraceFile, stacktrace);
					} catch (IOException ignore) {
					}
				}
			}
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
