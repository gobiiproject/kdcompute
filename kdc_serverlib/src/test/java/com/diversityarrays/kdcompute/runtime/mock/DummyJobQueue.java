/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.mock;

import java.io.PrintStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import org.greenrobot.eventbus.EventBus;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.EntityStore.RunStateChoice;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.JobQueueException;

/**
 * This implementation of JobQueue is designed for use in testing
 * and uses a version of EventBus that has been modified for use on JavaSE.
 * @author brianp
 */
public class DummyJobQueue implements JobQueue {

	static public class JobQueueEvent {

		public final int queueSize;
		public final AnalysisJob job;

		public JobQueueEvent(int queueSize, AnalysisJob job) {
			this.queueSize = queueSize;
			this.job = job;
		}

		@Override
		public String toString() {
		    return "Qsize=" + queueSize + ", job=" + job;
		}
	}

	EntityStore entityStore = null;

	private final Map<Long, AnalysisJob> jobsById = new HashMap<>();

	private final BlockingQueue<AnalysisJob> jobRunQueue = new LinkedBlockingQueue<>();

	public DummyJobQueue() { }

	public void report(PrintStream out) {
		out.println();
		out.println("***** ***** ***** ***** ***** ***** ***** ***** ***** *****");
		out.println("***** ***** ***** " + this.getClass().getName());
		out.println();

		out.println("Job Count: " + jobsById.size());
		for (AnalysisJob r : jobsById.values()) {
			out.println("\t" + r.getId() + ": " + r);
		}
	}

	@Override
	public void visitJobs(Predicate<AnalysisJob> visitor) {
		for (AnalysisJob r : jobsById.values()) {
			if (! visitor.test(r)) {
				return;
			}
		}
	}

	/*
	 * @see com.diversityarrays.kdcompute.runtime.JobQueue#submitJob(com.diversityarrays.kdcompute.db.AnalysisJob)
	 * Calls entityStore.persist(analysisJob);
	 */
	@Override
	public void submitJob(AnalysisJob analysisJob) throws JobQueueException {
		entityStore.persist(analysisJob);
		jobsById.put(analysisJob.getId(), analysisJob);
		jobRunQueue.add(analysisJob);
		EventBus.getDefault().post(new JobQueueEvent(getQueueLength(), analysisJob));
	}

	@Override
    public AnalysisJob takeNextJob(int millis) throws InterruptedException {
		AnalysisJob result = jobRunQueue.poll(millis, TimeUnit.MILLISECONDS);
		if (result != null) {
			EventBus.getDefault().post(new JobQueueEvent(getQueueLength(), result));
		}
		return result;
	}

	@Override
    public int getQueueLength() {
		return jobRunQueue.size();
	}

	@Override
	public void setEntityStore(EntityStore entityStore) {
		this.entityStore = entityStore;
	}

	@Override
	public List<AnalysisJob> getActiveJobs() {
       RunStateChoice choice = RunStateChoice.create("Active jobs",
                RunState.NEW, RunState.RUNNING, RunState.UNKNOWN,
                RunState.PAUSED // initial code did not include this option
                );

		List<AnalysisJob> activeJobs  =  entityStore.getAnalysisJobs(choice);
		return activeJobs;
	}

	@Override
	public void jobStatusChanged(AnalysisJob job) {
	    // Ensure that the instance in the entityStore gets updated
		AnalysisJob analysisJob = entityStore.getById(AnalysisJob.class, job.getId());
		analysisJob.setRunState(job.getRunState());
		analysisJob.setExitCode(job.getExitCode());
		entityStore.merge(analysisJob);
		entityStore.refresh(analysisJob);
	}

	@Override
	public AnalysisJob takeNextJob() {
		return jobRunQueue.poll();
	}

	@Override
	public List<AnalysisJob> getAllAnalysisJobs() {
		return entityStore.getAllAnalysisJobs();
	}

	@Override
	public List<AnalysisJob> getRecentJobs(Duration duration) {
		Date backtrack_to_date = new Date(System.currentTimeMillis() - duration.toMillis());
		List<AnalysisJob> jobs = new ArrayList<>();
		for (AnalysisJob j : jobsById.values()) {
			if (j.getCreationDateTime().after(backtrack_to_date)){
				jobs.add(j);
			}
		}
		return jobs;
	}



}
