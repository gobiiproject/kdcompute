/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.BeforeClass;
import org.junit.Test;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.IOUtil;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.JobQueueException;
import com.diversityarrays.kdcompute.runtime.ParameterException;
import com.diversityarrays.kdcompute.runtime.SimpleAnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.SimpleFileStorage;
import com.diversityarrays.kdcompute.runtime.mock.DummyEntityStore;
import com.diversityarrays.kdcompute.runtime.mock.DummyJobQueue;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;

public class SimpleJobSchedulerTest {

	private static final int reps = 12;
	private static EntityStore entityStore;
	private static FileStorage fileStorage;
	private static JobQueue jobQueue;
	private static SimpleJobScheduler jobScheduler;

	private static AdditionalVariables emptyAdditionalVariables = AdditionalVariables.emptyAdditionalVariables();

	@BeforeClass
	static public void setup() {

		File userDir = new File("/tmp");
		File resultsDir = new File(userDir, "TestOutput_UserDirs");
		File kdcomputePlugins = new File(userDir, "KDCompute_Plugins");
		kdcomputePlugins.mkdir();

		fileStorage = new SimpleFileStorage();
		fileStorage.setResultsRootDir(resultsDir);
		fileStorage.setKdcomputePluginsDir(kdcomputePlugins);

		entityStore = new DummyEntityStore();
		((DummyEntityStore) entityStore).setFileStorage(fileStorage);

		DummyJobQueue djobQueue = new DummyJobQueue();
		djobQueue.setEntityStore(entityStore);
		jobQueue = djobQueue;

		jobScheduler = new SimpleJobScheduler(djobQueue, "std.out", "std.err", emptyAdditionalVariables);
		Map<String, CallScript> callScriptsMap = new HashMap<>();
		callScriptsMap.put("sh", new BashCallScript());
		jobScheduler.setCallScriptMap(callScriptsMap);
		jobScheduler.start();

		SimpleAnalysisJobFactory sjobRunner = new SimpleAnalysisJobFactory();
		// sjobRunner.setEntityStore(entityStore);

		sjobRunner.setFileStorage(fileStorage);

		// sjobRunner.setJobQueue(jobQueue);
	}

	@Test
	public void test() throws IOException {

		SimpleAnalysisJobFactory jobRunner = new SimpleAnalysisJobFactory();
		jobRunner.setFileStorage(fileStorage);

		/*
		 * Create an algorithm with integer parameters a and b
		 */
		Knob knob_a = new Knob("a", "Integer a knob", KnobDataType.INTEGER, "0..100");
		Knob knob_b = new Knob("b", "Integer b knob", KnobDataType.INTEGER, "0..100");
		Plugin algorithm = new Plugin("a and b Algorithm", 1, 0, 0);
		algorithm.setKnobs(Arrays.asList(knob_a, knob_b));

		// Push to the entity manager
		entityStore.persist(algorithm);

		/*
		 * Create an Analysis job for this algorithm. This is equivalent of a
		 * submitted job(s)
		 */
		RunBinding binding = new RunBinding(algorithm);
		binding.addKnobBinding(new KnobBinding(knob_a, "6"));
		binding.addKnobBinding(new KnobBinding(knob_b, "7"));
		AnalysisRequest req = new AnalysisRequest(binding);

		// Push to the entity manager
		entityStore.persist(binding);
		entityStore.persist(req);

		Consumer<File> resultsFolderCreated = new Consumer<File>() {
			@Override
			public void accept(File f) {
				System.out.println(f.getAbsolutePath());
			}
		};
		String forUser = System.getProperty("user.name");

		long startTime = System.nanoTime();

		ArrayList<AnalysisJob> jobs = new ArrayList<>();
		for (int i = 0; i < reps; i++) {
			String msg = "hellofrom_" + Integer.toString(i);
			try {
				AnalysisJob job = jobRunner.createAnalysisJob(req, forUser, resultsFolderCreated,
						emptyAdditionalVariables.instantiateAdditionalKDComputeSystemVariables(forUser));

				// create a temp file
				File temp = File.createTempFile("temp-file-name", ".sh");

				System.out.println("Created file " + temp.getAbsolutePath());

				FileWriter fw = new FileWriter(temp.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("#!/bin/bash\nsleep 3\necho " + msg);
				bw.close();

				job.setShellFilePath(temp.getAbsolutePath());
				jobQueue.submitJob(job);

				jobs.add(job);
			} catch (ParameterException | IOException | UserIsNotLoggedInException | JobQueueException e) {
				e.printStackTrace();
				fail(e.getMessage());
			}
		}

		for (int i = 0; i < reps; i++) {

			while (jobs.get(i).getRunState() != RunState.COMPLETED) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			String msg = "hellofrom_" + Integer.toString(i);

			File dir = jobs.get(i).getResultsFolder();

			assert (msg.equals(readFileToString(jobScheduler.getStdout_file(dir))));

			assert ("".equals(readFileToString(jobScheduler.getStderr_file(dir))));
		}

		System.out.println("Time taken: " + Long.toString(System.nanoTime() - startTime)); // divide
																							// by
																							// 1000000
																							// to
																							// get
																							// milliseconds.

		jobScheduler.interrupt();

	}

	static private String readFileToString(File file) {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while (null != (line = br.readLine())) {
				sb.append(line).append("\n");
			}
		} catch (IOException ignore) {

		} finally {
			IOUtil.closeQuietly(br, null);
		}
		return sb.toString();
	}
}
