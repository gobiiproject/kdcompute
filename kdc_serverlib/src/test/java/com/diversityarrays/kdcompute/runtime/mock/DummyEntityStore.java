/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.mock;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.greenrobot.eventbus.EventBus;

import com.diversityarrays.kdcompute.runtime.InMemoryEntityStore;

@SuppressWarnings("nls")
public class DummyEntityStore extends InMemoryEntityStore {

	static public class DataChangedEvent {

		public final Date timestamp = new Date();
		public final Class<?> dataClass;

		public DataChangedEvent(Class<?> clz) {
			this.dataClass = clz;
		}
	}

	public DummyEntityStore() {
		super();
		setOnDataChanged(clz -> onDataChanged(clz));
	}

	private void onDataChanged(Class<?> recordClass) {
		EventBus.getDefault().post(new DataChangedEvent(recordClass));
	}

	@Override
	public boolean supportsVisitListOfEntities() {
		return true;
	}

	@Override
	public <T> Optional<T> visitListOfEntities(Class<? extends T> tclass, Predicate<T> predicate) {
		T result = null;
		Map<Long, Object> recordMap = getRecordMap(tclass);
		if (recordMap != null) {
			for (Object obj : recordMap.values()) {
				T t = (T) obj;
				if (predicate.test(t)) {
					result = t;
					break;
				}
			}
		}
		return Optional.ofNullable(result);
	}

}
