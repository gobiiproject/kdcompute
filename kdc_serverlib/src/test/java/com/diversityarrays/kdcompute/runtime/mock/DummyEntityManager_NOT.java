/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.mock;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.Id;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.metamodel.Metamodel;

public class DummyEntityManager_NOT {

    private long nextId = 1;
    private boolean closed = false;
    private final Map<Class<?>, Map<Long, Object>> recordsByClass = new HashMap<>();

    private final Logger logger;

    private final IdFieldStore idFieldStore = new IdFieldStore();
    private FlushModeType flushModeType = FlushModeType.AUTO;

    static class IdFieldStore {

        private final Map<Class<?>, Field> idFieldByClass = new HashMap<>();

        public Field getIdField(Class<?> tclass) {
            Field idField = idFieldByClass.get(tclass);
            if (idField == null) {
                for (Field f : tclass.getDeclaredFields()) {
                    if (Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    Id id = f.getAnnotation(Id.class);
                    if (id != null) {
                        if (idField != null) {
                            throw new RuntimeException("Multiple @Id annotations in " + tclass.getName());
                        }
                        idField = f;
                    }
                }
                if (idField == null) {
                    throw new RuntimeException("No @Id field in " + tclass.getName());
                }
                if (Long.class != idField.getType()) {
                    throw new RuntimeException("@Id " + tclass.getName() + "." + idField.getName() + " is not a Long");
                }
                idField.setAccessible(true);
                idFieldByClass.put(tclass, idField);
            }
            return idField;
        }
    }

    public DummyEntityManager_NOT() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    public void report(PrintStream out) {
        List<Class<?>> classes = new ArrayList<>(recordsByClass.keySet());
        Comparator<Class<?>> comp = new Comparator<Class<?>>() {

            @Override
            public int compare(Class<?> o1, Class<?> o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        classes.sort(comp);

        out.println();
        out.println("***** ***** ***** ***** ***** ***** ***** ***** ***** *****");
        out.println("***** ***** ***** " + this.getClass().getName());
        out.println();
        for (Class<?> clz : classes) {
            out.println("==== Class: " + clz.getName());
            Map<Long, Object> map = recordsByClass.get(clz);
            for (Long id : map.keySet()) {
                Object record = map.get(id);
                out.println(id + "\t" + record);
            }
        }
    }


    public void clear() {
        if (closed) {
            throw new IllegalStateException();
        }

        recordsByClass.clear();
    }


    public void close() {
        closed = true;
    }


    public boolean contains(Object record) {
        if (closed) {
            throw new IllegalStateException();
        }

        boolean result = false;
        Class<?> rclass = record.getClass();
        Map<Long, Object> map = recordsByClass.get(rclass);
        if (map != null) {
            Field idField = idFieldStore.getIdField(rclass);
            try {
                Long id = (Long) idField.get(record);
                result = map.containsKey(id);
            }
            catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return result;
    }


    public Query createNamedQuery(String arg0) {
        throw new UnsupportedOperationException();
    }


    public <T> TypedQuery<T> createNamedQuery(String arg0, Class<T> arg1) {
        throw new UnsupportedOperationException();
    }


    public Query createNativeQuery(String arg0) {
        throw new UnsupportedOperationException();
    }


    public Query createNativeQuery(String arg0, @SuppressWarnings("rawtypes") Class arg1) {
        throw new UnsupportedOperationException();
    }


    public Query createNativeQuery(String arg0, String arg1) {
        throw new UnsupportedOperationException();
    }


    public Query createQuery(String arg0) {
        throw new UnsupportedOperationException();
    }


    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
        throw new UnsupportedOperationException();
    }


    public <T> TypedQuery<T> createQuery(String qry, Class<T> tclass) {
        Pattern pattern = Pattern.compile("^select ([a-z]+) from ([a-z][a-z_]+) (\\1)$", Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(qry.trim());
        if (! m.matches()) {
            throw new IllegalArgumentException("Invalid query: " + qry);
        }
        String className = m.group(2);

        if (! tclass.getSimpleName().equalsIgnoreCase(className)) {
            throw new IllegalArgumentException(className + " does not match " + tclass.getSimpleName());
        }



        throw new UnsupportedOperationException();
    }


    public void detach(Object record) {
        if (closed) {
            throw new IllegalStateException();
        }

        Class<?> rclass = record.getClass();
        Map<Long, Object> map = recordsByClass.get(rclass);
        if (map != null) {
            Field idField = idFieldStore.getIdField(rclass);
            try {
                Long id = (Long) idField.get(record);
                map.remove(id);
            }
            catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SuppressWarnings("unchecked")

    public <T> T find(Class<T> tclass, Object id) {
        if (closed) {
            throw new IllegalStateException();
        }

        T result = null;
        if (! (id instanceof Long)) {
            throw new IllegalArgumentException("id must be of type Long, not " + id.getClass().getName());
        }
        Map<Long, Object> map = recordsByClass.get(tclass);
        if (map != null) {
            result = (T) map.get(id);
        }
        return result;
    }


    public <T> T find(Class<T> tclass, Object id, Map<String, Object> properties) {
        return find(tclass, id);
    }


    public <T> T find(Class<T> tclass, Object id, LockModeType arg2) {
        return find(tclass, id);
    }


    public <T> T find(Class<T> tclass, Object id, LockModeType arg2, Map<String, Object> arg3) {
        return find(tclass, id);
    }


    public void flush() {
        // No-op
        // TODO perhaps do this to some json files?
    }


    public CriteriaBuilder getCriteriaBuilder() {
        if (closed) {
            throw new IllegalStateException();
        }
        throw new UnsupportedOperationException();
    }


    public Object getDelegate() {
        // TODO Auto-generated method stub
        return null;
    }


    public EntityManagerFactory getEntityManagerFactory() {
        if (closed) {
            throw new IllegalStateException();
        }
        throw new UnsupportedOperationException();
    }


    public FlushModeType getFlushMode() {
        return flushModeType;
    }


    public void setFlushMode(FlushModeType type) {
        this.flushModeType = type;
    }


    public LockModeType getLockMode(Object arg0) {
        return LockModeType.NONE;
    }


    public Metamodel getMetamodel() {
        throw new UnsupportedOperationException();
    }


    public Map<String, Object> getProperties() {
        return Collections.emptyMap();
    }


    public <T> T getReference(Class<T> tclass, Object id) {
        T result = find(tclass, id);
        if (result == null) {
            throw new EntityNotFoundException(tclass.getName() + " ID=" + id);
        }
        return result;
    }


    public EntityTransaction getTransaction() {
        throw new UnsupportedOperationException();
    }


    public boolean isOpen() {
        return ! closed;
    }


    public void joinTransaction() {
        throw new UnsupportedOperationException();
    }


    public void lock(Object arg0, LockModeType arg1) {
        throw new UnsupportedOperationException();
    }


    public void lock(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
        throw new UnsupportedOperationException();
    }

    /**
     * Merge the state of the given entity into the current persistence context.
     * @return the managed instance that the state was merged to
     */

    public <T> T merge(T record) {
        @SuppressWarnings("unchecked")
        T persisted = (T) getPersistedInstance(record);
        if (persisted == null) {
            throw new IllegalArgumentException("no persisted instance for " + record);
        }
        if (persisted != record) {
            copyFieldValuesFromTo(record, persisted);
        }
        return persisted;
    }

    /**
     * Make an instance managed and persistent.
     */

    public void persist(Object record) {
        Class<?> tclass = record.getClass();
        Field idField =  idFieldStore.getIdField(tclass);

        try {
            Object idValue = idField.get(record);
            if (idValue != null) {
                throw new EntityExistsException("ID(" + idValue + ") Already assigned: " + tclass.getName() + "." + idField.getName());
            }

            Long id = nextId++;
            idField.set(record, id);

            logger.info("created id=" + id + " for " + tclass.getName());
            Map<Long, Object> map = getRecordMap(tclass);
            map.put(id, record);
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

//        EventBus.getDefault().post(new DataChangedEvent(tclass));
    }

    private Object getPersistedInstance(Object record) {
        Object result = null;
        Class<?> tclass = record.getClass();
        Map<Long, Object> map = getRecordMap(tclass);
        if (map != null) {
            Field idField = idFieldStore.getIdField(tclass);
            try {
                Long id = (Long) idField.get(record);
                result = map.get(id);
            }
            catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return result;
    }

    private Map<Long, Object> getRecordMap(Class<?> tclass) {
        Map<Long, Object> map = recordsByClass.get(tclass);
        if (map == null) {
            map = new HashMap<>();
            recordsByClass.put(tclass, map);
        }
        return map;
    }

    /**
     * Refresh the state of the instance from the database, overwriting changes made to the entity, if any.
     */

    public void refresh(Object record) {
        Object persisted = getPersistedInstance(record);
        if (persisted == null) {
            throw new IllegalArgumentException("no persisted instance for " + record);
        }
        if (persisted != record) {
            copyFieldValuesFromTo(persisted, record);
        }
    }

    private final Map<Class<?>, List<Field>> fieldsByClass = new HashMap<>();

    private void copyFieldValuesFromTo(Object from, Object to) {
        Class<?> clz = to.getClass();
        if (! clz.equals(from.getClass())) {
            throw new RuntimeException("Different classes: " + from.getClass().getName() + " <> " + to.getClass().getName());
        }
        List<Field> fields = fieldsByClass.get(clz);
        if (fields == null) {
            fields = new ArrayList<>();
            for (Field f : clz.getDeclaredFields()) {
                int mods = f.getModifiers();
                if (! Modifier.isStatic(mods)) {
                    f.setAccessible(true);
                    fields.add(f);
                }
            }
            fieldsByClass.put(clz, fields);
        }

        for (Field f : fields) {
            try {
                Object fromValue = f.get(from);
                Object toValue = f.get(to);
                f.set(to, fromValue);

                if (fromValue == null) {
                    if (toValue != null) {
                    }
                }
                else if (! fromValue.equals(toValue)) {
                }
            }
            catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

//        if (anyChanged) {
//            EventBus.getDefault().post(new DataChangedEvent(clz));
//        }
    }


    public void refresh(Object record, Map<String, Object> arg1) {
        refresh(record);
    }


    public void refresh(Object record, LockModeType arg1) {
        refresh(record);
    }


    public void refresh(Object record, LockModeType arg1, Map<String, Object> arg2) {
        refresh(record);
    }


    public void remove(Object record) {
        Class<?> tclass = record.getClass();
        Field idField =  idFieldStore.getIdField(tclass);

        try {
            Long id = idField.getLong(record);

            Map<Long, Object> map = recordsByClass.get(tclass);
            if (map != null) {
                map.remove(id);
            }
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }



    public void setProperty(String arg0, Object arg1) {
        throw new UnsupportedOperationException();
    }


    public <T> T unwrap(Class<T> arg0) {
        throw new UnsupportedOperationException();
    }
     @SuppressWarnings("unchecked")
    public <T> List<T> getRecords(Class<T> tclass) {
         Map<Long, Object> recordMap = getRecordMap(tclass);
         List<T> result = new ArrayList<>();
         for (Object obj : recordMap.values()) {
             result.add((T) obj);
         }
        return result;
    }

}
