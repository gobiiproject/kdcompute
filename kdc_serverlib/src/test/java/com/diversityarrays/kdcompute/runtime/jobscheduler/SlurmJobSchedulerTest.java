/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.BeforeClass;
import org.junit.Test;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.AnalysisRequest;
import com.diversityarrays.kdcompute.db.Knob;
import com.diversityarrays.kdcompute.db.KnobBinding;
import com.diversityarrays.kdcompute.db.KnobDataType;
import com.diversityarrays.kdcompute.db.Plugin;
import com.diversityarrays.kdcompute.db.RunBinding;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.EntityStore;
import com.diversityarrays.kdcompute.runtime.FileStorage;
import com.diversityarrays.kdcompute.runtime.JobQueue;
import com.diversityarrays.kdcompute.runtime.JobQueueException;
import com.diversityarrays.kdcompute.runtime.ParameterException;
import com.diversityarrays.kdcompute.runtime.SimpleAnalysisJobFactory;
import com.diversityarrays.kdcompute.runtime.SimpleFileStorage;
import com.diversityarrays.kdcompute.runtime.mock.DummyEntityStore;
import com.diversityarrays.kdcompute.runtime.mock.DummyJobQueue;
import com.diversityarrays.kdcompute.script.AdditionalVariables;
import com.diversityarrays.kdcompute.script.UserIsNotLoggedInException;

public class SlurmJobSchedulerTest {

	private static EntityStore entityStore;
	private static FileStorage fileStorage;
	private static JobQueue jobQueue;
	private static JobScheduler jobScheduler;
	private static AdditionalVariables additionalVariables;

	@BeforeClass
	static public void setup() {

		File userDir = new File("/tmp");
		File resultsDir = new File(userDir, "TestOutput_UserDirs");
		File kdcomputePlugins = new File(userDir, "KDCompute_Plugins");
		kdcomputePlugins.mkdir();

		fileStorage = new SimpleFileStorage();
		fileStorage.setResultsRootDir(resultsDir);
		fileStorage.setKdcomputePluginsDir(kdcomputePlugins);

		entityStore = new DummyEntityStore();
		((DummyEntityStore) entityStore).setFileStorage(fileStorage);

		DummyJobQueue djobQueue = new DummyJobQueue();
		djobQueue.setEntityStore(entityStore);
		jobQueue = djobQueue;
		jobScheduler = new SlurmJobScheduler(djobQueue);
		jobScheduler.start();

		SimpleAnalysisJobFactory sjobRunner = new SimpleAnalysisJobFactory();
		// sjobRunner.setEntityStore(entityStore);

		sjobRunner.setFileStorage(fileStorage);

		// sjobRunner.setJobQueue(jobQueue);
		additionalVariables = AdditionalVariables.emptyAdditionalVariables();
	}

	@Test
	public void test() {

		SimpleAnalysisJobFactory jobRunner = new SimpleAnalysisJobFactory();
		jobRunner.setFileStorage(fileStorage);

		/*
		 * Create an algorithm with integer parameters a and b
		 */
		Knob knob_a = new Knob("a", "Integer a knob", KnobDataType.INTEGER, "0..100");
		Knob knob_b = new Knob("b", "Integer b knob", KnobDataType.INTEGER, "0..100");
		Plugin algorithm = new Plugin("a and b Algorithm", 1, 0, 0);
		algorithm.setKnobs(Arrays.asList(knob_a, knob_b));

		// Push to the entity manager
		entityStore.persist(algorithm);

		/*
		 * Create an Analysis job for this algorithm. This is equivalent of a
		 * submitted job(s)
		 */
		RunBinding binding = new RunBinding(algorithm);
		binding.addKnobBinding(new KnobBinding(knob_a, "6"));
		binding.addKnobBinding(new KnobBinding(knob_b, "7"));
		AnalysisRequest req = new AnalysisRequest(binding);

		// Push to the entity manager
		entityStore.persist(binding);
		entityStore.persist(req);

		Consumer<File> resultsFolderCreated = new Consumer<File>() {
			@Override
			public void accept(File f) {
				System.out.println(f.getAbsolutePath());
			}
		};
		String forUser = System.getProperty("user.name");
		AnalysisJob job = null;

		long id = 0;

		try {
			job = jobRunner.createAnalysisJob(req, forUser, resultsFolderCreated,
					additionalVariables.instantiateAdditionalKDComputeSystemVariables(forUser));
			jobQueue.submitJob(job);
			id = job.getId();
		} catch (JobQueueException | UserIsNotLoggedInException | IOException | ParameterException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		while (entityStore.getById(AnalysisJob.class, id).getRunState() != RunState.COMPLETED) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		jobScheduler.interrupt();

	}

}
