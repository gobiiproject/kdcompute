/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.diversityarrays.kdcompute.db.AnalysisJob;
import com.diversityarrays.kdcompute.db.RunState;
import com.diversityarrays.kdcompute.runtime.EntityStore.RunStateChoice;

public class TestInMemoryGetAnalyisJobs {

    private static InMemoryEntityStore ENTITY_STORE;

    static private final String USER_NAME = "user1";

    static private int nJobs = 0;
    static private final Date TEST_START_DATE = new Date();
    static private final Random RANDOM = new Random(1);

    static private Date randomDateOffset(int seconds) {
        long offset = (long) (Math.signum(seconds) * RANDOM.nextInt(Math.abs(seconds)) + 1000);
        return new Date(TEST_START_DATE.getTime() +  offset);
    }

    static private Date dateOffset(int seconds) {
        return new Date(TEST_START_DATE.getTime() +  seconds * 1000);
    }

    static private final File OUTPUT_USERDIRS;
    static private final File USER_FOLDER;
    static {
        File userDir = new File(System.getProperty("user.dir"));
        OUTPUT_USERDIRS = new File(userDir, "TestOutput_UserDirs");
        USER_FOLDER = new File(OUTPUT_USERDIRS, USER_NAME);
    }

    static private void createJob(RunState rs) {
        AnalysisJob job = new AnalysisJob(null,
                dateOffset(-300),
                "Job#" + (nJobs++) + " for " + USER_NAME);

        job.setRequestingUser(USER_NAME);
        job.setRunState(rs);

        switch (rs) {
        case CANCELLED:
            job.setStartDateTime(randomDateOffset(-10));
            break;
        case COMPLETED:
            job.setStartDateTime(randomDateOffset(-100));
            job.setEndDateTime(new Date());
            // Assume 25% utilisation
            job.setCpuMillisConsumed(job.getEndDateTime().getTime() - job.getStartDateTime().getTime() / 4);
            break;
        case ERASED:
            job.setStartDateTime(randomDateOffset(-100));
            job.setEndDateTime(new Date());
            // Assume 25% utilisation
            job.setCpuMillisConsumed(job.getEndDateTime().getTime() - job.getStartDateTime().getTime() / 4);
            break;
        case FAILED:
            job.setStartDateTime(randomDateOffset(-100));
            job.setError("Failed job");
            break;
        case NEW:
            break;
        case PAUSED:
            job.setStartDateTime(randomDateOffset(-100));
            break;
        case RUNNING:
            job.setStartDateTime(randomDateOffset(-100));
            break;
        case UNKNOWN:
            break;
        default:
            throw new RuntimeException("unhandled: " + rs);
        }

        ENTITY_STORE.persist(job);
        Long id = job.getId();
        File resultsFolder = new File(USER_FOLDER, "job-" + id);
        job.setResultsFolderPath(resultsFolder.getPath());
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ENTITY_STORE = new InMemoryEntityStore();

        for (RunState rs : RunState.values()) {
            createJob(rs);
        }
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testJobCount() {
        List<AnalysisJob> allJobs = ENTITY_STORE.getAllAnalysisJobs();
        assertEquals("Wrong number of AnalysisJobs", RunState.values().length, allJobs.size());
        for (AnalysisJob job : allJobs) {
            String expected = job.getResultsFolderPath();
            String jobIdent = job.getDisplayName()+"["+expected+"] : ";
            try {
                File file = job.getResultsFolder();
                if (file==null) {
                    fail(jobIdent + "null from getResultsFolder()");
                }
                assertEquals("folderPath and folder mismatch", expected, file.getPath());
            }
            catch (IOException e) {
                fail(jobIdent + e.getMessage());
            }
        }
    }

    @Test
    public void testSingleRunState() {
        for (RunState rs : RunState.values()) {
            RunStateChoice rsc = RunStateChoice.create("Only " + rs.name(), rs);
            if (RunState.FAILED == rs) {
                int dbg = 0;
                ++dbg;
            }
            List<AnalysisJob> jobs = ENTITY_STORE.getAnalysisJobs(rsc);
            assertEquals("Invalid result for " + rsc.getDescription(), 1, jobs.size());
        }
    }

    @Test
    public void testNotSingleRunState() {
        RunState[] all = RunState.values();
        int expectedCount = all.length - 1;
        for (RunState rs : all) {
            RunState[] states = new RunState[expectedCount];
            int i = 0;
            for (RunState s : all) {
                if (s != rs) {
                    states[i++] = s;
                }
            }
            RunStateChoice rsc = RunStateChoice.create("All except " + rs.name(), states);
            List<AnalysisJob> jobs = ENTITY_STORE.getAnalysisJobs(rsc);
            assertEquals("Invalid result for " + rsc.getDescription(), expectedCount, jobs.size());
        }
    }

    @Test
    public void testAllJobs() {
        List<AnalysisJob> allJobs_1 = ENTITY_STORE.getAnalysisJobs(EntityStore.ALL_JOBS);
        List<AnalysisJob> allJobs_2 = ENTITY_STORE.getAllAnalysisJobs();
        assertEquals("getAnalysisJobs(ALL_JOBS) <> getAllAnalysisJobs()", allJobs_1.size(), allJobs_2.size());
    }
    @Test
    public void testActiveJobs() {
        List<AnalysisJob> jobs = ENTITY_STORE.getAnalysisJobs(EntityStore.ACTIVE_JOBS);
        long expected = Arrays.asList(RunState.values()).stream()
            .filter(RunState::isActive)
            .count();
        assertEquals("Active Jobs wrong count", expected, jobs.size());
    }

    @Test
    public void testCompletedJobs() {
        List<AnalysisJob> jobs = ENTITY_STORE.getAnalysisJobs(EntityStore.COMPLETED_JOBS);
        assertEquals("Completed Jobs wrong count", 1, jobs.size());
    }

    @Test
    public void testActive() {
        List<AnalysisJob> jobs = ENTITY_STORE.getAnalysisJobs(EntityStore.INACTIVE_JOBS);
        long expected = Arrays.asList(RunState.values()).stream()
                .filter(rs -> ! rs.isActive())
                .count();
            assertEquals("Inactive Jobs wrong count", expected, jobs.size());
    }

}
