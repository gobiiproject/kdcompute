/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.runtime.jobscheduler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.diversityarrays.jobscheduler.slurm.SlurmController;

public class SlurmControllerTest {

    private static SlurmController slurmController;

    @Before
    static public void init() {
        slurmController = new SlurmController();
    }
	
	@Test
	public void cmdWithTimeoutFail() {
		try {
			slurmController.execCmdTimeout("sleep 1",100);
		} catch (IOException |StderrException  e) {
			fail();
		} catch (InterruptedException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void cmdWithTimeoutSucess() {
		try {
			slurmController.execCmdTimeout("sleep 1",1100);
		} catch (IOException |StderrException  e) {
			fail();
		} catch (InterruptedException e) {
			fail();	
		}
		
	}
	
	@Test
	public void sbatch_and_squeue() {
		try {
			assertEquals(0,slurmController.squeue().size());
			
			//create a temp file
			File temp = File.createTempFile("tempfile", ".sh"); 

			//write it
			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			bw.write("#!/bin/bash\n"
					+ "srun -N1 sleep 3"
					);
			bw.close();

			for(int i=0;i<3;i++) {
				slurmController.sbatch(temp.getAbsolutePath());
			}
			
			assertEquals(3,slurmController.squeue().size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}


}
