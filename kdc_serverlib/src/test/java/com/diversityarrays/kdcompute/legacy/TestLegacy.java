/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.legacy;

import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.junit.Ignore;
import org.junit.Test;

import com.diversityarrays.kdcompute.Pair;
import com.diversityarrays.kdcompute.db.Plugin;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("nls")
public class TestLegacy {
    
    private static int VERBOSE = 0;

    @Test
    public void testDistMatrixCalculatorSpec() {
        doSpecTemplate("dist_matrix_calculator");
    }
    
    @Test
    public void testImportMarkerSpec() {
        doSpecTemplate("import_marker");
    }
    
    @Test
    public void testExportMarkerSpec() {
        doSpecTemplate("export_marker");
    }
    
    private void doSpecTemplate(String pluginName) {
        String resourceName = pluginName + "/" + pluginName + "_spec.json";
        
        InputStream is = getClass().getResourceAsStream(resourceName);
        if (is == null) {
            fail("Missing resource: " + resourceName);
        }
        
        try {
            String json = readContent(is);
            Gson gson = new GsonBuilder().create();
            
            AlgorithmSpec spec = gson.fromJson(json, AlgorithmSpec.class);
            System.out.println("AlgrithmSpec: Found '" + spec.AlgorithmName);
            System.out.println(" # args=" + spec.Args.size());
            System.out.println(spec);
            
            doLegacyConverter(pluginName, spec);
        }
        catch (IOException e) {
            fail(pluginName + ": " + e.getMessage());
        }
        
    }
    
    static private String readContent(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[8096];
        int nb;
        while (-1 != (nb = is.read(buffer))) {
            baos.write(buffer, 0, nb);
        }
        baos.close();
        
        return baos.toString();
    }

    @Test
    public void testDistMatrixCalculatorTemplate() {
        doPluginTemplate("dist_matrix_calculator", 8);
    }
    
    @Test
    public void testImportMarkerTemplate() {
        doPluginTemplate("import_marker", 12);
    }
    
    @Test
    public void testExportMarkerTemplate() {
        doPluginTemplate("export_marker", 8);
    }

    private void doPluginTemplate(String pluginName, Object expectedUnbound) {
        String resourceName = pluginName + "/" + pluginName + "_bash.tmpl";
        
        InputStream is = getClass().getResourceAsStream(resourceName);
        if (is == null) {
            fail("Missing resource: " + resourceName);
        }
        
        try {
            LegacyTemplate t = new LegacyTemplate(readContent(is));
            t.addIgnore("YOUR_TMPL_VAR_NAME");
            
            Map<String, String> vmap = LegacyTemplate.makeOldTemplateVariablesMap();
            
            final List<String> missing = new ArrayList<>();
            Consumer<Set<String>> handleMissing = new Consumer<Set<String>>() {
                @Override
                public void accept(Set<String> set) {
                    missing.addAll(set);
                }
            };
            System.out.println("=== plugin: " + pluginName);
            
            String filledTemplate = t.replaceVariables(vmap, handleMissing);            
            if (VERBOSE > 1) {
                System.out.println(filledTemplate);
            }
            
            if (! missing.isEmpty()) {
                if (VERBOSE > 0) {
                    Collections.sort(missing);
                    System.out.println("- - - - - - - - - - -");
                    System.out.println("Unbound: " + missing.size());
                    for (String v : missing) {
                        System.out.println("\t" + v);
                    }
                }
            }

            org.junit.Assert.assertEquals(pluginName + ": Unbound variable count mismatch", 
                    expectedUnbound, missing.size());

//            List<String> vars = new ArrayList<>(t.getVariableNames());
//            Collections.sort(vars);
//            for (String v : vars) {
//                System.out.println("\t" + v);
//            }
        }
        catch (IOException e) {
            fail(e.getMessage());
        }
        finally {
            try { is.close(); } catch (IOException ignore) {}
        }
    }

    @Ignore
    public void test() {
        
        String[] lines = {
                "First line",
                "DALCOOKIEPATH=<TMPL_VAR NAME=DALCOOKIEPATH>",
                "DALBASEURL=<TMPL_VAR NAME=DALBASEURL>",
                "DALWRITETOKEN=<TMPL_VAR NAME=DALWRITETOKEN>",
                "",
                "last line"
        };
        StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(line).append('\n');
        }
        String s = sb.toString();
        LegacyTemplate template = new LegacyTemplate(s);
        
        Map<String,String> map = new HashMap<>();
        map.put("DALCOOKIEPATH",  "a cookie path");
        map.put("DALBASEURL",  "a base url");
        map.put("DALWRITETOKEN",  "my write token");
        
        String out = template.replaceVariables(map);
        System.out.println("========");
        System.out.println(out);
//        fail("Not yet implemented");
    }

    
    private void doLegacyConverter(String pluginName, AlgorithmSpec spec) {
        List<String> warnings = new ArrayList<>();
        try {
            Pair<String,Plugin> pair = LegacyConverter.createFrom(pluginName, spec, warnings);
            System.out.println(pair.first);
            System.out.println("\t" + pair.second);
        }
        catch (LegacyConversionException e) {
            fail(e.getMessage());
        }
    }
}
