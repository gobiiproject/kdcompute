#!/usr/bin/perl -w

use strict;
use warnings;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST GET);
use HTTP::Cookies;
use Time::HiRes qw( tv_interval gettimeofday );
use String::Random qw( random_string );

use Digest::HMAC_SHA1 qw(hmac_sha1 hmac_sha1_hex);
use Digest::MD5;

if (scalar(@ARGV) < 1) {

  print "Data file name is missing!\n";
  exit;
}

my $upload_file = $ARGV[0];

$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

my $browser  = LWP::UserAgent->new();

my $default_timeout = $browser->timeout();

print "Default Timeout in Seconds: $default_timeout\n";

$browser->timeout(20000);

my $new_timeout = $browser->timeout();

print "New Timeout in Seconds: $new_timeout\n";

my $anal_id                 = $ARGV[1];
my $forced                  = 1;
my $header_row              = $ARGV[2];
my $marker_name_col         = $ARGV[3] - 1;
my $seq_col                 = $ARGV[4] - 1;
my $marker_meta_col_start   = $ARGV[5] - 1;
my $marker_meta_col_end     = $ARGV[6] - 1;
my $marker_data_col_start   = $ARGV[7] - 1;
my $marker_data_col_end     = $ARGV[8] - 1;
my $dal_base_url            = $ARGV[9];
my $dal_cookie_file         = $ARGV[10];
my $write_token             = $ARGV[11];
my $dataset_type            = $ARGV[12];

print "
Submitting to DAL for marker dataset import:
Marker name col:      $marker_name_col
Sequence col:         $seq_col
Marker metacol start: $marker_meta_col_start
Marker metacol end:   $marker_meta_col_end
Marker datacol start: $marker_data_col_start
Marker datacol end:   $marker_data_col_end
Dataset type id:      $dataset_type
";

my $cookie_jar = HTTP::Cookies->new();

$cookie_jar->load($dal_cookie_file);
$browser->cookie_jar($cookie_jar);

my $rand_num = make_random_number();

my $atomic_data = '';
$atomic_data   .= "$forced";
$atomic_data   .= "$marker_name_col";
$atomic_data   .= "$seq_col";
$atomic_data   .= "$marker_meta_col_start";
$atomic_data   .= "$marker_meta_col_end";
$atomic_data   .= "$marker_data_col_start";
$atomic_data   .= "$marker_data_col_end";
$atomic_data   .= "$header_row";
$atomic_data   .= "$dataset_type";

my $para_order = '';
$para_order   .= 'Forced,';
$para_order   .= 'MarkerNameCol,';
$para_order   .= 'SequenceCol,';
$para_order   .= 'MarkerMetaColStart,';
$para_order   .= 'MarkerMetaColEnd,';
$para_order   .= 'MarkerDataColStart,';
$para_order   .= 'MarkerDataColEnd,';
$para_order   .= 'HeaderRow,';
$para_order   .= 'DataSetType,';

open(my $upload_fh, "$upload_file");

my $md5_engine = Digest::MD5->new();
$md5_engine->addfile($upload_fh);

close($upload_fh);

my $upload_file_md5 = $md5_engine->hexdigest();

my $url = $dal_base_url . "/analysisgroup/$anal_id/import/markerdata/csv";
my $data2sign = '';
$data2sign   .= "$url";
$data2sign   .= "$rand_num";
$data2sign   .= "$atomic_data";
$data2sign   .= "$upload_file_md5";

my $signature = hmac_sha1_hex($data2sign, $write_token);

print "Write token: $write_token\n";

my $parameter = [];

push(@{$parameter}, uploadfile           => [$upload_file]);
push(@{$parameter}, rand_num             => "$rand_num");
push(@{$parameter}, url                  => "$url");
push(@{$parameter}, Forced               => "$forced");
push(@{$parameter}, HeaderRow            => "$header_row");
push(@{$parameter}, MarkerNameCol        => "$marker_name_col");
push(@{$parameter}, SequenceCol          => "$seq_col");
push(@{$parameter}, MarkerMetaColStart   => "$marker_meta_col_start");
push(@{$parameter}, MarkerMetaColEnd     => "$marker_meta_col_end");
push(@{$parameter}, MarkerDataColStart   => "$marker_data_col_start");
push(@{$parameter}, MarkerDataColEnd     => "$marker_data_col_end");
push(@{$parameter}, DataSetType          => "$dataset_type");
push(@{$parameter}, param_order          => "$para_order");
push(@{$parameter}, signature            => "$signature");

my $inside_req_res = POST ($url, Content_Type => 'multipart/form-data', Content => $parameter);

my $inside_req = $browser->request($inside_req_res);

print $inside_req->content() . "\n";

sub make_random_number {

        my $len = 32;
        my %args = @_;
        if ($args{-len}) { $len = $args{-len}; }
        my @chars2use = (0 .. 9);
        my $randstring = join("", map $chars2use[rand @chars2use], 0 .. $len);
        return $randstring;
}
