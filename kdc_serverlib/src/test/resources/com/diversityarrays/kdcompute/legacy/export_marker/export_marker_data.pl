#!/usr/bin/perl -w

use strict;
use warnings;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST GET);
use HTTP::Cookies;
use Time::HiRes qw( tv_interval gettimeofday );
use String::Random qw( random_string );

use Digest::MD5 qw(md5 md5_hex md5_base64);
use Digest::HMAC_SHA1 qw(hmac_sha1 hmac_sha1_hex);
use XML::Simple;
use XML::Writer;
use Getopt::Long;
use Text::CSV_XS;

sub make_random_number {

    my $len = 32;
    my %args = @_;
    if ($args{-len}) { $len = $args{-len}; }
    my @chars2use = (0 .. 9);
    my $randstring = join("", map $chars2use[rand @chars2use], 0 .. $len);
    return $randstring;
}

sub xml2arrayref {

  my $data_src = $_[0];
  my $tag_name = $_[1];

  my $data_ref = XMLin($data_src);

  my $data_row_ref = [];
  if ($data_ref->{$tag_name}) {

    # when there is only one tag inside the root tag, XMLin
    # returns a hash reference rather than array reference.
    if ( ref $data_ref->{$tag_name} eq 'HASH' ) {

      my @forced_aref;
      push(@forced_aref, $data_ref->{$tag_name});
      $data_row_ref = \@forced_aref;
    }
    elsif (ref $data_ref->{$tag_name} eq 'ARRAY') {

      $data_row_ref = $data_ref->{$tag_name};
    }
  }

  return $data_row_ref;
}

sub print_flapjack_files {

    # Assuming original file format has only 2 metadata columns (with Chromosome and ChromosomePosition), 1 marker name column and multiple marker data columns
    
    my $marker_name_col = $_[0];
    my $last_header_row = $_[1];
    my $marker_metadata_start_col = $_[2];
    my $marker_metadata_end_col = $_[3];
    my $marker_data_start_col = $_[4];
    my $marker_data_end_col = $_[5];
    my $total_marker = $_[6];
    my $out_dir = $_[7];
    my $csv_input_file = $_[8];
    
    my $flapjack_out_map_file_path = $out_dir . "flapjack_map_file.csv";
    my $flapjack_out_genotype_file_path = $out_dir . "flapjack_genotype_file.csv";
    my $csv = Text::CSV_XS->new ({ binary => 1, auto_diag => 1 });
    
    # Create file handlers
    open my $in_fh, "<:encoding(utf8)", $csv_input_file or die "$csv_input_file: $!";
    open my $out_map_fh, ">:encoding(utf8)", $flapjack_out_map_file_path or die "$flapjack_out_map_file_path: $!";
    open my $out_genotype_fh, ">:encoding(utf8)", $flapjack_out_genotype_file_path or die "$flapjack_out_genotype_file_path: $!";
    
    my $row_num = 0;
    my %genotype_row_object;
    
    while (my $row = $csv->getline ($in_fh)) {
    
      if ($row_num > $last_header_row) {
	      
	  # Print map file data
	  my $new_row_str = $row->[$marker_name_col] . ",";
	  for (my $i = $marker_metadata_start_col; $i <= $marker_metadata_end_col; ++$i) {
	  
	      $new_row_str .= $row->[$i] . ",";
	  }
	  
	  # Remove last comma and add new line
	  chop($new_row_str);
	  $new_row_str .= "\n";
	  print $out_map_fh $new_row_str;
	  
	  # Store score data to hash
	  for (my $i = $marker_data_start_col; $i <= $marker_data_end_col; ++$i) {
	  
	      push( @{ $genotype_row_object { $i } }, $row->[$i]);
	  }
	  
      }
      elsif ($row_num == $last_header_row) {
      
	  # Prepare the object genotype_row_data. Column number of marker as key, a new array as value
	  for (my $i = $marker_data_start_col; $i <= $marker_data_end_col; ++$i) {
	  
	      my @genotype_row = ($row->[$i]);
	      $genotype_row_object{$i} = \@genotype_row;
	  }
	  
	  # Now get all marker names
	  my $csv_marker_data = Text::CSV_XS->new ({ binary => 1, auto_diag => 1 });
	  my $marker_header_str = ",";
	  
	  open my $marker_in_fh, "<:encoding(utf8)", $csv_input_file or die "$csv_input_file: $!";
	  my $marker_row_num = 0;
	  while (my $marker_row = $csv_marker_data->getline ($marker_in_fh)) {
	  
	      if ($marker_row_num > $last_header_row) {
		  
		  $marker_header_str .= $marker_row->[$marker_name_col] . ",";
	      }
	      ++$marker_row_num;
	  }
	  close $marker_in_fh;
	  
	  # Remove last comma and add new line
	  chop($marker_header_str);
	  $marker_header_str .= "\n";
	  print $out_genotype_fh $marker_header_str;
      }
      ++$row_num;
    }
    
    # Now we print out the remaining genotype flapjack file data
    for my $hash_key ( keys %genotype_row_object ) {

	my $str_to_print = join(',', @{ $genotype_row_object{$hash_key} }) . "\n";
	print $out_genotype_fh $str_to_print;
    }

    close $in_fh;
    close $out_map_fh;
    close $out_genotype_fh;
}

sub PrintHelp {

print "
    Usage:
    export_marker_data.pl -a 10 --metaDataCol MarkerName,Sequence --makerDataCol 2013,2014,2015 --outDir /Somedir/currentdir

    -a --analId      		input folder
       --dalBaseUrl		dal base url
       --dalCookieFile		list of extract IDs from KDDArT
       --metaDataCol		metadata column
       --markerDataCol		list of extract IDs from KDDArT
       --outDir			output directory
       --transposeData		transpose columns into rows and vice versa
    -h --help       		this help
";
}

if (!@ARGV) { &PrintHelp(); exit 0; }

my $anal_id		= "";
my $dal_base_url	= "";
my $dal_cookie_file	= "";
my $meta_data_col       = "";
my $metadata_filter_row = "";
my $marker_data_col     = "";
my $output_dir 		= "";
my $help		= "";
my $transpose_opt	= "";

my $optres = GetOptions(
	"help|h|?"         	=> \$help,
	"analId|a=s"        	=> \$anal_id,
	"outDir=s"		=> \$output_dir,
	"dalBaseUrl=s"		=> \$dal_base_url,
	"dalCookieFile=s"	=> \$dal_cookie_file,
	"metaDataCol=s"		=> \$meta_data_col,
	"metaDataRow=s"		=> \$metadata_filter_row,
	"markerDataCol=s"	=> \$marker_data_col,
	"transposeData=s"	=> \$transpose_opt,
);

if ($help) { &PrintHelp(); exit 0; }

# parameters verification
unless ($anal_id)                            { print "Error! Analysis id not provided!\n"; exit 1; }
unless (-e $output_dir)                      { print "Error! Output dir [$output_dir] does not exists!\n"; exit 1; }
unless ($dal_base_url)                       { print "Error! Dal base URL not provided!\n"; exit 1; }
unless ($dal_cookie_file)                    { print "Dal Cookie File is not provided!\n"; exit 1; }

$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

my $browser  = LWP::UserAgent->new();

$browser->timeout(20000);

#$browser->default_header('Accept' => 'application/json');

my $cookie_jar = HTTP::Cookies->new();

$cookie_jar->load($dal_cookie_file);
$browser->cookie_jar($cookie_jar);

my $url = $dal_base_url . "/analysisgroup/${anal_id}/export/markerdata/dart";

my $filtering  = qq{$metadata_filter_row}; # row filtering
my $metadata_field_list = qq|$meta_data_col|;
my $markerdata_field_list = qq|$marker_data_col|;

print "Metadata field list: $metadata_field_list\n";
print "Metadata filter row list: $metadata_filter_row\n";
print "Markerdata field list: $markerdata_field_list\n";

my $parameter = [];
my $exported_file_path = "";

push(@{$parameter}, 'Filtering' => $filtering);
push(@{$parameter}, 'MarkerMetaFieldList' => $metadata_field_list);
push(@{$parameter}, 'MarkerDataFieldList' => $markerdata_field_list);

my $inside_req_res = POST ($url, $parameter);

my $inside_req = $browser->request($inside_req_res);

print $inside_req->content() . "\n";

my $output_file_xml  = $inside_req->content();
my $output_file_aref = xml2arrayref($output_file_xml, 'OutputFile');

for my $item (@{$output_file_aref}) {

  for my $k (keys(%{$item})) {

    my $file_url = $item->{$k};
    my @file_url_block = split('/', $file_url);
    my $filename = $file_url_block[-1];
    
    $exported_file_path = $output_dir . $filename;
    my $download_response = $browser->get($file_url,
                                          ':content_file' => $output_dir . "$filename"
                                            );
    print "Download header: " . $download_response->header('Set-Cookie') . "\n";
    print "Downloading $file_url: " . $download_response->content() . "\n";
    print "$k : $file_url\n";
  }
}

if ($transpose_opt > 0) {

    my $xml_data = XMLin($output_file_xml);
    my $markername_col = $xml_data->{OutputFileMeta}->{MarkerNameCol};
    my $lastheader_row = $xml_data->{OutputFileMeta}->{LastHeaderRow};
    my $metadata_start_col = $xml_data->{OutputFileMeta}->{MarkerMetaColStart};
    my $metadata_end_col = $xml_data->{OutputFileMeta}->{MarkerMetaColEnd};
    my $markerdata_start_col = $xml_data->{OutputFileMeta}->{MarkerDataColStart};
    my $markerdata_end_col = $xml_data->{OutputFileMeta}->{MarkerDataColEnd};
    my $num_of_markers = $xml_data->{OutputFileMeta}->{NumberOfMarkers};
    
    print "Printing flapjack files...\n";
    print "Markername col: $markername_col, last header row: $lastheader_row, metadata start col: $metadata_start_col, metadata end col: $metadata_end_col, "; 
    print "markerdata start col: $markerdata_start_col, markerdata end col: $markerdata_end_col, number of markers: $num_of_markers\n";
    
    print_flapjack_files($markername_col, $lastheader_row, $metadata_start_col, $metadata_end_col, $markerdata_start_col, $markerdata_end_col, $num_of_markers, $output_dir, $exported_file_path);
}
