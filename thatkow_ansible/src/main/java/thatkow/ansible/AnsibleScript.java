/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package thatkow.ansible;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class AnsibleScript extends AnsibleScriptBase {

	public AnsibleScript(File file) throws AnsiblePlaybookNotFoundException, IOException, InterruptedException {
		this(file, ANSIBLE_PLAYBOOK);
	}

	public AnsibleScript(File file, String ansiblePlaybookPath)
			throws AnsiblePlaybookNotFoundException, IOException, InterruptedException {
		super(file, ansiblePlaybookPath);
	}

	public static List<AnsibleScriptFile> identifySudos(List<File> listOfDepedencies) throws IOException {
		List<AnsibleScriptFile> dependencyFiles = new ArrayList<>();
		for (File f : listOfDepedencies) {
			String text = FileUtils.readFileToString(f, Charset.defaultCharset().name());
			Pattern p = Pattern.compile("\\bsudo:\\s*yes\\b");
			Matcher m = p.matcher(text);
			boolean find = m.find();
			dependencyFiles.add(new AnsibleScriptFile(f, find));
		}
		return dependencyFiles;
	}

	public List<String> getCommand() {
		String replaceAll = escape(this.file.getAbsolutePath());
		List<String> result = new ArrayList<>(
				Arrays.asList(escape(getAnsibleCommand()), replaceAll, "-i", "'localhost,'", "-c", "local"));
		return result;
	}

	public int install(PrintStream outPS, PrintStream errPS)
			throws IOException, InterruptedException, AnsiblePlaybookNotFoundException {

		String command = null;
		String alt = null;
		String absolutePath = escape(this.file.getAbsolutePath());

		command = escape(ansiblePlaybookPath) + " " + absolutePath + " -ilocalhost, -c local ";
		alt = "Equivalent: " + escape(ansiblePlaybookPath) + " -ilocalhost, -c local " + absolutePath;
		outPS.println(command);
		outPS.println(alt);

		File root = file.getParentFile();
		Process proc = Runtime.getRuntime().exec(command, null, root);
		final InputStream stderrStream = proc.getErrorStream();
		final InputStream stdoutStream = proc.getInputStream();

		Thread stdoutR = new Thread(new Runnable() {

			@Override
			public void run() {
				BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stdoutStream));
				try {
					String line;
					while ((line = brCleanUp.readLine()) != null) {
						outPS.println(line);
					}
					brCleanUp.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
				}
			}
		});

		Thread stderrR = new Thread(new Runnable() {

			@Override
			public void run() {
				BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stderrStream));
				try {
					String line;
					while ((line = brCleanUp.readLine()) != null) {
						errPS.println(line);
					}
					brCleanUp.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
				}
			}
		});
		stdoutR.run();
		stderrR.run();
		stdoutR.join();
		stderrR.join();

		proc.waitFor();
		return proc.exitValue();
	}

	@Override
	public String toString() {
		return "script=" + file.getName() + ", sudo=" + "no";
	}

}
