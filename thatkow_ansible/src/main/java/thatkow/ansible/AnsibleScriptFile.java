/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package thatkow.ansible;

import java.io.File;

public class AnsibleScriptFile {
	private File file;
	private boolean sudo;
	public File getFile() {
		return file;
	}
	public AnsibleScriptFile() {
		super();
	}
	public void setFile(File file) {
		this.file = file;
	}
	public boolean isSudo() {
		return sudo;
	}
	public void setSudo(boolean sudo) {
		this.sudo = sudo;
	}
	public AnsibleScriptFile(File file, boolean sudo) {
		super();
		this.file = file;
		this.sudo = sudo;
	}
}
