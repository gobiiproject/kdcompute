/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package thatkow.ansible;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public abstract class AnsibleScriptBase {

	public static final String ANSIBLE_PLAYBOOK = "ansible-playbook ";
	protected final File file;
	protected final String ansiblePlaybookPath;

	protected AnsibleScriptBase(File file, String ansiblePlaybookPath)
			throws AnsiblePlaybookNotFoundException, IOException, InterruptedException {
		super();
		this.file = file;

		if (ansiblePlaybookPath == null || ansiblePlaybookPath.isEmpty()) {
			throw new AnsiblePlaybookNotFoundException("Configured ansible-playbook '" + ansiblePlaybookPath
					+ "' does not exist. Please provide correct path in application.properties as 'ansiblePlaybookPath' under KDCompute Configuration");
		}

		Process ansiblePlaybookProc = null;

		String command = ansiblePlaybookPath + " --version";
		try {
			ansiblePlaybookProc = Runtime.getRuntime().exec(command);
			if (ansiblePlaybookProc.waitFor() != 0) {
				throw new AnsiblePlaybookNotFoundException(
						IOUtils.toString(ansiblePlaybookProc.getErrorStream(), Charset.defaultCharset().name()));
			}
		} catch (IOException e) {
			throw new AnsiblePlaybookNotFoundException(e.getMessage());
		}

		this.ansiblePlaybookPath = ansiblePlaybookPath;
	}

	/**
	 * Escapes spaces
	 * 
	 * @param str
	 * @return
	 */
	public static String escape(String str) {
		if (str == null) {
			return "";
		}
		str = str.replaceAll("\\ ", "\\\\\\\\ ");
		return str;
	}

	public String getAnsibleCommand() {
		if (ansiblePlaybookPath == null || ansiblePlaybookPath.isEmpty()) {
			return ANSIBLE_PLAYBOOK;
		} else {
			return ansiblePlaybookPath;
		}
	}

	/**
	 * @param outputRedirect
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws AnsiblePlaybookNotFoundException
	 *             If ansible-playbook command is not installed/found
	 * @throws InterruptedException
	 */
	public static AnsibleScriptBase create(File file)
			throws IOException, AnsiblePlaybookNotFoundException, InterruptedException {
		return create(file, ANSIBLE_PLAYBOOK);
	}

	/**
	 * @param outputRedirect
	 * @param file
	 * @param ansiblePlaybookPath
	 * @return
	 * @throws IOException
	 * @throws AnsiblePlaybookNotFoundException
	 *             If command given by {@code ansiblePlaybookPath} is not
	 *             installed/found
	 * @throws InterruptedException
	 */
	public static AnsibleScriptBase create(File file, String ansiblePlaybookPath)
			throws IOException, AnsiblePlaybookNotFoundException, InterruptedException {

		String text = FileUtils.readFileToString(file, Charset.defaultCharset().name());
		Pattern p = Pattern.compile("\\bsudo:\\s*yes\\b");
		Matcher m = p.matcher(text);
		boolean sudo = m.find();
		if (sudo) {
			return new AnsibleSudoScript(file, ansiblePlaybookPath);
		} else {
			return new AnsibleScript(file, ansiblePlaybookPath);
		}
	}

	@Override
	public String toString() {
		return "script=" + file.getName() + ", sudo=" + "yes";
	}

	public File getFile() {
		return file;
	}

	public abstract List<String> getCommand();

}
