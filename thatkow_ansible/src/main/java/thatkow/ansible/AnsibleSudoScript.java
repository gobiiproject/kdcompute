/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package thatkow.ansible;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class AnsibleSudoScript extends AnsibleScriptBase {

	protected static final String ERROR_DECRYPTION_FAILED_ON = "ERROR! Decryption failed on";

	public AnsibleSudoScript(File file) throws AnsiblePlaybookNotFoundException, IOException, InterruptedException {
		this(file, ANSIBLE_PLAYBOOK);
	}

	public AnsibleSudoScript(File file, String ansiblePlaybookPath)
			throws AnsiblePlaybookNotFoundException, IOException, InterruptedException {
		super(file, ansiblePlaybookPath);
	}

	public List<String> getCommand() {
		String replaceAll = escape(this.file.getAbsolutePath());
		List<String> result = new ArrayList<>(
				Arrays.asList(escape(getAnsibleCommand()), replaceAll, "-i", "'localhost,'", "-c", "local", "-K"));
		return result;
	}

	/**
	 * @param ansibleVaultFile
	 * @param vaultPassword
	 * @return Exit code of ansible script
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws AnsibleVaultDoesNotExistException
	 */
	public int install(File ansibleVaultFile, String vaultPassword, PrintStream outPS, PrintStream errPS)
			throws IOException, AnsibleScriptCredentialsException, InterruptedException,
			AnsibleVaultDoesNotExistException {

		if (!ansibleVaultFile.exists()) {
			throw new AnsibleVaultDoesNotExistException(ansibleVaultFile);
		}

		String command = null;
		String alt = null;
		File ansibleVaultSecretPass = null;
		File executedScript;
		/*
		 * Make a copy of the selected dependencies file, added reference to our
		 * secret file (See
		 */
		executedScript = File.createTempFile(this.file.getName(), ".yml", this.file.getParentFile());
		List<String> readAllLines = Files.readAllLines(this.file.toPath());

		ArrayList<String> executedScriptAsStringArr = new ArrayList<>(readAllLines);
		executedScriptAsStringArr.add("  vars_files:");
		executedScriptAsStringArr.add("  - " + escape(ansibleVaultFile.getAbsolutePath()));
		Files.write(executedScript.toPath(), executedScriptAsStringArr);

		int exitCode = 1;
		try {

			/*
			 * Create a Ansible vault file, containing password to
			 * ansibleVaultSecret
			 */
			ansibleVaultSecretPass = File.createTempFile(this.file.getName(), ".txt");

			FileUtils.writeStringToFile(ansibleVaultSecretPass, vaultPassword, "UTF-8");

			String absolutePath = escape(this.file.getAbsolutePath());
			command = ansiblePlaybookPath + " " + executedScript.getAbsolutePath()
					+ " -ilocalhost, -c local --sudo --vault-password-file="
					+ ansibleVaultSecretPass.getAbsolutePath().replaceAll(" ", "\\ ");
			alt = "Equivalent: ansible-playbook -ilocalhost, -c local -K " + absolutePath;

			outPS.println(command);
			outPS.println(alt);

			Process proc = Runtime.getRuntime().exec(command);
			final InputStream stderrStream = proc.getErrorStream();
			final InputStream stdoutStream = proc.getInputStream();

			Thread stdoutR = new Thread(new Runnable() {

				@Override
				public void run() {
					BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stdoutStream));
					try {
						String line;
						while ((line = brCleanUp.readLine()) != null) {
							outPS.println(line);
						}
						brCleanUp.close();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
					}
				}
			});

			final Boolean badCreds[] = new Boolean[1];
			badCreds[0] = false;
			Thread stderrR = new Thread(new Runnable() {

				@Override
				public void run() {
					BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stderrStream));
					try {
						String line;
						while ((line = brCleanUp.readLine()) != null) {
							errPS.println(line);
							if (line.contains(ERROR_DECRYPTION_FAILED_ON)) {
								badCreds[0] = true;
							}
						}
						brCleanUp.close();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
					}
				}
			});
			stdoutR.run();
			stderrR.run();
			stdoutR.join();
			stderrR.join();

			if (badCreds[0]) {
				ansibleVaultSecretPass.delete();
				executedScript.delete();
				throw new AnsibleScriptCredentialsException("Failed to decrypt Ansible vault '"
						+ ansibleVaultFile.getAbsolutePath() + "'. Perhaps bad password?");
			}

			ansibleVaultSecretPass.delete();
			executedScript.delete();
			exitCode = proc.waitFor();

		} catch (IOException | AnsibleScriptCredentialsException | InterruptedException e1) {
			throw e1;
		} finally {
			ansibleVaultSecretPass.delete();
			ansibleVaultSecretPass.delete();
		}
		return exitCode;
	}

}
