/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.diversityarrays.kdcompute.db.DataSet;
import com.diversityarrays.kdcompute.db.Knob;

public class KnobsAndInputs {

    private final List<Knob> knobs;
    private final List<DataSet> inputs;
    
    public KnobsAndInputs(List<Knob> k, List<DataSet> d) {
        this.knobs = new ArrayList<>(k);
        this.inputs = new ArrayList<>(d);
    }

    public List<Knob> getKnobs() {
        return Collections.unmodifiableList(knobs);
    }

    public List<DataSet> getInputs() {
        return Collections.unmodifiableList(inputs);
    }

}
