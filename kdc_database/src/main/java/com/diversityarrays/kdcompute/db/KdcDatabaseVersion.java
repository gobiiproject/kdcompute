/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.UpdateTimestamp;


@Table
@Entity
public class KdcDatabaseVersion {
	
	@Id
	@GeneratedValue
	@Column(name = "KdcDatabaseVersion_Id")
	private Long id;

	@Column(name = "versionNumberString", nullable = false)
	private String versionString = ""; //$NON-NLS-1$
	
	@Column(name = "versionNumber", nullable = false)
	private int versionNumber;
	
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(nullable = false)
    private Date whenLastUpdated;

	public String getVersionString() {
		return versionString;
	}

	public void setVersionString(String versionString) {
		this.versionString = versionString;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	public Date getWhenLastUpdated() {
		return whenLastUpdated;
	}

	public void setWhenLastUpdated(Date whenLastUpdated) {
		this.whenLastUpdated = whenLastUpdated;
	}
	
}
