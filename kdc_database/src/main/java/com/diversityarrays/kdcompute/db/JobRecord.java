/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("nls")
@Entity
@Table
public class JobRecord {

    @Id
    @GeneratedValue
    @Column(name="id" ,nullable  = false)
    long id;
    
    @Column(name="status", nullable=false)
    String status;
    
    @Column(name="pluginName",nullable= false)
    String pluginName;
    
    @Column(name="pluginVersion", nullable = true)
    int pluginVersion;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="submittedDate", nullable = true)
    Date submittedDate;
    
    @Column(name="resultsFolderPath", nullable = true)
    String resultsFolderPath;
    
    public JobRecord() {
    }
    
    @Override
    public String toString() {
        return String.format("%s(%d %s.%d submitted=%s status=%s",
                JobRecord.class.getSimpleName(),
                id,
                pluginName, pluginVersion,
                (submittedDate == null ? "" : new SimpleDateFormat("yyyy/MM/dd").format(submittedDate)).length(),
                status);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }
    
}
