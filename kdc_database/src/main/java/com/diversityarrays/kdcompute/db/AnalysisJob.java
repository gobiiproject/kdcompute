/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.diversityarrays.kdcompute.util.ZipUtils;

/**
 * Provides a record of the data about a run. The existence of at least one
 * <i>AnalysisJob</i> record for an <i>AnalysisRequest</i> means that the
 * <i>AnalysisRequest</i> is then frozen.
 *
 * @author brianp
 *
 */
@Entity
@Table(name = "AnalysisJob")
public class AnalysisJob {

    private static final String ANALYSIS_JOB_ID = "AnalysisJob_Id";
    public static final String ANALYSIS_JOB_RUNSTATE = "runState";

	@Id
	@GeneratedValue
	@Column(name = ANALYSIS_JOB_ID, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	private AnalysisRequest analysisRequest;

	@Column(name = ANALYSIS_JOB_RUNSTATE, nullable = false)
	private RunState runState = RunState.NEW;

	@Column(nullable = false)
	private String displayName;

	/**
	 * When the AnalysisJob was created.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creationDateTime", nullable = false)
	private Date creationDateTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "startDateTime", nullable = true)
	private Date startDateTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pauseDateTime", nullable = true)
	private Date pauseDateTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endDateTime", nullable = true)
	private Date endDateTime;

	@ElementCollection
	@JoinColumn(name = ANALYSIS_JOB_ID)
	private List<String> tags;

	private String shellFilePath;

	private String resultsFolderPath;

	/**
	 * If zero then not yet run/completed.
	 */
	private long cpuMillisConsumed;

	private String requestingUser;

	private String error;

	@Transient
	private PostCompletionCall postCompletionCall;

	private Integer exitCode;

	public AnalysisJob() {
	}

	public AnalysisJob(AnalysisRequest req, Date d, String displayName) {
		this.analysisRequest = req;
		this.creationDateTime = d;
		this.displayName = displayName;
	}

	public PostCompletionCall getPostCompletionCall() {
		return postCompletionCall;
	}

	public void setPostCompletionCall(PostCompletionCall postCompletionCall) {
		this.postCompletionCall = postCompletionCall;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("AnalysisJob("); //$NON-NLS-1$
		sb.append(" for ").append(requestingUser); //$NON-NLS-1$
		sb.append(" with AnalysisRequest#").append(analysisRequest.getId()); //$NON-NLS-1$
		sb.append(" state=").append(runState); //$NON-NLS-1$
		sb.append(" results_in:\n\t").append(resultsFolderPath); //$NON-NLS-1$
		sb.append(')');
		return sb.toString();
	}

	public Long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public RunState getRunState() {
		return runState;
	}

	public void setRunState(RunState rs) {
		runState = rs;
	}

	public AnalysisRequest getAnalysisRequest() {
		return analysisRequest;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreationDateTime() {
		return creationDateTime;
	}

	public void setStartDateTime(Date d) {
		startDateTime = d;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartDateTime() {
		return startDateTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date d) {
		endDateTime = d;
	}

	public long getCpuMillisConsumed() {
		return cpuMillisConsumed;
	}

	public void setCpuMillisConsumed(long millis) {
		cpuMillisConsumed = millis;
	}

	public void setShellFilePath(String path) {
		shellFilePath = resultsFolderPath != null && !resultsFolderPath.isEmpty()
				? path.replace(resultsFolderPath + File.separator, "")
				: path;
	}

	public String getShellFilePath() {
		return resultsFolderPath != null && !resultsFolderPath.isEmpty()
				? resultsFolderPath + File.separator + shellFilePath
				: shellFilePath;
	}

	public String getResultsFolderPath() {
		return resultsFolderPath;
	}

	public void setResultsFolderPath(String path) {
		this.resultsFolderPath = path;
	}

	public String getRequestingUser() {
		return requestingUser;
	}

	public void setRequestingUser(String name) {
		requestingUser = name;
	}

	public void setError(String error) {
		runState = RunState.FAILED;
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Integer getExitCode() {
		return exitCode;
	}

	public void setExitCode(Integer exitCode) {
		this.exitCode = exitCode;
	}

	public static File toZipFile(File file) {
		return FilenameUtils.getExtension(file.getPath()).equals("zip") ? new File(file.getPath())
				: new File(file.getPath().concat(".zip"));
	}

	public static File toPlainFile(File file) {
		return FilenameUtils.getExtension(file.getPath()).equals("zip")
				? new File(FilenameUtils.removeExtension(file.getPath()))
				: new File(file.getPath());
	}

	/**
	 * @return Results as a zip file
	 * @throws IOException
	 */
	public File getResultsZip() throws Exception {
		File unzipped = getResultsPlain();
		File zipped = getResultsZipped();

		if (unzipped.exists() && unzipped.isDirectory() && unzipped.listFiles().length > 0) {
			if (zipped.exists())
				zipped.delete();
			ZipUtils.zipFile(unzipped, zipped);
			return zipped;
		} else if (zipped.exists()) {
			return zipped;
		}
		return null;
	}

	/**
	 * @return Results as a folder
	 * @throws IOException
	 */
	public File getResultsFolder() throws IOException {
		File unzipped = getResultsPlain();
		File zipped = getResultsZipped();

		if (unzipped.exists() && unzipped.isDirectory() && unzipped.listFiles().length > 0) {
			try {
				if (zipped.exists())
					FileUtils.forceDelete(zipped); // Should'nt have both,
				// delete
			} catch (IOException ignore) {
			}
			return unzipped;
		} else {
			if (zipped.exists()) {
				FileUtils.deleteDirectory(unzipped);
				FileUtils.forceMkdir(unzipped);
				try {
					ZipUtils.unzip(zipped, unzipped);
					zipped.delete();
					return unzipped;
				} catch (Exception e) {
					FileUtils.deleteDirectory(unzipped);
					throw e;
				}
			} else {
				return unzipped;
			}
		}
	}

	private File getResultsZipped() {
		return toZipFile(new File(getResultsFolderPath()));
	}

	private File getResultsPlain() {
		return toPlainFile(new File(getResultsFolderPath()));
	}

	public void eraseResults() throws IOException, JobStillActiveException {
		if (runState.isActive()) {
			throw new JobStillActiveException(runState);
		}

		FileUtils.deleteDirectory(getResultsPlain());
		getResultsZipped().delete();
		runState = RunState.ERASED;
	}

	public List<String> getTags() {
		return tags;
	}

	public void clearTags() {
		this.tags.clear();
	}

	public void addTag(String tag) {
		tags.add(tag);
	}

}
