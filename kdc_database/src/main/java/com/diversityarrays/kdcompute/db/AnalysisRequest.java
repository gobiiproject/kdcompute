/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Collects the information about a process that some user
 * wants to run to perform some analysis.
 * <p>
 * At least one <i>RunBinding</i> must be provided.
 * Once an <i>AnalysisRequest</i> has been run (i.e. a <i>RunStatistics</i> entity
 * has been created no other changes may be applied
 * to the <i>AnalysisRequest</i>.
 * <p>
 * However, this means that an <i>AnalysisRequest</i> can be re-submitted at any time.
 * @author brianp
 *
 */
@Entity
@Table
public class AnalysisRequest {

    @Id
    @GeneratedValue
    @Column(name="id", nullable = false)
    private Long id;

    @Column(name="forUser")
    private String forUser;
    // TODO Need more features such as who the job is being run for.

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dateFirstSubmitted", nullable = true)
    private Date dateFirstSubmitted;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "AnalysisRequest_Id")
    private List<RunBinding> runBindings = new ArrayList<>();

    public AnalysisRequest() { }

    public AnalysisRequest(RunBinding ... bindings) {
        if (bindings != null) {
            Collections.addAll(runBindings, bindings);
        }
    }

    public AnalysisRequest(Set<RunBinding> set) {
        if (set != null) {
            runBindings.addAll(set);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("AnalysisRequest("); //$NON-NLS-1$
        sb.append(id);
        sb.append(' ').append(runBindings.size()).append(" bindings:"); //$NON-NLS-1$
        String nl = ""; //$NON-NLS-1$
        if (runBindings.size() > 1) {
            nl = "\n  "; //$NON-NLS-1$
        }
        for (RunBinding rb : runBindings) {
            sb.append(nl).append(rb.getPlugin().getAlgorithmName()).append(":" ); //$NON-NLS-1$
            for (KnobBinding kb : rb.getKnobBindings()) {
                sb.append(' ')
                    .append(kb.getKnob().getKnobName())
                    .append('=').append(kb.getKnobValue());
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public Date getDateFirstSubmitted() {
        return dateFirstSubmitted;
    }

    public void setDateFirstSubmitted(Date d) {
        if (dateFirstSubmitted != null) {
            dateFirstSubmitted = d;
        }
    }

    public Long getId() {
        return id;
    }

    public String getForUser() {
        return forUser;
    }

    public void setForUser(String forUser) {
        this.forUser = forUser;
    }

    public List<RunBinding> getRunBindings() {
        return Collections.unmodifiableList(runBindings);
    }


    public List<Plugin> getAlgorithms() {
        return new ArrayList<>(runBindings.stream()
            .map(RunBinding::getPlugin)
            .collect(Collectors.toSet()));
    }

	public void setId(long i) {
		id = i;
	}
}
