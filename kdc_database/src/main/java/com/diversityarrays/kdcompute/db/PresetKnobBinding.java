/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@SuppressWarnings("nls")
@Entity
@Table
public class PresetKnobBinding {
    
    @Id
    @GeneratedValue
    @Column(name = "id", nullable=false)
    private Long id;

    @Column(name = "boundPluginKnob" ,nullable = false)
    private Knob boundPluginKnob;
    
    @Column(name= "boundKnobValue" ,  nullable = false)
    private String boundKnobValue = "";

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @Embedded
    @Column(name = "bindings" , nullable = false)
    private List<KnobBinding> bindings = new ArrayList<KnobBinding>();
    
    public PresetKnobBinding() {
    }

    public PresetKnobBinding(Knob boundKnob, String value, KnobBinding ... bs) {
        this.boundPluginKnob = boundKnob;
        this.boundKnobValue = value;
        if (bs != null) {
            Collections.addAll(bindings, bs);
        }
    }
    
    public PresetKnobBinding(Knob boundKnob, String value, Set<KnobBinding> bs) {
        this.boundPluginKnob = boundKnob;
        this.boundKnobValue = value;
        if (bs != null) {
            bindings.addAll(bs);
        }
    }

    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PresetKnobBinding(");
        sb.append("boundKnob=").append(boundPluginKnob.getBriefString());
        sb.append("\n for value= ").append(boundKnobValue)
          .append("\n  bindings= ").append(bindings);
        sb.append(')');
        return sb.toString();
    }
    
    public Long getId() {
        return id;
    }
    
    @Override
    public int hashCode() {
        return boundPluginKnob.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (! (o instanceof PresetKnobBinding)) return false;

        PresetKnobBinding other = (PresetKnobBinding) o;
        return this.boundPluginKnob.equals(other.boundPluginKnob);
    }

    public Knob getBoundPluginKnob() {
        return boundPluginKnob;
    }

    public void setBoundPluginKnob(Knob k) {
        this.boundPluginKnob = k;
    }

    public Set<KnobBinding> getBindings() {
        return new HashSet<>(bindings);
    }
    
}
