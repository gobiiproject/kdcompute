@org.hibernate.annotations.GenericGenerator(
        name = "ID_GENERATOR",
        strategy = "enhanced-sequence",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "KDCJPA_SEQUENCE"),
                @org.hibernate.annotations.Parameter(
                        name="initial_value",
                        value="1000")
        }
        )
/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;
/**
 * This package provides the basic entities required by KDCompute to
 * define plugin algorithms, their parameters (a.k.a <i>Knobs</i>) 
 * and input data sets. 
 * <p>
 * An <i>Algorithm</i> definition provides sufficient information
 * to automatically generate a simple user interface
 * for either a web or desktop UI to gather the values and input data files
 * required to run an analysis program.
 * <p>
 * To actually run
 * <ul>
 *   <li>
 *    each <i>Knob</i> must have a value supplied
 *    (appropriate for the <i>Knob</i>'s data type</li>
 *   <li>each <i>Input Data Set</i> must be bound to a URL that locates the data</i>
 *   <li>the <i>Output Folder</i> must be provided to store what the program produces</li>
 *  </ul>
 * The entity providing all of these is the <i>RunBinding</i>.
 * <p>
 * An <i>AnalysisJob</i> is what actually is entered into a job queue
 * and is eventually used by an instance of <i>AnalysisJobRunner</i>.
 * <p>
 * Different types of <i>AnalysisJobRunner</i> should be created for various purposes
 * including testing.
 */
