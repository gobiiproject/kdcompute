/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.diversityarrays.kdcompute.runtime.UserRoleSpecification;

@Entity
@Table
public class UserRole {

	@Id
	@GeneratedValue
	@Column(name = "UserRole_Id")
	private Long id;

	@Column(name = "UserName")
	private String userName;

	@Column(name = "UserRoleSpec", nullable  = false)
	private UserRoleSpecification userRoleSpec;

	// Need this for Role Matching in WebSecurityConfig
	@Column(name = "UserRoleSpecString",  nullable = false)
	private String userRoleSpecString;

	public UserRole(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String user) {
		this.userName = user;
	}

	public UserRoleSpecification getUserRole() {
		return userRoleSpec;
	}

	public void setUserRole(UserRoleSpecification userRole) {
		this.userRoleSpec = userRole;
	}

	public String getUserRoleSpecString() {
		return userRoleSpecString;
	}

	public void setUserRoleSpecString(String userRoleSpecString) {
		this.userRoleSpecString = userRoleSpecString;
	}

}
