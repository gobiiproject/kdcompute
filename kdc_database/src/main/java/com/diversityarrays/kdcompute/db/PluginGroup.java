/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@SuppressWarnings("nls")
@Entity
@Table(name = "PluginGroup")
public class PluginGroup {

	public final static String PluginGroupName = "groupName";
	@Id
	@GeneratedValue
	@Column(name = "PluginGroup_Id", nullable=false)
	private Long id;

	@Column(name = PluginGroupName, nullable = false, unique = true)
	private String groupName = "";

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "PluginGroup_Plugin" , joinColumns = {@JoinColumn (name= "PluginGroup_Id" , nullable = false, updatable = false)} ,
	inverseJoinColumns = {@JoinColumn(name = "Plugin_Id", nullable = false, updatable = false)})
	private List<Plugin> plugins = new ArrayList<>();

	protected PluginGroup() {}

	public PluginGroup(String n) {
		groupName = n;
	}

	@Override
	public int hashCode() {
		return groupName.toLowerCase().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (! (o instanceof PluginGroup)) return false;

		return this.groupName.equalsIgnoreCase(((PluginGroup) o).groupName);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String n) {
		this.groupName = n;
	}


	public List<Plugin> getPlugins() {
		return Collections.unmodifiableList(plugins);
	}

	public void setPlugins(List<Plugin> list) {
		this.plugins = list;
	}

	public void addPlugin(Plugin a) {
		plugins.add(a);
	}

	public void removePlugin(Plugin plugin) {
		plugins.remove(plugin);
	}

	@Override
	public String toString() {
		return this.groupName + ": " + this.plugins + "\n";
	}

}
