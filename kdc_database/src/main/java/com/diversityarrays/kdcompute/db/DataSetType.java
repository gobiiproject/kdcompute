/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.Arrays;
import java.util.List;

import com.diversityarrays.kdcompute.util.Check;

public enum DataSetType {
    ANY,  // validation: any text
    FILE_REF, // a file in the user's file repository
    URL, // just somewhere on the web
    ANALYSIS_GROUP, // list/analysisgroup/100/page/1?ctype=json&callback=?
    MARKER_DATASET,;

    static public final List<String> URL_PREFIXES;
    static {
        URL_PREFIXES = Arrays.asList(
                "http://", //$NON-NLS-1$
                "https://", //$NON-NLS-1$
                "ftp://"); //$NON-NLS-1$
    }
    public static DataSetType classify(String srcURL, DataSetType defalt) {
        DataSetType result = defalt;
        if (! Check.isEmpty(srcURL)) {
            String src = srcURL.toLowerCase();
            if (src.contains("/analysisgroup/")) { //$NON-NLS-1$
                result = ANALYSIS_GROUP;
            }
            else if (src.contains("/markerdataset/")) { //$NON-NLS-1$
                result = MARKER_DATASET;
            }
            else if (src.startsWith("file://")) { //$NON-NLS-1$
                result = FILE_REF;
            }
            else {
                for (String prefix : URL_PREFIXES) {
                    if (src.startsWith(prefix)) {
                        result = URL;
                        break;
                    }
                }
            }
        }
        // TODO Auto-generated method stub
        return result;
    } // list/type/markerdataset/active?ctype=json&callback=?
}
