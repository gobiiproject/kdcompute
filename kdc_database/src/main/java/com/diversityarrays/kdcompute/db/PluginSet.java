/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.diversityarrays.kdcompute.db.helper.DbUtil;

@SuppressWarnings("nls")
@Entity
@Table
public class PluginSet implements HasVisibility {

    @Id
    @GeneratedValue
    @Column(name = "PluginSet_Id", nullable=false)
    private Long id;

    /**
     * Value of zero means no versions exist. Non-zero means this is a copy
     * 
     */
    @Column(name = "versionNumber", nullable=true)
    private int versionNumber;
    
    @Column(name = "parentPluginSetId", nullable = true)
    private Long parentPluginSetId;
    
    @Column(name = "visibility", nullable = false)
    private Visibility visibility = Visibility.PRIVATE;

    @Column(name = "pluginSetName", nullable = false)
    private String pluginSetName;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "PluginSet_Plugin" , joinColumns = {@JoinColumn (name= "PluginSet_Id" , nullable = false, updatable = false)} ,
    inverseJoinColumns = {@JoinColumn(name = "Plugin_Id", nullable = false, updatable = false)})
    private Set<Plugin> plugins = new HashSet<>();

    public PluginSet() { }
    
    public PluginSet(String name) {
        pluginSetName = name;
    }

    public Long getId() {
        return id;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PluginSet(");
        sb.append("id=").append(id);
        sb.append(" version=").append(versionNumber);
        sb.append(" parent=").append(parentPluginSetId);
        sb.append(" '").append(pluginSetName).append('\'');
        DbUtil.appendListInfo(sb, "  plugins", plugins, Plugin::getAlgorithmName);
        sb.append(')');
        return sb.toString();
    }
    
    
    public void setAlgorithmSetName(String n) {
        this.pluginSetName = n;
    }

    /**
     * If you set the parent you MUST set the version number.
     * @param pid
     * @param v
     */
    public void setParentAndVersion(long pid, int v) {
        if (pid <= 0) {
            throw new IllegalArgumentException("parentId must be positive"); //$NON-NLS-1$
        }
        if (v <= 0) {
            throw new IllegalArgumentException("version must be positive"); //$NON-NLS-1$
        }
        this.parentPluginSetId = pid;
        versionNumber = v;
    }

    public Long getParentAlgorithmSetId() {
        return parentPluginSetId;
    }
    
    public int getVersionNumber() {
        return versionNumber;
    }
    
    @Override
    public Visibility getVisibility() {
        return visibility;
    }

    @Override
    public void setVisibility(Visibility v) {
        this.visibility = v;
    }

    public String getAlgorithmSetName() {
        return pluginSetName;
    }
    

    public Set<Plugin> getAlgorithms() {
        return plugins;
    }
    
    public void addAlgorithm(Plugin ... algs) {
        Collections.addAll(plugins, algs);
    }
}
