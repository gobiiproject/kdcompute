/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

/**
 * The states of an <i>AnalysisJob</i>
 * 
 * @author brianp
 *
 */
public enum RunState {
	/**
	 * The run has been entered into the run queue but has not yet started.
	 */
	NEW("rotate_grey.gif", "This job is waiting to run"),
	/**
	 * The run has been started on some processor.
	 */
	RUNNING("refresh.gif", "This job is currently running"),
	/**
	 * The run has been paused for some reason. It can be resumed.
	 */
	PAUSED("refresh-yellow.gif", "This job has been paused"),
	/**
	 * The run successfully completed.
	 */
	COMPLETED("green_tick.png", "This job has completed successfully"),
	/**
	 * The run completed but has errors.
	 */
	FAILED("cross.png", "This job failed due to errors. See stderr.txt"),
	/**
	 * The job had been cancelled
	 */
	CANCELLED("greycross.png", "This job has been cancelled"),
	/**
	 * Unknown.
	 */
	ERASED("trash.png", "This job has been erased, but a record maintained"),
	/**
	 * Unknown.
	 */

	UNKNOWN("questionmark.png", "The state of this job is unknown"),;

	public final String imageResource;
	public final String description;

	RunState(String img, String desc) {
		imageResource = img;
		description = desc;
	}

	public boolean isActive() {
		return this.equals(NEW) || this.equals(RUNNING) || this.equals(PAUSED);
	}
}
