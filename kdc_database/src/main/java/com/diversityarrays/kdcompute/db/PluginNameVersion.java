/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.io.File;
import java.text.ParseException;

public class PluginNameVersion extends PluginName implements Comparable<PluginNameVersion> {

	public static final String VERSION_SEP = "-v";

	private PluginVersion pluginVersion;

	public PluginNameVersion(String pluginName, PluginVersion pluginVersion) {
		super(pluginName);
		this.pluginVersion = pluginVersion;
	}

	public PluginNameVersion(String pluginName) {
		super(pluginName);
		this.pluginVersion = null;
	}

	public PluginNameVersion(String pluginName, Integer version) {
		this(pluginName, new PluginVersion(0, version, 0));
	}

	public static PluginNameVersion from(String pluginNameVersion) throws ParseException {
		if (!pluginNameVersion.contains(VERSION_SEP)) {
			throw new ParseException("Couldn't find version sepeator '" + VERSION_SEP + "' in pluginNameVersion '"
					+ pluginNameVersion + "'", 0);
		}
		String[] split = pluginNameVersion.split(VERSION_SEP);
		if (split.length != 2) {
			throw new org.springframework.expression.ParseException(0,
					"Expected 2 tokens from '" + pluginNameVersion + "', but got: " + split);
		}
		return new PluginNameVersion(split[0], new PluginVersion(split[1]));
	}

	public PluginVersion getPluginVersion() {
		return pluginVersion;
	}

	public void setPluginVersion(PluginVersion pluginVersion) {
		this.pluginVersion = pluginVersion;
	}

	@Override
	public String toString() {
		if (pluginVersion == null) {
			return pluginName;
		} else {
			return pluginName + VERSION_SEP + pluginVersion.toString();
		}
	}

	public File getPluginFolder(File pluginsDir) {
		File file = new File(pluginsDir, this.toString());
		if (file.exists()) {
			return file;
		} else {
			// May be old format

			File old = new File(pluginsDir,
					SingleVersioningPlugin.getNameWithVersion(pluginName, pluginVersion.getMinor()));
			if (old.exists() && old.isDirectory()) {
				return old;
			}
		}
		return null;
	}

	@Override
	public int compareTo(PluginNameVersion o) {
		int compareTo = pluginName.compareTo(o.getPluginName());
		if (compareTo == 0) {
			return pluginVersion.compareTo(o.getPluginVersion());
		} else {
			return compareTo;
		}
	}

}
