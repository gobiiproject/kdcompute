/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.text.ParseException;

public class PluginVersion implements Comparable<PluginVersion> {
	private int major;
	private int minor;
	private int revison;

	public PluginVersion(int major, int minor, int revison) {
		super();
		this.major = major;
		this.minor = minor;
		this.revison = revison;
	}

	public PluginVersion() {
		this(0, 1, 0);
	}

	public PluginVersion(String string) throws ParseException {
		String[] split = string.split("\\.");
		if (split.length != 3) {
			throw new ParseException("Failed to parse plugin Version " + string, 0);
		}
		this.major = Integer.parseInt(split[0]);
		this.minor = Integer.parseInt(split[1]);
		this.revison = Integer.parseInt(split[2]);
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	public void setMinor(int minor) {
		this.minor = minor;
	}

	public int getRevison() {
		return revison;
	}

	public void setRevison(int revison) {
		this.revison = revison;
	}

	@Override
	public String toString() {
		return Integer.toString(major) + "." + Integer.toString(minor) + "." + Integer.toString(revison);
	}

	@Override
	public int hashCode() {
		return major & 65536 + minor & 256 + revison;
	}

	@Override
	public int compareTo(PluginVersion o) {
		if (o.getMajor() > this.getMajor()) {
			return -1;
		} else if (o.getMajor() < this.getMajor()) {
			return 1;
		} else {
			if (o.getMinor() > this.getMinor()) {
				return -1;
			} else if (o.getMinor() < this.getMinor()) {
				return 1;
			} else {
				if (o.getRevison() > this.getRevison()) {
					return -1;
				} else if (o.getRevison() == this.getRevison()) {
					return 0;
				} else {
					return 1;
				}
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PluginVersion) {
			PluginVersion o = (PluginVersion) obj;
			return o.compareTo(this) == 0;
		} else {
			return false;
		}
	}

	public PluginVersion incMajor() {
		return new PluginVersion(this.getMajor() + 1, this.getMinor(), this.getRevison());
	}

	public PluginVersion incMinor() {
		return new PluginVersion(this.getMajor(), this.getMinor() + 1, this.getRevison() + 1);
	}

	public PluginVersion incRevison() {
		return new PluginVersion(this.getMajor(), this.getMinor(), this.getRevison() + 1);
	}

}
