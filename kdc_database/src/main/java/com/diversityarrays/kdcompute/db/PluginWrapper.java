/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.diversityarrays.kdcompute.db.helper.DbUtil;

@SuppressWarnings("nls")
public class PluginWrapper {

    private String pluginName = "";

    private Plugin wrappedPlugin;
    
    private SortedSet<Knob> knobs = new TreeSet<>();
    
    private List<PresetKnobBinding> presetKnobBindings = new ArrayList<>();
    
    public PluginWrapper() {
    }

    public PluginWrapper(Plugin a) {
        wrappedPlugin = a;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("WrappedPlugin(");
        sb.append(" '").append(pluginName).append('\'');
        DbUtil.appendListInfo(sb, ", knobs", knobs, Knob::getKnobName);
        DbUtil.appendListInfo(sb, "  inputs", getInputDataSets(), DataSet::getDataSetName);
//        DbUtil.appendListInfo(sb, "  outputs", getOutputDataSets(), DataSet::getDataSetName);
        sb.append(")");
        return sb.toString();
    }
    
    public void addKnob(Knob ... more) {
        Collections.addAll(knobs, more);
    }

    public void removeKnob(Knob k) {
        knobs.remove(k);
    }

    public boolean isFullyBound() {
        return getUnboundAlgorithmKnobs().isEmpty();
    }
    
    public Set<Knob> getUnboundAlgorithmKnobs() {
        Set<Knob> result = new HashSet<>(wrappedPlugin.getKnobs());
        
        result.removeAll(getPluginKnobsWithPresets());

        return result;
    }

    /**
     * Return the Knobs of the <i>Algorithm</i> that have preset values provided by
     * at least one Knob of this BoundAlgorithm.
     * @return
     */
    public Set<Knob> getPluginKnobsWithPresets() {
        return presetKnobBindings.stream()
                .flatMap(p -> p.getBindings().stream())
                .map(KnobBinding::getKnob)
                .collect(Collectors.toSet());
    }
    
    /**
     * Return the KnobBindings for the given Knob of this WrappedAlgorithm.
     * @param boundKnob
     * @return PresetKnobBinding or null
     */
    public PresetKnobBinding getPresetKnobBinding(Knob knob) {
        
        if (! knobs.contains(knob)) {
            throw new IllegalArgumentException("Unknown Knob: " + knob);
        }
        
        Optional<PresetKnobBinding> optional = presetKnobBindings.stream()
            .filter(pkb -> pkb.getBoundPluginKnob().equals(knob))
            .findFirst();
        
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }
    
    public void setPreset(Knob boundPluginKnob, String boundKnobValue, KnobBinding ... bindings) {
        Set<KnobBinding> set = new HashSet<>();
        if (bindings != null) {
            Collections.addAll(set, bindings);
        }
        setPreset(boundPluginKnob, boundKnobValue, set);
    }
    
    /**
     * Replace the presets for the given bindingKnob.
     * @param boundPluginKnob
     * @param value
     */
    public void setPreset(Knob boundPluginKnob, String boundKnobValue, Set<KnobBinding> bindings) {
        
        if (! knobs.contains(boundPluginKnob)) {
            throw new IllegalArgumentException("Not a Knob for this Bound Algorithm");
        }
        
        DbUtil.exceptionUnlessAllValueKnobsAreInPlugin(wrappedPlugin, bindings);

        Optional<PresetKnobBinding> optional = presetKnobBindings.stream()
            .filter(pkb -> boundPluginKnob.equals(pkb.getBoundPluginKnob())).findFirst();
        
        if (optional.isPresent()) {
            presetKnobBindings.remove(optional.get());
        }
        presetKnobBindings.add(new PresetKnobBinding(boundPluginKnob, boundKnobValue, bindings));
    }

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String n) {
        pluginName = n;
    }

    public Set<Knob> getKnobs() {
        return Collections.unmodifiableSet(knobs);
    }

    public Collection<DataSet> getInputDataSets() {
        return wrappedPlugin.getInputDataSets();
    }

//    public Collection<DataSet> getOutputDataSets() {
//        return wrappedAlgorithm.getOutputDataSets();
//    }
    
}
