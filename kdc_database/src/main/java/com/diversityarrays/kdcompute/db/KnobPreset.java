/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Collects the information for the Knob values of
 * the parent <i>Algorithm</i> of a <i>BoundAlgorithm</i>.
 * @author brianp
 *
 */
@Entity
@Table
public class KnobPreset {
    
    static public final KnobPreset EMPTY = new KnobPreset();

    @Id
    @GeneratedValue
    @Column(name = "id", nullable=false)
    private Long id;
    
    @Column(name = "knob", nullable=false)
    private Knob knob;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
	@CollectionTable(
			name="KnobPreset_knobBindings",
			joinColumns=@JoinColumn(name="Runbinding_Id"))
    private List<KnobBinding> knobBindings = new ArrayList<KnobBinding>();
    
    public Long getId() {
        return id;
    }
    
    public Knob getKnob() {
        return knob;
    }
    
    public void addKnobBinding(KnobBinding b) {
        knobBindings.add(b);
    }
  
    //@OneToMany
    public List<KnobBinding> getKnobBindings() {
        return Collections.unmodifiableList(knobBindings);
    }
}
