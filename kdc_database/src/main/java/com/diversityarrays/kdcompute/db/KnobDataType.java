/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import java.util.Arrays;

public enum KnobDataType {
    CHOICE, // set of text values  "choice(a|b|c)"
//    DATE,  // date (possibly constrained) "date"
    DECIMAL, // with n decimal places  various forms
    INTEGER,
    TEXT,
    FORMULA,

    // TODO: something that gives a set of column numbers, the "prompts" are the
    
    FILE_UPLOAD;

	public static final String CHOICE_SEP = "|";
	
    public String getDefaultValidationRuleString() {
        switch (this) {
        case CHOICE:
            return String.join(CHOICE_SEP,Arrays.asList("a","b","c")); //$NON-NLS-1$
//        case DATE:
//            return "";
        case DECIMAL:
            return "0..100.0"; //$NON-NLS-1$
        case TEXT:
            return "text"; //$NON-NLS-1$
        case FILE_UPLOAD:
            return ""; //$NON-NLS-1$
        case FORMULA:
            return "javascript: return 1"; //$NON-NLS-1$
        case INTEGER:
            return "0..100"; //$NON-NLS-1$
        default:
            break;
        }
        throw new RuntimeException("Not yet supported: " + this.name()); //$NON-NLS-1$
    }

    public boolean requiresValidationRule() {
        switch (this) {
//        case DATE:
        case TEXT:
            return false;
        case CHOICE:
        case DECIMAL:
        case FILE_UPLOAD:
        case FORMULA:
        case INTEGER:
        default:
            break;
        }
        return ! getDefaultValidationRuleString().isEmpty();
    }

    // TODO replace with a Msg translation
    public String getDisplayName() {
        return name();
    }

}
