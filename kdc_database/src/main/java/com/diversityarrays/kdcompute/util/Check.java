/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.util;

import java.util.Collection;
import java.util.Map;

public class Check {

    static public boolean isEmpty(Collection<?> c) {
        return c==null || c.isEmpty();
    }
    
    static public boolean isEmpty(@SuppressWarnings("rawtypes") Map c) {
        return c==null || c.isEmpty();
    }
    
    static public boolean isEmpty(CharSequence c) {
        return c==null || c.length()<=0;
    }
    
    static public boolean isEmpty(Object[] array) {
        return array==null || array.length <= 0;
    }
    
    private Check() {}
}
