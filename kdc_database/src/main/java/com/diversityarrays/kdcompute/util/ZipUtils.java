/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.zeroturnaround.zip.ZipUtil;

public class ZipUtils {

	public static void zipFile(File folder, File zipFile) throws IOException {
		String zipFileName = zipFile.getPath();

		FileOutputStream fos = new FileOutputStream(zipFileName);
		ZipOutputStream zos = new ZipOutputStream(fos);

		zos.putNextEntry(new ZipEntry(folder.getName()));

		byte[] bytes = Files.readAllBytes(folder.toPath());
		zos.write(bytes, 0, bytes.length);
		zos.closeEntry();
		zos.close();

		if (new File(zipFileName).exists()) {
			folder.delete();
		}
	}

	public static void unzip(File file, File output) {
		if (file.exists()) {
			ZipUtil.unpack(file, output);
		}
	}

}
