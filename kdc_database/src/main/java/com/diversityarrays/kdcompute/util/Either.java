/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.util;

import java.util.function.Consumer;

public class Either<L,R> {
    
    public static <L,R> Either<L,R> left(L l) {
        return new Either<L,R>(l, null);
    }

    public static <L,R> Either<L,R> right(R r) {
        return new Either<L,R>(null, r);
    }
    
    public void consume(Consumer<L> leftOption, Consumer<R> rightOption) {
        if (right == null) {
            leftOption.accept(left);
        }
        else {
            rightOption.accept(right);
        }
    }
    
    private final L left;
    private final R right;

    protected Either(L l, R r) {
        left = l;
        right = r;
    }
    
    @Override
    public String toString() {
        return left != null ? "Left:"+left : "Right:"+right; //$NON-NLS-1$ //$NON-NLS-2$
    }

    public L left() {
        return left;
    }

    public boolean isLeft() {
        return left != null;
    }
    
    public R right() {
        return right;
    }
    
    public boolean isRight() {
        // so it's ok to have a null value for right
        return left == null;
    }
}
