/*
    KDCompute provides efficient and extensible data analysis within plugin framework
    Copyright (C) 2016,2017,2018,2019  Diversity Arrays Technology, Pty Ltd.
    
    KDCompute may be redistributed and may be modified under the terms
    of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    KDCompute is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with KDCompute.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.diversityarrays.kdcompute.db;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Test;

/**
 * Just to make sure that src/test/java stays in the build path

 * @author brianp
 *
 */
public class PlaceHolder {

//    @BeforeClass
//    public static void setUpBeforeClass() throws Exception {
//    }
//
//    @AfterClass
//    public static void tearDownAfterClass() throws Exception {
//    }

    @Test
    public void test() {
        String resourceName = "META-INF/persistence.xml";
        InputStream is = getClass().getClassLoader().getResourceAsStream(resourceName);
        if (is == null) {
            fail("Unable to get resource " + resourceName);
        }
        else {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            try {
                System.out.println("Content of: " + resourceName);
                String line;
                while (null != (line = br.readLine())) {
                    System.out.println(line);
                }
                System.out.println("---");
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                try { br.close(); }
                catch (IOException ignore) { }
            }
        }
    }

}
